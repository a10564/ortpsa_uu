﻿using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess
{
    public class ExaminationMasterRepository : MicroServiceGenericRepository<ExaminationMaster>, IExaminationMasterRepository
    {
        public ExaminationMasterRepository(DbContext dbContext) : base(dbContext) { }

        public IQueryable<ExaminationMaster> GetServiceWiseExaminationMasterList()
        {
            return DbSet
                 //.Include(p => p.ServiceConveniencePricingMaster)
                 .Include(p => p.ServiceMaster)
                 .Where(p => p.IsActive == true);
        }
    }
}
