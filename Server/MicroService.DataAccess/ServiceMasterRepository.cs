﻿using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace MicroService.DataAccess
{
    public class ServiceMasterRepository : MicroServiceGenericRepository<ServiceMaster>, IServiceMasterRepository
    {
        public ServiceMasterRepository(DbContext dbContext) : base(dbContext) { }

        public ServiceMaster GetServiceMasterById(long id)
        {
            return DbSet
                 //.Include(p => p.ServiceConveniencePricingMaster)
                 .Include(p => p.ServicePricingMasters)
                 .Where(p => p.Id == id) //&& p.IsActive == true
                 .FirstOrDefault();
        }
        public List<ServiceMaster> GetServiceMaster(long currentUserType)
        {
            return DbSet
                 //.Include(p => p.ServiceConveniencePricingMaster)
                 .Include(p => p.RoleServiceMapper)
                 .Where(p=>p.IsActive == true && p.RoleServiceMapper.Select(a=>a.RoleId).Contains(currentUserType)).ToList();
        }

        public List<ServiceMaster> GetServiceMaster(string collegeCode)
        {
            return DbSet
                 //.Include(p => p.ServiceConveniencePricingMaster)
                 .Include(p => p.CollegeServiceMapper)
                 .Where(p => p.IsActive == true && p.CollegeServiceMapper.Select(a => a.CollegeCode).Contains(collegeCode)).ToList();
        }
    }
}
