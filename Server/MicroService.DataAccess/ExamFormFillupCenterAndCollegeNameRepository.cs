﻿using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Entity.Lookup;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess
{
    /// <summary>
    /// Author  :   Sumbul Samreen
    /// Date    :   13-1-2020
    /// Description :   This method is used to get exam centers allocated to colleges
    /// </summary>
    /// <param name="filterparams"></param>
    /// <returns></returns>
    class ExamFormFillupCenterAndCollegeNameRepository : MicroServiceGenericRepository<CenterAllocation>, IExamFormFillupCenterAndCollegeNameRepository
    {
        public ExamFormFillupCenterAndCollegeNameRepository(DbContext dbContext) : base(dbContext) { }
      
        public IQueryable<CollegeAndAllocatedCenterNames> GetAllCollegeName(Guid sessionId)
        {
           
            var data = DbSet.
                        Join(DbContext.Set<CollegeMaster>(), c => c.CollegeCode, col => col.LookupCode, (c, col) => new { c, col })
                        .Join(DbContext.Set<CollegeMaster>(), c1 => c1.c.CenterCollegeCode, col1 => col1.LookupCode, (c1, col1) => new { c1, col1 })
                        .Where(j => j.col1.IsActive && j.c1.c.ExaminationSessionId == sessionId && j.c1.c.IsActive)
                       .Select(s => new CollegeAndAllocatedCenterNames
                       {
                           CollegeCode = s.c1.c.CollegeCode,
                           CollegeString = s.c1.col.LookupDesc,
                           CenterCode = s.c1.c.CenterCollegeCode,
                           CenterName=s.col1.LookupDesc,
                           Id= s.c1.c.Id
                       }).OrderBy(c => c.CollegeString);
            
            return data;

        }

        public IQueryable<CollegeAndAllocatedCenterNames> GetAllCollegeBySubject(Guid sessionId,long subjectMasterId)
        {
            var data = DbSet.
                        Join(DbContext.Set<CollegeMaster>(), c => c.CollegeCode, col => col.LookupCode, (c, col) => new { c, col })
                        //.Join(DbContext.Set<CollegeMaster>(), c1 => c1.c.CenterCollegeCode, col1 => col1.LookupCode, (c1, col1) => new { c1, col1 })
                        // .Where(j => j.col1.IsActive && j.c1.c.ExaminationSessionId == sessionId && j.c1.c.IsActive)
                        .Join(DbContext.Set<CollegeSubjectMapper>(), c1 => c1.c.CenterCollegeCode, sub => sub.CollegeCode, (c1, col1) => new { c1, col1 })
                        .Where(j => j.c1.c.IsActive && j.c1.col.IsActive && j.col1.IsActive && j.c1.c.ExaminationSessionId == sessionId
                        && j.col1.SubjectMasterId==subjectMasterId)
                       .Select(s => new CollegeAndAllocatedCenterNames
                       {
                           CollegeCode = s.c1.c.CollegeCode,
                           CollegeString = s.c1.col.LookupDesc,
                           CenterCode = s.c1.c.CenterCollegeCode,
                           CenterName = s.c1.col.LookupDesc,
                           Id = s.c1.c.Id
                       }).OrderBy(c => c.CollegeString);

            var dad = data.ToList();


            return data;

        }
    }
}
