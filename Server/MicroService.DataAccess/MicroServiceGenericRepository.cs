﻿using MicroService.DataAccess.Abstract;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace MicroService.DataAccess
{
    public class MicroServiceGenericRepository<T> : IRepository<T> where T : class
    {
        public const string spCallSyntaxt = "CALL";
        protected DbContext DbContext { get; set; }

        protected DbSet<T> DbSet { get; set; }

        public MicroServiceGenericRepository(DbContext dbContext)
        {
            if (dbContext == null)
                throw new ArgumentNullException("TestDbContext");
            DbContext = dbContext;
            DbSet = DbContext.Set<T>();
        }

        public IQueryable<T> GetAll()
        {
            return DbSet;
        }

        //public IQueryable<T> GetWithQueryFilters()
        //{
        //    return DbSet.IgnoreQueryFilters();
        //}

        public IQueryable<T> GetAllIgnoreGlobalFilter()
        {
            return DbSet.IgnoreQueryFilters();
        }

        public T GetById(long id)
        {
            var entityType = DbContext.Model.FindEntityType(typeof(T));
            var keys = entityType.FindPrimaryKey();
            var key = keys.Properties[0].Name.ToString();

            var parameter = Expression.Parameter(typeof(T), "x");
            var query = DbSet.Where((Expression<Func<T, bool>>)
                Expression.Lambda(
                    Expression.Equal(
                        Expression.Property(parameter, key),
                        Expression.Constant(id)),
                    parameter));

            // Look in the database
            return query.FirstOrDefault();
        }

        public void Add(T entity)
        {
            EntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }
        }

        public void Update(T entity)
        {
            var key = GetKey(entity);

            if (entity == null)
                throw new ArgumentNullException("entity");
            T exist = DbContext.Set<T>().Find(key);
            if (exist != null)
            {
                // Commented since value objects were not getting updated using Pomelo
                // DbContext.Entry(exist).CurrentValues.SetValues(entity);
                DbSet.Update(entity);
            }

        }

        public void UpdateSingleColumn(T entity, string property)
        {
            EntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            try
            {
                dbEntityEntry.Property(property).IsModified = true;
            }
            catch (Exception ex)
            {// Not a valid property, so try for navigation property
                try
                {
                    dbEntityEntry.Member(property).IsModified = true;
                    //dbEntityEntry.Reference(property).IsModified = true;
                    //DbSet.Attach(entity);
                }
                catch (Exception exc)
                {// Not a valid property nor even navigation property. So don't update

                }
            }
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return this.DbSet.Where(predicate);
        }

        public void Remove(T entity)
        {

            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            var key = this.GetKey(entity);
            var currentEntry = DbSet.Find(key);
            if (currentEntry != null)
            {
                DbSet.Remove(currentEntry);
            }
        }

        public void AddRange(List<T> entity)
        {
            DbSet.AddRange(entity);
        }
        public void UpdateRange(List<T> entity)
        {
            DbSet.UpdateRange(entity);
        }
        public void RemoveRange(List<T> entity)
        {
            DbSet.RemoveRange(entity);

        }

        //public void AddMultipleRecord(List<T> entities)
        //{
        //    DbSet.AddRange(entities);
        //}

        private object GetPrimaryKey(EntityEntry entry)
        {
            var myObject = entry.Entity;
            var property = myObject.GetType()
                           .GetProperties().FirstOrDefault(prop => Attribute.IsDefined(prop, typeof(KeyAttribute)));
            return property.GetValue(myObject, null);
        }

        public object GetKey(T entity)
        {
            var keyName = DbContext.Model.FindEntityType(typeof(T)).FindPrimaryKey().Properties
                .Select(x => x.Name).Single();

            return entity.GetType().GetProperty(keyName).GetValue(entity, null);
        }

        public long Add_GetPrimaryKey(T entity)
        {
            EntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }

            //Commiting the netity to the database
            DbContext.SaveChanges();

            //Reloading the DB Context so that the primary key is populated to the present DB Context
            DbContext.Entry(entity).Reload();

            var data = this.GetPrimaryKey(dbEntityEntry);

            return Convert.ToInt64(data);
        }
        public List<T> CallStoredProcedureGetDetails(string StoredProcedureName, List<string> searchParams)
        {

            StringBuilder storedProcedureCall = new StringBuilder();
            storedProcedureCall.Append(spCallSyntaxt);
            storedProcedureCall.Append(" ");
            storedProcedureCall.Append(StoredProcedureName);
            if (searchParams[0] == null || searchParams[0] == "null")
                storedProcedureCall.Append("(");
            else
                storedProcedureCall.Append("('");


            for (int i = 0; i < searchParams.Count; i++)
            {
                if (searchParams[i] == null || searchParams[i] == "null")
                {
                    if (i > 0)
                        storedProcedureCall.Append(",null");
                    else
                        storedProcedureCall.Append("null");
                }
                else
                {
                    if (i > 0)
                        storedProcedureCall.Append(",'");

                    storedProcedureCall.Append(searchParams[i] + "'");
                }

            }

            //cmd.Parameters.Add(new SqlParameter("@firstName", DbType.String) { Value = "Steve" });
            //cmd.Parameters.Add(new SqlParameter("@lastName", DbType.VarChar) { Value = "Smith" });

            //cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.BigInt) { Direction = ParameterDirection.Output });

            storedProcedureCall.Append(")");

            DbCommand cmd = DbContext.Database.GetDbConnection().CreateCommand();
            //cmd.ex
            cmd.CommandText = storedProcedureCall.ToString();
            cmd.CommandType = CommandType.Text;

            if (cmd.Connection.State != ConnectionState.Open)
            {
                cmd.Connection.Open();
            }

            DbDataReader dr = cmd.ExecuteReader();
            Type t = typeof(T);
            List<T> result = CreateList(dr);


            cmd.Connection.Close();
            return result;
        }
        public virtual List<T> CreateList(DbDataReader reader)
        {
            var results = new List<T>();
            return this.GetReader(reader);
        }
        public object GetInstance(Type type)
        {
            return (T)Activator.CreateInstance(type);
        }

        private List<T> GetReader(DbDataReader reader)
        {
            Delegate resDelegate;
            Type inputObjectType = typeof(T);
            var results = new List<T>();
            object inputObject = null;
            try
            {
                List<string> readerColumns = new List<string>();
                for (int index = 0; index < reader.FieldCount; index++)
                    readerColumns.Add(reader.GetName(index));
                while (reader.Read())
                {
                    inputObject = GetInstance(inputObjectType);
                    foreach (var prop in inputObjectType.GetProperties())
                    {

                        if (prop != null)
                        {
                            if (readerColumns.Contains(prop.Name))
                            {
                                object propertyVal = reader[prop.Name];
                                string propertyName = prop.Name;

                                SetpropertyValues(inputObject, propertyName, propertyVal, prop);
                            }
                        }

                    }
                    results.Add((T)inputObject);
                }
            }
            catch (Exception ex)
            {
            }
            return results;
        }

        private static void SetpropertyValues(object inputObject, string propertyName, object propertyVal, PropertyInfo propertyInfo)
        {

            //find the property type
            Type propertyType = propertyInfo.PropertyType;

            //Convert.ChangeType does not handle conversion to nullable types
            //if the property type is nullable, we need to get the underlying type of the property
            var targetType = IsNullableType(propertyInfo.PropertyType) ? Nullable.GetUnderlyingType(propertyInfo.PropertyType) : propertyInfo.PropertyType;
            /// var targetType =  propertyInfo.PropertyType;

            //Returns an System.Object with the specified System.Type and whose value is
            //equivalent to the specified object.
            propertyVal = Convert.ChangeType(propertyVal, targetType);

            //Set the value of the property
            propertyInfo.SetValue(inputObject, propertyVal);

        }
        private static bool IsNullableType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
        }
    }
}
