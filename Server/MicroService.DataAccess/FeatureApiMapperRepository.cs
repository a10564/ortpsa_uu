﻿using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess
{
    public class FeatureApiMapperRepository : MicroServiceGenericRepository<FeatureApiMapper>, IFeatureApiMapperRepository
    {
        public FeatureApiMapperRepository(DbContext dbContext) : base(dbContext) { }

        public IQueryable<FeatureApiMapper> GetAllFeatureApi()
        {//Not used now. Code modified
            return DbSet;//.Include(a => a.ApiDetails);
        }
    }
}
