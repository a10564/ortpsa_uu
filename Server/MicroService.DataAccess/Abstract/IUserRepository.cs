﻿using MicroService.Domain.Models.Dto.BackgroundProcess;
using MicroService.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess.Abstract
{
    public interface IUserRepository : IRepository<Users>
    {
        IQueryable<Users> GetAllUsersData();
        IQueryable<Users> GetAllUsersWithAddress();
        dynamic callSp(BackgroundProcessDto param);
    }
}
