﻿using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Dto.Nursing;
using MicroService.Domain.Models.Entity.Nursing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess.Abstract
{
    public interface INursingFormFillUpRepository :IRepository<NursingFormfillupDetails>
    {
        IQueryable<NursingFormfillupDetails> GetAllNursingFormFillUpData();
        IQueryable<CollegeWiseAmountForCoeDto> GetAllStudentStateWise(SearchParamDto param, string instanceTrigger);
    }
}
