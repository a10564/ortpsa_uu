﻿using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess.Abstract
{
    public interface IExamFormFillupCenterAndCollegeNameRepository : IRepository<CenterAllocation>
    {
        IQueryable<CollegeAndAllocatedCenterNames> GetAllCollegeName(Guid sessionId);
        IQueryable<CollegeAndAllocatedCenterNames> GetAllCollegeBySubject(Guid sessionId, long subjectMasterId);
    }
}
