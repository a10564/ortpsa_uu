﻿//using MicroService.Domain.Models;
//using MicroService.Domain.Models.AggregateRoot;
//using MicroService.Domain.Models.Entity;

using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Entity.Lookup;
using MicroService.Domain.Models.Entity.Nursing;
//using MicroService.Domain.Models.Entity.Workflow;
using MicroService.Domain.Models.Views;
//using MService = MicroService.Domain.Models.Entity.Workflow;

namespace MicroService.DataAccess.Abstract
{
    public interface IMicroServiceUOW
    {
        // Save pending changes to the data store.
        void Commit();

        // Repositories
        //IRepository<FarmerRegistration> FarmerRegistration { get; }

        //IRepository<Farmer> Farmer { get; }
        //IRepository<LandDetail> LandDetail { get; }
        //IRepository<CorsPolicyPermission> CorsPolicy { get; }

        IRepository<Users> Users { get; }
        IRepository<UserLoginDetail> UserLoginDetail { get; }
        IRepository<UserOtp> UserOtp { get; }
        IRepository<Tenants> Tenants { get; }
        IRepository<UserRoles> UserRoles { get; }
        IRepository<UserRoleGeographyMapper> UserRoleGeographyMapper { get; }
        IRepository<RoleFeatureApiMapper> RoleFeatureApiMapper { get; }
        IRepository<Role> Role { get; }
        IRepository<Feature> Feature { get; }
        IRepository<FeatureApiMapper> FeatureApiMapper { get; }
        IRepository<FeatureOrder> FeatureOrder { get; }
        IRepository<ApiDetail> ApiDetail { get; }
        IRepository<GeographyOrder> GeographyOrder { get; }
        IRepository<GeographyDetail> GeographyDetail { get; }
        IRepository<Language> Language { get; }
        IRepository<Module> Module { get; }
        IRepository<Multilingual> Multilingual { get; }
        IRepository<ServiceMaster> ServiceMaster { get; }
        IRepository<UserServiceApplied> UserServiceApplied { get; }
        IRepository<EmailTemplate> EmailTemplate { get; }
        IRepository<LookupType> LookupType { get; }
        IRepository<SubjectMaster> SubjectMaster { get; }
        IRepository<CollegeMaster> CollegeMaster { get; }
        IRepository<DepartmentMaster> DepartmentMaster { get; }
        IRepository<Lookup> Lookup { get; }
        IRepository<UserAddress> UserAddress { get; }
        //IRepository<Document> Document { get; }
        IRepository<UniversityRegistrationDetail> UniversityRegistrationDetail { get; }
        //clone table of Document table
        IRepository<Documents> Documents { get; }
        /*end*/
        IRepository<DuplicateRegistrationDetail> DuplicateRegistrationDetail { get; }
        IRepository<UniversityMigrationDetail> UniversityMigrationDetail { get; }
        IRepository<ProvisionalCertificateApplicationDetail> ProvisionalCertificateApplicationDetail { get; }
        IRepository<BackgroundProcess> BackgroundProcess { get; }
        IRepository<ExaminationMaster> ExaminationMaster { get; }
        IRepository<ExaminationPaperMaster> ExaminationPaperMaster { get; }
        IRepository<ExaminationFormFillUpDates> ExaminationFormFillUpDates { get; }
        IRepository<ExaminationSession> ExaminationSession { get; }
        IRepository<ExaminationFormFillUpDetails> ExaminationFormFillUpDetails { get; }
        IRepository<ServiceStudentAddress> ServiceStudentAddress { get; }
        IRepository<ExaminationPaperDetail> ExaminationPaperDetail { get; }
        IRepository<ProcessTracker> ProcessTracker { get; }
		IRepository<NumberGenerator> NumberGenerator { get; }
		IRepository<TransactionLog> TransactionLog { get; }
		IRepository<ExaminationPricingMaster> ExaminationPricingMaster { get; }
		IRepository<LateFineMaster> LateFineMaster { get; }
        IRepository<CenterAllocation> CenterAllocation { get; }
        IRepository<ApplyCLC> ApplyCLC { get; }
        IRepository<CollegeSubjectMapper> CollegeSubjectMapper { get; }
        IRepository<RoleServiceMapper> RoleServiceMapper { get; }
        IRepository<StudentProfile> StudentProfile { get; }
        IRepository<ExaminationFormFillUpSemesterMarks> ExaminationFormFillUpSemesterMarks { get; }
        IRepository<NursingFormfillupDetails> NursingFormfillupDetails { get; }  
        IRepository<CollegeServiceMapper> CollegeServiceMapper { get; }
        IRepository<RoleUserWiseSubjectMapper> RoleUserWiseSubjectMapper { get; } 

        #region VIEWS
        IRepository<UserView> UserView { get; }
        IRepository<FeatureView> FeatureView { get; }
        IRepository<GeographyDetailView> GeographyDetailView { get; }
        IRepository<AcknowledgementView> AcknowledgementView { get; }
        IRepository<MigrationView> MigrationView { get; }
        IRepository<DuplicateRegistrationView> DuplicateRegistrationView { get; }
        IRepository<ExaminationView> ExaminationView { get; }
        IRepository<ExaminationViewExcludeBackgroundProcessTable> ExaminationViewExcludeBackgroundProcessTable { get; }
        IRepository<FormFillupViewForAdmitCard> FormFillupViewForAdmitCard { get; }
        IRepository<AdmitCardCenterAndExamNameView> AdmitCardCenterAndExamNameView { get; }
        IRepository<CLCView> CLCView { get; }
        IRepository<StudentProfileView> StudentProfileView { get; }
        IRepository<AcademicStatementView> AcademicStatementView { get; }
        IRepository<MarkStatementView> MarkStatementView { get; } 
        IRepository<NursingViewExcludeBackgroundProcessTable> NursingViewExcludeBackgroundProcessTable { get; }
        IRepository<NursingMarkStatementView> NursingMarkStatementView { get; }

        #endregion


        #region Repository        
        //IFarmerRepository FarmerRepository { get; }
        IUserRepository UserRepository { get; }
        IRoleRepository RoleRepository { get; }
        IFeatureApiMapperRepository FeatureApiMapperRepository { get; }
        IUniversityRegistrationDetailsRepository UniversityRegistrationDetailsRepository { get; }
        IExaminationFormFillUpRepository ExaminationFormFillUpRepository { get; }
        IExamFormFillupCenterAndCollegeNameRepository ExamFormFillupCenterAndCollegeNameRepository { get; }
		IServiceMasterRepository ServiceMasterRepository { get; }
        IUniversityMigrationDetailsRepository UniversityMigrationDetailsRepository { get; }
        ICLCRepository CLCRepository { get; }
        IAcademicStatementViewRepository AcademicStatementViewRepository { get; }
        IExaminationFormFillupSemesterMarkRepository ExaminationFormFillupSemesterMarkRepository { get; }
        ISubjectMasterRepository SubjectMasterRepository { get; } 
        INursingFormFillUpRepository NursingFormFillUpRepository { get; }
        IExaminationMasterRepository ExaminationMasterRepository { get; }
        ITransactionLogRepository TransactionLogRepository { get; }
        #endregion
    }
}


