﻿using MicroService.Domain.Models.Entity.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess.Abstract
{
    public interface ISubjectMasterRepository : IRepository<SubjectMaster>
    {
        IQueryable<SubjectMaster> GetSubjectMasterByCollegeCode(string collegeCode);
    }
}
