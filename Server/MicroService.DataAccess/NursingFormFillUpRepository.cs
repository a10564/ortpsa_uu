﻿using System;
using System.Collections.Generic;
using System.Text;
using MicroService.Domain.Models.Dto.Nursing;
using MicroService.Domain.Models.Entity.Nursing;
using Microservice.Common;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Entity.Lookup;
using Microsoft.EntityFrameworkCore;
using System.Linq;



namespace MicroService.DataAccess
{
    public class NursingFormFillUpRepository:MicroServiceGenericRepository<NursingFormfillupDetails>, INursingFormFillUpRepository
    {
        public NursingFormFillUpRepository(DbContext dbContext) : base(dbContext) { }
        public IQueryable<NursingFormfillupDetails> GetAllNursingFormFillUpData()
        {
            return DbSet.Include(a => a.ExaminationPaperDetails).Include(a => a.ImageDocument);
        }

        public IQueryable<CollegeWiseAmountForCoeDto> GetAllStudentStateWise(SearchParamDto param, string instanceTrigger)
        {
            //select how many college with the same filter parameter already existed in the background table
            //so we need to remove that much college
            List<string> collegeCodeList = new List<string>();
            collegeCodeList = DbContext.Set<BackgroundProcess>().Where(d => d.IsActive && d.CourseTypeCode.Trim() == param.CourseTypeCode.Trim() && param.CollegeCode.Contains(d.CollegeCode)
              && d.StreamCode == param.StreamCode && d.SubjectCode == param.SubjectCode && d.AcademicStart == param.StartYear
              && d.CurrentAcademicYear == param.CurrentYear && d.InstanceTrigger.Trim().ToLower() == instanceTrigger.Trim().ToLower()).Select(d => d.CollegeCode).ToList();

            var result = DbSet.
            Join(DbContext.Set<CollegeMaster>(), u => u.CollegeCode, uir => uir.LookupCode,
            (u, uir) => new { u, uir }).Where(m => !collegeCodeList.Contains(m.u.CollegeCode) && m.u.IsActive && m.u.CourseTypeCode.Trim() == param.CourseTypeCode.Trim()
            && param.CollegeCode.Contains(m.u.CollegeCode)
            && m.u.StreamCode == param.StreamCode && m.u.SubjectCode == param.SubjectCode && m.u.AcademicStart == param.StartYear
            && m.u.CurrentAcademicYear == param.CurrentYear && m.u.State.Trim().ToLower() == param.State.Trim().ToLower() && m.uir.IsActive)
            .GroupBy(d => d.u.CollegeCode).
            Select(n => new CollegeWiseAmountForCoeDto
            {
                CollegeCode = n.Key,
                CollegeName = n.Select(c => c.uir.LookupDesc).FirstOrDefault(),
                RecordCount = n.Count(),
                FormFillUpAmount = n.Sum(d => (Decimal)d.u.FormFillUpMoneyToBePaid)
            })
            .ToList();
            var data = DbSet.
            Join(DbContext.Set<CollegeMaster>(), u => u.CollegeCode, uir => uir.LookupCode,
            (u, uir) => new { u, uir }).Where(m => m.u.IsActive && m.u.CourseTypeCode.Trim() == param.CourseTypeCode.Trim()
            && !collegeCodeList.Contains(m.u.CollegeCode)
            && param.CollegeCode.Contains(m.u.CollegeCode)
            && m.u.StreamCode == param.StreamCode && m.u.SubjectCode == param.SubjectCode && m.u.AcademicStart == param.StartYear
            && m.u.CurrentAcademicYear == param.CurrentYear && m.u.State.Trim().ToLower() == param.State.Trim().ToLower() && m.uir.IsActive)
            .GroupBy(d => d.u.CollegeCode).
            Select(n => new CollegeWiseAmountForCoeDto
            {
                CollegeCode = n.Key,
                CollegeName = n.Select(c => c.uir.LookupDesc).FirstOrDefault(),
                RecordCount = n.Count(),
                FormFillUpAmount = n.Sum(d => (Decimal)d.u.FormFillUpMoneyToBePaid)
            });
            return DbSet.
            Join(DbContext.Set<CollegeMaster>(), u => u.CollegeCode, uir => uir.LookupCode,
            (u, uir) => new { u, uir }).Where(m => m.u.IsActive && m.u.CourseTypeCode.Trim() == param.CourseTypeCode.Trim()
            && !collegeCodeList.Contains(m.u.CollegeCode)
            && param.CollegeCode.Contains(m.u.CollegeCode)
            && m.u.StreamCode == param.StreamCode && m.u.SubjectCode == param.SubjectCode && m.u.AcademicStart == param.StartYear
            && m.u.CurrentAcademicYear == param.CurrentYear && m.u.State.Trim().ToLower() == param.State.Trim().ToLower() && m.uir.IsActive)
            .GroupBy(d => d.u.CollegeCode).
            Select(n => new CollegeWiseAmountForCoeDto
            {
                CollegeCode = n.Key,
                CollegeName = n.Select(c => c.uir.LookupDesc).FirstOrDefault(),
                RecordCount = n.Count(),
                FormFillUpAmount = n.Sum(d => (Decimal)d.u.FormFillUpMoneyToBePaid)
            });

        }
    }
}
