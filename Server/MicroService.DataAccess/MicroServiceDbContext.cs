﻿using MicroService.Core.WorkflowEntity;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Entity.Lookup;
using MicroService.Domain.Models.Entity.Nursing;
//using MicroService.Domain.Models.Entity.Workflow;
using MicroService.Domain.Models.Views;
//using MicroService.Domain.Models;
//using MicroService.Domain.Models.AggregateRoot;
//using MicroService.Domain.Models.Entity;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
//using MService = MicroService.Domain.Models.Entity.Workflow;
//using static MicroService.DataAccess.EntityConfigurations;

namespace MicroService.DataAccess
{
    public class MicroServiceDbContext : DbContext
    {
        public static string conString;

        private long _tenantId;
        private long _currentUserId;
        private IMicroServiceLogger logger;
        public MicroServiceDbContext()
        {

        }
        public MicroServiceDbContext(long currentUserId, long currentTenantId, IMicroServiceLogger _logger)
        {
            logger = _logger;
            _currentUserId = currentUserId;
            _tenantId = currentTenantId;
        }
        public MicroServiceDbContext(DbContextOptions<MicroServiceDbContext> options) : base(options)
        {
            
        }
        //Configure the connection sting 
        // 'conString' is intialize in Startup.cs file
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var x = conString;
            //optionsBuilder.UseMySQL(@"Server=192.168.1.222;Database=nec_retail_db;uid=root;pwd=root;SslMode=None;port=3306;persistsecurityinfo=True;");
            //optionsBuilder.UseMySQL(conString);
            optionsBuilder.UseMySql(conString);
            //optionsBuilder.UseNpgsql(conString);

            //optionsBuilder.UseNpgsql("Server=localhost;Database=nec_retail_db;uid=postgres;pwd=root;port=5432;persistsecurityinfo=True;");
        }
        //Mapping  
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            //modelBuilder.Entity<FarmerRegistration>().ToTable("farmerregistration");
            //var dataType = new Type[] { typeof(string), typeof(long), typeof(Guid) };
            //var genericBase = typeof(List<>);
            //var combinedType = genericBase.MakeGenericType(dataType);
            //modelBuilder.Entity<CorsPolicyPermission>().ToTable("corspolicy");

            foreach (var type in GetEntityTypes())
            {
                if (type.IsSubclassOf(typeof(WorkflowBaseEntity<long>)))
                {
                    var method = SetGlobalQueryMethodForWorkflowBaseEntity.MakeGenericMethod(type);
                    method.Invoke(this, new object[] { modelBuilder });
                }
                if (type.IsSubclassOf(typeof(SharedKernel.AuditedEntity<long>)))
                {
                    var method = SetGlobalQueryMethodForAuditedEntity.MakeGenericMethod(type);
                    method.Invoke(this, new object[] { modelBuilder });
                }
                if (type.IsSubclassOf(typeof(WorkflowBaseEntity<Guid>)))
                {
                    var method = SetGlobalQueryMethodForWorkflowBaseEntityForGuId.MakeGenericMethod(type);
                    method.Invoke(this, new object[] { modelBuilder });
                }
                if (type.IsSubclassOf(typeof(SharedKernel.AuditedEntity<Guid>)))
                {
                    var method = SetGlobalQueryMethodForAuditedEntityForGuId.MakeGenericMethod(type);
                    method.Invoke(this, new object[] { modelBuilder });
                }

            }
            base.OnModelCreating(modelBuilder);
            //modelBuilder.ApplyConfiguration(new FarmerRegistrationConfiguration());
            //modelBuilder.ApplyConfiguration(new FarmerConfiguration());
            //modelBuilder.ApplyConfiguration(new LandDetailConfiguration());
        }
        static readonly MethodInfo SetGlobalQueryMethodForWorkflowBaseEntity = typeof(MicroServiceDbContext).GetMethods(BindingFlags.Public | BindingFlags.Instance)
                                                              .Single(t => t.IsGenericMethod && t.Name == "SetGlobalQuery");
        static readonly MethodInfo SetGlobalQueryMethodForAuditedEntity = typeof(MicroServiceDbContext).GetMethods(BindingFlags.Public | BindingFlags.Instance)
                                                               .Single(t => t.IsGenericMethod && t.Name == "SetGlobalQueryAuditedEntity");

        static readonly MethodInfo SetGlobalQueryMethodForWorkflowBaseEntityForGuId = typeof(MicroServiceDbContext).GetMethods(BindingFlags.Public | BindingFlags.Instance)
                                                              .Single(t => t.IsGenericMethod && t.Name == "SetGlobalQueryForGuId");
        static readonly MethodInfo SetGlobalQueryMethodForAuditedEntityForGuId = typeof(MicroServiceDbContext).GetMethods(BindingFlags.Public | BindingFlags.Instance)
                                                               .Single(t => t.IsGenericMethod && t.Name == "SetGlobalQueryAuditedEntityForGuId");

        public void SetGlobalQuery<T>(ModelBuilder builder) where T : WorkflowBaseEntity<long>
        {
            //_tenantId = 1;
            builder.Entity<T>().HasKey(e => e.Id);
            builder.Entity<T>().HasQueryFilter(e => _tenantId == 0 ? true : e.TenantId == _tenantId);

        }
        public void SetGlobalQueryAuditedEntity<T>(ModelBuilder builder) where T : SharedKernel.AuditedEntity<long>
        {
            //_tenantId = 1;
            builder.Entity<T>().HasKey(e => e.Id);
            builder.Entity<T>().HasQueryFilter(e => _tenantId == 0 ? true : e.TenantId == _tenantId);
        }
        public void SetGlobalQueryForGuId<T>(ModelBuilder builder) where T : WorkflowBaseEntity<Guid>
        {
            builder.Entity<T>().HasKey(e => e.Id);
            builder.Entity<T>().HasQueryFilter(e => _tenantId==0?true:e.TenantId == _tenantId);
        }
        public void SetGlobalQueryAuditedEntityForGuId<T>(ModelBuilder builder) where T : SharedKernel.AuditedEntity<Guid>
        {
            //_tenantId = 1;
            builder.Entity<T>().HasKey(e => e.Id);
            builder.Entity<T>().HasQueryFilter(e => _tenantId == 0 ? true : e.TenantId == _tenantId);
        }


        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            OnBeforeSaving();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        private void OnBeforeSaving()
        {
            // get entries that are being Added or Updated
            var modifiedEntries = ChangeTracker.Entries()
                    .Where(x => (x.State == EntityState.Added || x.State == EntityState.Modified));
            var now = DateTime.UtcNow;
            
            foreach (var entry in modifiedEntries)
            {
                if (entry.Entity.GetType().IsSubclassOf(typeof(AuditedEntity<long>)) || entry.Entity.GetType().IsSubclassOf(typeof(WorkflowBaseEntity<long>))||
                    entry.Entity.GetType().IsSubclassOf(typeof(AuditedEntity<Guid>)) || entry.Entity.GetType().IsSubclassOf(typeof(WorkflowBaseEntity<Guid>)))
                {
                    //entry.CurrentValues["TenantId"] = _tenantId;
                    entry.CurrentValues["TenantId"] = Convert.ToInt64(3); //tenant id set as hardcoded as there is only tenant which is utkal university this hardcoded student is register under single tenant onlly
                    switch (entry.State)
                    {
                        case EntityState.Modified:
                            entry.CurrentValues["LastModificationTime"] = now;
                            entry.CurrentValues["LastModifierUserId"] = _currentUserId;
                            //logger.Info(entry.Entity, EntityState.Modified.ToString(), _tenantId.ToString(), _currentUserId.ToString());
                            break;

                        case EntityState.Added:
                            entry.CurrentValues["CreationTime"] = now;
                            entry.CurrentValues["CreatorUserId"] = _currentUserId;
                            //logger.Info(entry.Entity, EntityState.Added.ToString(), _tenantId.ToString(), _currentUserId.ToString());
                            entry.CurrentValues["IsActive"] = true;
                            break;
                    }
                }
                
            }
        }

        private static IList<Type> _entityTypeCache;

        public IEnumerable<Type> GetEntityTypes()
        {
            if (_entityTypeCache != null)
            {
                return _entityTypeCache.ToList();
            }

            _entityTypeCache = (from a in GetReferencingAssemblies()
                                from t in a.DefinedTypes
                                where t.BaseType == typeof(WorkflowBaseEntity<long>)
                                || t.BaseType == typeof(SharedKernel.AuditedEntity<long>)
                                select t.AsType()).ToList();

            return _entityTypeCache;
        }

        private static IEnumerable<Assembly> GetReferencingAssemblies()
        {

            //Need to check  projects which contains the Domain.
            var assemblies = new List<Assembly>();
            var dependencies = DependencyContext.Default.RuntimeLibraries;

            foreach (var library in dependencies)
            {
                try
                {
                    var assembly = Assembly.Load(new AssemblyName(library.Name));
                    assemblies.Add(assembly);
                }
                catch (FileNotFoundException)
                { }
            }
            return assemblies;
        }

        //public DbSet<FarmerRegistration> FarmerRegistration { get; set; }
        //public DbSet<Farmer> FarmerSet { get; set; }

        #region ENTITY
        public DbSet<Users> Users { get; set; }
        public DbSet<UserOtp> UserOtp { get; set; }
        public DbSet<Tenants> Tenants { get; set; }
        public DbSet<UserRoles> UserRoles { get; set; }
        public DbSet<RoleFeatureApiMapper> RoleFeatureApiMapper { get; set; }
        public DbSet<UserRoleGeographyMapper> UserRoleGeographyMapper { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Feature> Feature { get; set; }
        public DbSet<FeatureOrder> FeatureOrder { get; set; }
        public DbSet<FeatureApiMapper> FeatureApiMapper { get; set; }
        public DbSet<ApiDetail> ApiDetail { get; set; }
        public DbSet<GeographyOrder> GeographyOrder { get; set; }
        public DbSet<GeographyDetail> GeographyDetail { get; set; }
        public DbSet<Language> Language { get; set; }
        public DbSet<Domain.Models.Entity.Module> Module { get; set; }
        public DbSet<Multilingual> Multilingual { get; set; }
        public DbSet<UserLoginDetail> UserLoginDetail { get; set; }
        public DbSet<ServiceMaster> ServiceMaster { get; set; }
        public DbSet<UserServiceApplied> UserServiceApplied { get; set; } 
        public DbSet<EmailTemplate> EmailTemplate { get; set; }
		public DbSet<LookupType> LookupType { get; set; }
		public DbSet<Lookup> Lookup { get; set; }
		public DbSet<SubjectMaster> SubjectMaster { get; set; }
		public DbSet<CollegeMaster> CollegeMaster { get; set; }
		public DbSet<DepartmentMaster> DepartmentMaster { get; set; }
		public DbSet<UserAddress> UserAddress { get; set; }
        //public DbSet<Document> Document { get; set; }
        public DbSet<UniversityRegistrationDetail> UniversityRegistrationDetail { get; set; }
        //clone table of Document table
        public DbSet<Documents> Documents { get; set; }
        public DbSet<DuplicateRegistrationDetail> DuplicateRegistrationDetail { get; set; }
        public DbSet<UniversityMigrationDetail> UniversityMigrationDetail { get; set; }
        public DbSet<ProvisionalCertificateApplicationDetail> ProvisionalCertificateApplicationDetail { get; set; }        

        public DbSet<ExaminationPaperMaster> ExaminationPaperMaster { get; set; }
        public DbSet<BackgroundProcess> BackgroundProcess { get; set; }
        public DbSet<ExaminationMaster> ExaminationMaster { get; set; }
        public DbSet<ExaminationFormFillUpDates> ExaminationFormFillUpDates { get; set; }
        public DbSet<ExaminationSession> ExaminationSession { get; set; }
        public DbSet<ExaminationFormFillUpDetails> ExaminationFormFillUpDetails { get; set; }
        public DbSet<ServiceStudentAddress> ServiceStudentAddress { get; set; }
        public DbSet<ExaminationPaperDetail> ExaminationPaperDetail { get; set; }
        public DbSet<ProcessTracker> ProcessTracker { get; set; }
		public DbSet<TransactionLog> TransactionLog { get; set; }
		public DbSet<NumberGenerator> NumberGenerator { get; set; }
		public DbSet<LateFineMaster> LateFineMaster { get; set; }
		public DbSet<ExaminationPricingMaster> ExaminationPricingMaster { get; set; }
        public DbSet<CenterAllocation> CenterAllocation { get; set; }
        public DbSet<ApplyCLC> ApplyCLC { get; set; }
        public DbSet<CollegeSubjectMapper> CollegeSubjectMapper { get; set; }
        public DbSet<RoleServiceMapper> RoleServiceMapper { get; set; }
        public DbSet<StudentProfile> StudentProfile { get; set; }
        public DbSet<ExaminationFormFillUpSemesterMarks> ExaminationFormFillUpSemesterMarks { get; set; }
        public DbSet<NursingFormfillupDetails> NursingFormfillupDetails { get; set; } 
        public DbSet<CollegeServiceMapper> CollegeServiceMapper { get; set; }
        public DbSet<RoleUserWiseSubjectMapper> RoleUserWiseSubjectMapper { get; set; } 
        #region WORKFLOW ENTITY
        //public DbSet<MessageTemplate> MessageTemplate { get; set; }
        //public DbSet<MService.Microservice> Microservice { get; set; }
        //public DbSet<RoleConfiguration> RoleConfiguration { get; set; }
        ////public DbSet<TenantDtoConfig> TenantDtoConfig { get; set; }
        ////public DbSet<TenantWorkflow> TenantWorkflow { get; set; }
        ////public DbSet<TenantWorkflowTransitionConfig> TenantWorkflowTransitionConfig { get; set; }
        //public DbSet<TenantWorkflowTransitionHistory> TenantWorkflowTransitionHistory { get; set; }
        ////public DbSet<Workflow> Workflow { get; set; }
        //public DbSet<WorkflowTransitionEscalationRule> WorkflowTransitionEscalationRule { get; set; }
        #endregion

        #endregion


        #region VIEWS
        public DbSet<FeatureView> FeatureView { get; set; }
        public DbSet<UserView> UserView { get; set; }
        public DbSet<GeographyDetailView> GeographyDetailView { get; set; }
        public DbSet<AcknowledgementView> AcknowledgementView { get; set; }
        public DbSet<DuplicateRegistrationView> DuplicateRegistrationView { get; set; }        
        public DbSet<MigrationView> MigrationView { get; set; }
        public DbSet<ExaminationView> ExaminationView { get; set; }
        public DbSet<ExaminationViewExcludeBackgroundProcessTable> ExaminationViewExcludeBackgroundProcessTable { get; set; }
        public DbSet<FormFillupViewForAdmitCard> FormFillupViewForAdmitCard { get; set; }
        public DbSet<AdmitCardCenterAndExamNameView> AdmitCardCenterAndExamNameView { get; set; }
        public DbSet<CLCView> CLCView { get; set; }
        public DbSet<StudentProfileView> StudentProfileView { get; set; }

        public DbSet<AcademicStatementView> AcademicStatementView { get; set; }
        public DbSet<MarkStatementView> MarkStatementView { get; set; } 
        public DbSet<NursingViewExcludeBackgroundProcessTable> NursingViewExcludeBackgroundProcessTable { get; set; }
        public DbSet<NursingMarkStatementView> NursingMarkStateView { get; set; }

        #endregion
    }
}
