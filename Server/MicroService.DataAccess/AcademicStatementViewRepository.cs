﻿using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Views;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess
{
    public class AcademicStatementViewRepository : MicroServiceGenericRepository<AcademicStatementView>, IAcademicStatementViewRepository
    {
        public AcademicStatementViewRepository(DbContext dbContext) : base(dbContext) { }

        public IQueryable<AcademicStatementView> GetAllAcademicStatementDetails(long tenantId)
        {
            return DbSet.Where(sub => sub.IsActive == true && sub.TenantId == tenantId) ;
            
            
        }
        public IQueryable<AcademicStatementView> GetAllAcademicStatementDetailswithsearchparam(SearchParamDto searchParam, long tenantId)
        {
            string collegecode = searchParam.CollegeCode.FirstOrDefault();
            return DbSet.Where(sub => sub.IsActive == true &&
                             sub.TenantId == tenantId && sub.CollegeCode == collegecode &&
                             sub.CourseTypeCode == searchParam.CourseTypeCode &&
                             sub.StreamCode == searchParam.StreamCode &&
                             sub.AcademicStart == searchParam.StartYear &&
                             sub.SubjectCode == searchParam.SubjectCode &&
                             sub.SemesterCode == searchParam.SemesterCode 
                             // && sub.State == searchParam.State
                             );
        }
    }
}
