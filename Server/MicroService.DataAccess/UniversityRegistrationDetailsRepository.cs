﻿using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess
{
    public class UniversityRegistrationDetailsRepository : MicroServiceGenericRepository<UniversityRegistrationDetail>, IUniversityRegistrationDetailsRepository
    {
        public UniversityRegistrationDetailsRepository(DbContext dbContext) : base(dbContext) { }

        public IQueryable<UniversityRegistrationDetail> GetAllData()
        {
            return DbSet.Include(a => a.RegistrationDocument);
        }

        public IQueryable<UniversityRegistrationDetail> GetAllDataById()
        {
            var data = from a in DbSet
                       join b in DbContext.Set<Documents>() on a.Id equals b.ParentSourceId
                       where a.IsActive == true
                       select new UniversityRegistrationDetail
                       {
                           Id = a.Id,
                           StudentName = a.StudentName,
                           FatherName = a.FatherName,
                           DateOfBirth = a.DateOfBirth,
                           CollegeCode = a.CollegeCode,
                           CourseType = a.CourseType,
                           StreamCode = a.StreamCode,
                           SubjectCode = a.SubjectCode,
                           CourseCode=a.CourseCode,
                           DepartmentCode = a.DepartmentCode,
                           YearOfAdmission = a.YearOfAdmission,
                           State = a.State,
                           TenantWorkflowId = a.TenantWorkflowId,
                           IsRegistrationNumberGenerated = a.IsRegistrationNumberGenerated,

                           PrincipalRemarks = a.PrincipalRemarks,
                           //RegistrarRemarks = a.RegistrarRemarks,
                           ComputerCellRemarks = a.ComputerCellRemarks,
                           AssistantCOERemarks = a.AssistantCOERemarks,
                           
                           RegistrationDocument = a.RegistrationDocument
                                                  .Where(doc=>doc.ParentSourceId == a.Id && doc.IsActive)
                                                  .Select(p => new Documents
                                                  {
                                                      Id = p.Id,
                                                      FileName = p.FileName,
                                                      FilePath = p.FilePath,
                                                      FileExtension = p.FileExtension
                                                  }).ToList()
                       };
            return data;
        }
    }
}
