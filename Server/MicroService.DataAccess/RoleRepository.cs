﻿using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess
{
    public class RoleRepository : MicroServiceGenericRepository<Role>, IRoleRepository
    {
        public RoleRepository(DbContext dbContext) : base(dbContext) { }

        public IQueryable<Role> GetAllRolesData()
        {
            return DbSet.Include(a => a.RoleFeatureApiMappers);
        }
    }
}
