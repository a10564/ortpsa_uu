﻿using MicroService.DataAccess.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace MicroService.DataAccess.Helpers
{
    public class RepositoryFactories
    {
        private readonly IDictionary<Type, Func<DbContext, object>> _repositoryFactories;

        private IDictionary<Type, Func<DbContext, object>> GetMicroserviceFactories()
        {
            return new Dictionary<Type, Func<DbContext, object>>
            {
                    //{typeof(IFarmerRepository), dbContext => new FarmerRepository(dbContext)}
                    
                {typeof(IUserRepository), dbContext => new UserRepository(dbContext)},
                {typeof(IRoleRepository), dbContext => new RoleRepository(dbContext)},
                {typeof(IFeatureApiMapperRepository), dbContext => new FeatureApiMapperRepository(dbContext)},
                {typeof(IUniversityRegistrationDetailsRepository), dbContext => new UniversityRegistrationDetailsRepository(dbContext)},
                {typeof(IExaminationFormFillUpRepository), dbContext => new ExaminationFormFillUpRepository(dbContext)},
                {typeof(IExamFormFillupCenterAndCollegeNameRepository), dbContext => new ExamFormFillupCenterAndCollegeNameRepository(dbContext)},
				{typeof(IServiceMasterRepository), dbContext => new ServiceMasterRepository(dbContext)},
                {typeof(IUniversityMigrationDetailsRepository), dbContext => new UniversityMigrationDetailsRepository(dbContext)},
                {typeof(ICLCRepository), dbContext => new CLCRepository(dbContext)},
                {typeof(IAcademicStatementViewRepository), dbContext => new AcademicStatementViewRepository(dbContext)},
                {typeof(IExaminationFormFillupSemesterMarkRepository), dbContext => new ExaminationFormFillupSemesterMarkRepository(dbContext)},
                {typeof(INursingFormFillUpRepository), dbContext => new NursingFormFillUpRepository(dbContext)},
                {typeof(ISubjectMasterRepository), dbContext => new SubjectMasterRepository(dbContext)},
                {typeof(IExaminationMasterRepository), dbContext => new ExaminationMasterRepository(dbContext)},
                {typeof(ITransactionLogRepository), dbContext => new TransactionLogRepository(dbContext)}

            };
            //return dic;
           
        }




        /// <summary>
        /// Constructor that initializes with an arbitrary collection of factories
        /// </summary>
        /// <param name="factories">
        /// The repository factory functions for this instance. 
        /// </param>
        /// <remarks>
        /// This ctor is primarily useful for testing this class
        /// </remarks>
        public RepositoryFactories()
        {
            _repositoryFactories = GetMicroserviceFactories();
            //_repositoryFactories
        }

        /// <summary>
        /// Get the repository factory function for the type.
        /// </summary>
        /// <typeparam name="T">Type serving as the repository factory lookup key.</typeparam>
        /// <returns>The repository function if found, else null.</returns>
        /// <remarks>
        /// The type parameter, T, is typically the repository type 
        /// but could be any type (e.g., an entity type)
        /// </remarks>
        public Func<DbContext, object> GetRepositoryFactory<T>()
        {

            Func<DbContext, object> factory;
            _repositoryFactories.TryGetValue(typeof(T), out factory);
            return factory;
        }

        /// <summary>
        /// Get the factory for <see cref="IRepository{T}"/> where T is an entity type.
        /// </summary>
        /// <typeparam name="T">The root type of the repository, typically an entity type.</typeparam>
        /// <returns>
        /// A factory that creates the <see cref="IRepository{T}"/>, given an EF <see cref="DbContext"/>.
        /// </returns>
        /// <remarks>
        /// Looks first for a custom factory in <see cref="_repositoryFactories"/>.
        /// If not, falls back to the <see cref="DefaultEntityRepositoryFactory{T}"/>.
        /// You can substitute an alternative factory for the default one by adding
        /// a repository factory for type "T" to <see cref="_repositoryFactories"/>.
        /// </remarks>
        public Func<DbContext, object> GetRepositoryFactoryForEntityType<T>() where T : class
        {
            return GetRepositoryFactory<T>() ?? DefaultEntityRepositoryFactory<T>();
        }

        /// <summary>
        /// Default factory for a <see cref="IRepository{T}"/> where T is an entity.
        /// </summary>
        /// <typeparam name="T">Type of the repository's root entity</typeparam>
        protected virtual Func<DbContext, object> DefaultEntityRepositoryFactory<T>() where T : class
        {
            return dbContext => new MicroServiceGenericRepository<T>(dbContext);
        }


    }
}
