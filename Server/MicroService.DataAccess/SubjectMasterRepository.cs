﻿using Microservice.Common;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Entity.Lookup;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess
{
    public class SubjectMasterRepository : MicroServiceGenericRepository<SubjectMaster>, ISubjectMasterRepository
    {
        public SubjectMasterRepository(DbContext dbContext) : base(dbContext) { }

        public IQueryable<SubjectMaster> GetSubjectMasterByCollegeCode(string collegeCode)
        {
            return DbSet
                 //.Include(p => p.ServiceConveniencePricingMaster)
                 .Include(p => p.CollegeSubjectMapper)
                 .Where(p => p.IsActive == true 
                 && p.CollegeSubjectMapper.Select(a => a.CollegeCode).Contains(collegeCode));
        }
    }
}
