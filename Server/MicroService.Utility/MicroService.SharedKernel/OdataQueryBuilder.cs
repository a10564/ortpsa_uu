﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.SharedKernel
{
    /// <summary>
    /// Saswat Mallik
    /// OdataQueryBuilder
    /// This is the argument type Odata requires in the Post method type to use OdataCommunityLinq
    /// </summary>
    public class OdataQueryBuilder
    {
        /// <summary>
        /// This is the Query in string
        /// </summary>
        public string Filter { get; set; }
        /// <summary>
        /// this is the order by property and type
        /// </summary>
        public OdataOrderByQuery OrderBy { get; set; }
        /// <summary>
        /// How many record user want to skip from the filtered record
        /// </summary>
        public long Skip { get; set; }
        /// <summary>
        /// How many record user want to take from the filtered record
        /// </summary>
        public long Take { get; set; }
    }
    /// <summary>
    /// Used For Oredr by data in OdataQueryBuilder class
    /// </summary>
    public class OdataOrderByQuery
    {
        /// <summary>
        /// This is property name on which user want to apply order by
        /// </summary>
        public string Property { get; set; }
        /// <summary>
        /// user have to give either of these two  asc,desc to user order by
        /// </summary>
        public string OrderType { get; set; }  //ToDo:: Enum use for asc or desc
    }
}
