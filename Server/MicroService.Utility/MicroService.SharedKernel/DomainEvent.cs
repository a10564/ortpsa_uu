﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.SharedKernel
{
    /// <summary>
    /// 
    /// </summary>
    public static class DomainEvent
    {
        [ThreadStatic]
        private static List<Delegate> _actions;

        static DomainEvent()
        {
            //TODO: Get Ninject bindings here
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="callback"></param>
        public static void Register<T>(Action<T> callback) where T : IDomianEvent
        {
            if (_actions == null)
            {
                _actions = new List<Delegate>();
            }

            if (!_actions.Contains(callback) && !_actions.Exists(t => t.Method == callback.Method))
                _actions.Add(callback);
        }
        /// <summary>
        /// 
        /// </summary>
        public static void ClearCallbacks()
        {
            _actions = null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="args"></param>
        public static void Raise<T>(T args) where T : IDomianEvent
        {
            //TODO: Put Ninject stuff here

            if (_actions == null) return;
            foreach (var action in _actions)
            {
                if (action is Action<T>)
                {
                    ((Action<T>)action)(args);
                }
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IDomianEvent
    {
        /// <summary>
        /// 
        /// </summary>
        DateTime DateTimeEventOccured { get; }
    }
}
