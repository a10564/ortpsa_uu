﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;

namespace MicroService.Utility.Common
{
    public class Constant
    {
        public const string CULTURE_ENGLISH = "en-US";
        public const string RESOURCE_ERROR_PATH = "Resources.RESError";
        public const string APP_GLOBALRESOURCES = "App_GlobalResources";
        public const string ERROR_GENERIC = "GEN100";
        public const string unicodeMessageType = "unicode";

        private static string GetDateString(DateTime date)
        {
            string str = date.ToString("d{0} MMM, yyyy", new System.Globalization.CultureInfo("en-us"));
            string format;
            switch (date.Day)
            {
                case 1:
                case 21:
                case 31:
                    format = "st";
                    break;
                case 2:
                case 22:
                    format = "nd";
                    break;
                case 3:
                case 33:
                    format = "rd";
                    break;
                default:
                    format = "th";
                    break;
            }

            return String.Format(str, format);
        }
        private static string GetEnumDescription(object enumValue)
        {
            string defDesc = string.Empty;
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

            if (null != fi)
            {
                object[] attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }

            return defDesc;
        }

    }

    public enum EnTaskServiceType
    {
        EmailTask = 1,
        SMSTask = 2,
        Notification = 3
    }
    public enum EnEmailAttachmentType
    {
        Pdf = 1
    }



    public enum EnMonthName
    {
        [Description("January")]
        January = 1,
        [Description("February")]
        February = 2,
        [Description("March")]
        March = 3,
        [Description("April")]
        April = 4,
        [Description("May")]
        May = 5,
        [Description("June")]
        June = 6,
        [Description("July")]
        July = 7,
        [Description("August")]
        August = 8,
        [Description("September")]
        September = 9,
        [Description("October")]
        October = 10,
        [Description("November")]
        November = 11,
        [Description("December")]
        Deceember = 12,

    }

    public enum EnOperationStatus
    {
        SUCCESS,
        FAILED,
        ERROR
    }



    public enum EnImageSize
    {
        GalleryThumbnail = 75,
        GalleryThumbnailRetina = 150,
        FullImage = 480
    }

    public enum EnImageExtension
    {
        Bmp = 1,
        Emf = 2,
        Exif = 3,
        Gif = 4,
        Icon = 5,
        Jpeg = 6,
        MemoryBmp = 7,
        Png = 8,
        Tiff = 9,
        Wmf = 10
    }

    public enum EnImageType
    {
        GalleryThumbnailType = 1,
        GalleryThumbnailRetinaType = 2,
        FullImageType = 3,
        OriginalImageType = 4
    }

    public enum EnGender
    {
        Male = 1,
        Female = 2
    }
    public enum EnWeekDays
    {
        [Description("Sunday")]
        Sun,
        [Description("Monday")]
        Mon,
        [Description("Tuesday")]
        Tue,
        [Description("Wednesday")]
        Wed,
        [Description("Thursday")]
        Thu,
        [Description("Friday")]
        Fri,
        [Description("Saturday")]
        Sat
    }
}
