﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using MicroService.Utility.Common.Abstract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MicroService.Utility.Common
{
    public class ExcelParserCommon : IExcelParserCommon
    {
        public string getStringValueOfCell(Cell c, WorkbookPart workbookPart)
        {
            string towerinfo = "";
            if (c != null && c.CellValue != null && c.CellValue.Text != null)
            {
                towerinfo = c.CellValue.Text;
                towerinfo = c.InnerText;
                if (c.DataType != null && c.DataType == CellValues.SharedString)
                {
                    int id = -1;
                    if (Int32.TryParse(towerinfo, out id))
                    {
                        SharedStringItem item =
                        workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(id);

                        if (item.Text != null)
                        {
                            towerinfo = item.Text.Text;
                        }
                        else if (item.InnerText != null)
                        {
                            towerinfo = item.InnerText;
                        }
                        else if (item.InnerXml != null)
                        {
                            towerinfo = item.InnerXml;
                        }
                    }
                }
                //else
                //{
                //    double d = double.Parse(c.CellValue.Text);
                //    DateTime conv = DateTime.FromOADate(d);
                //    towerinfo = conv.ToShortDateString();
                //}
            }

            return towerinfo;
        }

        public bool detectFormulaInExcel(string path)
        {
            bool isFormulaDetected = false;
            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fs, true))
                    {
                        WorkbookPart workbookPart = doc.WorkbookPart;
                        SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();


                        // VbaProjectPart newPart = workbookPart.GetPartsOfType<VbaProjectPart>().FirstOrDefault();
                        //System.IO.Stream data = GetBinaryDataStream(partData);
                        //newPart.FeedData(data);

                        List<Sheet> allWorksheets = workbookPart.Workbook.Descendants<Sheet>().ToList();
                        foreach (var onesheet in allWorksheets)
                        {
                            var sheetname = onesheet.Name;
                            Sheet theSheet = workbookPart.Workbook.Descendants<Sheet>().Where(s => s.Name == sheetname).FirstOrDefault();

                            WorksheetPart worksheetPart = (WorksheetPart)(workbookPart.GetPartById(theSheet.Id));
                            Worksheet sheet = worksheetPart.Worksheet;

                            var rows = sheet.Descendants<Row>();

                            List<string> rowNumberList = new List<string>();

                            CalculationChainPart calculationChainPart = workbookPart.CalculationChainPart;
                            CalculationChain calculationChain = calculationChainPart.CalculationChain;
                            var calculationCells = calculationChain.Elements<CalculationCell>().ToList();

                            if (isFormulaDetected)
                            {
                                isFormulaDetected = true;
                                break;
                            }
                            foreach (Row row in worksheetPart.Worksheet.GetFirstChild<SheetData>().Elements<Row>())
                            {
                                var cell = row.Elements<Cell>().Where(a => a.CellFormula != null && a.CellValue != null).FirstOrDefault();
                                if (cell != null)
                                {
                                    isFormulaDetected = true;
                                    break;
                                }


                            }




                        }
                        doc.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            if (isFormulaDetected == true)
            {
                File.Delete(path);
            }
            return isFormulaDetected;
        }
    }
}
