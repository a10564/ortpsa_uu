﻿using SelectPdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Mail;
using System.Text;

namespace MicroService.Utility.Common
{
    public static class HtmlToPdfConverter
    {
        public static Attachment AttachPDF(string htmlContent, string fileName, int height= 1122, int width= 794)
        {          
            
            // read parameters from the webpage
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                PdfPageSize.A4.ToString(), true);

            PdfPageOrientation pdfOrientation =
                (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                PdfPageOrientation.Portrait.ToString(), true);

            int webPageWidth = width;
            int webPageHeight = height;



            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;

            // create a new pdf document converting an url
            PdfDocument doc = converter.ConvertHtmlString(htmlContent);

            // save pdf document
            byte[] pdf = doc.Save();

            // close pdf document
            doc.Close();

            Attachment attach = new Attachment(new MemoryStream(pdf), fileName + ".pdf", "application/pdf");
            return attach;
        }

        public static byte[] ExportToPDF(string htmlContent, int height = 1122, int width = 794)
        {
            try
            {
                // read parameters from the webpage
                PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                    PdfPageSize.A4.ToString(), true);

                PdfPageOrientation pdfOrientation =
                    (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                    PdfPageOrientation.Portrait.ToString(), true);

                int webPageWidth = width;
                int webPageHeight = height;



                // instantiate a html to pdf converter object
                HtmlToPdf converter = new HtmlToPdf();

                // set converter options
                converter.Options.PdfPageSize = pageSize;
                converter.Options.PdfPageOrientation = pdfOrientation;
                converter.Options.WebPageWidth = webPageWidth;
                converter.Options.WebPageHeight = webPageHeight;

                // create a new pdf document converting an url
                PdfDocument doc = converter.ConvertHtmlString(htmlContent);

                // save pdf document
                byte[] pdf = doc.Save();

                // close pdf document
                doc.Close();
                return pdf;
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Failed, exception thrown: " + ex.Message);
                Byte[] array = new Byte[64];
                Array.Clear(array, 0, array.Length);
                return array;
            }
        }
    }

    
}
