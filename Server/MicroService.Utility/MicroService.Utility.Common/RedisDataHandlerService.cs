﻿using MicroService.Utility.Common.Abstract;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Utility.Common
{
    public class RedisDataHandlerService: IRedisDataHandlerService
    {
        private readonly IHttpContextAccessor _context;
        protected IDistributedCache _memoryCache;

        public RedisDataHandlerService(IHttpContextAccessor context, IDistributedCache memoryCache)
        {
            _context = context;
            _memoryCache = memoryCache;
        }

        private JObject GetMemoryCache()
        {
            JObject redisData = new JObject();
            try
            {
                var authorization = _context.HttpContext.Request.Headers["Authorization"];
                if (!StringValues.IsNullOrEmpty(authorization))
                {
                    string authTokenKey = Convert.ToString(authorization).Replace("Bearer", "").Trim();
                    redisData = JObject.Parse(Encoding.UTF8.GetString(_memoryCache.Get("urn:Users:" + authTokenKey)));
                }
            }
            catch (Exception ex)
            {
                throw new Exception("User data not found.");
            }
            return redisData;
        }

        public T GetCurrentRedisData<T>(string key)
        {
            JObject redisCache = GetMemoryCache();
            T redisData = default(T);
            try
            {
                var redisDataToken = redisCache.SelectToken(key);
                redisData = redisDataToken.ToObject<T>();
            }
            catch (Exception ex)
            {
                redisData = default(T);
            }
            return redisData;
        }

        public void SetMemoryCache<T>(string key, T value)
        {
            var cacheOptions = new DistributedCacheEntryOptions
            {
                AbsoluteExpiration = DateTime.UtcNow.AddYears(1)
            };

            string serializeValue = JsonConvert.SerializeObject(value);
            _memoryCache.Set(key, Encoding.UTF8.GetBytes(serializeValue), cacheOptions);
        }
    }
}
