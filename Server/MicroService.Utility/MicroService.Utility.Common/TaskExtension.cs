﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using MicroService.Utility.Common.CustomAttributes;

namespace MicroService.Utility.Common
{
    public static class TaskExtension
    {
        /// <summary>
        /// This method gets the properties of an interface
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static PropertyInfo[] GetPublicInterfaceProperties(this Type type)
        {
            if (type.IsInterface)
            {
                var propertyInfos = new List<PropertyInfo>();

                var considered = new List<Type>();
                var queue = new Queue<Type>();
                considered.Add(type);
                queue.Enqueue(type);
                while (queue.Count > 0)
                {
                    var subType = queue.Dequeue();
                    foreach (var subInterface in subType.GetInterfaces())
                    {
                        if (considered.Contains(subInterface)) continue;

                        considered.Add(subInterface);
                        queue.Enqueue(subInterface);
                    }

                    var typeProperties = subType.GetProperties(
                        BindingFlags.FlattenHierarchy
                        | BindingFlags.Public
                        | BindingFlags.Instance);

                    var newPropertyInfos = typeProperties
                        .Where(x => !propertyInfos.Contains(x));

                    propertyInfos.InsertRange(0, newPropertyInfos);
                }

                return propertyInfos.ToArray();
            }

            return type.GetProperties(BindingFlags.FlattenHierarchy
                | BindingFlags.Public | BindingFlags.Instance);
        }

        public static void AddProperties(this Dictionary<string, object> dictionary, List<PropertyInfo> properties)
        {
            properties.ForEach(propInfo =>
            {
                dictionary.Add(propInfo.Name, string.Empty);
            });
        }
        /// <summary>
        /// This method Get the Properties of the Model with the basic Task Properties
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tmodelObj"></param>
        /// <returns></returns>
        public static Dictionary<string, object> GetTaskModelPropertyAndValue<T>(this T tmodelObj) where T:class
        {
            Dictionary<string, object> _dict = new Dictionary<string, object>();

            //Getting Type of Generic Class Model
            Type tModelType = tmodelObj.GetType();

            //We will be defining a PropertyInfo Object which contains details about the class property 
            PropertyInfo[] arrayPropertyInfos = tModelType.GetProperties();

            //Now we will loop in all properties one by one to get value
            foreach (PropertyInfo prop in arrayPropertyInfos)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    TaskParameterAttribute authAttr = attr as TaskParameterAttribute;
                    if (authAttr != null)
                    {
                        string propName = prop.Name;
                        object propValue = prop.GetValue(tmodelObj);

                        _dict.Add(propName, propValue);
                    }
                }
            }

            var properties = typeof(ITaskModelParam).GetPublicInterfaceProperties().ToList();
            if (properties.Count > 0)
            {
                foreach (PropertyInfo entry in properties)
                {
                    // do something with entry.Value or entry.Name
                    _dict.Add(entry.Name, GetPropValue(tmodelObj, entry.Name));
                   
                }               
            }            
            return _dict;
        }
        public static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        public static Dictionary<string, object> ImplementTaskBaseProperties(this Dictionary<string, object> dict)
        {
            var properties = typeof(ITaskModelParam).GetPublicInterfaceProperties().ToList();
            if (properties.Count > 0)
            {
                dict.AddProperties(properties);
            }
            return dict;
        }        
        public static void Set(this IDictionary<string, object> dictionary,
                               string key, object value)
        {
            dictionary[key] = value;
        }

    }    
    public interface ITaskModelParam
    {
        long WorkflowTransitionConfigId { get; set; }
    }
}
