﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Utility.Common.Abstract
{
    public interface IRedisDataHandlerService
    {
        T GetCurrentRedisData<T>(string key);
    }
}
