﻿
using System.Collections.Generic;
using System;

namespace MicroService.Utility.Caching.Abstract
{
    public interface ICacheService
    {
        T Get<T>(string cacheID, Func<T> getItemCallback) where T : class;
        void ClearCache(string cacheID);
        void ClearCache();
    }
}
