﻿using MicroService.Utility.Caching.Abstract;
using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using Microsoft.Extensions.Caching.Memory;
using System.Reflection;

namespace MicroService.Utility.Caching
{
    public class CacheService : ICacheService
    {
        private readonly IMemoryCache _memoryCache;
        public CacheService(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        /// <summary>
        /// If there will be data for cacheID it will return that.
        /// Otherwise it will execute the getItemCallback method and send the data as well as set the data in cache for forther use.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheID"></param>
        /// <param name="getItemCallback"></param>
        /// <returns></returns>
        public T Get<T>(string cacheID, Func<T> getItemCallback) where T : class
        {
           
            T item = _memoryCache.Get(cacheID) as T;
            if (item == null)
            {
                item = getItemCallback();
                _memoryCache.Set(cacheID, item);
            }
            return item;
        }

        /// <summary>
        /// Method to clear cache by a key
        /// It will take cacheId as  argument and remove the the assosiated recored 
        /// </summary>
        /// <param name="cacheId"></param>
        public void ClearCache(string cacheId)
        {
            _memoryCache.Remove(cacheId);
        }
        /// <summary>
        /// It will remove all the cache memory
        /// </summary>
        public void ClearCache()
        {
            PropertyInfo prop = _memoryCache.GetType().GetProperty("EntriesCollection", BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.NonPublic | BindingFlags.Public);
            object innerCache = prop.GetValue(_memoryCache);
            MethodInfo clearMethod = innerCache.GetType().GetMethod("Clear", BindingFlags.Instance | BindingFlags.Public);
            clearMethod.Invoke(innerCache, null);
        }
    }
}
