﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Utility.Exception
{
   public class MicroServiceException : RankException
    {
        public int? StatusCode { get; set;}
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public object[] SubstitutionParams { get; set; }
        public bool isPartial { get; set; }
    }
}
