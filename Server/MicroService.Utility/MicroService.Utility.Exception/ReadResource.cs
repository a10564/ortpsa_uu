﻿using MicroService.Utility.Common;
using MicroService.Utility.Exception;
using System;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Utility.Exception
{
    public class ReadResource
    {
        private static CultureInfo _cultureInfo = new System.Globalization.CultureInfo(Constant.CULTURE_ENGLISH);
        private static System.Resources.ResourceManager resourceManagerError = new ResourceManager(Constant.RESOURCE_ERROR_PATH, Assembly.Load(new AssemblyName(Constant.APP_GLOBALRESOURCES)));

        /// <summary>
        /// Get the error message from Resource file
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string GetMessageForCode(string message)
        {
            //string key = Constant.ERROR;
            string key = message;
            string value = String.Empty;
            try
            {
                //key = Constant.ERROR + message;
                value = resourceManagerError.GetString(key, _cultureInfo) ?? GetDefaultErrorMessage();
            }
            catch (MicroServiceException ex)
            {
                value = GetDefaultErrorMessage();
            }
            return value;
        }

        public static string GetErrorMessage(MicroServiceException _MicroServiceException)
        {
            if (_MicroServiceException.SubstitutionParams == null)
            {
                if (!String.IsNullOrEmpty(_MicroServiceException.ErrorMessage))
                {
                    return _MicroServiceException.ErrorMessage;
                }
                else
                {
                    return GetMessageForCode(_MicroServiceException.ErrorCode);
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(_MicroServiceException.ErrorMessage))
                {
                    return string.Format(_MicroServiceException.ErrorMessage, _MicroServiceException.SubstitutionParams);
                }
                else
                {
                    string errorMessage = GetMessageForCode(_MicroServiceException.ErrorCode);
                    //Now replace the parameters
                    if (_MicroServiceException.SubstitutionParams.Length > 1)
                    {
                        return string.Format(_MicroServiceException.ErrorMessage, _MicroServiceException.SubstitutionParams);
                    }
                    else
                    {
                        return string.Format(errorMessage, _MicroServiceException.SubstitutionParams);
                    }
                }
            }

        }
        

        private static string GetDefaultErrorMessage()
        {
            //return resourceManagerError.GetString(Constant.ERROR + Constant.ERROR_EXCEPTION, _cultureInfo);
            return resourceManagerError.GetString(Constant.ERROR_GENERIC, _cultureInfo);
        }

        //To get the description from resouce file
        /// <summary>
        /// Get the message
        /// </summary>
        /// <param name="sMsgCode">parameter Name</param>
        /// <returns>Parameter messages</returns>
        //public static string GetMessageDescription(string sMsgCode)
        //{
        //    string value = String.Empty;
        //    try
        //    {
        //        //get the Values field for specific MessageCode                
        //        value = resourceManagerRouting.GetString(sMsgCode, _cultureInfo);
        //    }
        //    catch (Exception ex)
        //    {
        //        value = resourceManagerRouting.GetString(Constant.CARELOGIN_LOGON, _cultureInfo);
        //    }
        //    return value;
        //}
    }
}
