﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Serilog.Core;
using Serilog;

namespace MicroService.Utility.Logging
{
    public sealed class AzureLogConfiguration
    {
        // internal singleton instance
		
        private static AzureLogConfiguration _instance;

        // lock for thread-safety laziness
        private static readonly object _mutex = new object();
        public string LoggerType { get; }
        public string WorkSpaceID { get; }
        public string AuthenticationID { get; }
        public string LoggingSchema { get; }
        public string MicroserviceName { get; }
        private AzureLogConfiguration()
        {

            // Adding JSON file into IConfiguration.
            var builder = new ConfigurationBuilder()
                 .SetBasePath(Directory.GetCurrentDirectory())
                 .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();
            LoggerType = Convert.ToString(configuration.GetSection($"{"AzureLogging"}:{"LoggerType"}").Value);
            WorkSpaceID = Convert.ToString(configuration.GetSection($"{"AzureLogging"}:{"AzureLogWorkSpaceID"}").Value);
             AuthenticationID = Convert.ToString(configuration.GetSection($"{"AzureLogging"}:{"AzureLogAuthenticationID"}").Value);
             LoggingSchema = Convert.ToString(configuration.GetSection($"{"AzureLogging"}:{"AzureLogLoggingSchema"}").Value);
             MicroserviceName = Convert.ToString(configuration.GetSection($"{"AzureLogging"}:{"AzureLogMicroserviceName"}").Value);
        }

        // public method, used to obtain an instance of this class
        // implements double-locking
        public static AzureLogConfiguration Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_mutex)
                    {
                        if (_instance == null)
                        {
                            _instance = new AzureLogConfiguration();
                        }
                    }
                }

                return _instance;
            }
        }

    }
    
}
