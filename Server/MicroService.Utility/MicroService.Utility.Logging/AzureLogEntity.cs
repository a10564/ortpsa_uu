﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Utility.Logging
{
    public class AzureLogEntity
    {
        public string Category { get; set; }
        public string Description { get; set; }
        public DateTime LogDateDateTime { get; set; }
        public Guid ID { get; set; }
        public string LogLevel { get; set; }
        public string TenantId { get; set; }
        public string UserId { get; set; }
        public string Activity { get; set; }
    }
	
}
