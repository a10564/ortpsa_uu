﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Utility.Logging.Abstract
{
    public interface IMicroServiceLogger
    {
       // void Trace(string Message);
        void Debug(string Message);
        void Info(string Message);
        void Warn(string Message);
        void Error(string Message);
        void Error(Exception ex);
        void Fatal(string Message);
        void Fatal(Exception ex);
        void Verbose(string Message);
        void Info(object data, string activity, string tenantId, string userId);
    }
}
