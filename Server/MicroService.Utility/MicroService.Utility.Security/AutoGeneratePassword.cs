﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Utility.Security
{
    /// <summary>
    /// AutoGeneratePassword
    /// </summary>
    public static class AutoGeneratePassword
    {
        #region AutoGenPassword
        private static readonly Random objRandom = new Random();
        private static readonly object syncLock = new object();

        /// <summary>
        /// Generates a random password
        /// </summary>
        /// <returns></returns>
        public static string GeneratePassword()
        {
            int randomNumber;
            string strPassword = "";
            string strTemp = "";
            int length = 6;
            char[] chararray = { 'B', '1', '2', 'F', '3', 'G', '4', 'H', '5', 'J', '#', 'K', '7', 'L', '8', 'M', '9', 'N', '$', 'W', 'X', '6', 'Z', '@' };
            for (int i = 1; i <= length; i++)
            {
                if (i == 1)
                {
                    lock (syncLock)
                    {
                        randomNumber = objRandom.Next(0, 22);
                    }
                }
                else
                {
                    lock (syncLock)
                    {
                        randomNumber = objRandom.Next(0, 23);
                    }

                }

                strTemp = chararray[randomNumber].ToString();
                strPassword += strTemp;
            }
            return strPassword;
        }

        /// <summary>
        /// generates a rendom string used as coupon code
        /// </summary>
        /// <returns></returns>
        public static string GenerateCouponCode()
        {
            int randomNumber;
            string strCouponCode = "";
            string strTemp = "";
            int length = 8;
            char[] chararray = { 'B', 'C', 'A', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'M', 'P', 'S', 'T', 'Y', 'X', 'Z', '0', '2', '1', '3', '4', '8', '6', '7', '5', '9' };
            for (int i = 1; i <= length; i++)
            {
                if (i == 1)
                {
                    lock (syncLock)
                    {
                        randomNumber = objRandom.Next(0, 16);
                    }
                }
                else
                {
                    lock (syncLock)
                    {
                        randomNumber = objRandom.Next(0, 26);
                    }

                }

                strTemp = chararray[randomNumber].ToString();
                strCouponCode += strTemp;
            }
            return strCouponCode;
        }

        /// <summary>
        /// generates a random Name
        /// </summary>
        /// <returns></returns>
        public static string GenerateWebinarRandomName()
        {
            int randomNumber;
            string strPassword = "";
            string strTemp = "";
            int length = 6;
            char[] chararray = { 'B', '1', '2', 'F', '3', 'G', '4', 'H', '5', 'J', 'K', '7', 'L', '8', 'M', '9', 'N', 'W', 'X', '6', 'Z' };
            for (int i = 1; i <= length; i++)
            {
                if (i == 1)
                {
                    lock (syncLock)
                    {
                        randomNumber = objRandom.Next(0, chararray.Length);
                    }
                }
                else
                {
                    lock (syncLock)
                    {
                        randomNumber = objRandom.Next(0, chararray.Length);
                    }

                }

                strTemp = chararray[randomNumber].ToString();
                strPassword += strTemp;
            }
            return strPassword;
        }
        #endregion
    }
}
