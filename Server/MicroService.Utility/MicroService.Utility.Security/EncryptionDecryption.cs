﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace MicroService.Utility.Security
{
    public class EncryptionDecryption
    {
        /// <summary>
        /// It returns Hash according to the input string
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string getHash(string text)
        {
            // SHA512 is disposable by inheritance.
            using (var sha256 = SHA256.Create())
            {
                // Send a sample text to hash.
                var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(text));

                // Get the hashed string.
                return BitConverter.ToString(hashedBytes);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string getSalt()
        {
            byte[] bytes = new byte[128 / 8];
            using (var keyGenerator = RandomNumberGenerator.Create())
            {
                keyGenerator.GetBytes(bytes);

                return BitConverter.ToString(bytes);
            }
        }
        /// <summary>
        /// Used to compair the Hash
        /// </summary>
        /// <param name="stored"></param>
        /// <param name="supplied"></param>
        /// <returns></returns>
        public static bool CompareHash(string stored, string supplied)
        {
            //First check if they are the same length or not
            if (stored.Length != supplied.Length)
            {
                return false;
            }

            //Now check if the individual bits are the same
            for (int i = 0; i < stored.Length; i++)
            {
                if (stored[i] != supplied[i])
                {
                    return false;
                }
            }

            return true;
        }
        /// <summary>
        /// Encriptor class
        /// </summary>
        public static class Encryptor
        {
            /// <summary>
            /// key
            /// </summary>
            public static string key = "ABAB808080808080";
            private static Random random = new Random();
            /// <summary>
            /// This returns a rendom string
            /// </summary>
            /// <param name="length"></param>
            /// <returns></returns>
            public static string RandomString(int length)
            {
                string chars = "ABCDEFGH" + (DateTime.Now.ToString()) + "IJKLMNOPQRSTUVWXYZ0123456789";
                return new string(Enumerable.Repeat(chars, length)
                  .Select(s => s[random.Next(s.Length)]).ToArray());
            }

        }
        
    }

}

