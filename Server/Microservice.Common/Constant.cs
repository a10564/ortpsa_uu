﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Microservice.Common
{

    public class Constant
    {
        public const string RollNumberGenerationPending = "Roll No. Generation Pending";
        public const string AdmitCardGenerationPending = "Admitcard Generation Pending";
        public const string AdmitCardGenerated = "Admitcard Generated";
        public const string MarkUploaded = "Mark Uploaded";
        public const string ResultPublication = "Result Publication";
        public const string PaymentDone = "Payment Done";
        

        public const string POST = "POST";
		public const string PUT = "PUT";
		public const string DELETE = "DELETE";
		public const string GET = "GET";

        #region Cache names
        public const string CollegeMasterCacheId = "UtkalORTPSA_CacheCollegeMaster";
        public const string LateFineMasterCacheId = "UtkalORTPSA_CacheLateFineMaster";
        public const string LookupCacheId = "UtkalORTPSA_CacheLookUp";
        public const string ExaminationPaperMasterCacheId = "UtkalORTPSA_CacheExaminationPaperMaster";
        public const string ExaminationMasterCacheId = "UtkalORTPSA_CacheExaminationMaster";
        public const string ExaminationSessionCacheId = "UtkalORTPSA_CacheExaminationSession";
        public const string EmailTemplateCacheId = "UtkalORTPSA_CacheEmailTemplate";
        #endregion

        #region Constant Name used in the bussiness
        public const string SemesterExamNotificationText = "Click here to form fill up 2nd semester";
        
        #endregion
    }

    public enum enRoles
    {
        Student = 4,
        Principal = 5,
        Registrar = 6,
        CompCell = 7,
        AsstCoE = 8,
        HOD = 9,
		COE = 10,
        Level1Support = 11
    }

    public enum enGender
    {
        Male = 1,
        Female = 2
    }

    public enum enLoginResultType
    {
        Success = 1,
        InvalidUserNameOrEmailAddress = 2,
        InvalidPassword = 3,
        UserIsNotActive = 4,
        InvalidTenancyName = 5,
        TenantIsNotActive = 6,
        UserEmailIsNotConfirmed = 7,
        //UnknownExternalLogin = 8,
        //LockedOut = 9,
        //UserPhoneNumberIsNotConfirmed = 10
    }

    public enum enMultiLingualLanguageType
    {
        en = 1,
        hi = 2,
        cn = 3,
        fr = 4
    }

    public enum enModule
    {
        Login = 0,
        UserModule = 1,
        SharedModule = 2,
        ProductModule = 3
    }

    public enum enServiceType
    {
        MigrationService = 1,
        RegistrationService = 2,
        ProvisionalCertificate = 3,
        CollegeLeavingCertificate = 4,
        DuplicateRegistrationCertificate = 5,
        VerificationofAdditionofMarks = 6,
        IssueofPaperwiseMarks = 7,
        BCABBAExamApplicationForm = 8,
        StudentList = 9,
        NursingApplicationForm = 10
    }
    

    public enum enEmailTemplate
    {
        MigrationCertificate = 1,
        ChangePassword = 2,
        UserRegistration = 3,
        OtpGeneration = 4,
        ResetPassword = 5,
        RegistrationCertificate = 6,
        RegistrationNoAcknowledgement = 7,
        AcknowledgementNo = 8,
        RegistrationNumberGenerated = 9,
        ServiceRequestReceived = 10,
        ApplicationModified = 11,
        MigrationAcknowledgement = 12,
        DuplicateRegistrationAcknowledgement = 13,
        AdmitCardToStudent=20,
        ExaminationFormFillUp = 23,
        PaymentSuccessful=26,
        PaymentFailed=27,
        PaymentCancelled=28,
        CnrReport=30,
        AcknowledgementSMS=31,
        ExamFormFillupPaymentPendingEmail = 34,
        ExamFormFillupPaymentPendingSMS = 35,
        ExamFormFillupRejectionEmail = 36,
        ExamFormFillupRejectionSMS = 37,
        ExamFormFillupAdmitCardGenerated = 38,
        ExamFormFillupAdmitCardGeneratedSMS = 39,
        OTPGenerationSMS=40,
        PaymentReceipt=41,
        ApplicationAcknowledgementSMS = 43,
        RegistrationNumberGeneratedSMS=44,
        ApplicationRejectedMail = 45,
        ApplicationRejectedSMS = 46,
        MailToPrincipalOnRegistrationNumberRejection=47,
        ApplicationAcknowledgementEmail = 48,
        ApplicationUpdatedSMS=49,
        PaymentSuccessfulSMS=50,
        PaymentFailedSMS=51,
        AccountCreatedSMS=52,
        MigrationCertificateApproved=53,
        CLCAcknowledgementForm = 54,
        AccountCreatedByPrincipalOrHOD = 55,
        AccountCreatedByPrincipalOrHODSMS = 56,
        AccountStatement=57,
        StatementRow=58,
        StatementHeader=59,
        Pageno=60,
        SemesterMarkReport = 61,
        PaperSubjectWiseMarkReportMainContent = 62,
        PaperSubjectWiseMarkMainContaintValue = 63,
        PaperSubjectWiseMarkMainBodyValue = 64,
        NursingExamFormFillUp=65,
        NursingSemesterMarkReport = 66,
        NursingConductCertificates=67
    }

    public enum enStatusType
    {
        Completed,
        Present,
        Future
    }

    public enum enUserType
    {
        Student = 4,
        Principal = 5,
        Registrar = 6,
        CompCell = 7,
        AsstCoE = 8,
        HOD = 9,
		COE = 10,
        Level1Support = 11
    }

    public enum enCourseType
    {
        [Description("01")]
        Ug = 1,
        [Description("02")]
        Pg = 2
    }
    public enum enAcknowledgementFormat
    {
        [Description("ACK")]
        ACK = 1
    }
    public enum enProcessName
    {
        SendToCOE = 1,
        RoleNumberGeneration = 2,
        AdmitCardGeneration = 3,
        MarkForwardToCOE=4


    }
    public enum enProcessStatus
    {
        [Description("Added")]
        Added = 1,
        [Description("Ready For Processing")]
        ReadyForProcessing = 2,
        [Description("Start")]
        Start = 3,
        [Description("Complete")]
        Complete = 4
    }

    public enum enDeviceType
    {
        [Description("m")]
        Mobile=1,
        [Description("w")]
        Web =2,
		[Description("t")]
		Tab = 3
	}
    public enum enBackgroundProcessStatus
    {
        [Description("0")]
        WorkDone = 0,
        [Description("1")]
        Pending = 1      
    }
    public enum enRollNumberlength
    {
        UG=3
    }
    public enum enAddressType
    {
        [Description("01")]
        Permanent = 1,
        [Description("02")]
        Present =2
    }
	public enum EnPaymentStatus
	{
		Success = 1,
		Failure = 2,
		Cancel = 3,
		Incomplete = 4
	}
	public enum EnProcessType
	{
		Transaction = 1,
		Reciept = 2,
        RegistrationNumberGeneration=3,
        Migration = 4,
        AcknowledgmentNo=5
	}
    public enum enSpName
    {
        [Description("SP_CANDIDATE_LIST")]
        SpCandidateList = 1,
    }
    public enum EnSemester
    {
        [Description("01")]
        FirstSemester = 1,
        [Description("02")]
        SecondSemester = 2,
        [Description("03")]
        ThirdSemester = 3,
        [Description("04")]
        FourSemester = 4,
        [Description("05")]
        FiveSemester = 5,
        [Description("06")]
        SixSemester = 6
    }

    public enum enCollegeType
    {
        Has_UG = 1,
        Has_UG_BBA_BCA = 2,
        Has_BBA_BCA = 3,
        Has_PG = 4,
        Has_UG_PBN_BSCN = 5
    }
    public enum enStreamCode
    {
        [Description("0400")]
        SelfFinance = 1
    }
    public enum enTransaction
    {
        [Description("Failed")]
        Failed = 1,
        [Description("Cancelled")]
        Cancelled = 2,
        [Description("Successful")]
        Successful = 3
    }

	public enum enCallType
	{	
		Request,	
		Response		
	}

	public enum enOrientation
	{
		Portrait,
		Landscape
	}

    public enum enDevelopmentEnvironment
    {
        win,
        linux
    }

    public enum enLookUpType
    {
        Country = 9,
        State = 1,
        City = 3,
        Semester = 18,
        ApplicationType = 19,
        MarkType = 29,
        BscNursing = 31,
        SessionList = 33,
        UniversityCouseType = 8,
        UniversityStream = 4,
        UniversityCourseCode = 11,
        UniversityDepartment = 7,
        ExaminationDocumentUpload = 28
    }
    public enum enState
    {
        [Description("Reg. No. Application Rejected")]
        RegNoApplicationRejected = 1,
        [Description("Reg. No. Application Approved")]
        RegNoApplicationApproved = 2
    }

    public enum enRegistartionDocType
    {
        RegistartionPhoto=10
    }
    public enum enDisplyCss
    {
        [Description("block")]
        Show =1,
        [Description("none")]
        Hide =2
    }

    public enum enMarkEntryType
    {
        [Description("01")]
        Internal =1,
        [Description("02")]
        External =2,
        [Description("03")]
        Practical = 3
    }

    public enum enMarkUploaded
    {
        [Description("Yes")]
        Yes = 1, 
        [Description("No")]
        No = 2,
    }
    public enum enNursingYear
    {
        [Description("01")]
        FirstYear = 1,
        [Description("02")]
        SecondYear = 2,
        [Description("03")]
        ThirdYear = 3,
        [Description("04")]
        FourYear = 4,
       
    }

    public enum enSubject
    {
        [Description("64")]
        PBNursing =64,
        [Description("65")]
        BSCNursing =65,
        [Description("BBA")]
        BBA = 63,
        [Description("BCA")]
        BCA = 62,
    }

    public enum enCollegeTypes
    { 
        SemesterFormFillUpCollege=1,
        NursingFormFillUpCollege = 2
    }
    public enum enSessionType
    {
        Semester = 1,
        Year =2
    }
    public enum enApplicantDocs
    {
        [Description("EDU-01")]
        ApplicantPhoto = 1,
        [Description("EDU-02")]
        ApplicantSign = 2
    }
    public enum enCasteCode 
    {
        [Description("01")]
        SC = 1,
        [Description("02")]
        ST = 2,
        [Description("03")]
        OBC = 3,
        [Description("04")]
        GEN = 4
    }
}
