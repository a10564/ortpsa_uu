﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Microservice.Common
{
    public class EncryptionDecryption
    {
        private static string _privateKey = "<RSAKeyValue><Modulus>s6lpjspk+3o2GOK5TM7JySARhhxE5gB96e9XLSSRuWY2W9F951MfistKRzVtg0cjJTdSk5mnWAVHLfKOEqp8PszpJx9z4IaRCwQ937KJmn2/2VyjcUsCsor+fdbIHOiJpaxBlsuI9N++4MgF/jb0tOVudiUutDqqDut7rhrB/oc=</Modulus><Exponent>AQAB</Exponent><P>3J2+VWMVWcuLjjnLULe5TmSN7ts0n/TPJqe+bg9avuewu1rDsz+OBfP66/+rpYMs5+JolDceZSiOT+ACW2Neuw==</P><Q>0HogL5BnWjj9BlfpILQt8ajJnBHYrCiPaJ4npghdD5n/JYV8BNOiOP1T7u1xmvtr2U4mMObE17rZjNOTa1rQpQ==</Q><DP>jbXh2dVQlKJznUMwf0PUiy96IDC8R/cnzQu4/ddtEe2fj2lJBe3QG7DRwCA1sJZnFPhQ9svFAXOgnlwlB3D4Gw==</DP><DQ>evrP6b8BeNONTySkvUoMoDW1WH+elVAH6OsC8IqWexGY1YV8t0wwsfWegZ9IGOifojzbgpVfIPN0SgK1P+r+kQ==</DQ><InverseQ>LeEoFGI+IOY/J+9SjCPKAKduP280epOTeSKxs115gW1b9CP4glavkUcfQTzkTPe2t21kl1OrnvXEe5Wrzkk8rA==</InverseQ><D>HD0rn0sGtlROPnkcgQsbwmYs+vRki/ZV1DhPboQJ96cuMh5qeLqjAZDUev7V2MWMq6PXceW73OTvfDRcymhLoNvobE4Ekiwc87+TwzS3811mOmt5DJya9SliqU/ro+iEicjO4v3nC+HujdpDh9CVXfUAWebKnd7Vo5p6LwC9nIk=</D></RSAKeyValue>";
        private static string _publicKey = "<RSAKeyValue><Modulus>s6lpjspk+3o2GOK5TM7JySARhhxE5gB96e9XLSSRuWY2W9F951MfistKRzVtg0cjJTdSk5mnWAVHLfKOEqp8PszpJx9z4IaRCwQ937KJmn2/2VyjcUsCsor+fdbIHOiJpaxBlsuI9N++4MgF/jb0tOVudiUutDqqDut7rhrB/oc=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";

        private static string _md5key = "s6lpjspk";
        private static UnicodeEncoding _encoder = new UnicodeEncoding();

        #region  RSA

        public static string Decrypt(string data)
        {
            try
            {
                var rsa = new RSACryptoServiceProvider();
                var dataArray = data.Split(new char[] { ',' });
                byte[] dataByte = new byte[dataArray.Length];
                for (int i = 0; i < dataArray.Length; i++)
                {
                    dataByte[i] = Convert.ToByte(dataArray[i]);
                }

                rsa.FromXmlString(_privateKey);
                var decryptedByte = rsa.Decrypt(dataByte, false);
                return _encoder.GetString(decryptedByte);
            }
            catch
            {
                Exception e = new Exception();
                throw e;
            }
        }

        public static string Encrypt(string data)
        {
            var rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(_publicKey);
            var dataToEncrypt = _encoder.GetBytes(data);
            var encryptedByteArray = rsa.Encrypt(dataToEncrypt, false).ToArray();
            var length = encryptedByteArray.Count();
            var item = 0;
            var sb = new StringBuilder();
            foreach (var x in encryptedByteArray)
            {
                item++;
                sb.Append(x);

                if (item < length)
                    sb.Append(",");
            }

            return sb.ToString();
        }

        #endregion

        #region MD5

        public static byte[] EncryptMD5(byte[] toEncryptArray, bool useHashing)
        {
            try
            {
                byte[] keyArray;
                //     byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

                //System.Configuration.AppSettingsReader settingsReader =
                //                                    new System.Configuration.AppSettingsReader();

                string key = _md5key;

                if (useHashing)
                {
                    MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                    keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                    hashmd5.Clear();
                }
                else
                    keyArray = UTF8Encoding.UTF8.GetBytes(key);

                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                tdes.Key = keyArray;
                tdes.Mode = CipherMode.ECB;
                tdes.Padding = PaddingMode.PKCS7;
                ICryptoTransform cTransform = tdes.CreateEncryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                tdes.Clear();
                //return Convert.ToBase64String(resultArray, 0, resultArray.Length);
                return resultArray;
            }
            catch
            {
                throw;
            }
        }
        public static byte[] DecryptMD5(byte[] toDecryptArray, bool useHashing)
        {
            try
            {
                byte[] keyArray;
                // byte[] toDecryptArray = Convert.FromBase64String(cipherString);

                //System.Configuration.AppSettingsReader settingsReader =
                //                                    new System.Configuration.AppSettingsReader();
                //string key = (string)settingsReader.GetValue("SecurityKey",
                //                                             typeof(String));
                string key = _md5key;
                if (useHashing)
                {
                    MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                    keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                    hashmd5.Clear();
                }
                else
                {
                    keyArray = UTF8Encoding.UTF8.GetBytes(key);
                }

                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
                tdes.Key = keyArray;
                tdes.Mode = CipherMode.ECB;
                tdes.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = tdes.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(
                                     toDecryptArray, 0, toDecryptArray.Length);
                tdes.Clear();
                //return UTF8Encoding.UTF8.GetString(resultArray);
                return resultArray;
            }
            catch
            {
                throw;
            }
        }

        #endregion

        // SALT
        #region SALT

        public static byte[] GenerateSalt()
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[32];
            rng.GetBytes(buff);
            return buff;
        }

        public static byte[] GenerateHashWithSalt(byte[] password, byte[] salt)
        {
            //  HashAlgorithm algorithm = new SHA256Managed();
            byte[] plainTextWithSaltBytes =
                new byte[password.Length + salt.Length];

            for (int i = 0; i < password.Length; i++)
            {
                plainTextWithSaltBytes[i] = password[i];
            }
            for (int i = 0; i < salt.Length; i++)
            {
                plainTextWithSaltBytes[password.Length + i] = salt[i];
            }

            //  return algorithm.ComputeHash(plainTextWithSaltBytes);         
            return EncryptMD5(plainTextWithSaltBytes, true);
        }

        public static bool CompareHash(byte[] stored, byte[] supplied)
        {
            //First check if they are the same length or not
            if (stored.Length != supplied.Length)
            {
                return false;
            }

            //Now check if the individual bits are the same
            for (int i = 0; i < stored.Length; i++)
            {
                if (stored[i] != supplied[i])
                {
                    return false;
                }
            }


            return true;
        }


        public static string DecryptHashWithSalt(byte[] passwordwithsalt, byte[] salt)
        {
            byte[] plainTextWithSaltBytes = DecryptMD5(passwordwithsalt, true);



            byte[] plainTextPassword =
                new byte[plainTextWithSaltBytes.Length - salt.Length];

            for (int i = 0; i < plainTextPassword.Length; i++)
            {
                plainTextPassword[i] = plainTextWithSaltBytes[i];
            }
            //for (int i = 0; i < salt.Length; i++)
            //{
            //    plainTextWithSaltBytes[passwordwithsalt.Length + i] = salt[i];
            //}

            //  return algorithm.ComputeHash(plainTextWithSaltBytes);         
            return UTF8Encoding.UTF8.GetString(plainTextPassword);
        }

        #endregion
    }
}
