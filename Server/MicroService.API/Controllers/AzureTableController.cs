﻿using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Storage.Net;
using Storage.Net.Blob;
using Storage.Net.KeyValue;
using Storage.Net.Microsoft.Azure.Storage.KeyValue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers
{
    public class AzureTableController : BaseController
    {
        private string _accountName = "rentalerpstorage";
        private string _masterKey = "x4EjNrZxH+0Z/yrTk606SgOvvxwg24TG+4F1MWeTPfG4ijJSoes2R/4OmB6N7sGB0KR2GFqOBxVrMyuHPdk8Rg==";
        private readonly IHostingEnvironment _environment;
        public AzureTableController(IMicroServiceLogger Logger, IHostingEnvironment environment): base(Logger)
        {
            _environment = environment;
        }

        [HttpGet("GetTableData")]
        //[Route("api/[controller]/Download")]
        public async Task<IActionResult> GetTableData(string partitionKey, string rowKey)
        {
            //Key _key = new Key("8dcc01da-2b74-4edd-a32d-2ce9d5e7e3d3", "b7635318-6876-41e4-bea2-023ec0c6c3b2");
            Key _key = new Key(partitionKey, rowKey);
            IKeyValueStorage _storage = StorageFactory.KeyValue.AzureTableStorage(_accountName, _masterKey);
            var data = await _storage.GetAsync("CorsOrigin", _key);
            return Ok(new { Data = data });
        }

        [HttpPost("SaveTableData")]
        //[Route("api/[controller]/Download")]
        public async Task<IActionResult> SaveTableData(string domain)
        {
            try
            {
                IKeyValueStorage _storage = StorageFactory.KeyValue.AzureTableStorage(_accountName, _masterKey);
                //Value _value = new Value(Guid.NewGuid().ToString(), Guid.NewGuid().ToString());
                //_value.Add(new KeyValuePair<string, object>("Domain", domain));
                //List<Value> _valueList = new List<Value>();
                //_valueList.Add(_value);
                //await _storage.InsertAsync("CorsOrigin", _valueList);
                var _storageInsert = (IAzureTableStorageKeyValueStorageNative)_storage;
                await _storageInsert.InsertIfNotExist("CorsOrigin", "Domain", domain);
                return Ok(new { Data = "Success" });
            }
            catch (Exception ex) {
                return Ok(new { Message = ex.Message });
            }
        }
    }
}
