﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Core.WorkflowAttribute;
using MicroService.Domain.Models.Dto.CLC;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Mvc;
using Rmp.AssetMgmt.API.CustomAttribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class ApplyCLCController : BaseController
    {
        private IStudentCLCService _studentCLCService;
        public ApplyCLCController(IMicroServiceLogger Logger, IStudentCLCService studentCLCService) : base(Logger)
        {
            _studentCLCService = studentCLCService;
        }

        #region CLC APPLIED
        /// <summary>
        /// Author: Suchismita Senapati
        /// Date: 25-02-2020
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateModel]
        [Route("/api/services/app/Create")]
        public IActionResult Create([FromBody] ApplyCLCDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentCLCService.StudentCLCApplied(input),
                    Success = true,
                    Message = "CLC application submitted successfully."
                });
            });
        }
        #endregion

        #region CLC UPDATE
        /// <summary>
        /// Author: Suchismita Senapati
        /// Date: 03-03-2020
        /// Description     : This method is used to update student clc details.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("/api/services/app/Update/{id}")]
        public IActionResult Update(Guid id, [FromBody]ApplyCLCDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentCLCService.Update(id, input),
                    Success = true,
                    Message = "Application updated successfully"
                });
            });
        }
        #endregion

        #region GET EXAM FORM FILL UP DETAILS BY ID
        /// <summary>
        /// Author  :   Sitikanta Pradhan
        /// Date    :   26-Feb-19
        /// Description :   This methode is used to CLC data by id
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/GetClcDetailsById/{id}")]
        public IActionResult GetExamFormFillUpDetails(Guid id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentCLCService.GetClcDetailById(id),
                    Success = true
                });
            });
        }
		#endregion


		#region STATE CHANGE APIs FOR COLLEGE LEAVING CERTIFICATE WORKFLOW ENTITY
	
		[ActionName("College Leaving Certificate Application Status Update")]
		[BelongsToWorkflow("College Leaving Certificate Workflow")]
		[Route("/api/services/app/CollegeLeavingCertificate1/{id}/workflow-state-update")]
		[HttpPut]
		public IActionResult ForwardProcess(Guid id, [FromBody]ApplyCLCDto universityMigrationDto)
		{
			return GetHttpResponse(() =>
			{
				//var result = _studentMigrationService.UpdateMigrationApplicationStatus(id, universityMigrationDto);
				return Ok(new RequestResult
				{
					Result = null,
					Success = true
				});
			});
		}

		
		[ActionName("College Leaving Certificate Application Status Update By Principal")]
		[BelongsToWorkflow("College Leaving Certificate Workflow")]
		[Route("/api/services/app/CollegeLeavingCertificate2/{id}/workflow-state-update-principal")]
		[HttpPut]
		public IActionResult ForwardProcessByPrinicipal(Guid id, [FromBody]ApplyCLCDto universityRegDto)
		{
			return GetHttpResponse(() =>
			{
				//var result = _studentMigrationService.MigrationCertificateApplicationStatusUpdateByPrincipal(id, universityRegDto);
				return Ok(new RequestResult
				{
					Result = null,
					Success = true
				});
			});
		}




		[ActionName("College Leaving Certificate Application Status Update By Dept. Library")]
		[BelongsToWorkflow("College Leaving Certificate Workflow")]
		[Route("/api/services/app/CollegeLeavingCertificate3/{id}/workflow-state-update-principal")]
		[HttpPut]
		public IActionResult ForwardProcessByDeptLibrary(Guid id, [FromBody]ApplyCLCDto universityRegDto)
		{
			return GetHttpResponse(() =>
			{
				//var result = _studentMigrationService.MigrationCertificateApplicationStatusUpdateByPrincipal(id, universityRegDto);
				return Ok(new RequestResult
				{
					Result = null,
					Success = true
				});
			});
		}

		[ActionName("College Leaving Certificate Application Status Update By Central Library")]
		[BelongsToWorkflow("College Leaving Certificate Workflow")]
		[Route("/api/services/app/CollegeLeavingCertificate4/{id}/workflow-state-update-principal")]
		[HttpPut]
		public IActionResult ForwardProcessByCentralLibrary(Guid id, [FromBody]ApplyCLCDto universityRegDto)
		{
			return GetHttpResponse(() =>
			{
				//var result = _studentMigrationService.MigrationCertificateApplicationStatusUpdateByPrincipal(id, universityRegDto);
				return Ok(new RequestResult
				{
					Result = null,
					Success = true
				});
			});
		}



		[ActionName("College Leaving Certificate Application Status Update By Hostel Warden")]
		[BelongsToWorkflow("College Leaving Certificate Workflow")]
		[Route("/api/services/app/CollegeLeavingCertificate5/{id}/workflow-state-update-principal")]
		[HttpPut]
		public IActionResult ForwardProcessByHostelWarden(Guid id, [FromBody]ApplyCLCDto universityRegDto)
		{
			return GetHttpResponse(() =>
			{
				//var result = _studentMigrationService.MigrationCertificateApplicationStatusUpdateByPrincipal(id, universityRegDto);
				return Ok(new RequestResult
				{
					Result = null,
					Success = true
				});
			});
		}





		[ActionName("College Leaving Certificate Application Status Update By PG Council Office")]
		[BelongsToWorkflow("College Leaving Certificate Workflow")]
		[Route("/api/services/app/CollegeLeavingCertificate6/{id}/workflow-state-update-pgcounciloffice")]
		[HttpPut]
		public IActionResult ForwardProcessByPGCouncilOffice(Guid id, [FromBody]ApplyCLCDto universityRegDto)
		{
			return GetHttpResponse(() =>
			{
				//var result = _studentMigrationService.MigrationCertificateApplicationStatusUpdateByPrincipal(id, universityRegDto);
				return Ok(new RequestResult
				{
					Result = null,
					Success = true
				});
			});
		}




		[ActionName("College Leaving Certificate Application Status Reject")]
		[BelongsToWorkflow("College Leaving Certificate Workflow")]
		[Route("/api/services/app/CollegeLeavingCertificate7/{id}/workflow-state-update-reject")]
		[HttpPut]
		public IActionResult RejectProcess(Guid id, [FromBody]ApplyCLCDto universityRegDto)
		{
			return GetHttpResponse(() =>
			{
				//var result = _studentRegistrationNoService.UpdateRegistrationNumberApplicationStatusToRejectState(id, universityRegDto);
				return Ok(new RequestResult
				{
					Result = null,
					Success = true
				});
			});
		}


		#endregion


	}
}
