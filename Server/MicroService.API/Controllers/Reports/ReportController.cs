﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.BackgroundProcess;
using MicroService.Domain.Models.Dto.Feature;
using MicroService.Domain.Models.Entity;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers.Reports
{
    [Produces("application/json")]
    //[CustomAuthorize(APIId = 0)]
    public class ReportController : BaseController
    {
        private IReportService _reportService;
        public ReportController(IMicroServiceLogger Logger, IReportService reportService) : base(Logger)
        {
            _reportService = reportService;
        }

        #region CNR Report
        /// <summary>
        /// Author: Sumbul Samreen
        /// Date: 16th-Dec-2019
        /// </summary>
        /// <param name="reportParam"></param>
        /// <returns></returns>
         
        [HttpPost]
        [Route("/api/services/app/GetAllStudentReport")]
        public IActionResult GetAllStudentReport([FromBody] BackgroundProcessDto reportParam)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _reportService.GetAllStudentReport(reportParam),
                    Success = true
                });
            });
        }
        #endregion

        #region CNR Report Download
        /// <summary>
        /// Author: Sumbul Samreen
        /// Date: 16th-Dec-2019
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/services/app/DownloadCnrReport")]
        public IActionResult DownloadCnrReport([FromBody] BackgroundProcessDto param)
        {
           
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _reportService.CallSPForReportDownload(param),
                    Success = true,
                    Message = "File Download Successfully"
                });
            });
        }
       #endregion
    }

}
