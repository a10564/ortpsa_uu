﻿using MicroService.Business.Abstract;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.Tenants;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    public class TenantAppController : BaseController
    {
        ITenantAppService _tenantAppService;
        public TenantAppController(IMicroServiceLogger logger, ITenantAppService tenantAppService)
             : base(logger)
        {
            _tenantAppService = tenantAppService;
        }

        //[AllowAnonymous]
        [HttpPost]
        [Route("/api/services/app/Tenant")]
        public IActionResult Create([FromBody] CreateTenantDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _tenantAppService.CreateTenant(input),
                    Success = true,
                    Message = "Tenant created successfully"
                });
            });
        }

        //[AllowAnonymous]
        [HttpPut]
        [Route("/api/services/app/Tenant/{id}")]
        public IActionResult Update(long id,[FromBody] TenantDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _tenantAppService.UpdateTenant(id,input),
                    Success = true,
                    Message = "Tenant updated successfully"
                });
            });
        }

        //[AllowAnonymous]
        [HttpGet]
        [Route("/api/services/app/Tenant/{id}")]
        public IActionResult GetTenant(long id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _tenantAppService.GetTenantDetails(id),
                    Success = true,
                    //Message = "Tenant Created Successfully"
                });
            });
        }

        //[AllowAnonymous]
        [HttpDelete]
        [Route("/api/services/app/Tenant/{id}")]
        public IActionResult DeleteTenant(long id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _tenantAppService.DeleteTenantDetails(id),
                    Success = true,
                    Message = "Tenant deleted successfully"
                });
            });
        }

    }
}
