﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Entity;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers.OData
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class RolesController : ODataBaseController
    {
        private IRoleAppService _roleAppService;
        public RolesController(IMicroServiceLogger Logger, IRoleAppService roleAppService) : base(Logger)
        {
            _roleAppService = roleAppService;
        }

        [EnableQuery]
        [HttpGet]
        public IQueryable<Role> Get()
        {
            return _roleAppService.GetAllRolesData();
        }
    }
}
