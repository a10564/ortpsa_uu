﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers.OData
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class StudentProfileController : ODataBaseController
    {
        private IUserService _userService;
        public StudentProfileController(IMicroServiceLogger logger, IUserService userService)
             : base(logger)
        {
            _userService = userService;
        }


        [HttpGet]
        [EnableQuery]
        public IQueryable<StudentProfileView> Get()
        {
            return _userService.GetStudentProfileData();
        }
    }
}
