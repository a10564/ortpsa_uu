﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers.OData
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class DuplicateRegistrationViewController : ODataBaseController
    {
        private IDuplicateRegistrationService _duplicateRegistrationService;
        public DuplicateRegistrationViewController(IMicroServiceLogger Logger, IDuplicateRegistrationService duplicateRegistrationService) : base(Logger)
        {
            _duplicateRegistrationService = duplicateRegistrationService;
        }

        [EnableQuery]
        [HttpGet]
        public IQueryable<DuplicateRegistrationView> Get()
        {
            return _duplicateRegistrationService.GetRoleBasedActiveDuplicateRegistrationInboxData();
        }
    }
}
