﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers.OData
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class ExaminationViewController : ODataBaseController
    {
        private IExaminationFormFillUpService _examinationFormFillUpService;
        public ExaminationViewController(IMicroServiceLogger Logger, IExaminationFormFillUpService examinationFormFillUpService) : base(Logger)
        {
            _examinationFormFillUpService = examinationFormFillUpService;
        }

        [HttpGet]
        [EnableQuery]
        public IQueryable<ExaminationViewExcludeBackgroundProcessTable> Get()
        {
            return _examinationFormFillUpService.GetRoleBasedActiveExaminationInboxData();
        }
    }
}
