﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Entity;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers.OData
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class TenantsController : ODataBaseController
    {
        private ITenantAppService _tenantAppService;

        public TenantsController(IMicroServiceLogger Logger, ITenantAppService tenantAppService) : base(Logger)
        {
            _tenantAppService = tenantAppService;
        }

        [EnableQuery]
        [HttpGet]
        public IQueryable<Tenants> Get()
        {
            return _tenantAppService.GetAllTenantsData();
        }
    }
}
