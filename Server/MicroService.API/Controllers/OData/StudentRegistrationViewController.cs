﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace MicroService.API.Controllers.OData
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class StudentRegistrationViewController : ODataBaseController
    {
        private IStudentRegistrationNoService _studentRegistrationNoService;
        public StudentRegistrationViewController(IMicroServiceLogger Logger, IStudentRegistrationNoService studentRegistrationNoService) : base(Logger)
        {
            _studentRegistrationNoService = studentRegistrationNoService;
        }


        [HttpGet]
        [EnableQuery]
        public IQueryable<AcknowledgementView> Get()
        {           
            return _studentRegistrationNoService.GetRoleBasedActiveRegistrationInboxData();
        }
    }
}
