﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers.OData
{
    [Produces("application/json")]
    //[CustomAuthorize(APIId = 0)]
    public class StudentMigrationViewController : ODataBaseController
    {
        private IStudentMigrationService _studentMigrationService;
        public StudentMigrationViewController(IMicroServiceLogger Logger, IStudentMigrationService studentMigrationService) : base(Logger)
        {
            _studentMigrationService = studentMigrationService;
        }


        [HttpGet]
        [EnableQuery]
        public IQueryable<MigrationView> Get()
        {
        
            return _studentMigrationService.GetRoleBasedActiveMigrationInboxData();
        }
    }
}
