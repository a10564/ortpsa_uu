﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Entity.Lookup;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers.OData
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class CollegeOdataController : ODataBaseController
    {
        private ICommonAppService _commonAppService;
        public CollegeOdataController(IMicroServiceLogger logger, ICommonAppService commonAppService)
             : base(logger)
        {
            _commonAppService = commonAppService;
        }

        [EnableQuery]
        public IQueryable<CollegeMaster> Get()
        {
            return _commonAppService.GetAllCollege();
        }
    }
}
