﻿using MicroService.API.CustomAttribute;
using MicroService.Business;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers.OData
{

    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class MarkStatementViewODataController : ODataBaseController
    {

        private IExaminationFormFillUpService _examinationFormFillUpService;
        public MarkStatementViewODataController(IMicroServiceLogger logger, IExaminationFormFillUpService examinationFormFillUpService) 
             : base(logger)
        {
            _examinationFormFillUpService = examinationFormFillUpService;
        }

        [EnableQuery]
        public IQueryable<MarkStatementView> Get() 
        {
            return _examinationFormFillUpService.GetAllMarkDdetails();
        }
    }
}
