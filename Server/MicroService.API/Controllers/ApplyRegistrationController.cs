﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Core.WorkflowAttribute;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Dto.StudentDashboard;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rmp.AssetMgmt.API.CustomAttribute;
using System;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class ApplyRegistrationController : BaseController
    {
        private IStudentRegistrationNoService _studentRegistrationNoService;
        public ApplyRegistrationController(IMicroServiceLogger Logger, IStudentRegistrationNoService studentRegistrationNoService) : base(Logger)
        {
            _studentRegistrationNoService = studentRegistrationNoService;
        }

        #region STATE CHANGE APIs FOR STUDENT REG.NO. WORKFLOW ENTITY
        /// <summary>
        /// Author          : Dattatreya Dash
        /// Date            : 20-10-2019
        /// Description     : Normal state change api, no business involved
        /// </summary>
        /// <param name="id">Id of the university registration entity</param>
        /// <param name="universityRegDto">University registration dto</param>
        /// <returns></returns>
        [ActionName("Student Reg.No. Application Status Update")]
        [BelongsToWorkflow("Registration Number Workflow")]
        [Route("/api/services/app/StudentRegistrationNo/{id}/workflow-state-update")]
        [HttpPut]
        public IActionResult ForwardProcess(Guid id, [FromBody]UniversityRegistrationDetailDto universityRegDto)
        {
            return GetHttpResponse(() =>
            {
                var result = _studentRegistrationNoService.UpdateRegistrationNumberApplicationStatus(id, universityRegDto);
                return Ok(new RequestResult
                {
                    Result = result,
                    Success = true
                });
            });
        }

        [ActionName("Student Reg.No. Application Status Reject")]
        [BelongsToWorkflow("Registration Number Workflow")]
        [Route("/api/services/app/StudentRegistrationNo/{id}/workflow-state-update-reject")]
        [HttpPut]
        public IActionResult RejectProcess(Guid id, [FromBody]UniversityRegistrationDetailDto universityRegDto)
        {
            return GetHttpResponse(() =>
            {
                var result = _studentRegistrationNoService.UpdateRegistrationNumberApplicationStatusToRejectState(id, universityRegDto);
                return Ok(new RequestResult
                {
                    Result = result,
                    Success = true
                });
            });
        }
        #endregion

        #region STATE CHANGE APIs FOR STUDENT REG.NO. WORKFLOW ENTITY
        /// <summary>
        /// Author          : Sumbul Samreen
        /// Date            : 20-10-2019
        /// Description     : Normal state change api, no business involved
        /// </summary>
        /// <param name="id">Id of the university registration entity</param>
        /// <param name="universityRegDto">University registration dto</param>
        /// <returns></returns>
        [ActionName("Student Reg.No. Application Status Update by Comp.Cell")]
        [BelongsToWorkflow("Registration Number Workflow")]
        [Route("/api/services/app/StudentRegistrationNo/{id}/workflow-state-update-by-comp-cell")]
        [HttpPut]
        public IActionResult ForwardProcessByCompCell(Guid id, [FromBody]UniversityRegistrationDetailDto universityRegDto)
        {
            return GetHttpResponse(() =>
            {
                var result = _studentRegistrationNoService.UpdateRegistrationNumberApplicationStatusByCompCell(id, universityRegDto);
                return Ok(new RequestResult
                {
                    Result = result,
                    Success = true
                });
            });
        }
        // Don't delete
        [ActionName("Student Reg.No. Application Status Update By Computer Cell")]
        [BelongsToWorkflow("Registration Number Workflow")]
        [Route("/api/services/app/StudentRegistrationNo/{id}/workflow-state-update-comp-cell")]
        [HttpPut]
        public IActionResult ForwardProcessByComputerCell(Guid id, [FromBody]UniversityRegistrationDetailDto universityRegDto)
        {
            return GetHttpResponse(() =>
            {
                var result = _studentRegistrationNoService.UpdateRegistrationNumberApplicationStatus(id, universityRegDto);
                return Ok(new RequestResult
                {
                    Result = result,
                    Success = true
                });
            });
        }

        /// <summary>
        /// Author          : Dattatreya Dash
        /// Date            : 01-10-2019
        /// Description     : State change action triggered by Principal. Logic involved. Before 48hours of last principal update, state can't be changed
        /// </summary>
        /// <param name="id">Id of the university registration entity</param>
        /// <param name="universityRegDto">University registration dto</param>
        /// <returns></returns>
        [ActionName("Student Reg.No. Application Status Update By Principal")]
        [BelongsToWorkflow("Registration Number Workflow")]
        [Route("/api/services/app/StudentRegistrationNo/{id}/workflow-state-update-principal")]
        [HttpPut]
        public IActionResult ForwardProcessByPrinicipal(Guid id, [FromBody]UniversityRegistrationDetailDto universityRegDto)
        {
            return GetHttpResponse(() =>
            {
                var result = _studentRegistrationNoService.RegistrationNumberApplicationStatusUpdateByPrincipal(id, universityRegDto);
                return Ok(new RequestResult
                {
                    Result = result,
                    Success = true
                });
            });
        }
        #endregion

        #region GET DOCUMENT DETAILS BY PARENT SOURCE ID
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 17-oct-2019
        /// Get document details of given parentsourceid using universityregistrationdetail entity
        /// </summary>
        /// <param name="parentSourceId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/ApplyRegistrationNo/GetDocumentDetailsByParentSourceId/{parentSourceId}")]
        public IActionResult GetDocumentDetailsByParentSourceId(Guid parentSourceId)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentRegistrationNoService.GetDocumentDetailsByParentSourceId(parentSourceId),
                    Success = true,
                    Message = ""
                });
            });
        }
        #endregion

        #region REGISTRATION NUMBER APPLICATION SUBMIT
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 17-oct-2019
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateModel]
        [Route("/api/services/app/UniversityRegistrationNumber")]
        public IActionResult UniversityRegistrationNumber([FromBody]UniversityRegistrationDetailDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentRegistrationNoService.StudentUniversityRegistrationNumberApply(input),
                    Success = true,
                    Message = "Registration no. application submitted successfully."
                });
            });
        }
        #endregion

        #region GET STUDENT REGISTRATION DETAILS BY ID THROUGH UNIVERSITY REGISTRATION ENTITY
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 17-oct-2019
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/UniversityRegistrationNumber/{id}")]
        public IActionResult UniversityRegistrationNumber(Guid id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentRegistrationNoService.GetStudentUniversityRegistrationDetailsById(id),
                    Success = true
                });
            });
        }
        #endregion

        #region REGISTRATION NUMBER APPLICATION UPDATE
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 17-oct-2019
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut]
        [ValidateModel]
        [Route("/api/services/app/UniversityRegistrationNumber/{id}")]
        public IActionResult UniversityRegistrationNumber(Guid id, [FromBody]UniversityRegistrationDetailDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentRegistrationNoService.GetStudentUniversityRegistrationDetailsById(id, input),
                    Success = true,
                    Message = "Application updated successfully"
                });
            });
        }
        #endregion

        #region DOWNLOAD STUDENT DOCUMENT
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 21-oct-2019
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        /// this method is not used our client side (if this method is used, so except student)
        [HttpGet]
        [Route("/api/services/app/DownloadStudentDocument/{id}")]
        public IActionResult DownloadStudentDocument(Guid id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentRegistrationNoService.DownloadStudentDocument(id),
                    Success = true,
                    Message = "File downloaded successfully"
                });
            });
        }
        #endregion

        #region REGISTRATION NUMBER PREVIEW
        /// <summary>
        /// Author:Sitikanta Pradhan
        /// Date:24-oct-2019
        /// Get student details on given id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/RegistrationNumberPreview/{id}")]
        public IActionResult RegistrationNumberPreview(Guid id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentRegistrationNoService.RegistrationNumberPreview(id),
                    Success = true
                });
            });
        }
        #endregion

        #region UPDATE REGISTRATION NUMBER IN UNIVERSITY REGISTRATION DETAIL TABLE
        /// <summary>
        /// Author:Sitikanta Pradhan
        /// Date:24-oct-2019
        /// update registration number on university registration details table in database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("/api/services/app/UniversityRegistrationNumber/UpdateRegistrationNumberPreview/{id}")]
        public IActionResult GenerateRegistrationNumber(Guid id,[FromBody] UniversityRegistrationDetailDto param)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentRegistrationNoService.UpdateGenerateRegistrationNumber(id, param),
                    Success = true
                });
            });
        }
        #endregion

        #region UPDATE REGISTRATION NUMBER IN UNIVERSITY REGISTRATION DETAIL TABLE
        /// <summary>
        /// Author:Sitikanta Pradhan
        /// Date:24-oct-2019
        /// update registration number on university registration details table in database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("/api/services/app/UniversityRegistrationNumber/ConfirmRegistrationNumberPreview/{id}")]
        public IActionResult ConfirmRegistrationNumberPreview(Guid id, [FromBody] UniversityRegistrationDetailDto param)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentRegistrationNoService.ConfirmRegistrationNumberPreview(id, param),
                    Success = true
                });
            });
        }
        #endregion

        #region DOWNLOAD STUDENT DOCUMENT (THIS METHOD USING DOCUMENTS ENTITY)
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 31-oct-2019
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/DownloadStudentDocumentNew/{id}")]
        public IActionResult DownloadStudentDocumentNew(Guid id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentRegistrationNoService.DownloadStudentDocumentNew(id),
                    Success = true,
                    Message = "File Download Successfully"
                });
            });
        }
		#endregion
			   
		#region Get Role wise State List
		/// <summary>
		/// Author:Sonymon 
		/// Date:24-oct-2019
		/// Get student details on given id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[HttpGet]
		[Route("/api/services/app/GetRoleSpecificWorkflowStateList")]
		public IActionResult GetRoleSpecificWorkflowStateList()
		{
			return GetHttpResponse(() =>
			{
				return Ok(new RequestResult
				{
					Result = _studentRegistrationNoService.GetRoleSpecificWorkflowStateList(),
					Success = true
				});
			});
		}
        #endregion

        #region DOWNLOAD REG NO FOR PRINCIPAL
        /// <summary>
        /// Author          : Anurag Digal
        /// Date            : 10-02-2020
        /// Description     : Download Registration card for principal
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/services/app/Registration/DownloadRegistrationNo")]
        public IActionResult DownloadRegistrationNo([FromBody] certificateDownloadDto obj)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentRegistrationNoService.DownloadRegistrationNo(obj),
                    Success = true,
                    Message = "Registration Card downloaded successfully"
                });
            });
        }
        #endregion

    }
}