﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Core.WorkflowAttribute;
using MicroService.Core.WorkflowEntity;
using MicroService.Core.WorkflowService;
using MicroService.Domain.Models.Dto.AdminActivity;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class CommonController : BaseController
    {
        ICommonAppService _commonSrvc;
        private readonly IApiDescriptionGroupCollectionProvider _apiExplorer;
        IWorkflowAppService workflowService;
        IReportService _reportService;

        public CommonController(IMicroServiceLogger logger, ICommonAppService commonSrvc, IWorkflowAppService workflowSrvc, IApiDescriptionGroupCollectionProvider apiExplorer
            , IReportService reportService)
             : base(logger)
        {
            _commonSrvc = commonSrvc;
            _apiExplorer = apiExplorer;
            workflowService = workflowSrvc;
            _reportService = reportService;
        }

        [HttpGet]
        [Route("app/services/apiexplorer/workflowapis")]
        public IActionResult WorkflowApis(string workflowName = "")
        {
            return GetHttpResponse(() =>
            {
                var result = _apiExplorer.ApiDescriptionGroups.Items.ToList();
                var apiList = new List<APIMetadata>();
                foreach (var group in result)
                {
                    foreach (var method in group.Items)
                    {
                        var methodInfo = (MethodInfo)method.ActionDescriptor
                        .GetType()
                        .GetProperty("MethodInfo")
                        .GetValue(method.ActionDescriptor);

                        var customAttribute = methodInfo.GetCustomAttribute<BelongsToWorkflow>();

                        if (customAttribute != null)
                        {
                            if (string.IsNullOrWhiteSpace(workflowName))
                            {
                                apiList.Add(new APIMetadata
                                {
                                    Method = method.HttpMethod.ToString(),
                                    FriendlyName = method.ActionDescriptor.GetType().GetProperty("ActionName").GetValue(method.ActionDescriptor).ToString(),
                                    RelativePath = method.RelativePath,
                                    MethodGroupName = group.GroupName,
                                    ReturnType = method.SupportedResponseTypes != null && method.SupportedResponseTypes.Count > 0 ? method.SupportedResponseTypes.First().Type.FullName : "",
                                    Parameters = GetParameterDescription(method.ParameterDescriptions)
                                });
                            }
                            else
                            {
                                if (workflowName.ToUpper() == customAttribute.GetWorkflowName().ToUpper())
                                {
                                    apiList.Add(new APIMetadata
                                    {
                                        Method = method.HttpMethod.ToString(),
                                        FriendlyName = method.ActionDescriptor.GetType().GetProperty("ActionName").GetValue(method.ActionDescriptor).ToString(),
                                        RelativePath = method.RelativePath,
                                        MethodGroupName = group.GroupName,
                                        ReturnType = method.SupportedResponseTypes != null && method.SupportedResponseTypes.Count > 0 ? method.SupportedResponseTypes.First().Type.FullName : "",
                                        Parameters = GetParameterDescription(method.ParameterDescriptions)
                                    });
                                }
                            }
                        }
                    }
                }
                return Ok(new RequestResult
                {
                    Result = apiList,
                    Success = true,
                    Message = ""
                });
            });
        }

        private List<APIParamMetadata> GetParameterDescription(IList<ApiParameterDescription> apiParams)
        {
            var paramsMetadata = new List<APIParamMetadata>();
            if (apiParams != null && apiParams.Count > 0)
            {
                foreach (var param in apiParams)
                {
                    paramsMetadata.Add(new APIParamMetadata
                    {
                        Name = param.Name,
                        Type = param.Type?.Name,
                        ParameterSource = param.Source.DisplayName
                    });
                }
            }
            return paramsMetadata;
        }

        #region POSSIBLE TRANSITION FOR A WORKFLOW ENTITY( Filtered by logged in user's role)
        [HttpGet]
        [Route("api/services/app/tenantworkflows/{tenantWorkflowId}/workflowinstances/{workflowInstanceId}/workflowTransitions/{status}")]
        public IActionResult GetPossibleTransitionsForWorkflowInstance(int tenantWorkflowId, int workflowInstanceId, string status)
        {
            return GetHttpResponse(() =>
            {
                var result = workflowService.GetPossibleTransitionsForWorkflowInstance(tenantWorkflowId, workflowInstanceId, status);
                return Ok(new RequestResult
                {
                    Result = result,
                    Success = true
                });
            });
        }
        #endregion

        #region POSSIBLE FORM FIELDS FOR A TRIGGER( Filtered by logged in user's role)
        /// <summary>
        /// This method is used to get the workflow transition object which contains the possible form fields required for a trigger
        /// </summary>
        /// <param name="transitionId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/services/app/workflowTransitions/{transitionId}")]
        public IActionResult GetWorkflowTransition(long transitionId)
        {
            return GetHttpResponse(() =>
            {
                var result = workflowService.GetWorkflowTransition(transitionId);
                return Ok(new RequestResult
                {
                    Result = result,
                    Success = true
                });
            });
        }
        #endregion

        #region GET CURRENT DATE
        [HttpGet]
        [Route("api/services/app/GetCurrentDate")]
        public IActionResult GetCurrentDate()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = DateTime.Now,
                    Success = true
                });
            });
        }
        #endregion

        #region GET COLLEGE DETAILS
        [HttpGet]
        [Route("api/services/app/GetCollegeDetails/{courseTypeCode}")]
        public IActionResult GetCollegeDetails(string courseTypeCode)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _commonSrvc.GetCollegeDetails(courseTypeCode),
                    Success = true
                });
            });
        }
        #endregion

        #region GET DEPARTMENT DETAILS
        [HttpGet]
        [Route("api/services/app/GetDepartmentDetails/{streamCode}/{courseCode}")]
        public IActionResult GetDepartmentDetails(string streamCode,string courseCode)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _commonSrvc.GetDepartmentDetails(streamCode,courseCode),
                    Success = true
                });
            });
        }
        #endregion

        #region GET SUBJECT DETAILS
        [HttpGet]
        [Route("api/services/app/GetSubjectDetails")]
        public IActionResult GetSubjectDetails(string filterCode, string departmentCode)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _commonSrvc.GetSubjectDetails(filterCode, departmentCode),
                    Success = true
                });
            });
        }
        #endregion

        #region GET PAPER DETAILS
        [HttpGet]
        [Route("api/services/app/GetPaperDetails/{courseType}/{courseCode}/{streamCode}/{departmentCode}/{subjectCode}/{semesterCode}")]
        public IActionResult GetPaperDetails(string courseType, string courseCode, string streamCode, string departmentCode, string subjectCode, string semesterCode)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _commonSrvc.GetPaperDetails(courseType,courseCode,streamCode,departmentCode,subjectCode,semesterCode),
                    Success = true
                });
            });
        }
        #endregion

        #region GET PROCESS TRACKER STATUS
        /// <summary>
        /// Author      :Anurag Digal
        /// Date        :19-11-2019
        /// This method is used to check whether is process completed or pending by providing process type
        /// </summary>
        /// <param name="processType"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/services/app/GetProcessType/{processType}/{serviceType}")]
        public IActionResult GetProcessType(long processType,long serviceType)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _commonSrvc.GetProcessType(processType, serviceType),
                    Success = true
                });
            });
        }
        #endregion

        #region GET COLLEGE DETAILS - Coursetype and Streamcode
        [HttpGet]
        [Route("api/services/app/GetCollegeDetails/{courseTypeCode}/{streamCode}/{subjectCode}")]
        public IActionResult GetCollegeDetails(string courseTypeCode, string streamCode, string subjectCode)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _commonSrvc.GetCollegeDetails(courseTypeCode, streamCode, subjectCode),
                    Success = true
                });
            });
        }
        #endregion

        #region Get college Code
        /// <summary>
        /// Author  :   Sitikanta Pradhan
        /// Date    :   17-Feb-19
        /// Description :   Get user role wise college
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/GetUserRoleWiseCollege/{roleId}")]
        public IActionResult GetUserRoleWiseCollege(long roleId)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _commonSrvc.GetUserRoleWiseCollege(roleId),
                    Success = true
                });
            });
        }
        #endregion

        #region Get Account Statement
        /// <summary>
        /// Author  :   Biswaranjan Ghadai
        /// Date    :   16-Sept-21
        /// Description :   Download account statement
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        [Route("/api/services/app/DownloadAccountStatement")]
        public IActionResult DownloadStatement([FromBody] SearchParamDto searchvalue)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _reportService.DownloadStatement(searchvalue),
                    Success = true,
                    Message= "Document download successfully"
                });
            });
        }
        #endregion

        #region Get college Code
        /// <summary>
        /// Author  :   Satya Prakash Sahoo
        /// Date    :   17-Dec-21
        /// Description :   Get session List By Subject Code
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        [Route("/api/services/app/GetAllSemesterBySubject/{subjectCode}")]
        public IActionResult GetAllSemesterListBySubject(string subjectCode)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _commonSrvc.GetSessionListBySubject(subjectCode),
                    Success = true,
                    Message = "Document download successfully"
                });
            });
        }
        #endregion

        #region Get Account Statement
        /// <summary>
        /// Author  :   Biswaranjan Ghadai
        /// Date    :   16-Sept-21
        /// Description :   Download account statement
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        [Route("/api/services/app/Common/GetAllCollegesBySubject/{courseTypeCode}/{streamCode}/{subjectCode}")]
        public IActionResult GetAllCollegesBySubject(string courseTypeCode, string streamCode, string subjectCode)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _commonSrvc.GetCollegeMasterBySubject(courseTypeCode, streamCode, subjectCode),
                    Success = true,
                    Message = ""
                });
            });
        }
        #endregion

        #region Get Role User Wise Subject
        /// <summary>
        /// Author  :   Bikash kumar
        /// Date    :   4-3-22
        /// Description :   role user wise subject
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        [Route("/api/services/app/Common/GetRoleUserWiseSubject")]
        public IActionResult GetRoleUserWiseSubject()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _commonSrvc.GetRoleUserWiseSubject(),
                    Success = true,
                    Message = ""
                });
            });
        }
        #endregion

        #region Get College wise Subject details 
        /// <summary>
        /// Author  :   Bikash kumar
        /// Date    :   4-3-22
        /// Description :   role user wise subject
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        [Route("/api/services/app/Common/GetCollegeWiseServiceDetails/{CollegeCode}")]
        public IActionResult GetCollegeWiseServiceDetails(string CollegeCode)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _commonSrvc.GetCollegeWiseServiceDetails(CollegeCode), 
                    Success = true,
                    Message = ""
                });
            });
        }
        #endregion
    }
}





