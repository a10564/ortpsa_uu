﻿using Microservice.Common;
using MicroService.API.Provider.JWT;
using MicroService.Business.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.User;
using MicroService.Domain.Models.Entity;
using MicroService.SharedKernel;
using MicroService.Utility.Common;
using MicroService.Utility.Exception;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    public class TokenAuthController : BaseController
    {
        private IMicroServiceUOW _MSUoW;
        IConfiguration _iconfiguration;
        private readonly IDistributedCache _cacheMemory;
        private readonly IHttpContextAccessor _context;
        private IUserService _user;

        public TokenAuthController(IMicroServiceLogger logger, IDistributedCache redisCache, IUserService user,
            IMicroServiceUOW MSUoW, IConfiguration iconfiguration, IHttpContextAccessor context)
             : base(logger)
        {
            _MSUoW = MSUoW;
            _iconfiguration = iconfiguration;
            _cacheMemory = redisCache;
            _context = context;
            _user = user;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("/api/TokenAuth/Authenticate")]
        public IActionResult Authenticate([FromBody] AuthenticateModelDto model)
        {
            return GetHttpResponse(() =>
            {
                var loginResult = _user.AuthenticateUser(model);

                List<string> geographyDetailsId = new List<string>();
                if (loginResult.UserRoleGeographyMappers != null && loginResult.UserRoleGeographyMappers.Count > 0)
                {
                    geographyDetailsId = loginResult.UserRoleGeographyMappers
                                                         .Select(t => Convert.ToString(t.GeographyDetailsId))
                                                         .Distinct()
                                                         .ToList<string>();
                }

                List<string> roleIds = new List<string>();

                if (loginResult.RoleIds != null && loginResult.RoleIds.Length > 0)
                {
                    roleIds = loginResult.RoleIds.Select(t => Convert.ToString(t))
                                                        .Distinct()
                                                        .ToList<string>();
                }

                List<string> roleFeatureApiIds = new List<string>();

                if (loginResult.RoleFeatureApiIds != null && loginResult.RoleFeatureApiIds.Length > 0)
                {
                    roleFeatureApiIds = loginResult.RoleFeatureApiIds.Select(t => Convert.ToString(t))
                                                        .Distinct()
                                                        .ToList<string>();
                }

                var accessToken = new JwtTokenBuilder()
                                        .AddSecurityKey(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_iconfiguration.GetSection("SecurityKey").ToString())))
                                        .AddSubject(loginResult.UserName)
                                        .AddIssuer("RentalERP.Security.Bearer")
                                        .AddAudience("RentalERP.Security.Bearer")
                                        .AddClaim("Id", Convert.ToString(loginResult.Id))
                                        .AddClaim("TenantId", Convert.ToString(loginResult.TenantId))
                                        .AddClaim("IsLoggedIn", Convert.ToString(true))
                                        .AddClaim("IsAdmin", Convert.ToString(loginResult.IsAdmin))
                                        .AddClaim("UserId", Convert.ToString(loginResult.Id))
                                        .AddClaim("EmailId", Convert.ToString(loginResult.EmailAddress))
                                        .AddClaim("Geography", String.Join(",", geographyDetailsId))
                                        .AddClaim("RoleIds", String.Join(",", roleIds))
                                        .AddClaim("RoleFeatureApiIds", String.Join(",", roleFeatureApiIds))
                                        .AddClaim("CollegeCode", loginResult.CollegeCode??"")
                                        .AddClaim("UserType", Convert.ToString(loginResult.UserType))
                                        .AddClaim("DepartmentCode", loginResult.DepartmentCode ?? "")
                                        .AddClaim("DeviceType", loginResult.DeviceType)
                                        //.AddClaim("IsAffiliated", Convert.ToString(loginResult.IsAffiliated))
                                        .AddExpiry(24 * 60)    //24hr * 60 minute
                                        .Build();

                var result = new AuthenticateResultDto
                {
                    AccessToken = Convert.ToString(accessToken.Value),
                    EncryptedAccessToken = GetEncrpyedAccessToken(accessToken), // FOR RETURNING ENCRYPTED ACCESSTOKEN CHANGES FROM BYTE ARRAY TO STRING
                    ExpireInSeconds = (24 * 60),
                    UserId = loginResult.Id,
                    TenantId = Convert.ToInt32(loginResult.TenantId)
                };

                return Ok(new RequestResult
                {
                    Result = result,
                    Success = true,
                    Message = "Authenticate Successfully"
                });
            });
        }


        private string GetEncrpyedAccessToken(JwtToken accessToken)
        {
            byte[] b = ASCIIEncoding.ASCII.GetBytes(Convert.ToString(accessToken));
            string encrypted = Convert.ToBase64String(b);
            return encrypted;
        }



        [Route("/api/TokenAuth/LogOut")]
        [HttpGet]
        public IActionResult LogOut()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = LogOutUser(),
                    Success = true,
                    Message = "Logout Successful"
                });
            });
        }

        private bool LogOutUser()
        {
            try
            {
                var authorization = _context.HttpContext.Request.Headers["Authorization"];
                if (!StringValues.IsNullOrEmpty(authorization))
                {
                    string authTokenKey = Convert.ToString(authorization).Replace("Bearer", "").Trim();
                    _cacheMemory.Remove("urn:Users:" + authTokenKey);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new MicroServiceException { ErrorCode = "OUU001" };
            }
        }

        /// <summary>
        /// Modified -Anurag Digal
        /// Updated Date - 13-12-2019
        /// Description -  This methode is updated to async call for sending OTP to user by SMS
        /// </summary>
        /// <param name="otp"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("/api/TokenAuth/GenerateOtp")]
        [HttpPost]
        public IActionResult GenerateOtpForUserRegistration([FromBody] UserOtpDto otp)
        {
            return GetHttpResponse(() =>
            {
                _user.GenerateAndSendOtp(otp);
                return Ok(new RequestResult
                {
                    Result = EnOperationStatus.SUCCESS.ToString(),
                    Success = true,
                    Message = "OTP has been sent to your Email Id and Phone No."
                });
            });
        }

        [AllowAnonymous]
        [Route("/api/TokenAuth/GetGeneratedOtp")]
        [HttpPost]
        public IActionResult GetGeneratedOtpForUserRegistration([FromBody] UserOtpDto otp)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _user.GetGeneratedOtp(otp.UnregisterdEmailId, otp.UnregisterdMobileNumber),
                    Success = true,
                    Message = "Latest OTP for provided email-id and  mobile number."
                });
            });
        }
    }
}
