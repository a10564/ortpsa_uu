﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Dto.DuplicateRegistrationDetail;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class DuplicateRegistrationController : BaseController
    {
        private IDuplicateRegistrationService _duplicateRegistrationService;
        public DuplicateRegistrationController(IMicroServiceLogger logger, IDuplicateRegistrationService duplicateRegistrationService)
             : base(logger)
        {
            _duplicateRegistrationService = duplicateRegistrationService;
        }

        /// <summary>
        /// Author  :       Anurag Digal
        /// Date    :       07-11-19
        /// Description :   This methode is used to call the GetRegistrationDetail from duplicateRegistrationService
        /// </summary>
        /// <param name="regNo"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/DuplicateRegistration/GetRegistrationDetail/{regNo}")]
        public IActionResult GetRegistrationDetail(string regNo)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _duplicateRegistrationService.GetRegistrationDetail(regNo),
                    Success = true
                });
            });
        }


        /// <summary>
        /// Author  :       Anurag Digal
        /// Date    :       06-11-2019
        /// Description :   This methode is used to call the DuplicateRegistrationDetailApply from duplicateRegistrationService
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/services/app/DuplicateRegistration/DuplicateRegistrationDetail")]
        public IActionResult DuplicateRegistrationDetail([FromBody] DuplicateRegistrationDetailDto param)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _duplicateRegistrationService.DuplicateRegistrationDetailApply(param),
                    Success = true,
                    Message = "Duplicate Registration Apply Successfully"
                });
            });
        }


        /// <summary>
        /// Author  :       Anurag Digal
        /// Date    :       06-11-2019
        /// Description :   This methode is used to call the DuplicateRegistrationDetailApply from duplicateRegistrationService
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/DuplicateRegistration/GetDuplicateRegistrationDetail/{id}")]
        public IActionResult GetDuplicateRegistrationDetail(Guid id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _duplicateRegistrationService.GetDuplicateRegistrationDetail(id),
                    Success = true
                });
            });
        }

    }
}
