﻿using MicroService.Business.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.Feature;
using MicroService.Domain.Models.Entity;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    public class FeatureAppController : BaseController
    {
        private IMicroServiceUOW _MSUoW;
        private IFeatureAppService _featureAppService;

        public FeatureAppController(IMicroServiceLogger logger, IMicroServiceUOW MSUoW, IFeatureAppService featureAppService)
              : base(logger)
        {
            _MSUoW = MSUoW;
            _featureAppService = featureAppService;
        }

        #region USER AUTHORIZATION
        [HttpGet]
        [Route("/api/services/app/UserAuthorizationDetails")]
        public IActionResult UserAuthorizedDetails()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _featureAppService.GetUserAuthorizedDetails(),
                    Success = true
                });
            });
        }
        #endregion

        #region FEATURE SECTION
        [HttpPost]
        [Route("/api/services/app/Feature")]
        public IActionResult CreateFeature([FromBody] FeatureCreateDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _featureAppService.CreateFeature(input),
                    Success = true
                });
            });
        }

        [HttpPut]
        [Route("/api/services/app/Feature/{id}")]
        public IActionResult UpdateFeature(long id,[FromBody] FeatureDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _featureAppService.UpdateFeature(id,input),
                    Success = true
                });
            });
        }

        [HttpGet]
        [Route("/api/services/app/Feature/{id}")]
        public IActionResult GetFeature(long id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _featureAppService.GetFeature(id),
                    Success = true
                });
            });
        }

        [HttpDelete]
        [Route("/api/services/app/Feature/{id}")]
        public IActionResult DeleteFeature(long id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _featureAppService.DeleteFeature(id),
                    Success = true
                });
            });
        }

        [HttpGet]
        [Route("/api/services/app/Feature")]
        public IActionResult GetAllFeature()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _featureAppService.GetAllFeature(),
                    Success = true
                });
            });
        }
        #endregion

        #region FEATUREORDER
        [HttpPost]
        [Route("/api/services/app/FeatureOrder")]
        public IActionResult CreateFeatureOrder([FromBody] FeatureOrderCreateDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _featureAppService.CreateFeatureOrder(input),
                    Success = true
                });
            });
        }

        [HttpPut]
        [Route("/api/services/app/FeatureOrder/{id}")]
        public IActionResult UpdateFeatureOrder(long id,[FromBody] FeatureOrderDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _featureAppService.UpdateFeatureOrder(id,input),
                    Success = true
                });
            });
        }

        [HttpGet]
        [Route("/api/services/app/FeatureOrder/{id}")]
        public IActionResult GetFeatureOrder(long id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _featureAppService.GetFeatureOrder(id),
                    Success = true
                });
            });
        }

        [HttpDelete]
        [Route("/api/services/app/FeatureOrder/{id}")]
        public IActionResult DeleteFeatureOrder(long id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _featureAppService.DeleteFeatureOrder(id),
                    Success = true
                });
            });
        }

        [HttpGet]
        [Route("/api/services/app/FeatureOrder")]
        public IActionResult GetAllFeatureOrder()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _featureAppService.GetAllFeatureOrder(),
                    Success = true
                });
            });
        }

        [HttpGet]
        [Route("/api/services/app/{orderId}/Feature")]
        public IActionResult GetAllFeatureByOrderId(long orderId)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _featureAppService.GetAllFeatureByOrderId(orderId),
                    Success = true
                });
            });
        }

        #endregion

        #region API Details
        [HttpPost]
        [Route("/api/services/app/ApiDetail")]
        public IActionResult CreateApiDetail([FromBody] ApiDetailsDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _featureAppService.CreateApiDetail(input),
                    Success = true
                });
            });
        }

        [HttpGet]
        [Route("/api/services/app/ApiDetails")]
        public IActionResult GetApiDetailsList()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _featureAppService.GetApiDetailsList(),
                    Success = true
                });
            });
        }

        [HttpGet]
        [Route("/api/services/app/FeatureApiMappers")]
        public IActionResult GetFeatureApiMapperList()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _featureAppService.GetFeatureApiMapperList(),
                    Success = true
                });
            });
        }

        #endregion
    }
}
