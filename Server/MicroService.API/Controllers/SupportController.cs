﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.AdminActivity;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.SharedKernel;
using MicroService.Utility.Exception;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using TimeZoneConverter;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class SupportController : BaseController
    {
        private ISupportClass _support;
        private ICommonAppService _commonAppService;
        public SupportController(IMicroServiceLogger logger, ISupportClass support, ICommonAppService commonAppService) : base(logger)
        {
            _support = support;
            _commonAppService = commonAppService;
        }

        /// <summary>
        /// Author  :       Bikash ku sethi
        /// Date    :      14-09-2021
        /// Description :   This methode is used to get all payable service details
        /// </summary>
        /// <param name="regNo"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        [Route("/api/services/app/Support/GetAllServiceDetails")]
        public IActionResult GetAllServiceDetails() 
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _support.GetAllServiceDetails(),
                    Success = true,
                    Message = ""
                });
            });
        }

        /// <summary>
        /// Author  :       Bikash ku sethi
        /// Date    :      14-09-2021
        /// Description :   This methode is used to get all college master details from cache
        /// </summary>
        /// <param name="regNo"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        [Route("/api/services/app/Support/GetAllCollegeMaster")]
        public IActionResult GetAllCollegeMaster()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _support.GetAllCollegeListData(),
                    Success = true,
                    Message = ""
                });
            });
        } 

        /// <summary>
        /// Author  :       Anurag Digal
        /// Date    :      18-09-2021
        /// Description :   This methode is used to get all college wise applicants payment details report
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/services/app/Support/GetAllCollegeWiseCandidateFeeReport")]
        public IActionResult GetAllCollegeWiseCandidateFeeReport([FromBody] SearchParamDto param)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _support.GetAllCollegeWiseCandidateFeeReport(param),
                    Success = true,
                    Message = ""
                });
            });
        }

        #region Download report statement in excel for Level1 role
        [HttpPost]
        [Route("/api/services/app/Support/Level1DownloadExcelReport")]
        public IActionResult Level1DownloadExcelReport([FromBody] SearchParamDto param)
        {
            if(param==null)
            {
                throw new MicroServiceException() { ErrorMessage = "Invalid data received." };
            }
            DataTable dataTable = _support.GetReportsCollegeWise(param);
            string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";            

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            if (dataTable.Rows.Count > 0)
            {
                using (var package = new ExcelPackage())
                {
                    var worksheet = package.Workbook.Worksheets.Add("Transactions");
                    worksheet.Cells["A1"].LoadFromDataTable(dataTable, PrintHeaders: true);
                    worksheet.Cells["A1:I1"].Style.Font.Bold = true;
                    for (var col = 1; col < dataTable.Columns.Count + 1; col++)
                    {
                        worksheet.Column(col).AutoFit();
                    }
                    //return File(package.GetAsByteArray(), XlsxContentType, "AccountStatementReport.xlsx");
                    return GetHttpResponse(() =>
                    {
                        return Ok(new RequestResult
                        {
                            Result = File(package.GetAsByteArray(), XlsxContentType, dataTable.TableName+".xlsx"),
                            Success = true,
                            Message = "File downloaded successfully"
                        });
                    });
                }
            }
            else
            {
                return GetHttpResponse(() =>
                {
                    return Ok(new RequestResult
                    {
                        Result = null,
                        Success = false,
                        Message = "No record found."
                    });
                });
            }

            
        }
        #endregion
    }
}
