﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Core.WorkflowAttribute;
using MicroService.Domain.Models.Dto.Nursing;
using MicroService.SharedKernel;
using MicroService.Domain.Models.Dto.Nursing;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Mvc;
using Rmp.AssetMgmt.API.CustomAttribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroService.Domain.Models.Dto.BackgroundProcess;
using MicroService.Utility.Common;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class NursingFormFillUpController : BaseController
    {
        private INursingFormFillUpService _nursingFormFillUpService;
        public NursingFormFillUpController(IMicroServiceLogger logger, INursingFormFillUpService nursingFormFillUpService)
             : base(logger)
        {
            _nursingFormFillUpService = nursingFormFillUpService;
        }


        #region STATE CHANGE APIs FOR STUDENT NURSING FORM FILLUP WORKFLOW WORKFLOW ENTITY
        /// <summary>
        /// Author          : Anurag Digal
        /// Date            : 08-12-2020
        /// Description     : Principal verifies examination form details
        /// </summary>
        /// <param name="id">Id of the examination form entity</param>
        /// <param name="examFormDto">Examination form dto</param>
        /// <returns></returns>
        [ActionName("Principal Nursing Form Fillup Verification Status Update")]
        [BelongsToWorkflow("Nursing Form Fillup Workflow")]
        [Route("/api/services/app/NursingFormFillUp/{id}/workflow-principal-verification")]
        [HttpPut]
        public IActionResult PrincipalVerification(Guid id, [FromBody] NursingFormFillUpDto examFormDto)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _nursingFormFillUpService.VerificationByPrincipal(id, examFormDto),
                    Success = true
                });
            });
        }

        /// <summary>
        /// Author          : Anurag Digal
        /// Date            : 08-12-2020
        /// Description     : Principal rejects examination form details
        /// </summary>
        /// <param name="id">Id of the examination form entity</param>
        /// <param name="examFormDto">Examination form dto</param>
        /// <returns></returns>
        [ActionName("Principal Nursing Form Fillup Rejection Status Update")]
        [BelongsToWorkflow("Nursing Form Fillup Workflow")]
        [Route("/api/services/app/NursingFormFillUp/{id}/workflow-principal-rejection")]
        [HttpPut]
        public IActionResult PrincipalReject(Guid id, [FromBody] NursingFormFillUpDto examFormDto)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _nursingFormFillUpService.RejectedByPrincipal(id, examFormDto),
                    Success = true
                });
            });
        }

        /// <summary>
        /// Author          : Anurag Digal
        /// Date            : 08-12-2020
        /// Description     : Principal verifies examination form details
        /// </summary>
        /// <param name="id">Id of the examination form entity</param>
        /// <param name="examFormDto">Examination form dto</param>
        /// <returns></returns>
        [ActionName("Nursing Form Fillup Status Update")]
        [BelongsToWorkflow("Nursing Form Fillup Workflow")]
        [Route("/api/services/app/NursingFormFillUp/{id}/workflow-status-update")]
        [HttpPut]
        public IActionResult UpdateState(Guid id, [FromBody] NursingFormFillUpDto examFormDto)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _nursingFormFillUpService.UpdateState(id, examFormDto),
                    Success = true
                });
            });
        }
        #endregion

        /// <summary>
        /// Author  :   Biaksh kumar sethi
        /// Date    :  07=12-2021
        /// Description :   This methode is used to call nursingformfillupservice to insert data
        /// </summary>
        /// <param name="regNo"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateModel]
        [Route("/api/services/app/NursingFormFillUp/NursingFormFillUpDetails")]
        public IActionResult ExamFormFillUpDetails([FromBody] NursingFormFillUpDto param)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _nursingFormFillUpService.ApplyNursingFormFillUp(param),
                    Success = true,
                    Message = "Nursing application submitted successfully."
                });
            });
        }

        /// <summary>
        ///Author   :   Satya Prakash Sahoo
        ///Date     :   10-12-2021
        ///This method is used to get the nursing form fill up details by Id
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/NursingFormFillUp/GetNursingFillUpDetails/{id}")]
        public IActionResult GetExamFormFillUpDetails(Guid id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _nursingFormFillUpService.GetNursingFormFillUpDetails(id),
                    Success = true
                });
            });
        }

        [HttpPost]
        [Route("/api/services/app/NursingFormFillUp/AddNursingFormDetailsToBackgroundTable")]
        public IActionResult AddExamFormDetailsToBackgroundTable([FromBody] BackgroundProcessDto param)
        {
            return GetHttpResponse(() =>
            {

                _nursingFormFillUpService.AddNursingFormDetailsToBackgroundTable(param);
                return Ok(new RequestResult
                {
                    Result = EnOperationStatus.SUCCESS.ToString(),
                    Message = "The request has been submitted and the intimation will be sent through mail",
                    Success = true
                });
            });
        }
        [HttpPost]
        [ValidateModel]
        [Route("/api/services/app/NursingFormFillUp/GetCollegeWiseFormFillUpAmount")]
        public IActionResult GetCollegeWiseFormFillUpAmount([FromBody] SearchParamDto param)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _nursingFormFillUpService.GetCollegeWiseFormFillUpAmount(param),
                    Success = true
                });
            });
        }
        [HttpPost]
        [Route("/api/services/app/NursingFormFillUp/StateWiseMoveToBackground")]
        public IActionResult StateWiseMoveToBackground([FromBody] BackgroundProcessDto param)
        {
            return GetHttpResponse(() =>
            {
                //_examinationFormFillUpService.StateWiseMoveToBackground(param);
                return Ok(new RequestResult
                {
                    Result = _nursingFormFillUpService.SaveToBackGroundTable(param),
                    Message = "The request has been submitted and the intimation will be sent through mail",
                    Success = true
                });
            });
        }
        [HttpPost]
        [Route("api/services/app/NursingFormFillUp/DownloadAdmitCard")]
        public IActionResult DownloadAdmitCard([FromBody] certificateDownloadDto obj)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _nursingFormFillUpService.DownloadAdmitCard(obj),
                    Success = true,
                    Message = "Admit Card downloaded successfully"
                });
            });
        }
        [HttpPost]
        [Route("/api/services/app/NursingFormFillUp/AddMarkDetailsToBackgroundTable")]
        public IActionResult AddMarkDetailsToBackgroundTable([FromBody] BackgroundProcessDto param)
        {
            return GetHttpResponse(() =>
            {

                _nursingFormFillUpService.AddMarkDetailsToBackgroundTable(param);
                return Ok(new RequestResult
                {
                    Result = EnOperationStatus.SUCCESS.ToString(),
                    Message = "The request has been submitted and the intimation will be sent through mail.",
                    Success = true
                });
            });
        }

        [HttpPost]
        [Route("/api/services/app/NursingFormFillUp/GetNursingMarkDetailsForL1Support")]
        public IActionResult GetMarkDetailsForL1Support([FromBody] SearchParamDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _nursingFormFillUpService.GetNursingMarkDetailsForL1Support(input),
                    Success = true,
                    Message = ""
                });
            });
        }
        [HttpPost]
        [Route("/api/services/app/NursingFormFillUp/DownloadSemesterExamPdfReport")]
        public IActionResult DownloadSemesterExamPdfReport([FromBody] SearchParamDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _nursingFormFillUpService.GetNursingMarkDetailsForL1Support(input),
                    Success = true,
                    Message = ""
                });
            });
        }

        [HttpPost]
        [Route("/api/services/app/NursingFormFillUp/DownloadConductCertificate")]
        public IActionResult DownloadConductCertificate([FromBody] certificateDownloadDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _nursingFormFillUpService.DownloadConductCertificate(input), 
                    Success = true,
                    Message = "File download successfully."
                });
            });
        }
    }
}
