﻿using Microsoft.AspNet.OData;
using MicroService.Utility.Logging.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroService.SharedKernel;
using Community.OData.Linq;

namespace MicroService.API.Controllers
{
    public class ODataBaseController : ODataController
    {
        public IMicroServiceLogger _logger;
        public ODataBaseController(IMicroServiceLogger Logger)
        {
            _logger = Logger;
        }
        public string CurrentUserName()
        {
            var userName = HttpContext.User.Claims.Where(us => us.Type == "User_Name").FirstOrDefault();
            return userName.Value;

        }

        public long CurrentUserID()
        {
            var currentUserID = HttpContext.User.Claims.Where(us => us.Type == "UserID").FirstOrDefault();
            if (!string.IsNullOrEmpty(currentUserID.Value))
                return Convert.ToInt64(currentUserID.Value);
            else
                return 0;

        }

        public IQueryable<T> OdataFilter<T>(ref IQueryable<T> data, OdataQueryBuilder builder)
        {
            if (!string.IsNullOrEmpty(builder.Filter))
            {
                builder.Filter = builder.Filter.Replace("'null'", "null");
                string wordTobeRemoved = "guid";
                var filters = builder.Filter.Split(new string[] { " and " }, StringSplitOptions.RemoveEmptyEntries).ToList();
                List<string> modifiedFilters = new List<string>();
                filters.ForEach(stringToCheck =>
                {
                    if (stringToCheck.Contains(wordTobeRemoved))
                    {
                        var ss = stringToCheck.ToString().Trim().Replace(wordTobeRemoved, string.Empty).Replace("'", string.Empty);
                        stringToCheck = ss;
                    }
                    modifiedFilters.Add(stringToCheck);
                });
                if (builder.Filter.Count() > 0)
                {
                    builder.Filter = string.Join(" and ", modifiedFilters.ToArray());
                }

                data = data.OData().Filter(builder.Filter);
            }
            if (builder.OrderBy != null)
            {
                if (!string.IsNullOrEmpty(builder.OrderBy.Property) && !string.IsNullOrEmpty(builder.OrderBy.OrderType))
                {
                    string orderByString = builder.OrderBy.Property + " " + builder.OrderBy.OrderType;
                    data = data.OData().OrderBy(orderByString);
                }
            }
            if (builder.Take != 0 || builder.Skip != 0)
            {
                data = data.OData().TopSkip(builder.Take.ToString(), builder.Skip.ToString());
            }
            return data;
        }
    }
}
