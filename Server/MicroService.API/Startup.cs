﻿using AutoMapper;
using DinkToPdf;
using DinkToPdf.Contracts;
using Microservice.Common;
using Microservice.Common.Abstract;
using MicroService.API.Provider.ODATA;
using MicroService.Business;
using MicroService.Business.Abstract;
using MicroService.Core.WorkflowService;
using MicroService.Core.WorkflowTransactionHistrory;
using MicroService.DataAccess;
using MicroService.DataAccess.Abstract;
using MicroService.DataAccess.Helpers;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.ApplyProvisionalCertificateDetail;
using MicroService.Domain.Models.Dto.CLC;
using MicroService.Domain.Models.Dto.DuplicateRegistrationDetail;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Dto.Feature;
using MicroService.Domain.Models.Dto.MigrationService;
using MicroService.Domain.Models.Dto.Nursing;
using MicroService.Domain.Models.Dto.Role;
using MicroService.Domain.Models.Dto.StudentDashboard;
using MicroService.Domain.Models.Dto.UniversityService;
using MicroService.Domain.Models.Dto.User;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Entity.Nursing;
using MicroService.Utility.Caching;
using MicroService.Utility.Caching.Abstract;
using MicroService.Utility.Common;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Logging;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNet.OData.Formatter;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Microsoft.OData.UriParser;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IConfiguration _configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Uncomment the below line if this is a workflow microservice and database does not have any entry.
            // Microservice owner should comment this line once the data is saved in 'TenantDtoConfig' table.
            //new WorkflowDtoConfigurationPublisher().GetWorkflowDtoPropertyMetadata(_configuration); // Publishing dto information on microservice startup

            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                //builder.WithOrigins("http://example.com")
                //                    .AllowAnyMethod()
                //                    .AllowAnyHeader();

                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));


            services.AddDistributedRedisCache(options =>
            {
                options.Configuration = _configuration["Redis:ServerIP"];
                options.InstanceName = "";
            });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters =
                             new TokenValidationParameters
                             {
                                 ValidateIssuer = true,
                                 ValidateAudience = true,
                                 ValidateLifetime = true,
                                 ValidateIssuerSigningKey = true,
                                 ValidIssuer = "RentalBI.Security.Bearer",
                                 ValidAudience = "RentalBI.Security.Bearer",
                                 IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration.GetSection("SecurityKey").ToString()))

                             };

                        options.Events = new JwtBearerEvents
                        {
                            OnAuthenticationFailed = context =>
                            {
                                Console.WriteLine("OnAuthenticationFailed: " + context.Exception.Message);
                                return Task.CompletedTask;
                            },
                            OnTokenValidated = context =>
                            {
                                Console.WriteLine("OnTokenValidated: " + context.SecurityToken);
                                return Task.CompletedTask;
                            }
                        };

                    });

            //services
            //        .AddMvc()
            //        .AddJsonOptions(options =>
            //        {
            //            options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            //            //options.SerializerSettings.ContractResolver
            //            //    = new Newtonsoft.Json.Serialization.DefaultContractResolver();
            //            options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            //        });

            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
               // options.Filters.Add(typeof(RequestValidationFilter));
            })
                   .AddJsonOptions(options =>
                   {
                       options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                        //options.SerializerSettings.ContractResolver
                        //    = new Newtonsoft.Json.Serialization.DefaultContractResolver();
                        options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                   }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);


            services.TryAddSingleton(_ =>
            {
                var mb = new ODataConventionModelBuilder(_, true);
                mb.EnableLowerCamelCase();
                return mb;
            });

            services.TryAddSingleton(_ => new ODataUriResolver() { EnableCaseInsensitive = true });


            services.AddOData();

            services.AddMvcCore(options =>
            {
                foreach (var outputFormatter in options.OutputFormatters.OfType<ODataOutputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
                {
                    outputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/prs.odatatestxx-odata"));
                }
                foreach (var inputFormatter in options.InputFormatters.OfType<ODataInputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
                {
                    inputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/prs.odatatestxx-odata"));
                }
            });

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            });

            //Dependancy injection
            services.AddTransient<IMicroServiceUOW, MicroServiceUOW>();
            services.AddSingleton<RepositoryFactories, RepositoryFactories>();
            services.AddTransient<IRepositoryProvider, RepositoryProvider>();
            services.AddTransient<ICommon, MicroServiceCommon>();
            services.AddTransient<IRedisDataHandlerService, RedisDataHandlerService>();
            services.AddTransient<ICacheService, CacheService>();
            services.AddTransient<IMicroServiceLogger, MicroServiceLogger>();
            //services.AddTransient<IEventSourceService, EventSourceService>();
            services.AddTransient<IWorkflowAppService, WorkflowAppService>();
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IWFTransactionHistroryAppService, WFTransactionHistroryAppService>();

            //services.AddTransient<IFarmerRegistrationService, FarmerRegistrationService>();
            services.AddTransient<IExcelParserCommon, ExcelParserCommon>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IFeatureAppService, FeatureAppService>();
            services.AddTransient<ITenantWiseMultilingualAppService, TenantWiseMultilingualAppService>();
            services.AddTransient<IGeographyAppService, GeographyAppService>();
            services.AddTransient<IRoleAppService, RoleAppService>();
            services.AddTransient<ITenantAppService, TenantAppService>();
            services.AddTransient<IStudentDashboardAppService, StudentDashboardAppService>();
            services.AddTransient<IMicroserviceCommon, MicroserviceCommon>();
            services.AddTransient<IStudentRegistrationNoService, StudentRegistrationNoService>();
            services.AddTransient<ICommonAppService, CommonAppService>();
            services.AddTransient<IDuplicateRegistrationService, DuplicateRegistrationService>();
            services.AddTransient<IStudentMigrationService, StudentMigrationService>();
            services.AddTransient<IApplyProvisionalCertificateService, ApplyProvisionalCertificateService>();
            //services.AddTransient<ICorsPolicyAccessorService, CorsPolicyAccessorService>();
            services.AddTransient<IExaminationFormFillUpService, ExaminationFormFillUpService>();
            services.AddTransient<IPaymentService, PaymentService>();
            services.AddTransient<IReportService, ReportService>();
            services.AddTransient<IStudentCLCService, StudentCLCService>();
            services.AddTransient<IAdminActivityService, AdminActivityService>();
            services.AddTransient<ISupportClass, SupportClass>();
            services.AddTransient<INursingFormFillUpService, NursingFormFillUpService>();


            try
            {

                services.AddSingleton(typeof(IConverter), new SynchronizedConverter(new PdfTools()));
            }
            catch (Exception ex)
            {
                IMicroServiceLogger _logger = new MicroServiceLogger();
                _logger.Error("LoadUnmanagedLibrary Error Load");
                _logger.Error(Convert.ToString(ex.StackTrace));
                _logger.Error(Convert.ToString(ex.InnerException));
                _logger.Error(Convert.ToString(ex.Message));
            }

            Mapper.Initialize(cfg =>
            {
                //cfg.CreateMap<FarmerRegistration, FarmerRegistrationDto>();//.ForSourceMember(src => src.);
                //cfg.CreateMap<Farmer, FarmerDto>()
                //    .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.Name.FirstName))
                //    .ForMember(dest => dest.SurName, opt => opt.MapFrom(src => src.Name.LastName))
                //    ;
                //cfg.CreateMap<LandDetail, LandDetailDto>()
                //    .ForMember(dest => dest.IFSCCode, opt => opt.MapFrom(src => src.BankDetail.IFSCCode))
                //    .ForMember(dest => dest.BranchCode, opt => opt.MapFrom(src => src.BankDetail.BranchCode))
                //    .ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.BankDetail.BranchName))
                //    .ForMember(dest => dest.BranchPinCode, opt => opt.MapFrom(src => src.BankDetail.BranchPinCode))
                //    ;
                cfg.CreateMap<UserDto, Users>()
                    .ForMember(dest => dest.RoleIds, opt => opt.MapFrom(src => src.RoleIds))
                    .ForMember(dest => dest.Password, opt => opt.Ignore())
                    .ForMember(dest => dest.EmailAddress, opt => opt.Ignore())
                    .ForMember(dest => dest.Salt, opt => opt.Ignore());
                //.ReverseMap();
                //.PreserveReferences();

                cfg.CreateMap<CreateUserDto, Users>()
                    .ForMember(dest => dest.NormalizedEmailAddress, opt => opt.Ignore())
                    .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                    .ForMember(dest => dest.Password, opt => opt.Ignore())
                    .ForMember(dest => dest.ObjectId, opt => opt.Ignore())
                    .ForMember(dest => dest.RoleIds, opt => opt.MapFrom(src => src.RoleIds))
                    .ForMember(dest => dest.Salt, opt => opt.Ignore())
                    .ForMember(dest => dest.UserAddresses, opt => opt.MapFrom(src => src.UserAddress))
                     .ForMember(dest => dest.UserType, opt => opt.Ignore());

                cfg.CreateMap<UserDetailsDto, Users>()
                    .ForMember(dest => dest.NormalizedEmailAddress, opt => opt.Ignore())
                    .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                    .ForMember(dest => dest.Password, opt => opt.Ignore())
                    .ForMember(dest => dest.ObjectId, opt => opt.Ignore())
                    .ForMember(dest => dest.RoleIds, opt => opt.MapFrom(src => src.RoleIds))
                    .ForMember(dest => dest.Salt, opt => opt.Ignore())
                     .ForMember(dest => dest.UserType, opt => opt.Ignore());

                cfg.CreateMap<UserRoleGeographyMapperDto, UserRoleGeographyMapper>();
                cfg.CreateMap<FeatureApiMapperRetrieveDto, FeatureApiMapper>();
                cfg.CreateMap<CreateRoleDto, Role>();
                cfg.CreateMap<UserAddressDto, UserAddress>();
                cfg.CreateMap<UniversityRegistrationDetail, UniversityRegistrationDetailDto>()
                    .ForMember(dest => dest._trigger, opt => opt.Ignore())
                    .ForMember(dest => dest.WorkflowMessage, opt => opt.Ignore())
                    .ForMember(dest => dest.LoggedInUserRoles, opt => opt.Ignore())
                    .ForMember(dest => dest.WorkflowTransitionConfigId, opt => opt.Ignore())
                    .ForMember(dest => dest.PossibleWorkflowTransitions, opt => opt.Ignore())
                    ;
                cfg.CreateMap<UniversityRegistrationDetail, RegistrationNumberPreviewDto>()
                    ;

                cfg.CreateMap<DuplicateRegistrationDetailDto, UniversityRegistrationDetail>()
                    .ForMember(dest => dest.RegistrationDocument, opt => opt.Ignore())
                   .ForMember(dest => dest.PrincipalRemarks, opt => opt.Ignore())
                   //.ForMember(dest => dest.RegistrarRemarks, opt => opt.Ignore())
                   .ForMember(dest => dest.ComputerCellRemarks, opt => opt.Ignore())
                   .ForMember(dest => dest.AssistantCOERemarks, opt => opt.Ignore())
                   .ForMember(dest => dest.AcknowledgementNo, opt => opt.Ignore())
                   .ForMember(dest => dest.IsRegistrationNumberGenerated, opt => opt.Ignore())
                   .ForMember(dest => dest.PrincipalLastUpdateTime, opt => opt.Ignore());

                cfg.CreateMap<UniversityRegistrationDetail, DuplicateRegistrationDetailDto>()
                    .ForMember(dest => dest._trigger, opt => opt.Ignore())
                    .ForMember(dest => dest.WorkflowMessage, opt => opt.Ignore())
                    .ForMember(dest => dest.LoggedInUserRoles, opt => opt.Ignore())
                    .ForMember(dest => dest.WorkflowTransitionConfigId, opt => opt.Ignore())
                    .ForMember(dest => dest.PossibleWorkflowTransitions, opt => opt.Ignore())
                    ;

                cfg.CreateMap<DuplicateRegistrationDetail, DuplicateRegistrationDetailDto>()
                    .ForMember(dest => dest._trigger, opt => opt.Ignore())
                    .ForMember(dest => dest.WorkflowMessage, opt => opt.Ignore())
                    .ForMember(dest => dest.LoggedInUserRoles, opt => opt.Ignore())
                    .ForMember(dest => dest.WorkflowTransitionConfigId, opt => opt.Ignore())
                    .ForMember(dest => dest.PossibleWorkflowTransitions, opt => opt.Ignore());


                cfg.CreateMap<UniversityMigrationDetail, UniversityMigrationDetailDto>();
                cfg.CreateMap<ProvisionalCertificateApplicationDetailDto, ProvisionalCertificateApplicationDetail>();

                cfg.CreateMap<ServiceStudentAddress, ServiceStudentAddressDto>();

                cfg.CreateMap<ExaminationFormFillUpDetails, ExaminationFormFillUpDetailsDto>()
                    .ForMember(dest => dest._trigger, opt => opt.Ignore())
                    .ForMember(dest => dest.WorkflowMessage, opt => opt.Ignore())
                    .ForMember(dest => dest.LoggedInUserRoles, opt => opt.Ignore())
                    .ForMember(dest => dest.WorkflowTransitionConfigId, opt => opt.Ignore())
                    .ForMember(dest => dest.PossibleWorkflowTransitions, opt => opt.Ignore())
                    .ForMember(dest => dest.ExaminationPaperDetails, opt => opt.MapFrom(src=>src.ExaminationPaperDetails))
                    .ForMember(dest => dest.ServiceStudentAddresses, opt => opt.MapFrom(src=>src.ServiceStudentAddresses))
                    ;

                
                cfg.CreateMap<ExaminationPaperDetail, ExaminationPaperDetailsDto>();
                cfg.CreateMap<ApplyCLC, ApplyCLCDto>();

                cfg.CreateMap<ExaminationFormFillUpSemesterMarks, ExaminationFormFillUpSemesterMarksDto>()
                .ForMember(dest => dest.MarkEntryType, opt => opt.Ignore())
                    .ForMember(dest => dest.PaperName, opt => opt.Ignore())
                    .ForMember(dest => dest.PaperAbbreviation, opt => opt.Ignore())
                    .ForMember(dest => dest.CategoryAbbreviation, opt => opt.Ignore())
                    ;
              
                cfg.CreateMap<NursingFormfillupDetails, NursingFormFillUpDto>()
                  .ForMember(dest => dest._trigger, opt => opt.Ignore())
                  .ForMember(dest => dest.WorkflowMessage, opt => opt.Ignore())
                  .ForMember(dest => dest.LoggedInUserRoles, opt => opt.Ignore())
                  .ForMember(dest => dest.WorkflowTransitionConfigId, opt => opt.Ignore())
                  .ForMember(dest => dest.PossibleWorkflowTransitions, opt => opt.Ignore())
                  .ForMember(dest => dest.ExaminationPaperDetails, opt => opt.MapFrom(src => src.ExaminationPaperDetails))                  
                  ;
            });




            //services.AddTransient<IImageResizeCommon, ImageResizeCommon>();
            var connectionString = _configuration.GetConnectionString("Default");
            services.AddTransient<EntityModelBuilder>();
            MicroServiceDbContext.conString = connectionString;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, EntityModelBuilder modelBuilder)
        {
            //Adding Model class to OData
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //###### this line of code used for linux deployment 

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            // ##### end of linux deployment code

            app.UseCors("MyPolicy");
            app.UseAuthentication();
            //app.UseMvc();
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                //routes.MapRoute(
                //        name: "default",
                //        template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "apiDefault",
                    template: "api/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                //routes.Count().Filter().OrderBy().Expand().Select().MaxTop(null);
                //routes.EnableDependencyInjection();

                routes.MapODataServiceRoute("ODataRoutes", "odata", modelBuilder.GetEdmModel(app.ApplicationServices));
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();
            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Microservice API");
                //options.RoutePrefix = string.Empty;
            }); // URL: /swagger
            string DocumentPath = Path.Combine(Directory.GetCurrentDirectory(), "Documents");
            if (!Directory.Exists(DocumentPath))
            {
                Directory.CreateDirectory(DocumentPath);
            }
            //static file browse
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(DocumentPath),
                RequestPath = "/Documents"
            });

            //app.UseDirectoryBrowser(new DirectoryBrowserOptions
            //{
            //    FileProvider = new PhysicalFileProvider(
            //        Path.Combine(Directory.GetCurrentDirectory(), "Documents")),
            //    RequestPath = "/Documents"
            //});

            string SignaturePath = Path.Combine(Directory.GetCurrentDirectory(), "Content");
            if (!Directory.Exists(SignaturePath))
            {
                Directory.CreateDirectory(SignaturePath);
            }
            //static file browse
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(SignaturePath),
                RequestPath = "/Content"
            });

            //This is used for email template of payment details
            string PaymentImg = Path.Combine(Directory.GetCurrentDirectory(), "Content");
            if (!Directory.Exists(PaymentImg))
            {
                Directory.CreateDirectory(PaymentImg);
            }
            //static file browse
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(PaymentImg),
                RequestPath = "/Content"
            });

            //This is used for linux deployment of mobile team
            string DefaultImgForLinux = Path.Combine(Directory.GetCurrentDirectory(), "Content");
            if (!Directory.Exists(DefaultImgForLinux))
            {
                Directory.CreateDirectory(DefaultImgForLinux);
            }
            //static file browse
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(DefaultImgForLinux),
                RequestPath = "/Content"
            });


            IMicroServiceLogger _logger = new MicroServiceLogger();
            try
            {
                //_logger.Error("CustomAssemblyLoadContext Started");
                var wkHtmlToPdfPath = String.Empty;
                if (_configuration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.win.ToString())
                {
                    wkHtmlToPdfPath = Path.Combine(Directory.GetCurrentDirectory(), "libwkhtmltox.dll");
                }
                else if (_configuration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.linux.ToString())
                {
                    wkHtmlToPdfPath = Path.Combine(_configuration.GetSection("LinuxDocumentPath").Value, "libwkhtmltox.so");
                }


                // _logger.Error("wkHtmlToPdfPath Path" + wkHtmlToPdfPath);
                CustomAssemblyLoadContext context = new CustomAssemblyLoadContext();
                //_logger.Error("new CustomAssemblyLoadContext");
                context.LoadUnmanagedLibrary(wkHtmlToPdfPath);
                //_logger.Error("LoadUnmanagedLibrary Loaded");
            }
            catch (Exception ex)
            {
                //_logger.Error("LoadUnmanagedLibrary Error Load");
                _logger.Error(Convert.ToString(ex.StackTrace));
                _logger.Error(Convert.ToString(ex.InnerException));
                _logger.Error(Convert.ToString(ex.Message));

            }

            //_logger.Error("Startup completed");

        }
    }
}
