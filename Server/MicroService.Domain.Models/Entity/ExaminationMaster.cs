﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class ExaminationMaster : AuditedEntity<Guid>
    {        
        public string CourseType { get; set; }
        public string StreamCode { get; set; }
        public string SubjectCode { get; set; }
        public string CourseCode { get; set; }
        public string DepartmentCode { get; set; }
        public string SemesterCode { get; set; }
        public long ServiceMasterId { get; set; }

        //[ForeignKey("ExaminationMasterId")]
        //public List<ExaminationSession> ExaminationSession { get; set; }

        public virtual ServiceMaster ServiceMaster { get; set; }
    }
}
