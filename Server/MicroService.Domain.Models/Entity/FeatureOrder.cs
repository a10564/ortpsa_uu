﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class FeatureOrder : AuditedEntity<long>
    {
        public int Order { get; set; }
        public string Name { get; set; }

        //To keep the Feature mapping: with FeatureOrder Custom
        [ForeignKey("FeatureOrderId")]
        public List<Feature> FeatureDetails { get; set; }
    }
}
