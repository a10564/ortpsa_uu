﻿using MicroService.Domain.Models.Entity.Payment;
using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class ServiceMaster : AuditedEntity<long>
    {
        public string ServiceName { get; set; }
        public bool IsMultipleApply { get; set; }
        public string WebActionURL { get; set; }
        public string MobileActionUrl { get; set; }
        public string InboxActionURL { get; set; }
        public bool IsPayOnlyServiceCharges { get; set; }

        public virtual ICollection<RoleServiceMapper> RoleServiceMapper { get; set; } = new List<RoleServiceMapper>();
        public virtual ICollection<ServicePricingMaster> ServicePricingMasters { get; set; } = new List<ServicePricingMaster>();
        public virtual ICollection<CollegeServiceMapper> CollegeServiceMapper { get; set; } = new List<CollegeServiceMapper>();
        //public ServiceConveniencePricingMaster ServiceConveniencePricingMaster { get; set; } = new ServiceConveniencePricingMaster();
        public virtual ICollection<ExaminationMaster> ExaminationMaster { get; set; } = new List<ExaminationMaster>();
    }
}
