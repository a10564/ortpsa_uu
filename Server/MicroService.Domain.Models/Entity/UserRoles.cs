﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class UserRoles:AuditedEntity<long>
    {
        //[ForeignKey("Role")]
        public long RoleId { get; set; }
        //[ForeignKey("Users")]
        public long UserId { get; set; }
    }
}

