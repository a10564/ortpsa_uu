﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class RoleUserWiseSubjectMapper : AuditedEntity<long>
    {
        public long RoleId { get; set; }
        public long UserId { get; set; }
        public long SubjectId { get; set; }   
        public string SubjectCode { get; set; }
    }
}

