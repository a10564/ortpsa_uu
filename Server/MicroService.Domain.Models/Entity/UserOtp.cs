﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class UserOtp
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public long Otp { get; set; }
        public DateTime OtpGenerationTime { get; set; }
        public string EmailId { get; set; }
        public string MobileNumber { get; set; }
    }
}
