﻿using MicroService.Core.WorkflowEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class UniversityRegistrationDetail : WorkflowBaseEntity<Guid>
    {
        public string StudentName { get; set; }
        public string FatherName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string CollegeCode { get; set; }
        public string CourseType { get; set; }
        public string StreamCode { get; set; }
        public string DepartmentCode { get; set; }
        public string SubjectCode { get; set; }
        public int YearOfAdmission { get; set; }
        public string RegistrationNo { get; set; }
        [ForeignKey("ParentSourceId")]
        public List<Documents> RegistrationDocument { get; set; }
        //public List<Document> RegistrationDocument { get; set; }
        public string PrincipalRemarks { get; set; }
        //public string RegistrarRemarks { get; set; }
        public string ComputerCellRemarks { get; set; }
        public string AssistantCOERemarks { get; set; }
        //public string HODRemarks { get; set; }
        public string AcknowledgementNo { get; set; }
        public bool IsRegistrationNumberGenerated { get; set; }
        public DateTime? PrincipalLastUpdateTime { get; set; }
        public string CourseCode { get; set; }
        [NotMapped]
        public long serviceId { get; set; }
        public string CSCPayid { get; set; }
        public DateTime? PaymentDate { get; set; }
    }
}
