﻿using MicroService.Core.WorkflowEntity;
using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class Documents : AuditedEntity<Guid>
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileExtension { get; set; }
        public string DocumentType { get; set; }
        public string FileType { get; set; }
        public Guid ParentSourceId { get; set; }
        public string Description { get; set; }
        public long SourceType { get; set; }

        [NotMapped]
        public byte[] FileContent { get; set; }
    }
}
