﻿using MicroService.SharedKernel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.Domain.Models.Entity.Workflow
{
    public class TaskConfig : AuditedEntity<long>
    {
        public TaskConfig()
        {
            SmsTasks = new List<SmsTaskDetails>();
            EmailTask = new List<EmailTaskDetails>();
            NotificationTask = new List<NotificationTaskDetails>();
            AnonymizeDataTask = new List<AnonymizeTaskDetails>();
            //LastModificationTime = Clock.Now;
        }

        public long TenantWorkflowTransitionConfigId { get; set; } // fk tenantworkflowtransitionconfig

        public List<SmsTaskDetails> SmsTasks { get; private set; }
        public List<EmailTaskDetails> EmailTask { get; private set; }
        public List<NotificationTaskDetails> NotificationTask { get; private set; }
        public List<AnonymizeTaskDetails> AnonymizeDataTask { get; private set; }

        public static TaskConfig CreateTaskConfig(long? TenantId)
        {
            var taskConfig = new TaskConfig();

            taskConfig.TenantId = TenantId;
            //taskConfig.TenantWorkflowTransitionConfigId = tenantWorkflowTransitionConfigId;
            return taskConfig;
        }
        public void CreateTask(string type, string paramsMapper)
        {
            var taskConfigProps = this.GetType().GetProperties();
            var myType = Type.GetType(type);
            var listType = typeof(List<>).MakeGenericType(myType);
            dynamic myObjects = JsonConvert.DeserializeObject(paramsMapper, listType);
            var selectedProp = taskConfigProps.Where(p => p.PropertyType.FullName == listType.FullName).FirstOrDefault();
            var existingTasks = selectedProp.GetValue(this);
            dynamic tasks = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(existingTasks), listType);
            tasks.Add(myObjects[0]);
            selectedProp.SetValue(this, tasks);


        }

    }
}
