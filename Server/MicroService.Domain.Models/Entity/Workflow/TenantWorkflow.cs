﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity.Workflow
{
    public class TenantWorkflow : AuditedEntity<long>
    {
        public long MicroserviceId { get; set; }
        public string MicroserviceName { get; set; }
        public long WorkflowId { get; set; }
        public string Workflow { get; set; }
        public string DotGraph { get; set; } // need to add this column in db as a long text 
        public int VersionNo { get; set; }
        public string DependantSubprocess { get; set; }

        public List<TenantWorkflowTransitionConfig> TransitionConfiguration { get; set; }
        
    }
}
