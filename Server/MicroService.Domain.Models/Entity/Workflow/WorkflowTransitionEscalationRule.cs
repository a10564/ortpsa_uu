﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity.Workflow
{
    public class WorkflowTransitionEscalationRule : AuditedEntity<long>
    {
        //public WorkflowTransitionEscalationRule()
        //{
        //    LastModificationTime = Clock.Now;
        //}
        public int TenantWorkflowTransitionConfigId { get; set; }
        public int WorkflowId { get; set; }
        public int Level { get; set; }
        public int EscalationTime { get; set; }
        public string Roles { get; set; }
    }
}
