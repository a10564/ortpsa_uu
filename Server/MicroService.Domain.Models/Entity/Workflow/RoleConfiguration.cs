﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity.Workflow
{
    public class RoleConfiguration : AuditedEntity<long>
    {
        public string RoleName { get; set; }
    }
}
