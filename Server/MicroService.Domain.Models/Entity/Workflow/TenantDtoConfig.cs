﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity.Workflow
{
    public class TenantDtoConfig : AuditedEntity<long>
    {
        public long MicroserviceId { get; set; }
        public string MicroserviceName { get; set; }
        public string Fields { get; set; } // json stringified fields for a dto
        public string DtoName { get; set; }
        public string Title { get; set; }
        public string TaskFields { get; set; }
    }
}
