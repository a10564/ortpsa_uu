﻿using MicroService.Core.WorkflowAttribute;
using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity.Workflow
{
    //[AppTask("SMS")]
    public class SmsTaskDetails : AuditedEntity<long>  //, IAppTask
    {
        [ForeignKey("TaskConfiguration")]
        public int TaskConfigId { get; set; }
        [AppTaskParam]
        public string To { get; set; }
        [ForeignKey("Template")]
        public int MessageTemplateId { get; set; }
        public string BodyParameters { get; set; }
        //public TaskConfig TaskConfiguration { get; set; }
        public MessageTemplate Template { get; set; }
        //public SmsTaskDetails()
        //{
        //    LastModificationTime = Clock.Now;
        //}
    }
}
