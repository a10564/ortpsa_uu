﻿using MicroService.Core.WorkflowAttribute;
using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity.Workflow
{
    //[AppTask("Anonymize")]
    public class AnonymizeTaskDetails : AuditedEntity<long> //, IAppTask
    {
        [ForeignKey("TaskConfiguration")]
        public long TaskConfigId { get; set; }
        [AppTaskParam]
        public string AnonymizedFields { get; set; }
        //public TaskConfig TaskConfiguration { get; set; }
        //public AnonymizeTaskDetails()
        //{
        //    LastModificationTime = Clock.Now;
        //}
    }
}
