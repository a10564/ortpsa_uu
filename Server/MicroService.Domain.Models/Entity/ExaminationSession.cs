﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class ExaminationSession : AuditedEntity<Guid>
    {
        public Guid ExaminationMasterId { get; set; }
        public int StartYear { get; set; }
        public int EndYear { get; set; }
        public int ExamYear { get; set; }
    }
}
