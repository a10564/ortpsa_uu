﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class UserLoginDetail
    {
        [Key]
        public long Id { get; set; }
        public long UserId { get; set; }
        public DateTime LastLoginTime { get; set; }
    }
}
