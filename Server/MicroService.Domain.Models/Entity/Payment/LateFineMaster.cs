﻿using MicroService.SharedKernel;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MicroService.Domain.Models.Entity
{
	[Table("latefinemaster")]
	public class LateFineMaster : AuditedEntity<Guid>
	{
        public long ServiceMasterid { get; set; } 
        public int SlotOrder { get; set; }
		public decimal FineAmount { get; set; }

	}
}
