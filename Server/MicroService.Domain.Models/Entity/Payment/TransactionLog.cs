﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    [Table("transactionlog")]
    public class TransactionLog : AuditedEntity<Guid>
    {
        public string CSCPayid { get; set; }
        public string RequestId { get;  set; }
        public string ConsumerName { get;  set; }
        public string EmailId { get; set; }
        public string ContactNo { get; set; }
        public string UniqueRefNo { get; set; }
		[NotMapped]
		public string RequestersUniqueRefNo { get; set; }
		public decimal Amount { get; set; }
        public string Status { get; set; }
        public string ProductInfo { get; set; }        
        public string CardNumber { get; set; }
        public string CardType { get; set; }
        public string NameOnCard { get; set; }
        public string BankRefNo { get; set; }
        public string BankCode { get; set; }
        public string PaymentId { get; set; }
        public string RecieptNo { get; set; }
        public string CallType { get; set; }
        public string HashSequence { get; set; }
        public string Description { get; set; }        
        public string MihPayid { get; set; }		
		public string ErrorDescription { get; set; }
		[NotMapped]
		public string DeviceType { get; set; }
        [NotMapped]
        public string ProductDesc { get; set; }
    }
}
