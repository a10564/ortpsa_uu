﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class BackgroundProcess
    {
        public long ServiceType { get; set; }
        public string InstanceTrigger { get; set; }
        public Guid InstanceId { get; set; }
        public string Status { get; set; }
        public string CourseTypeCode { get; set; }
        public string CollegeCode { get; set; }
        public string StreamCode { get; set; }
        public string SubjectCode { get; set; }
        public string DepartmentCode { get; set; }
        public string CourseCode { get; set; }
        public string SemesterCode { get; set; }
        public int AcademicStart { get; set; }
        public long TenantWorkflowId { get; set; }
        public Guid Id { get; set; }
        public long CreatorUserId { get; set; }
        public DateTime CreationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public bool IsActive { get; set; }
        public long? TenantId { get; set; }
        public string CurrentAcademicYear { get; set; }


    }
}
