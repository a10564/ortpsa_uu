﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class ServiceStudentAddress : AuditedEntity<Guid>
    {
        public long ServiceType { get; set; }
        public Guid InstanceId { get; set; }
        [MaxLength(500, ErrorMessage = "Address field character can't be more than 500")]
        public string Address { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string AddressType { get; set; }
    }
}
