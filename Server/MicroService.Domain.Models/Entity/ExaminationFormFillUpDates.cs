﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class ExaminationFormFillUpDates : AuditedEntity<Guid>
    {
        public Guid ExaminationMasterId { get; set; }
        public int SlotOrder { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid ExaminationSessionId { get; set; }
}
}
