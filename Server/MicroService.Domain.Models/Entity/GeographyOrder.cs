﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class GeographyOrder : AuditedEntity<long>
    {
        public long Order { get; set; }
        public string Name { get; set; }
        [ForeignKey("GeographyOrderId")]
        public List<GeographyDetail> GeographyDetails { get; set; }
    }
}
