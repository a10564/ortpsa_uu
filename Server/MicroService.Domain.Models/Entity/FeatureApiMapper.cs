﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class FeatureApiMapper : AuditedEntity<long>
    {
        //[ForeignKey("Feature")]
        public long FeatureId { get; set; }
        [ForeignKey("ApiDetail")]
        public long ApiDetailId { get; set; }
        public bool IsMandatory { get; set; }
        public string Path { get; set; }
        //[ForeignKey("ApiDetailId")]       
        [NotMapped]
        public List<ApiDetail> ApiDetails { get; set; }
        [NotMapped]
        public string ApiName { get; set; }
    }
}
