﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class GeographyDetail:AuditedEntity<long>
    {
        //[ForeignKey("GeographyOrder")]
        public long  GeographyOrderId { get; set; }
        public long ParentId { get; set; }
        public string Name { get; set; }
    }
}
