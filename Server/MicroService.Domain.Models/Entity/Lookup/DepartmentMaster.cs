﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity.Lookup
{
    public class DepartmentMaster : AuditedEntity<long>
    {
        [Column("DepartmentName")]
        public string LookupDesc { get; set; }      // Name
        //public string FullDesc { get; set; }        // Description
        [Column("DepartmentCode")]
        public string LookupCode { get; set; }      // Department-code
        public long LookupSequence { get; set; }    // Sequence
        public string StreamCode { get; set; }      // Lookup 'stream' code
        public string CourseCode { get; set; }      // Lookup 'course' code
    }
}
