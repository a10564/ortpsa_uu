﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity.Lookup
{
    public class Lookup : AuditedEntity<long>
    {
        public long LookupTypeId { get; set; }
        public string LookupDesc { get; set; }
        public string FullDesc { get; set; }
        public long LookupSequence { get; set; }
        public string LookupCode { get; set; }
        public string ParentLookupCode { get; set; }
        public long LookupId { get; set; }
    }
}
