﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity.Lookup
{
    public class LookupType : AuditedEntity<long>
    {
        public long LookupTypeId { get; set; }
        public string LookupTypeDesc { get; set; }
        //public string LookupTypeCode { get; set; }
        //public long? moduleId { get; set; }       // Can be used in future
    }
}
