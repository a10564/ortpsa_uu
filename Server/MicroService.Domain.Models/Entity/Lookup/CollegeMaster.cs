﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity.Lookup
{
    public class CollegeMaster : AuditedEntity<long>
    {
        [Column("CollegeName")]
        public string LookupDesc { get; set; }              // Name
        //[Column("")]
        //public string FullDesc { get; set; }                // Description
        [Column("CollegeCode")] 
        public string LookupCode { get; set; }              // College code
        public long LookupSequence { get; set; }            // Sequence
        public string Address { get; set; }                 // Text area, free text
        public string CourseTypeCode { get; set; }          // Lookup 'course-type' code
        public bool IsAffiliationRequired { get; set; }
        public string CollegeFilterCode { get; set; }
     }
}
