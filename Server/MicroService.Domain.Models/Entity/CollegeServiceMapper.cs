﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class CollegeServiceMapper : AuditedEntity<long>
    {
            public string CollegeCode { get; set; }
            public long ServiceMasterId { get; set; }
            public virtual ServiceMaster ServiceMaster { get; set; }        
    }

}