﻿using MicroService.Core.WorkflowEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class UniversityMigrationDetail : WorkflowBaseEntity<Guid>
    {
        public string StudentName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string CollegeCode { get; set; }
        public string CourseType { get; set; }
        public string StreamCode { get; set; }
        public string DepartmentCode { get; set; }
        public string SubjectCode { get; set; }
        public string CourseCode { get; set; }
        public DateTime YearOfAdmission { get; set; }
        public string RegistrationNo { get; set; }
        public string ReasonForMigrationCode { get; set; }
        public string RollNo { get; set; }
        public DateTime YearOfLeaving { get; set; }
        public string StudyDetail { get; set; }
        public string AcknowledgementNo { get; set; }
        [ForeignKey("ParentSourceId")]
        public List<Documents> MigrationDocument { get; set; }
        public string MigrationNumber { get; set; }
        public DateTime? PrincipalLastUpdateTime { get; set; }
        public string CSCPayid { get; set; }
        public DateTime? PaymentDate { get; set; }
    }
}
