﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class RoleServiceMapper : AuditedEntity<long>
    { 
        public long RoleId { get; set; }
        public long ServiceMasterId { get; set; }
        public virtual ServiceMaster ServiceMaster { get; set; }
    }
}
