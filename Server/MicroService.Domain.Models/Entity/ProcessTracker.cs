﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class ProcessTracker
    {
        public long ServiceId { get; set; }
        public int ProcessType { get; set; }
        public string ProcessStatus { get; set; }
        public Guid Id { get; set; }
        public long CreatorUserId { get; set; }
        public DateTime CreationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public bool IsActive { get; set; }
        public long? TenantId { get; set; }
    }
}
