﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class EmailTemplate
    {
        public long Id { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public long TemplateType { get; set; }
        public bool IsActive { get; set; }
        public long ServiceType { get; set; }
        public string SMSContentId { get; set; }
    }
}
