﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class StudentProfile : AuditedEntity<Guid>
    {
        public long UserId { get; set; }
        [Required(ErrorMessage = "Student Name field is required")]
        [MaxLength(40, ErrorMessage = "Student Name cannot be greater than 40 characters.")]
        public string StudentName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string CollegeCode { get; set; }
        public string CourseType { get; set; }
        public string StreamCode { get; set; }
        public string DepartmentCode { get; set; }
        public string SubjectCode { get; set; }
        public int YearOfAdmission { get; set; }
        public string CourseCode { get; set; }
        public long ServiceId { get; set; }
    }
}
