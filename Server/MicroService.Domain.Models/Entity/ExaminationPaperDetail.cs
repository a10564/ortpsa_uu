﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class ExaminationPaperDetail : AuditedEntity<Guid>
    {
        public Guid ExaminationFormFillUpDetailsId { get; set; }
        public Guid ExaminationPaperId { get; set; }
    }
}
