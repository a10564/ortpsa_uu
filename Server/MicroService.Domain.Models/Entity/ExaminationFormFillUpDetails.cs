﻿using MicroService.Core.WorkflowEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class ExaminationFormFillUpDetails : WorkflowBaseEntity<Guid>
    {
        public string CourseTypeCode { get; set; }
        public string RegistrationNumber { get; set; }
        public string RollNumber { get; set; }

        public string StudentName { get; set; }
        public string FatherName { get; set; }
        public string GuardianName { get; set; }
        public string Nationality { get; set; }
        public string Caste { get; set; }
        public string Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public bool IsSpeciallyAbled { get; set; }
        public bool IsSameAsPresent { get; set; }

        public int YearOfMatriculation { get; set; }
        public int YearOfPreviousExamPassed { get; set; }
        public string PreviousAcademicSubject { get; set; }

        public string CollegeCode { get; set; }
        public string CourseCode { get; set; }
        public string StreamCode { get; set; }
        public string DepartmentCode { get; set; }
        public string SubjectCode { get; set; }
        public string SemesterCode { get; set; }
        public string AcademicSession { get; set; }
        public string ApplicationType { get; set; }
        public string AcknowledgementNo { get; set; }

        public Decimal FormFillUpMoneyToBePaid { get; set; }
        public Decimal InternetHandlingCharges { get; set; }
        public int AcademicStart { get; set; }
        [ForeignKey("ParentSourceId")]
        public List<Documents> ImageDocument { get; set; }
        [ForeignKey("ExaminationFormFillUpDetailsId")]
        public List<ExaminationPaperDetail> ExaminationPaperDetails { get; set; }
        [ForeignKey("InstanceId")]
        public List<ServiceStudentAddress> ServiceStudentAddresses { get; set; }

        public string PrincipalHODRemarks { get; set; }
        public string CSCPayid { get; set; }
        public DateTime? PaymentDate { get; set; }
        public Decimal LateFeeAmount { get; set; }
    }
}
