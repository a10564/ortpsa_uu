﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.ExaminationFormFillUp
{
    public class TermEndMarkDto
    {
        public Guid Id { get; set; }
        public string RollNumber { get; set; }
        public string RegistrationNumber { get; set; }
        public Guid ExaminationPaperId { get; set; }
        public long? InternalMarkSecured { get; set; }
        public long ExternalMark { get; set; }
        public long? ExternalMarkSecured { get; set; }
        public long? PracticalMarkSecured { get; set; }
        public string AcknowledgementNo { get; set; }
        public bool IsExternalAttained { get; set; }
        public Guid ExaminationMasterId { get; set; }
        public long InternalMark { get; set; }
        public long PracticalMark { get; set; }
    }
}
