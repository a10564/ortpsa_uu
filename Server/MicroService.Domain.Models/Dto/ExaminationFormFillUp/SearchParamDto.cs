﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.ExaminationFormFillUp
{
    public class SearchParamDto
    {
        public string CourseTypeCode { get; set; }
        public List<string> CollegeCode { get; set; }
        public string StreamCode { get; set; }
        public string CourseCode { get; set; }
        public string DepartmentCode { get; set; }
        public string SubjectCode { get; set; }
        public string SemesterCode { get; set; }
        public string State { get; set; }
        public int StartYear { get; set; }
        public Guid? PaperId { get; set; }
        public string MarkType { get; set; }
        public string CurrentYear { get; set; }
        public long ServiceId { get; set; }
    }
}
