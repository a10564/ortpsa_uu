﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.ExaminationFormFillUp
{
    public class GridModelExcelDto
    {
        public List<GridColumnExcel> Columns { get; set; }
        public List<GridRowExcel> Rows { get; set; }
        public int Count { get; set; }
    }
    public class GridColumnExcel
    {
        public string ColumnName { get; set; }
        public bool IsHidden { get; set; }
    }
    public class GridRowExcel
    {
        public List<GridRowItemExcel> GridRowItems { get; set; }
        public bool IsRowShow { get; set; }
    }

    public class GridRowItemExcel
    {
        public string Value { get; set; }
        public bool IsHidden { get; set; }
        public bool IsCheckBox { get; set; }
        public bool Checked { get; set; }
    }
}
