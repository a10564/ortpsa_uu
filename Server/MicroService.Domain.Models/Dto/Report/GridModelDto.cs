﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.Report
{
    public class GridModelDto
    {
        public List<GridColumn> Columns { get; set; }
        public List<GridRow> Rows { get; set; }
    }
    public class GridColumn
    {
        public string ColumnName { get; set; }
        public string ColumnLabel { get; set; }
        // other relevant column metadata for sorting, formatting, etc
    }
    public class GridRow
    {
        //public List<string> HiddenFields { get; set; }
        public List<string> VisibleFields { get; set; }
    }
}
