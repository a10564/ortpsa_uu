﻿using Microservice.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.StudentDashboard
{
    public class ServiceStatusDto
    {
        public string Status { get; set; }
        public enStatusType StatusType { get; set; }
        public string StatusDate { get; set; }
    }
}
