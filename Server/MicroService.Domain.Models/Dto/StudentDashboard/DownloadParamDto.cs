﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.StudentDashboard
{
    public class DownloadParamDto
    {
        //public string RollNo { get; set; }
        public string TransanctionId { get; set; }
        public long DocType { get; set; }
    }
}
