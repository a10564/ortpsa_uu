﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.StudentDashboard
{
    public class PendingActionListDto
    {
        public long UserId { get; set; }
        public long ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string LastStatus { get; set; }
        public Guid InstanceId { get; set; }
    }
}
