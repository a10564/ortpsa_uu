﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.Workflow
{
    public class TriggerParameterDto
    {
        public string PropertyName { get; set; }
        public long ControlType { get; set; }
        public string Label { get; set; }
        public string DisplayName { get; set; }
        public string MinLength { get; set; }
        public string MaxLength { get; set; }
        public string Placeholder { get; set; }
        public bool IsMultiple { get; set; }
        public string TextAreaRows { get; set; }
        public bool FormControlIgnore { get; set; }
        public string VisibilityPages { get; set; }
        public string Validators { get; set; }
        public string ValidationMessages { get; set; }
        public string DefaultValue { get; set; }
        public string MinValue { get; set; }
        public string MaxValue { get; set; }
        public bool AutoCompleteHtmlAttribute { get; set; }
        public string TextMaskPattern { get; set; }
        public string GroupName { get; set; }
        public string GroupDisplayName { get; set; }
        public string FormControlRelation { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public string Hint { get; set; }
        public string Options { get; set; }
        public string OptionsApiURL { get; set; }
        public bool IsComposite { get; set; }
        public string Fields { get; set; }
        public string SequenceNo { get; set; }
        public string GroupSequenceNo { get; set; }
        public bool IsCompositeArray { get; set; }
        public bool IsCheckboxGroup { get; set; }
        public int ConfigType { get; set; }
        public string TypeId { get; set; }
    }
}
