﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.TenantWiseMultilingual
{
    public class LanguageDto : AuditedEntity<long>
    {        
        public string LanguageName { get; set; }
        public string LanguageCode { get; set; }
    }
}
