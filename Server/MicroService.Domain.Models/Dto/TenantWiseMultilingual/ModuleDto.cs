﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.TenantWiseMultilingual
{
    public class ModuleDto : AuditedEntity<long>
    {
        public string ModuleName { get; set; }
    }
}
