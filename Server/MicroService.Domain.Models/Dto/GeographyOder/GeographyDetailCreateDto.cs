﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.GeographyOder
{
    public class GeographyDetailDto : AuditedEntity<long>
    {
        public long GeographyOrderId { get; set; }
        public long ParentId { get; set; }
        public string Name { get; set; }
    }
    public class GeographyDetailCreateDto:AuditedEntity<long>
    {
        public long GeographyOrderId { get; set; }
        public long ParentId { get; set; }
        public string Name { get; set; }
    }
}
