﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.UniversityService
{
    public class AppliedServiceDto: AuditedEntity<long>
    {
        public string Name { get; set; }
        public bool IsApplied { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsAvailableToDl { get; set; }
        public long DocType { get; set; }
        public string WebActionURL { get; set; }
        public string MobileActionUrl { get; set; }
        public string LastStatus { get; set; }
        public long AppliedFormCount { get; set; }
        public string InboxActionURL { get; set; }
        public Guid InstanceId { get; set; }
    }
}
