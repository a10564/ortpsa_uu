﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.UniversityService
{
    public class RegistrationNumberPreviewDto
    // : AuditedEntity<long> // datta said do not used right now  
    {
        public string StudentName { get; set; }
        public string CollegeCode { get; set; }
        public string CourseType { get; set; }
        public string StreamCode { get; set; }
        public int YearOfAdmission { get; set; }
        public string RegistrationNo { get; set; }
        public string SubjectCode { get; set; }
        public string DepartmentCode { get; set; }
        public string CourseCode { get; set; }
    }
}
