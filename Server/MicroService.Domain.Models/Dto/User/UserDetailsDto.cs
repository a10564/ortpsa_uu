﻿using MicroService.Domain.Models.Entity;
using MicroService.SharedKernel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MicroService.Domain.Models.Dto.User
{
    public class UserDetailsDto : AuditedEntity<long>
    {
        [NotMapped]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Student Name field is required")]
        [MaxLength(40, ErrorMessage = "Student Name cannot be greater than 40 characters.")]
        public string Name { get; set; }
        [NotMapped]
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }

        [NotMapped]
        public long[] RoleIds { get; set; }

        //property added for custom role and geographical access for the user
        public List<UserRoleGeographyMapperDto> UserRoleGeographyMappers { get; set; }
        public string Password { get; set; }
        [NotMapped]
        public string UserDetails { get; set; }
        [NotMapped]
        public string UserAddressDetails { get; set; }

        public List<UserAddressDto> UserAddresses { get; set; }        
        public long CollegeCode { get; set; }
        [NotMapped]
        public List<StudentProfile> StudentProfiles { get; set; }
        [NotMapped]
        public List<Documents> StudentDocuments { get; set; }
    }
}
