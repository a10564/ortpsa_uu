﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.User
{
    public class LoginUserDetails
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime LastLoginTime { get; set; }
    }
}
