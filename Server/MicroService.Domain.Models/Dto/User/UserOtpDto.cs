﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.User
{
    public class UserOtpDto
    {
        public long UserId { get; set; }
        public long FreshOtp { get; set; }
        public DateTime OtpGenerationTime { get; set; }
        public string UnregisterdEmailId { get; set; }
        public string UnregisterdMobileNumber { get; set; }
    }
}
