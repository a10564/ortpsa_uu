﻿using Microservice.Common;
using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Dto.User
{
    public class UserDto : AuditedEntity<long>
    {
        public const int MaxUserNameLength = 32;
        public const int MaxEmailAddressLength = 256;
        public const int MaxNameLength = 32;
        public const int MaxSurnameLength = 32;
        public const int MaxAuthenticationSourceLength = 64;

        [Required]
        [StringLength(MaxUserNameLength)]        
        public string UserName { get; set; }

        [Required]
        [StringLength(MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(MaxSurnameLength)]
        public string Surname { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        [Required]
        [Phone]
        public string PhoneNumber { get; set; }
        //public string FullName { get; set; }
        [NotMapped] // Since automapper expects primitive list should be identical with entity
        public long[] RoleIds { get; set; }
        public List<UserRoleGeographyMapperDto> UserRoleGeographyMappers { get; set; }
        public string UserDetails { get; set; }
        public string UserAddressDetails { get; set; }

        public string CollegeCode { get; set; }
        public string DepartmentCode { get; set; }
    }
}
