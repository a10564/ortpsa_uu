﻿using MicroService.Domain.Models.Dto.StudentRegister;
using MicroService.Domain.Models.Entity;
using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Dto.User
{
    public class CreateUserDto: AuditedEntity<long>
    {
        [NotMapped]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Name field is required")]
        [RegularExpression(@"^[a-zA-Z\s\.]+$", ErrorMessage = "Name field accepts only alphabets.")]
        [MaxLength(40, ErrorMessage = "Name cannot be greater than 40 characters.")]
        public string Name { get; set; }
        [NotMapped]
        public string Surname { get; set; }
        [Required(ErrorMessage = "EmailAddress field is required")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "EmailAddress field should be in proper format.")]
        [MaxLength(256, ErrorMessage = "EmailAddress cannot be greater than 256 characters.")]
        public string EmailAddress { get; set; }
        [Required(ErrorMessage = "PhoneNumber field is required")]
        [MaxLength(10, ErrorMessage = "PhoneNumber cannot be greater than 10 characters.")]
        public string PhoneNumber { get; set; }

        [NotMapped]
        public long[] RoleIds { get; set; }

        //property added for custom role and geographical access for the user
        public List<UserRoleGeographyMapperDto> UserRoleGeographyMappers { get; set; }
        public string Password { get; set; }
        [NotMapped]
        public string UserDetails { get; set; }
        [NotMapped]
        public string UserAddressDetails { get; set; }

        [NotMapped]
        public long Otp { get; set; }

        public List<UserAddressDto> UserAddress { get; set; }
        //public long UserType { get; set; }
        public long CollegeCode { get; set; }

        public StudentProfileDto StudentProfile { get; set; }

        public List<DocumentsDto> StudentDocuments { get; set; }
    }
}
