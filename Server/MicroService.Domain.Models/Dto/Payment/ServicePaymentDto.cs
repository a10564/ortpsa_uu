﻿using MicroService.Core.WorkflowAttribute;
using MicroService.Core.WorkflowCommon;
using MicroService.Core.WorkflowEntity;
using MicroService.Domain.Models.Entity;
using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.Payment
{

	public class ServicePaymentDto 
	{
		public string FeeDescription { get; set; }
		public decimal? FeeAmount { get; set; }
	}
}


