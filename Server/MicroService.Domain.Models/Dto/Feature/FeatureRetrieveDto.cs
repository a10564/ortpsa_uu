﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.Feature
{
    public class FeatureRetrieveDto : AuditedEntity<long>
    {
        public long FeatureOrderId { get; set; }
        public long ParentId { get; set; }
        public string Name { get; set; }
        public int Sequence { get; set; }

        //Navigational properties
        public List<FeatureApiMapperRetrieveDto> FeatureApiMappers { get; set; }
        public FeatureRetrieveDto ParentFeature { get; set; }
        public List<FeatureRetrieveDto> ChildFeatures { get; set; }
        public List<ApiDetailRetrieveDto> ApiList { get; set; }
    }
}
