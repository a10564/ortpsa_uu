﻿using MicroService.Domain.Models.Dto.Feature;
using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto
{
    public class AuthorizedRoleFeatureDto : AuditedEntity<long>
    {
        //public List<Feature> FeatureHeirarchy { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<AuthorizedFeatureDto> AssignedFeatures { get; set; }
        public AuthorizedFeatureTreeDto FeatureHeirarchy { get; set; }
        public string Roles { get; set; }
    }
    public class AuthorizedFeatureDto : AuditedEntity<long>
    {
        public long FeatureOrderId { get; set; }
        public long ParentId { get; set; }
        public string Name { get; set; }
        public int Sequence { get; set; }
        public List<FeatureApiMapperRetrieveDto> FeatureApiMappers { get; set; }
        public List<ApiDetailRetrieveDto> ApiList { get; set; }
    }
    public class AuthorizedFeatureTreeDto : AuditedEntity<long>
    {
        public long FeatureOrderId { get; set; }
        public long ParentId { get; set; }
        public string Name { get; set; }
        public int Sequence { get; set; }
        public List<FeatureApiMapperRetrieveDto> FeatureApiMappers { get; set; }
        public List<ApiDetailRetrieveDto> ApiList { get; set; }

        public List<AuthorizedFeatureTreeDto> ChildFeatures { get; set; }

        public static AuthorizedFeatureTreeDto BuildTree(List<AuthorizedFeatureDto> nodes)
        {
            // Create a NULL-root tree
            AuthorizedFeatureTreeDto root = new AuthorizedFeatureTreeDto();

            // Add nodes (sub-trees) to the tree
            foreach (AuthorizedFeatureDto node in nodes)
            {
                // traverse tree, find the parent, add child
                root.TraverseAndAddNode(root, node);
            }

            // Return the NULL-root
            // If the tree has an actual root, it will be the first child of this root
            return root;
        }

        public bool TraverseAndAddNode(AuthorizedFeatureTreeDto root, AuthorizedFeatureDto node)
        {
            bool nodeAdded = false;

            // Check if the current root is the parent of the node to be added
            if (root.Id == node.ParentId)
            {
                if (root.ChildFeatures == null)
                    root.ChildFeatures = new List<AuthorizedFeatureTreeDto>();

                root.ChildFeatures.Add(new AuthorizedFeatureTreeDto()
                {
                    Id = node.Id,
                    FeatureOrderId = node.FeatureOrderId,
                    ParentId = node.ParentId,
                    Name = node.Name,
                    Sequence = node.Sequence,
                    IsActive = node.IsActive,
                    ApiList = node.ApiList,
                    FeatureApiMappers = node.FeatureApiMappers,
                    ChildFeatures = null
                });

                nodeAdded = true;
            }
            // Check if the current root has children and recurse the search
            else if (root.ChildFeatures != null)
            {
                foreach (AuthorizedFeatureTreeDto tree in root.ChildFeatures)
                {
                    nodeAdded = tree.TraverseAndAddNode(tree, node);

                    if (nodeAdded)
                        break;
                }
            }
            // Node's parent is not in the current tree
            else
            {
                nodeAdded = false;
            }

            return nodeAdded;
        }
    }

}
