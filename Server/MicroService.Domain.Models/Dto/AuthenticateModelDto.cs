﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MicroService.Domain.Models.Dto
{
    public class AuthenticateModelDto
    {
        [Required]
        //[StringLength(MaxEmailAddressLength)]
        //[StringLength(100, MinimumLength = 10, ErrorMessage = "field must be atleast 10 characters")]
        //[EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string UserNameOrEmailAddress { get; set; }

        [Required]
        //[StringLength(MaxPlainPasswordLength)]
        //[StringLength(1000, MinimumLength = 10, ErrorMessage = "field must be atleast 10 characters")]
        public string Password { get; set; }

        public bool RememberClient { get; set; }

        //Added by Amar to previlledge the tenant login passed as the param at the time of login.
        public string TenancyName { get; set; }

        //Added by Anurag to validate for mobile user login as student, for web no value to be passed
        public string DeviceType { get; set; }
    }
}
