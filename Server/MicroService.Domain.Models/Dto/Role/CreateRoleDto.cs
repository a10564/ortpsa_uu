﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MicroService.Domain.Models.Dto.Role
{
    public class CreateRoleDto:AuditedEntity<long>
    {
        public const int MaxDescriptionLength = 5000;
        public const int MaxNameLength = 32;
        public const int MaxDisplayNameLength = 64;

        [Required]
        [StringLength(MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(MaxDisplayNameLength)]
        public string DisplayName { get; set; }

        public string NormalizedName { get; set; }

        [StringLength(MaxDescriptionLength)]
        public string Description { get; set; }

        public bool IsStatic { get; set; }

        //public List<string> Permissions { get; set; }

        //To keep the role-feature-api mapping: Custom
        public List<RoleFeatureApiMapperCreateDto> RoleFeatureApiMappers { get; set; }
    }
}
