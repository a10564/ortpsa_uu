﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.AdminActivity
{
    public class ExamDateFilterPropertiesDto
    {
        public string CourseTypeCode { get; set; }
        public string StreamCode { get; set; }
        public string SubjectCode { get; set; }
        public string DepartmentCode { get; set; }
        public string CourseCode { get; set; }
        public string SemesterCode { get; set; }
        public string startAndEndYearFromSession { get; set; }
    }
    public class YearFromSession
    {
        public int StartYear { get; set; }
        public int EndYear { get; set; }
    }
  }
