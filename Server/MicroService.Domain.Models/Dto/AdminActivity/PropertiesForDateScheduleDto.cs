﻿using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.AdminActivity
{
    public class PropertiesForDateScheduleDto
    {
       public ExamDateFilterPropertiesDto FilterProperties{get;set;}       
        public List<ExamFormFillupDatesDto> Dates { get; set; }
    }
}
