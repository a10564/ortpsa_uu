﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto
{
    public class ServiceWiseStudentDetailsDto
    {
        public Guid Id{ get; set; }  
        public string StudentName { get; set; }
        public string FatherName { get; set; }
        public string GuardianName { get; set; }
        public string DateOfBirth { get; set; }
        public string RegistrationNumber { get; set; }
        public string RollNumber { get; set; }      
        public string AcknowledgementNo { get; set; }
        public String PaymentDate { get; set; }
        public DateTime CreationTime{ get; set; }
        public string CollegeCode { get; set; }
        public string CollegeName{ get; set; }  
        public decimal FormFillUpAmount{ get; set; }  
        public decimal LateFeeAmount { get; set; }  
        public decimal TotalAmountToPaid { get; set; }
        public string State { get; set; }
        public string Semester { get; set; } 
    }
}
