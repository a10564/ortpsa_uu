﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Views
{
    [Table("nursingviewexcludebackgroundtable")]
    public class NursingViewExcludeBackgroundProcessTable : AuditedEntity<Guid>
    {
        public string CourseTypeCode { get; set; }
        public string CourseTypeCodeString { get; set; }
        public string RegistrationNumber { get; set; }
        public string RollNumber { get; set; }
        public string StudentName { get; set; }
        public string Nationality { get; set; }
        public string NationalityString { get; set; }
        public string Caste { get; set; }
        public string CasteString { get; set; }
        public string Gender { get; set; }
        public string GenderString { get; set; }
        public DateTime DateOfBirth { get; set; }
        public bool IsPwd { get; set; }
        public string IsPwdString { get; set; }
        public bool IsSameAsPresent { get; set; }
        public string PresentAddress { get; set; }
        public string PresentCountry { get; set; }
        public string PresentCountryCode { get; set; }
        public string PresentState { get; set; }
        public string PresentStateCode { get; set; }
        public string PresentCity { get; set; }
        public string PresentCityCode { get; set; }
        public string PresentZip { get; set; }
        public string PermanentAddress { get; set; }
        public string PermanentCountry { get; set; }
        public string PermanentCountryCode { get; set; }
        public string PermanentState { get; set; }
        public string PermanentStateCode { get; set; }
        public string PermanentCity { get; set; }
        public string PermanentCityCode { get; set; }
        public string PermanentZip { get; set; }
        public int YearOfMatriculation { get; set; }
        public int YearOfPassingPreMedical { get; set; }
        public string CollegeCode { get; set; }
        public string CollegeCodeString { get; set; }
        public string CourseCode { get; set; }
        public string CourseCodeString { get; set; }
        public string StreamCode { get; set; }
        public string StreamCodeString { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentCodeString { get; set; }
        public string SubjectCode { get; set; }
        public string SubjectCodeString { get; set; }
        public int AcademicStart { get; set; }
        public string CurrentAcademicYearString { get; set; }
        public string CurrentAcademicYear { get; set; }
        public string ApplicationType { get; set; }
        public string AcknowledgementNo { get; set; }
        public decimal FormFillUpMoneyToBePaid { get; set; } 
        public decimal InternetHandlingCharges { get; set; }
        public decimal LateFeeAmount { get; set; }
        public decimal CenterCharges { get; set; }
        public decimal TotalAmountToPaid { get; set; }
        public bool IsGnmQualified { get; set; }
        public bool IsHscQualified { get; set; }
        public decimal HscPercentage { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string CSCPayid { get; set; }
        public string PrincipalHODRemarks { get; set; }
        public long TenantWorkflowId { get; set; }
        public string State { get; set; }
        public long? ParentWorkflowId { get; set; }
        public string ParentWorkflowTypeName { get; set; }
        public DateTime? LastStatusUpdateDate { get; set; }
        public string ParallelActionExecutedForCurrentState { get; set; }
        public string DepartmentString { get; set; }
        public string CourseString { get; set; }
        public long ServiceTypeCode { get; set; }
        public string ServiceString { get; set; }
        public string MarkUploaded { get; set; }
    }
}
