﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Views
{
    [Table("user_view")]
    public class UserView :AuditedEntity<long>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public long? GeographyDetailId { get; set; }
        public string GeographyName { get; set; }
        public string ChangePassword { get; set; }
    }
}
