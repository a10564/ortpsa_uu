﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Views
{
    [Table("studentprofileview")]
    public class StudentProfileView : AuditedEntity<Guid>
    {
        public long UserId { get; set; }
        public string StudentName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string CourseType { get; set; }
        public string CollegeCode { get; set; }
        public string CourseCode { get; set; }
        public string StreamCode { get; set; }
        public string DepartmentCode { get; set; }
        public string SubjectCode { get; set; }
        public int YearOfAdmission { get; set; }
        public string CourseString { get; set; }
        public string StreamString { get; set; }
        public string SubjectString { get; set; }
        public string CollegeString { get; set; }
        public string CourseTypeString { get; set; }
        public string DepartmentString { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string States { get; set; }
        public string City { get; set; }
        public string CountryString { get; set; }
        public string StateString { get; set; }
        public string CityString { get; set; }
        public string ZipCode { get; set; }
    }
}
