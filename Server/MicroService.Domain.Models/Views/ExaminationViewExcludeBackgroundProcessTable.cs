﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Views
{
    [Table("examinationviewexcludebackgroundtable")]
    public class ExaminationViewExcludeBackgroundProcessTable : AuditedEntity<Guid>
    {
        //ExaminationFormFillUpDetails
        public string CourseTypeCode { get; set; }
        public string CourseTypeString { get; set; }
        public string RegistrationNumber { get; set; }
        public string RollNumber { get; set; }
        public string StudentName { get; set; }
        public string FatherName { get; set; }
        public string GuardianName { get; set; }
        public string Nationality { get; set; }
        public string Caste { get; set; }
        public string Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public bool IsSpeciallyAbled { get; set; }
        public bool IsSameAsPresent { get; set; }
        public int YearOfMatriculation { get; set; }
        public int YearOfPreviousExamPassed { get; set; }
        public string PreviousAcademicSubject { get; set; }
        public string CollegeCode { get; set; }
        public string CollegeString { get; set; }
        public string CourseCode { get; set; }
        public string CourseString { get; set; }
        public string StreamCode { get; set; }
        public string StreamString { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentString { get; set; }
        public string SubjectCode { get; set; }
        public string SubjectString { get; set; }
        public string SemesterCode { get; set; }
        public string SemesterString { get; set; }
        public string AcademicSession { get; set; }
        public string ApplicationType { get; set; }
        public string ApplicationString { get; set; }
        public int AcademicStart { get; set; }
        public string State { get; set; }
        public long TenantWorkflowId { get; set; }
        //Documents
        public string FileName { get; set; }
        //ExaminationFormFillUpStudentAddress
        public long ServiceTypeCode { get; set; }
        public string ServiceString { get; set; }
        public Decimal FormFillUpMoneyToBePaid { get; set; }
        public Decimal InternetHandlingCharges { get; set; }
        public string AcknowledgementNo { get; set; }
        public string MarkUploaded { get; set; }
        
    }
}
