﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Views
{
    [Table("mark_statement_view")]
    public class MarkStatementView : AuditedEntity<Guid>
    {       
        public string StudentName { get; set; }
        public string RegistrationNumber { get; set; }
        public string RollNumber { get; set; }
        public string AcknowledgementNo { get; set; }
        public string CourseTypeCode { get; set; }
        public string CourseTypeString { get; set; }
        public string CollegeCode { get; set; }
        public string CollegeName { get; set; }
        public string CourseCode { get; set; }
        public string DepartmentCode { get; set; }
        public string StreamCode { get; set; }
        public string StreamString { get; set; }
        public string SubjectCode { get; set; }
        public string SubjectName { get; set; }
        public string SemesterCode { get; set; }
        public string SemesterString { get; set; }
        public int AcademicStart { get; set; }
        public string AcademicSession { get; set; }
        public string State { get; set; }
        public string MarkUploaded { get; set; }
        public string PaperWiseMarkDetails { get; set; }        
    }
}
