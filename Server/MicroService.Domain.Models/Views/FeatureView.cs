﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Views
{
    [Table("feature_view")]
    public class FeatureView: AuditedEntity<long>
    {

        public long FeatureOrderId { get; set; }
        public long ParentId { get; set; }
        public string Name { get; set; }
        public long FeatureOrder { get; set; }
        public string FeatureOrderName { get; set; }
        public string ParentName { get; set; }
        public int Sequence { get; set; }
    }
}
