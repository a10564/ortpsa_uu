﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Views
{
    [Table("geography_view")]
    public class GeographyDetailView: AuditedEntity<long>
    {
        public int GeographyOrderId { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
        public int GeographyOrder { get; set; }
        public string GeographyOrderName { get; set; }
        public string ParentName { get; set; }
    }
}
