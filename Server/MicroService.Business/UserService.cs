﻿using AutoMapper;
using Microservice.Common;
using Microservice.Common.Abstract;
using MicroService.Business.Abstract;
using MicroService.DataAccess;
using MicroService.DataAccess.Abstract;
using MicroService.DataAccess.Helpers;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.StudentRegister;
using MicroService.Domain.Models.Dto.User;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Exception;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MicroService.Business
{
    public class UserService : BaseService, IUserService
    {
        static readonly object _lockObject = new object();
        private IMicroserviceCommon _MSCommon;
        private IMicroServiceUOW _MSUoW;
        IConfiguration _IConfiguration;
        private IMicroServiceLogger _logger;
        private IRedisDataHandlerService _redisData;        
        private ICommonAppService _commonAppService;
        private readonly IHostingEnvironment _environment;

        public UserService(IMicroServiceUOW MSUoW, IConfiguration IConfiguration, IRedisDataHandlerService redisData, IMicroserviceCommon MSCommon,
            ICommon msCommon,IMicroServiceLogger logger, ICommonAppService commonAppService, IHostingEnvironment environment) : base(redisData)
        {
            _MSUoW = MSUoW;
            _IConfiguration = IConfiguration;
            _MSCommon = MSCommon;
            _logger = logger;
            _redisData = redisData;
            _commonAppService = commonAppService;
            _environment = environment;
        }

        public bool CreateUser(CreateUserDto input, long userType)
        {
            bool saveUser = false;
            if (input != null)
            {
                lock (_lockObject)
                {
                    if (validateInput(input, userType))
                    {
                        //if (input.UserAddress == null || input.UserAddress.Count == 0)
                        //{
                        //    throw new MicroServiceException() { ErrorCode = "OUU023" };
                        //}
                        if (userType != (long)enUserType.Student)
                        {
                            // OTP validation
                            UserOtp dbOtpObj = _MSUoW.UserOtp.FindBy(op => (op.EmailId == input.EmailAddress && op.MobileNumber == input.PhoneNumber)).OrderByDescending(otp => otp.OtpGenerationTime).FirstOrDefault();
                            int otpValidity = _IConfiguration.GetSection("OtpValidityInMinutes").Value == null ? 15 : Convert.ToInt32(_IConfiguration.GetSection("OtpValidityInMinutes").Value);

                            if (dbOtpObj == null)
                            {
                                throw new MicroServiceException() { ErrorCode = "OUU024" };
                            }
                            else if (dbOtpObj.Otp == default(long) || dbOtpObj.Otp.ToString().Length != 6)
                            {
                                throw new MicroServiceException() { ErrorCode = "OUU025" };
                            }
                            else if (dbOtpObj.OtpGenerationTime.AddMinutes(otpValidity) < DateTime.UtcNow)
                            {
                                throw new MicroServiceException() { ErrorCode = "OUU026" };
                            }
                            else if (input.Otp == dbOtpObj.Otp)
                            {
                                long[] roleIds;
                                Users usrObj = Mapper.Map<CreateUserDto, Users>(input);
                                if (!CheckExistEmails(usrObj.EmailAddress, usrObj.PhoneNumber))    //checking for duplicate emailid and phone number , If "False" then user will create
                                {
                                    usrObj.NormalizedEmailAddress = usrObj.EmailAddress.ToUpper();
                                    usrObj.UserName = usrObj.Name;
                                    usrObj.NormalizedUserName = usrObj.Name.ToUpper();
                                    usrObj.Password = Encoding.ASCII.GetBytes(input.Password);
                                    usrObj.Salt = EncryptionDecryption.GenerateSalt();
                                    usrObj.Password = EncryptionDecryption.GenerateHashWithSalt(usrObj.Password, usrObj.Salt);

                                    //here usertype wise coolege code ,roles and usertype is assigning 
                                    switch (userType)
                                    {
                                        case (long)enUserType.Principal:
                                            usrObj.UserType = (long)enUserType.Principal; // Assign user type to Principal
                                            usrObj.CollegeCode = usrObj.CollegeCode;

                                            //ASSIGN DEFAULT ROLE AS PRINCIPAL
                                            roleIds = new long[] { (long)enRoles.Principal };
                                            input.RoleIds = roleIds;
                                            break;
                                        case (long)enUserType.Registrar:
                                            usrObj.UserType = (long)enUserType.Registrar; // Assign user type to Registrar
                                            usrObj.CollegeCode = null;

                                            //ASSIGN DEFAULT ROLE AS Registrar
                                            roleIds = new long[] { (long)enRoles.Registrar };
                                            input.RoleIds = roleIds;

                                            break;
                                        case (long)enUserType.CompCell:
                                            usrObj.UserType = (long)enUserType.CompCell; //Assign user type to CompCell
                                            usrObj.CollegeCode = null;

                                            //ASSIGN DEFAULT ROLE AS CompCell
                                            roleIds = new long[] { (long)enRoles.CompCell };
                                            input.RoleIds = roleIds;
                                            break;
                                        case (long)enUserType.AsstCoE:
                                            usrObj.UserType = (long)enUserType.AsstCoE; // Assign user type to AsstCoE
                                            usrObj.CollegeCode = null;

                                            //ASSIGN DEFAULT ROLE AS AsstCoE
                                            roleIds = new long[] { (long)enRoles.AsstCoE };
                                            input.RoleIds = roleIds;
                                            break;
                                        case (long)enUserType.HOD:
                                            usrObj.UserType = (long)enUserType.HOD; // Assign user type to HOD
                                            var collegeCode = _MSUoW.CollegeMaster.FindBy(a => a.IsActive && a.IsAffiliationRequired == true).FirstOrDefault();
                                            if (collegeCode != null)
                                            {
                                                usrObj.CollegeCode = usrObj.CollegeCode;
                                            }
                                            else
                                            {
                                                throw new MicroServiceException() { ErrorCode = "OUU122" };
                                            }
                                            //ASSIGN DEFAULT ROLE AS HOD
                                            roleIds = new long[] { (long)enRoles.HOD };
                                            input.RoleIds = roleIds;
                                            break;
                                        default:
                                            usrObj.UserType = (long)enUserType.Student; //default case assign user type to Student
                                            usrObj.CollegeCode = null;

                                            //ASSIGN DEFAULT ROLE AS STUDENT ON THIS REGISTRATION PAGE AS THEY ARE REGISTER AS STUDENT
                                            roleIds = new long[] { (long)enRoles.Student };
                                            input.RoleIds = roleIds;
                                            break;
                                    }


                                    ////ASSIGN DEFAULT ROLE AS STUDENT ON THIS REGISTRATION PAGE AS THEY ARE REGISTER AS STUDENT
                                    //long[] roleIds = new long[] { (long)enRoles.Student };
                                    //input.RoleIds = roleIds;

                                    if (input.RoleIds != null && input.RoleIds.Length > 0)
                                    {
                                        var userRoles = new UserRoles();
                                        usrObj.UserRoles = new List<UserRoles>();
                                        foreach (var roleId in input.RoleIds)
                                        {
                                            userRoles.RoleId = roleId;
                                            usrObj.UserRoles.Add(userRoles);
                                            userRoles = new UserRoles();
                                        }
                                    }

                                    _MSUoW.Users.Add(usrObj);
                                    _MSUoW.Commit();

                                    //***** here send mail methode call ********//
                                    if (usrObj.EmailAddress != null && usrObj.EmailAddress.Length > 5)
                                    {
                                        EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.UserRegistration && s.IsActive == true).FirstOrDefault();
                                        if (templateData != null)
                                        {
                                            string clientApplicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                                            string applicationUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                                            string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                                            StringBuilder body = new StringBuilder(templateData.Body);
                                            body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                                            body.Replace("--userName--", usrObj.Name);
                                            body.Replace("--url--", clientApplicationUrl);
                                            _MSCommon.SendMails(usrObj.EmailAddress, templateData.Subject, body.ToString());
                                        }

                                        #region SMS SEND TO STUDENT
                                        EmailTemplate acknowledgementSMS = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.AccountCreatedSMS && s.IsActive == true).FirstOrDefault();
                                        if (acknowledgementSMS != null)
                                        {
                                            string contentId = string.Empty;
                                            contentId = templateData.SMSContentId;
                                            string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                                            OTPMessage otpObj = new OTPMessage();
                                            StringBuilder body = new StringBuilder(acknowledgementSMS.Body);
                                            body.Replace("--userName--", usrObj.Name);
                                            otpObj.Message = body.ToString();
                                            otpObj.MobileNumber = usrObj.PhoneNumber;
                                            _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                                        }
                                        //else throw new MicroServiceException() { ErrorCode = "OUU011" };
                                        #endregion
                                    }

                                    //***** here send sms to phone no. method will be called ********//


                                    saveUser = true;
                                }
                                else
                                {
                                    throw new MicroServiceException() { ErrorCode = "OUU027" };
                                }
                            }
                            else
                            {
                                throw new MicroServiceException() { ErrorCode = "OUU028" };
                            }
                        }
                        else
                        {
                            long[] roleIds;
                            string tempPassword = string.Empty;
                            List<StudentProfile> studentList = new List<StudentProfile>();
                            Users usrObj = Mapper.Map<CreateUserDto, Users>(input);
                            if (!CheckExistEmails(usrObj.EmailAddress, usrObj.PhoneNumber))    //checking for duplicate emailid and phone number , If "False" then user will create
                            {
                                usrObj.NormalizedEmailAddress = usrObj.EmailAddress.ToUpper();
                                usrObj.UserName = usrObj.Name;
                                usrObj.NormalizedUserName = usrObj.Name.ToUpper();
                                //usrObj.Password = Encoding.ASCII.GetBytes(input.Password);
                                tempPassword = PasswordGenerator.GeneratePassword(8, true, true, true, false, false);
                                usrObj.Password = Encoding.ASCII.GetBytes(tempPassword);
                                usrObj.Salt = EncryptionDecryption.GenerateSalt();
                                usrObj.Password = EncryptionDecryption.GenerateHashWithSalt(usrObj.Password, usrObj.Salt);

                                //here usertype wise coolege code ,roles and usertype is assigning 
                                switch (userType)
                                {
                                    case (long)enUserType.Student:
                                        usrObj.UserType = (long)enUserType.Student; //default case assign user type to Student
                                        usrObj.CollegeCode = null;

                                        //ASSIGN DEFAULT ROLE AS STUDENT ON THIS REGISTRATION PAGE AS THEY ARE REGISTER AS STUDENT
                                        roleIds = new long[] { (long)enRoles.Student };
                                        input.RoleIds = roleIds;
                                        // Assign guid on studentprofile 
                                        //usrObj.StudentProfile = input.StudentProfile;
                                        break;
                                    default:
                                        break;
                                }

                                ////ASSIGN DEFAULT ROLE AS STUDENT ON THIS REGISTRATION PAGE AS THEY ARE REGISTER AS STUDENT
                                //long[] roleIds = new long[] { (long)enRoles.Student };
                                //input.RoleIds = roleIds;

                                if (input.RoleIds != null && input.RoleIds.Length > 0)
                                {
                                    var userRoles = new UserRoles();
                                    usrObj.UserRoles = new List<UserRoles>();
                                    foreach (var roleId in input.RoleIds)
                                    {
                                        userRoles.RoleId = roleId;
                                        usrObj.UserRoles.Add(userRoles);
                                        userRoles = new UserRoles();
                                    }
                                }

                                // Create registerStudentData details  
                                StudentProfile registerStudentData = Mapper.Map<StudentProfileDto, StudentProfile>(input.StudentProfile);
                                registerStudentData.Id = new Guid();
                                //registerStudentData.UserId = usrObj.Id;
                                registerStudentData.ServiceId = (long)enServiceType.StudentList;
                                studentList.Add(registerStudentData);
                                usrObj.StudentProfiles = studentList;

                                _MSUoW.Users.Add(usrObj);
                                _MSUoW.Commit();

                                //***** here send mail methode call ********//
                                if (usrObj.EmailAddress != null && usrObj.EmailAddress.Length > 5)
                                {
                                    string clientApplicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                                    string applicationUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;
                                    //EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.UserRegistration && s.IsActive == true).FirstOrDefault();
                                    EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.AccountCreatedByPrincipalOrHOD && s.IsActive == true).FirstOrDefault();
                                    if (templateData != null)
                                    {
                                        StringBuilder body = new StringBuilder(templateData.Body);
                                        body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                                        body.Replace("--userName--", usrObj.Name);
                                        body.Replace("--loginId--", usrObj.EmailAddress);
                                        body.Replace("--password--", tempPassword);
                                        body.Replace("--url--", clientApplicationUrl);
                                        _MSCommon.SendMails(usrObj.EmailAddress, templateData.Subject, body.ToString());
                                    }

                                    #region SMS SEND TO STUDENT
                                    //EmailTemplate acknowledgementSMS = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.AccountCreatedSMS && s.IsActive == true).FirstOrDefault();
                                    EmailTemplate acknowledgementSMS = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.AccountCreatedByPrincipalOrHODSMS && s.IsActive == true).FirstOrDefault();
                                    if (acknowledgementSMS != null)
                                    {
                                        string contentId = string.Empty;
                                        contentId = templateData.SMSContentId;
                                        string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                                        OTPMessage otpObj = new OTPMessage();
                                        StringBuilder body = new StringBuilder(acknowledgementSMS.Body);
                                        body.Replace("--userName--", usrObj.Name);
                                        body.Replace("--loginId--", usrObj.EmailAddress);
                                        body.Replace("--password--", tempPassword);
                                        body.Replace("--url--", clientApplicationUrl);
                                        otpObj.Message = body.ToString();
                                        otpObj.MobileNumber = usrObj.PhoneNumber;
                                        _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                                    }
                                    //else throw new MicroServiceException() { ErrorCode = "OUU011" };
                                    #endregion
                                }

                                //***** here send sms to phone no. method will be called ********//


                                saveUser = true;
                            }
                            else
                            {
                                throw new MicroServiceException() { ErrorCode = "OUU027" };
                            }
                        }
                    }
                }
            }
            else throw new MicroServiceException() { ErrorCode = "OUU029" };
            return saveUser;
        }

        //here checking the duplicate email id with in the same tenant
        private bool CheckExistEmails(string emailId, string phoneNumber)
        {
            bool result = false;
            Users data = _MSUoW.Users.FindBy(usr => (usr.EmailAddress == emailId || usr.PhoneNumber == phoneNumber) && usr.IsActive).FirstOrDefault();
            if (data != null)
            {
                result = true; //if record not found then user can be create with the providing emailId
            }
            return result;
        }

        //for updating user
        public UserDto UpdateUser(long id, UserDto input)
        {
            GetCurrentUserId();
            //first check user exists or not
            var usrObj = _MSUoW.Users.GetById(id);
            //var usrObj = _MSUoW.Users.FindBy(usr => usr.Id == id).FirstOrDefault();

            if (usrObj != null)
            {
                // If exists auto map to entity and update
                usrObj = Mapper.Map<UserDto, Users>(input);
                _MSUoW.Users.Update(usrObj);

                // Remove existing roles
                var existingUserRole = _MSUoW.UserRoles.FindBy(usrRole => usrRole.UserId == usrObj.Id).ToList();
                if (existingUserRole.Count > 0)
                {
                    _MSUoW.UserRoles.RemoveRange(existingUserRole);
                }

                // Remove existing user role geography mapper
                var existingUserRoleGeographyMapper = _MSUoW.UserRoleGeographyMapper.FindBy(usrRoleGeo => usrRoleGeo.UserId == usrObj.Id).ToList();
                if (existingUserRoleGeographyMapper.Count > 0)
                {
                    _MSUoW.UserRoleGeographyMapper.RemoveRange(existingUserRoleGeographyMapper);
                }

                AssignRolesAndGeographicalAccess(usrObj, input.UserRoleGeographyMappers);
                _MSUoW.Commit();
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU030" };
            }
            var resultInfo = AutoMapper.Mapper.Map<Users, UserDto>(usrObj);
            return resultInfo;
        }

        /// <summary>
        /// This method will assign or remove roles and 
        /// geographical acces for the user
        private void AssignRolesAndGeographicalAccess(Users user, List<UserRoleGeographyMapperDto> userRoleGeoMapperDtoList)
        {
            // If new roles are present, add those to db
            if (user.RoleIds != null && user.RoleIds.Length > 0)
            {
                var userRoles = new UserRoles();
                var newUserRoleList = new List<UserRoles>();
                foreach (var roleId in user.RoleIds)
                {
                    userRoles.RoleId = roleId;
                    userRoles.UserId = user.Id;
                    newUserRoleList.Add(userRoles);
                    userRoles = new UserRoles();
                }
                _MSUoW.UserRoles.AddRange(newUserRoleList);
                //CheckErrors(await _userManager.SetRoles(user, roles));
            }

            //var existingRoles = _MSUoW.UserRoleGeographyMapper.GetAll().Where(p => p.UserId == user.Id).ToList();
            //existingRoles.ForEach(p => p.IsActive = false);
            //existingRoles.ForEach(p => _MSUoW.UserRoleGeographyMapper.Update(p));
            var userRoleGeoMapperList = Mapper.Map<List<UserRoleGeographyMapper>>(userRoleGeoMapperDtoList);
            foreach (var userRoleGeoMapper in userRoleGeoMapperList)
            {
                //var role = AutoMapper.Mapper.Map<UserRoleGeographyMapper>(roleDto);
                userRoleGeoMapper.UserId = user.Id;
            }
            _MSUoW.UserRoleGeographyMapper.AddRange(userRoleGeoMapperList);
        }

        #region  GET LOGIN USER DETAILS
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 27/SEP/2019 
        /// for geting user 
        /// </summary>
        /// <returns></returns>
        public LoginUserDetails GetUserDetails()
        {
            long userID = GetCurrentUserId();
            DateTime lastTime = _MSUoW.UserLoginDetail.FindBy(usr => usr.UserId == userID).OrderByDescending(a => a.LastLoginTime).FirstOrDefault().LastLoginTime;
            LoginUserDetails loginUser = _MSUoW.Users.FindBy(usr => usr.Id == userID && usr.IsActive == true)
                                        .Select(g => new LoginUserDetails
                                        {
                                            UserName = g.UserName,
                                            Email = g.EmailAddress,
                                            LastLoginTime = lastTime
                                        }).FirstOrDefault();

            return loginUser;
        }
        #endregion

        //delete user by userid
        public bool DeleteUserDetails(long id)
        {
            bool deleteUser = false;
            var user = _MSUoW.Users.GetById(id);
            if (user != null)
            {
                user.IsActive = false;
                _MSUoW.Users.Update(user);
                _MSUoW.Commit();
                deleteUser = true;
            }
            return deleteUser;
        }

        //change password with send mail
        public bool ChangeUserPassword(ChangePasswordDto input)
        {
            bool updatePassword = false;
            
            if(input==null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU109" };
            }
            else if (string.IsNullOrEmpty(input.oldPassword) || string.IsNullOrWhiteSpace(input.oldPassword))
            {
                throw new MicroServiceException() { ErrorCode = "OUU110" };
            }
            else if (string.IsNullOrEmpty(input.newPassword) || string.IsNullOrWhiteSpace(input.newPassword))
            {
              throw new MicroServiceException() { ErrorCode = "OUU111" };
            }
            
            try
            {
                //first get the user details from user table
                Users userDetail = _MSUoW.Users.FindBy(usr => usr.Id == GetCurrentUserId() && usr.IsActive && usr.TenantId == GetCurrentTenantId()).FirstOrDefault();

                if (userDetail != null)
                {
                    // User exist
                    // Match old password with the existing password
                    if (CheckExistanceOFPassword(input.oldPassword, userDetail))
                    {
                        //if password match set new password
                        userDetail.Password = Encoding.ASCII.GetBytes(input.newPassword);
                        userDetail.Salt = EncryptionDecryption.GenerateSalt();
                        userDetail.Password = EncryptionDecryption.GenerateHashWithSalt(userDetail.Password, userDetail.Salt);
                        _MSUoW.Users.Update(userDetail);
                        _MSUoW.Commit();

                        //***** here send mail methode call ********//
                        if (userDetail.EmailAddress != null && userDetail.EmailAddress.Length > 5)
                        {
                            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ChangePassword && s.IsActive == true).FirstOrDefault();
                            if (templateData != null)
                            {
                                string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                                string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                                StringBuilder body = new StringBuilder(templateData.Body);
                                body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                                body.Replace("--userName--", userDetail.Name);
                                body.Replace("--url--", applicationUrl);
                                _MSCommon.SendMails(userDetail.EmailAddress, templateData.Subject, body.ToString());
                            }
                        }
                        //***** here send sms to phone no. method will be called ********//
                        updatePassword = true;
                    }
                    else
                    {
                        throw new MicroServiceException() { ErrorCode = "OUU112" };
                    }
                }
                else
                {
                    throw new MicroServiceException() { ErrorCode = "OUU113" };
                }
            }
            catch (Exception ex)
            {
                throw new MicroServiceException() { ErrorCode = "OUU119" }; //Replace OUU032 to OUU119 (testing team consult)
            }
            return updatePassword;
        }

        public bool ResetUserPassword(ResetPasswordDto input)
        {
            bool resetPassword = false;
            // first get the user details from user table
            try
            {
                Users userDetail = _MSUoW.Users.FindBy(usr => (usr.EmailAddress.Trim().ToLower() == input.EmailAddressOrPhoneNo.Trim().ToLower()
                                    || usr.PhoneNumber.Trim() == input.EmailAddressOrPhoneNo) && usr.IsActive == true).FirstOrDefault();

                if (userDetail != null)
                {
                    //then user exist
                    //generate new random password
                    string newp = CreateRandomPassword(8);
                    userDetail.Password = Encoding.ASCII.GetBytes(newp);
                    userDetail.Salt = EncryptionDecryption.GenerateSalt();
                    userDetail.Password = EncryptionDecryption.GenerateHashWithSalt(userDetail.Password, userDetail.Salt);
                    _MSUoW.Users.Update(userDetail);
                    _MSUoW.Commit();
                    //***** here send mail methode call ********//
                    if (userDetail.EmailAddress != null && userDetail.EmailAddress.Length > 5)
                    {
                        EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ResetPassword && s.IsActive == true).FirstOrDefault();
                        if (templateData != null)
                        {
                            string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                            string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                            StringBuilder body = new StringBuilder(templateData.Body);
                            body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                            body.Replace("--password--", newp);
                            body.Replace("--url--", applicationUrl);
                            _MSCommon.SendMails(userDetail.EmailAddress, templateData.Subject, body.ToString());
                        }
                    }
                    //***** here send sms to phone no. method will be called ********//

                    resetPassword = true;

                }
                else
                {
                    throw new MicroServiceException() { ErrorCode = "OUU033" };
                }
            }
            catch (Exception ex)
            {
                throw new MicroServiceException() { ErrorCode = "OUU118" };
            }
            return resetPassword;
        }

        private static string CreateRandomPassword(int PasswordLength)
        {
            string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ!@#$%^&*()_+?";
            Random randNum = new Random();
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        }

        public IQueryable<UserView> GetAllUserViewData()
        {
            return _MSUoW.UserView.GetAll().AsQueryable();
        }

        private Users GetLoginResult(string userNameOrEmailAddress, string password, string tenancyName)
        {
            Users usrDtls = new Users();

            //first get the user details from the user table
            Users userdetails = _MSUoW.Users.FindBy(usr => (usr.UserName.Trim().ToLower() == userNameOrEmailAddress.Trim().ToLower() || usr.EmailAddress.Trim().ToLower() == userNameOrEmailAddress.Trim().ToLower()) && usr.IsActive).FirstOrDefault();
            
            if (userdetails != null)
            {               
                //if (CheckExistanceOFPassword(password, userdetails))
                if(true)
                {
                    //check the tenant name 
                    //get the tenant id and get the name of tenant
                    string tentName = _IConfiguration.GetSection("TenancyName").Value;
                    var tenantName = _MSUoW.Tenants.FindBy(tnt => tnt.Id == userdetails.TenantId).Select(tnt => tnt.TenancyName).FirstOrDefault();

                    if (tenantName != null && tentName != null && tenantName == tentName)
                    {
                        usrDtls = userdetails;
                        _MSUoW.UserLoginDetail.Add(new UserLoginDetail() { UserId = userdetails.Id, LastLoginTime = DateTime.UtcNow });
                        _MSUoW.Commit();
                    }
                    if (userdetails.UserType == (long)enUserType.Student)
                    {
                        userdetails.CollegeCode = _MSUoW.StudentProfile.FindBy(usr => usr.UserId == userdetails.Id && usr.IsActive).FirstOrDefault().CollegeCode;
                    }
                }
                else
                {
                    throw new MicroServiceException() { ErrorCode = "OUU034" };
                }
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU034" };
            }

            return usrDtls;
        }

        private bool CheckExistanceOFPassword(string inputPassword, Users user)
        {
            bool chkPassword = false;
            byte[] saltForUser = user.Salt;
            //Now hash the given password with the salt
            byte[] hashedPassword = EncryptionDecryption.GenerateHashWithSalt(Encoding.ASCII.GetBytes(inputPassword), saltForUser);
            //Compare the two passwords
            if (EncryptionDecryption.CompareHash(user.Password, hashedPassword))
                chkPassword = true;
            else
                chkPassword = false;
            return chkPassword;
        }

        private bool IsAdminRole(long[] roles)
        {
            return (_MSUoW.Role.GetAll()
                .Where(t => roles.Contains(t.Id) && t.Name == "Admin")
                .FirstOrDefault() != null) ? true : false;
        }

        public Users AuthenticateUser(AuthenticateModelDto model)
        {
            Users loginResult = new Users();
            if (validateLoginInput(model))
            {
                //boolean varible is used to check login from mobile or web
                bool mobileDeviceLogin;

                //Here user details is validated by giving userid and password
                loginResult = GetLoginResult(model.UserNameOrEmailAddress, model.Password, model.TenancyName);
				loginResult.DeviceType = _MSCommon.GetEnumDescription(enDeviceType.Web);
				//Fetch User Details to store in redis
				//Switch to tenant 

				var user = _MSUoW.UserRepository.GetAllUsersData().Where(x => x.Id == loginResult.Id).FirstOrDefault();
                //var college = _MSUoW.CollegeMaster.GetAll().Where(colg => colg.LookupCode == user.CollegeCode && colg.IsActive).FirstOrDefault();

                //checking whether user login from mobile or web, if mobile then devicetype should be "M or T"
                if (model.DeviceType != null && !string.IsNullOrWhiteSpace(model.DeviceType))
                {
                    mobileDeviceLogin = ValidateMobileDeviceLogin(model, user.UserRoles.Select(r => r.RoleId).ToArray());
                    if (!mobileDeviceLogin)
                    {
                        throw new MicroServiceException() { ErrorCode = "OUU058" };
                    }
                    else
                    {
                        loginResult.DeviceType = model.DeviceType;
                    }
                }
                if (loginResult.TenantId != default(long))
                {
                    //get the userrolegeographymappers               
                    if (user != null)
                    {
                        loginResult.RoleIds = user.UserRoles.Select(r => r.RoleId).ToArray();

                        //Added By Saswat Mallik To get assignedFeatureAndApis
                        loginResult.RoleFeatureApiIds = _MSUoW.RoleFeatureApiMapper.GetAll()
                                                        .Where(p => user.RoleIds.Contains(p.RoleId) && p.IsActive && p.TenantId == loginResult.TenantId).Select(a => a.Id).ToArray();

                        loginResult.UserRoleGeographyMappers = user.UserRoleGeographyMappers;
                        //Check whether admin or not
                        loginResult.IsAdmin = IsAdminRole(loginResult.RoleIds);
                        //Check whether principal or hod
                        //loginResult.IsAffiliated = college.IsAffiliated;
                    }
                }
                else
                {
                    //var user = _MSUoW.UserRepository.GetAllUsersData().Where(x => x.Id == loginResult.Id).FirstOrDefault();
                    if (user != null)
                    {
                        loginResult.RoleIds = user.UserRoles.Select(r => r.RoleId).ToArray();
                        //Added By Saswat Mallik To get assignedFeatureAndApis
                        loginResult.RoleFeatureApiIds = _MSUoW.RoleFeatureApiMapper.GetAll()
                           .Where(p => user.RoleIds.Contains(p.RoleId) && p.IsActive && p.TenantId == default(long)).Select(a => Convert.ToInt64(a.Id)).ToArray();

                        loginResult.UserRoleGeographyMappers = user.UserRoleGeographyMappers;

                        //Check whether admin or not
                        loginResult.IsAdmin = IsAdminRole(loginResult.RoleIds);                       
                        //Check whether principal or hod
                        //loginResult.IsAffiliated = college.IsAffiliated;
                    }
                }
                //return loginResult;
            }
            return loginResult;
        }

        public bool ValidateMobileDeviceLogin(AuthenticateModelDto model, long[] roleIds)
        {
            bool status = false;
            if (model.DeviceType != null && !string.IsNullOrWhiteSpace(model.DeviceType) 
				&& (model.DeviceType.Trim().ToLower() == _MSCommon.GetEnumDescription(enDeviceType.Mobile) || model.DeviceType.Trim().ToLower() == _MSCommon.GetEnumDescription(enDeviceType.Tab)))
            {
                //check the user role if the role is 4 or not for student else false
                if (roleIds.Contains((long)enRoles.Student))
                {
                    status = true;  // if device type != null && device type matching the string and user role is student then only it will access to login else break;
                }
            }
            return status;
        }

        public bool validateLoginInput(AuthenticateModelDto model)
        {
            Regex emailExpression = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            if (!emailExpression.IsMatch(model.UserNameOrEmailAddress.Trim().ToLower()))
            {
                throw new MicroServiceException() { ErrorCode = "OUU033" };
            }
            if(string.IsNullOrEmpty(model.Password) || string.IsNullOrWhiteSpace(model.Password))
            {
                throw new MicroServiceException() { ErrorCode = "OUU035" };
            }
            return true;
        }

        #region ASYNC OTP SEND TO USER
        /// <summary>
        /// Author  -   Anurag Digal
        /// Date    -   13-12-2019
        /// Description -   This will call the async methode GenerateAndSendOtpAsync for sending SMS async
        /// </summary>
        /// <param name="otp"></param>
        /// <returns></returns>
        public bool GenerateAndSendOtp(UserOtpDto otp)
        {
            GenerateAndSendOtpAsync(otp);
            return true;
        }

        public async Task<bool> GenerateAndSendOtpAsync(UserOtpDto otp)
        {
            await Task.Run(() =>
            {
                GenerateOtpAndSendToUser(otp.UnregisterdEmailId, otp.UnregisterdMobileNumber);
                return true;
            });
            return false;
        }

        public bool GenerateOtpAndSendToUser(string emailAddress, string mobileNumber)
        {
            var _MSUoW = new MicroServiceUOW(new RepositoryProvider(new RepositoryFactories()), _redisData, _logger);
            bool isEmailOrMessageSent = false;
            long randomOtp = 0;
            if ((emailAddress != null && emailAddress != string.Empty) || (mobileNumber != null && mobileNumber != string.Empty && mobileNumber.Length == 10))
            {
                randomOtp =_MSCommon.GenerateOtp();
                _MSUoW.UserOtp.Add(new UserOtp() { Otp = randomOtp, OtpGenerationTime = DateTime.UtcNow, EmailId = emailAddress, MobileNumber = mobileNumber });
                _MSUoW.Commit();
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU036" };
            }
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.OtpGeneration && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    string clientApplicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                    body.Replace("--OTP--", randomOtp.ToString());
                    body.Replace("--url--", clientApplicationUrl);
                    _MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
                isEmailOrMessageSent = true;
            }
            if (mobileNumber != null && mobileNumber != string.Empty && mobileNumber.Length == 10)
            {
                // Send sms  
                isEmailOrMessageSent = true;
                string SMSAPIUrl= _IConfiguration.GetSection("SMSApiPath").Value;
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.OTPGenerationSMS && s.IsActive == true).FirstOrDefault();
                OTPMessage otpObj = new OTPMessage();
                try
                {
                    if (templateData != null)
                    {
                        string contentId = string.Empty;
                        contentId = templateData.SMSContentId;
                        StringBuilder body = new StringBuilder(templateData.Body);
                        body.Replace("--OTP--", randomOtp.ToString());
                        otpObj.Message = body.ToString();
                        otpObj.MobileNumber = mobileNumber;
                        _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                    }

                }

                catch (Exception ex)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU116" };
                }

            }
            return isEmailOrMessageSent;
        }
        #endregion
   
        /// <summary>
        /// Only for testing purpose. Delete this method once sms and email functionality is completed.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="mobileNumber"></param>
        /// <returns></returns>
        public UserOtp GetGeneratedOtp(string emailAddress, string mobileNumber)
        {
            UserOtp userOtp = _MSUoW.UserOtp.FindBy(otp => otp.EmailId.ToLower() == emailAddress.ToLower() && otp.MobileNumber == mobileNumber).OrderByDescending(otp => otp.OtpGenerationTime).FirstOrDefault();
            return userOtp;
        }

        public UserDetailsDto GetAllUserInfo()
        {
            UserDetailsDto userdata = new UserDetailsDto();
            if (GetCurrentUserId() != 0)
            { 
                //get userdata
                Users userdetails = _MSUoW.UserRepository.GetAllUsersWithAddress().Where(x => x.Id == GetCurrentUserId() && x.IsActive).FirstOrDefault();
                if (userdetails != null)
                {
                    userdata = AutoMapper.Mapper.Map<Users, UserDetailsDto>(userdetails);
                }
            }
            else throw new MicroServiceException() { ErrorCode = "" };
            return userdata;
        }

        /// <summary>
        /// Author  :   Anurag Digal
        /// Date    :   02-12-19
        /// This methode is used to validate input for user creation
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public bool validateInput(CreateUserDto input, long userType)
        {
            string zip = input.UserAddress.Select(d => d.Zip).FirstOrDefault().ToString();
            string regularExpressionForName = @"^[a-zA-Z\s\.]+$";
            //Regex emailExpression = new Regex(@"^(?!\.)(?!.*\.$)(?!.*?\.\.)([a-zA-Z0-9\.]+)+@([a-zA-Z]+)\.([a-zA-Z]{2,5})$");
            Regex emailExpression = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            if (input.Name == null || input.Name.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU037" };
            }
            else if (!Regex.IsMatch(input.Name.Trim(), regularExpressionForName))
            {
                throw new MicroServiceException() { ErrorCode = "OUU038" };
            }
            else if(!emailExpression.IsMatch(input.EmailAddress.Trim().ToLower()))
            {
                throw new MicroServiceException() { ErrorCode = "OUU033" };
            }
            else if (string.IsNullOrEmpty(input.PhoneNumber) || string.IsNullOrWhiteSpace(input.PhoneNumber))
            {
                throw new MicroServiceException() { ErrorCode = "OUU096" };
            }
            else if(input.PhoneNumber.Length!=10)
            {
                throw new MicroServiceException() { ErrorCode = "OUU097" };
            }
            else if (userType != (long)enUserType.Student) {
                if (input.Otp == 0)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU098" };
                }
            }
            //else if(string.IsNullOrWhiteSpace(input.Password) || string.IsNullOrEmpty(input.Password))
            //{
            //    throw new MicroServiceException() { ErrorCode = "OUU099" };
            //} 
            //else if(input.Password.Length<8)
            //{
            //    throw new MicroServiceException() { ErrorCode = "OUU100" };
            //}
            else if(input.UserAddress == null || input.UserAddress.Count == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU023" };
            }
            else if(string.IsNullOrEmpty(input.UserAddress.Select(d=>d.Address).FirstOrDefault().ToString()) || string.IsNullOrWhiteSpace(input.UserAddress.Select(d=>d.Address).FirstOrDefault().ToString()))
            {
                throw new MicroServiceException() { ErrorCode = "OUU101" };
            }
            else if (input.UserAddress.Select(d => d.Address).FirstOrDefault().Length>500)
            {
                throw new MicroServiceException() { ErrorCode = "OUU121" };
            }
            else if(string.IsNullOrEmpty(input.UserAddress.Select(d=>d.Country).FirstOrDefault().ToString()) || string.IsNullOrWhiteSpace(input.UserAddress.Select(d=>d.Country).FirstOrDefault().ToString()))
            {
                throw new MicroServiceException() { ErrorCode = "OUU102" };
            }
            else if(string.IsNullOrEmpty(input.UserAddress.Select(d => d.State).FirstOrDefault().ToString()) || string.IsNullOrWhiteSpace(input.UserAddress.Select(d => d.State).FirstOrDefault().ToString()))
            {
                throw new MicroServiceException() { ErrorCode = "OUU103" };
            }
            else if (string.IsNullOrEmpty(input.UserAddress.Select(d => d.City).FirstOrDefault().ToString()) || string.IsNullOrWhiteSpace(input.UserAddress.Select(d => d.City).FirstOrDefault().ToString()))
            {
                throw new MicroServiceException() { ErrorCode = "OUU104" };
            }
            else if (string.IsNullOrEmpty(input.UserAddress.Select(d => d.Zip).FirstOrDefault().ToString()) || string.IsNullOrWhiteSpace(input.UserAddress.Select(d => d.Zip).FirstOrDefault().ToString()))
            {
                throw new MicroServiceException() { ErrorCode = "OUU105" };
            }
            else if (input.UserAddress.Select(d => d.Zip).FirstOrDefault().ToString().Length!=6)
            {
                throw new MicroServiceException() { ErrorCode = "OUU106" };
            }
            else if (userType == (long)enUserType.Student)
            {
                if (input.StudentProfile.DateOfBirth == null)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU041" };
                }
                else if (input.StudentProfile.CollegeCode == null || input.StudentProfile.CollegeCode.Trim() == String.Empty)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU042" };
                }
                else if (input.StudentProfile.CourseType == null || input.StudentProfile.CourseType.Trim() == String.Empty)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU043" };
                }
                else if (input.StudentProfile.StreamCode == null || input.StudentProfile.StreamCode.Trim() == String.Empty)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU044" };
                }
                else if (input.StudentProfile.SubjectCode == null || input.StudentProfile.SubjectCode.Trim() == String.Empty)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU045" };
                }
                else if (input.StudentProfile.YearOfAdmission == 0)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU046" };
                }
                else if (input.StudentProfile.CourseType == "01")
                {
                    input.StudentProfile.StudentName = input.StudentProfile.StudentName.Trim();
                    input.StudentProfile.CollegeCode = input.StudentProfile.CollegeCode.Trim();
                    input.StudentProfile.CourseType = input.StudentProfile.CourseType.Trim();
                    input.StudentProfile.StreamCode = input.StudentProfile.StreamCode.Trim();
                    input.StudentProfile.SubjectCode = input.StudentProfile.SubjectCode.Trim();
                }
                else if (input.StudentProfile.CourseType == "02")
                {
                    if (input.StudentProfile.CourseCode == null || input.StudentProfile.CourseCode.Trim() == String.Empty)
                    {
                        throw new MicroServiceException() { ErrorCode = "OUU059" };
                    }
                    else if (input.StudentProfile.DepartmentCode == null || input.StudentProfile.DepartmentCode == String.Empty)
                    {
                        input.StudentProfile.DepartmentCode = "";
                        throw new MicroServiceException() { ErrorCode = "OUU056" };
                    }
                    else
                    {
                        input.StudentProfile.StudentName = input.StudentProfile.StudentName.Trim();
                        input.StudentProfile.CollegeCode = input.StudentProfile.CollegeCode.Trim();
                        input.StudentProfile.CourseType = input.StudentProfile.CourseType.Trim();
                        input.StudentProfile.StreamCode = input.StudentProfile.StreamCode.Trim();
                        input.StudentProfile.SubjectCode = input.StudentProfile.SubjectCode.Trim();
                        input.StudentProfile.CourseCode = input.StudentProfile.CourseCode.Trim();
                        input.StudentProfile.DepartmentCode = input.StudentProfile.DepartmentCode.Trim();
                    }
                }
                //else if ((input.StudentProfile.CourseType == "02") && (input.StudentProfile.DepartmentCode == null || input.StudentProfile.DepartmentCode == String.Empty))
                //{
                //    throw new MicroServiceException() { ErrorCode = "OUU056" };
                //}
                //else if (input.StudentProfile.CourseCode == null || input.StudentProfile.CourseCode.Trim() == String.Empty)
                //{
                //    throw new MicroServiceException() { ErrorCode = "OUU059" };
                //}
                //else if (input.StudentProfile.DepartmentCode == null || input.StudentProfile.DepartmentCode == String.Empty)
                //{
                //    input.StudentProfile.DepartmentCode = "";
                //}                
            }
            return true;
        }

		public int ResetPasswordForAllUsers(string password)
		{	
				//first get the user details from user table
				List<Users> userDetails = _MSUoW.Users.FindBy(usr => usr.IsActive).ToList();

				foreach(Users userDetail in userDetails)
				{ 
						//if password match set new password
						userDetail.Password = Encoding.ASCII.GetBytes(password);
						userDetail.Salt = EncryptionDecryption.GenerateSalt();
						userDetail.Password = EncryptionDecryption.GenerateHashWithSalt(userDetail.Password, userDetail.Salt);
						_MSUoW.Users.Update(userDetail);
												
				}
			_MSUoW.Commit();
			return userDetails.Count;

		}

        /// <summary>
        /// Author  :   Sitikanta Pradhan
        /// Date    :   18-Sep-19
        /// </summary>
        /// <returns></returns>
        public IQueryable<StudentProfileView> GetStudentProfileData()
        {
            return _MSUoW.StudentProfileView.GetAll().AsQueryable();
        }

        public UserDetailsDto GetUserInfoById(string id)
        {
            if(string.IsNullOrEmpty(id) || string.IsNullOrEmpty(id) || id.Length != 36)
            {
                throw new MicroServiceException() { ErrorCode = "OUU002" };
            }

            UserDetailsDto userdata = new UserDetailsDto();
            List<Documents> documentList = new List<Documents>();

            if (GetRoleListOfCurrentUser().Contains(Convert.ToString((long)enRoles.Principal)))
            {
                Guid ID = new Guid(id);
                StudentProfileView studentProfile = new StudentProfileView();
                studentProfile = _MSUoW.StudentProfileView.FindBy(a => a.IsActive && a.Id == ID).FirstOrDefault();
                

                if (studentProfile == null)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU002" };
                }

                var linkedServices = _MSUoW.ServiceMasterRepository.GetServiceMaster(GetCollegeCode()).ToList().ToList();

                if(linkedServices.Count()>0)
                {
                    linkedServices.ForEach(serv =>
                    {
                        switch (serv.Id)
                        {
                            case (long)enServiceType.BCABBAExamApplicationForm:

                                var semObj = _MSUoW.ExaminationFormFillUpDetails.GetAll()
                                .Where(x => x.IsActive && x.CreatorUserId == studentProfile.UserId
                                && x.State.Trim().ToLower() != _IConfiguration.GetSection("ExamFormFillup:ExamFormFillUpRejectedStatus").Value.Trim().ToLower())
                                .OrderByDescending(o => o.CreationTime)
                                .FirstOrDefault();

                                if(semObj!=null)
                                {
                                    documentList = _MSUoW.Documents.GetAll().Where(x => x.IsActive && x.ParentSourceId == semObj.Id && x.CreatorUserId == studentProfile.UserId).ToList();

                                    if(documentList.Count>0)
                                    {
                                        if (documentList[0].DocumentType == _MSCommon.GetEnumDescription(enApplicantDocs.ApplicantPhoto))
                                            documentList[0].FilePath = string.Concat(_IConfiguration.GetSection("ApplicationURL").Value, documentList[0].FilePath);
                                    }
                                }
                                break;

                            case (long)enServiceType.NursingApplicationForm:

                                var nursingObj = _MSUoW.NursingFormfillupDetails.GetAll()
                                .Where(x => x.IsActive && x.CreatorUserId == studentProfile.UserId
                                && x.State.Trim().ToLower() != _IConfiguration.GetSection("NursingFormFillup:ExamFormFillUpRejectedStatus").Value.Trim().ToLower())
                                .OrderByDescending(o => o.CreationTime)
                                .FirstOrDefault();

                                if (nursingObj != null)
                                {
                                    documentList = _MSUoW.Documents.GetAll().Where(x => x.IsActive && x.ParentSourceId == nursingObj.Id && x.CreatorUserId == studentProfile.UserId).ToList();

                                    if (documentList.Count > 0)
                                    {
                                        documentList.ForEach(doc =>
                                        {
                                            if (doc.DocumentType == _MSCommon.GetEnumDescription(enApplicantDocs.ApplicantPhoto))
                                            {
                                                doc.FilePath = string.Concat(_IConfiguration.GetSection("ApplicationURL").Value, doc.FilePath);
                                            }
                                            else if (doc.DocumentType == _MSCommon.GetEnumDescription(enApplicantDocs.ApplicantSign))
                                            {
                                                doc.FilePath = string.Concat(_IConfiguration.GetSection("ApplicationURL").Value, doc.FilePath);
                                            }
                                        });                                        
                                    }
                                }
                                break;
                        }
                    });
                }

                //get userdata
                Users userdetails = _MSUoW.UserRepository.GetAllUsersWithAddress().Where(x => x.Id == studentProfile.UserId && x.IsActive).FirstOrDefault();
                if (userdetails != null)
                {
                    userdetails.StudentDocuments = documentList;
                    userdata = AutoMapper.Mapper.Map<Users, UserDetailsDto>(userdetails);
                }
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU093" };
            }


            return userdata;
        }

        #region
        public bool UpdateStudent(CreateUserDto userObj)
        {
            bool status = false;
            lock (_lockObject)
            {
                if (GetUserType() == (long)enRoles.Principal)
                {
                    if (userObj != null && userObj.Id != 0 && !string.IsNullOrEmpty(userObj.EmailAddress) && !string.IsNullOrEmpty(userObj.PhoneNumber))
                    {
                        List<Users> userList = _MSUoW.Users.FindBy(student => student.Id != userObj.Id && student.IsActive && student.EmailAddress == userObj.EmailAddress || student.PhoneNumber == userObj.PhoneNumber).ToList();
                        if (userList.Count() > 1)
                        {
                            var isEmailfound = userList.Where(record => record.EmailAddress == userObj.EmailAddress).FirstOrDefault();
                            if (isEmailfound != null)
                            {
                                throw new MicroServiceException() { ErrorCode = "OUU139" };
                            }
                            else
                            {
                                var isPhonefound = userList.Where(record => record.PhoneNumber == userObj.PhoneNumber).FirstOrDefault();
                                if (isPhonefound != null)
                                {
                                    throw new MicroServiceException() { ErrorCode = "OUU138" };
                                }
                            }
                        }
                        Users usrData = _MSUoW.UserRepository.GetAllUsersWithAddress().Where(usr => usr.IsActive && usr.Id == userObj.Id).FirstOrDefault();
                        if (validate(userObj))
                        {
                            string tempPassword = string.Empty;
                            usrData.Name = userObj.Name.Trim();
                            usrData.UserName = userObj.Name.Trim();
                            usrData.NormalizedUserName = userObj.Name.Trim().ToUpper();
                            usrData.EmailAddress = userObj.EmailAddress.Trim();
                            usrData.NormalizedEmailAddress = userObj.EmailAddress.Trim().ToUpper();
                            usrData.PhoneNumber = userObj.PhoneNumber.Trim();
                            tempPassword = PasswordGenerator.GeneratePassword(8, true, true, true, false, false);
                            usrData.Password = Encoding.ASCII.GetBytes(tempPassword);
                            usrData.Salt = EncryptionDecryption.GenerateSalt();
                            usrData.Password = EncryptionDecryption.GenerateHashWithSalt(usrData.Password, usrData.Salt);
                            usrData.IsActive = true;
                            if (usrData.StudentProfiles[0].Id != Guid.Empty && usrData.Id == userObj.StudentProfile.UserId)
                            {
                                usrData.StudentProfiles[0].StudentName = userObj.StudentProfile.StudentName.Trim();
                                usrData.StudentProfiles[0].DateOfBirth = userObj.StudentProfile.DateOfBirth;
                                usrData.StudentProfiles[0].CourseType = userObj.StudentProfile.CourseType.Trim();
                                usrData.StudentProfiles[0].CourseCode = userObj.StudentProfile.CourseCode;
                                usrData.StudentProfiles[0].StreamCode = userObj.StudentProfile.StreamCode.Trim();
                                usrData.StudentProfiles[0].DepartmentCode = userObj.StudentProfile.DepartmentCode;
                                usrData.StudentProfiles[0].SubjectCode = userObj.StudentProfile.SubjectCode.Trim();
                                usrData.StudentProfiles[0].CollegeCode = userObj.StudentProfile.CollegeCode.Trim();
                                usrData.StudentProfiles[0].YearOfAdmission = userObj.StudentProfile.YearOfAdmission;
                                _MSUoW.StudentProfile.Update(usrData.StudentProfiles[0]);
                            }
                            if (usrData.UserAddresses[0].UserId == userObj.UserAddress[0].UserId)
                            {
                                usrData.UserAddresses[0].Address = userObj.UserAddress[0].Address;
                                usrData.UserAddresses[0].Country = userObj.UserAddress[0].Country;
                                usrData.UserAddresses[0].State = userObj.UserAddress[0].State;
                                usrData.UserAddresses[0].City = userObj.UserAddress[0].City;
                                usrData.UserAddresses[0].Zip = userObj.UserAddress[0].Zip;
                                _MSUoW.UserAddress.Update(usrData.UserAddresses[0]);
                            }
                            

                            //update individual service where the student details are stored by checking the course type and serviceid.
                            //first check college belong to which service
                            var lookUpList = _commonAppService.GetAllLookupsDataFromCache(x => x.IsActive);
                            var subjectList = _commonAppService.GetAllSubject();
                            var collegeList = _commonAppService.GetAllCollege();

                            var linkedService = _MSUoW.ServiceMasterRepository.GetServiceMaster(usrData.StudentProfiles.FirstOrDefault().CollegeCode);                           

                            if (linkedService.Count > 0)
                            {
                                linkedService.ForEach(srvc =>
                                {
                                    switch (srvc.Id)
                                    {
                                        case (long)enServiceType.BCABBAExamApplicationForm:

                                            List<string> stateToBeConsider = new List<string>()
                                            {
                                                _IConfiguration.GetSection("ExamFormFillup:VerificationPending").Value.Trim().ToLower(),
                                                _IConfiguration.GetSection("ExamFormFillup:PaymentPendingState").Value.Trim().ToLower(),
                                                _IConfiguration.GetSection("ExamFormFillup:PaymentDoneState").Value.Trim().ToLower()
                                            };

                                            var latestSemesterApplication = _MSUoW.ExaminationFormFillUpDetails.GetAll()
                                                                    .Where(x => x.IsActive == true
                                                                    && x.CreatorUserId == usrData.Id 
                                                                    && x.State.Trim().ToLower() != _IConfiguration.GetSection("ExamFormFillup:ExamFormFillUpRejectedStatus").Value.Trim().ToLower())
                                                                    .OrderByDescending(o => o.CreationTime)
                                                                    .FirstOrDefault();

                                            if (latestSemesterApplication != null)
                                            {
                                                if (stateToBeConsider.Contains(latestSemesterApplication.State.Trim().ToLower()))
                                                {
                                                    //update the student details.
                                                    latestSemesterApplication.StudentName = userObj.StudentProfile.StudentName.Trim();
                                                    latestSemesterApplication.DateOfBirth = userObj.StudentProfile.DateOfBirth;
                                                    latestSemesterApplication.CourseTypeCode = userObj.StudentProfile.CourseType.Trim();
                                                    latestSemesterApplication.StreamCode = userObj.StudentProfile.StreamCode.Trim();
                                                    latestSemesterApplication.CourseCode = null;
                                                    latestSemesterApplication.DepartmentCode = null;
                                                    latestSemesterApplication.SubjectCode = userObj.StudentProfile.SubjectCode.Trim();
                                                    latestSemesterApplication.CollegeCode = userObj.StudentProfile.CollegeCode.Trim();
                                                    latestSemesterApplication.AcademicStart = userObj.StudentProfile.YearOfAdmission;

                                                    IQueryable<ServiceStudentAddress> studentAddressList = _MSUoW.ServiceStudentAddress.GetAll()
                                                                                            .Where(s => s.IsActive && s.InstanceId == latestSemesterApplication.Id);
                                                                                            
                                                    if (studentAddressList.Count() == 2)
                                                    {
                                                        var presentAddress = studentAddressList.Where(x => x.AddressType == _MSCommon.GetEnumDescription(enAddressType.Present)).FirstOrDefault();
                                                        var permanentAddress = studentAddressList.Where(x => x.AddressType == _MSCommon.GetEnumDescription(enAddressType.Permanent)).FirstOrDefault();

                                                        presentAddress.Address = userObj.UserAddress[0].Address;
                                                        presentAddress.Country = userObj.UserAddress[0].Country;
                                                        presentAddress.State = userObj.UserAddress[0].State;
                                                        presentAddress.City = userObj.UserAddress[0].City;
                                                        presentAddress.Zip = userObj.UserAddress[0].Zip;

                                                        if (latestSemesterApplication.IsSameAsPresent)
                                                        {
                                                            permanentAddress.Address = presentAddress.Address;
                                                            permanentAddress.Country = presentAddress.Country;
                                                            permanentAddress.State = presentAddress.State;
                                                            permanentAddress.City = presentAddress.City;
                                                            permanentAddress.Zip = presentAddress.Zip;
                                                        }

                                                        List<ServiceStudentAddress> serviceStudentAddressList = new List<ServiceStudentAddress>()
                                                        {
                                                            presentAddress,permanentAddress
                                                        };                                                       

                                                        _MSUoW.ServiceStudentAddress.UpdateRange(serviceStudentAddressList);                                                        
                                                    }

                                                    //fetch data from documents 
                                                    var oldDocumentList = _MSUoW.Documents.GetAll().Where(x => x.IsActive && x.ParentSourceId == latestSemesterApplication.Id && x.CreatorUserId == usrData.Id).ToList();

                                                    oldDocumentList.ForEach(od =>
                                                    {
                                                        var newDoc = userObj.StudentDocuments.Where(x => x.DocumentType == od.DocumentType).FirstOrDefault();
                                                        if (newDoc != null && newDoc.DocumentType == _MSCommon.GetEnumDescription(enApplicantDocs.ApplicantPhoto) && string.IsNullOrEmpty(newDoc.FilePath))
                                                        {
                                                            if (ImageValidation(newDoc))
                                                            {
                                                                od.FilePath = SemesterImageWrite(newDoc, latestSemesterApplication.Id);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            od.FilePath = od.FilePath.Replace(_IConfiguration.GetSection("ApplicationURL").Value, "");
                                                        }
                                                    });

                                                    _MSUoW.Documents.UpdateRange(oldDocumentList);
                                                    _MSUoW.ExaminationFormFillUpDetails.Update(latestSemesterApplication);
                                                }
                                                else
                                                {
                                                    _logger.Warn("Student update rejected as application forwarded to higher authority. Updated by Principal Id: " + GetCurrentUserId() + " For the student Id: " + userObj.Id + " and Sudent Email: " + userObj.EmailAddress);
                                                    throw new MicroServiceException() { ErrorMessage = "Student update rejected as application forwarded to higher authority." };
                                                }
                                            }
                                            else
                                            {
                                                _logger.Warn("No record found for student update. Updated by UserId: " + GetCurrentUserId() + " For the student Id: " + userObj.Id + " and Sudent Email: " + userObj.EmailAddress);
                                            }

                                            break;

                                        case (long)enServiceType.NursingApplicationForm:

                                            List<string> stateToBeConsiderForNursing = new List<string>()
                                            {
                                                _IConfiguration.GetSection("NursingFormFillup:VerificationPending").Value.Trim().ToLower(),
                                                _IConfiguration.GetSection("NursingFormFillup:PaymentPendingState").Value.Trim().ToLower(),
                                                _IConfiguration.GetSection("NursingFormFillup:PaymentDoneState").Value.Trim().ToLower(),
                                                _IConfiguration.GetSection("NursingFormFillup:AdmitcardGenerated").Value.Trim().ToLower()
                                                
                                            };

                                            var latestApplication = _MSUoW.NursingFormfillupDetails.GetAll()
                                                                    .Where(x => x.IsActive == true
                                                                    && x.CreatorUserId == usrData.Id
                                                                    && x.State.Trim().ToLower() != _IConfiguration.GetSection("NursingFormFillup:ExamFormFillUpRejectedStatus").Value.Trim().ToLower())
                                                                    .OrderByDescending(o=>o.CreationTime)
                                                                    .FirstOrDefault();

                                            if (latestApplication != null)
                                            {
                                                if (stateToBeConsiderForNursing.Contains(latestApplication.State.Trim().ToLower()))
                                                {
                                                    //update the student details.
                                                    latestApplication.StudentName = userObj.StudentProfile.StudentName.Trim();
                                                    latestApplication.DateOfBirth = userObj.StudentProfile.DateOfBirth;
                                                    latestApplication.CourseTypeCode = userObj.StudentProfile.CourseType.Trim();

                                                    latestApplication.CourseTypeCodeString = lookUpList.Where(x => x.LookupTypeId == (long)enLookUpType.UniversityCouseType
                                                                                             && x.LookupCode.Trim().ToLower() == userObj.StudentProfile.CourseType.Trim().ToLower() && x.IsActive == true)
                                                                                            .FirstOrDefault()
                                                                                            .LookupDesc.ToUpper();

                                                    latestApplication.StreamCode = userObj.StudentProfile.StreamCode.Trim();
                                                    latestApplication.CourseTypeCodeString = lookUpList.Where(x => x.LookupTypeId == (long)enLookUpType.UniversityStream
                                                                                             && x.LookupCode.Trim().ToLower() == userObj.StudentProfile.StreamCode.Trim().ToLower() && x.IsActive == true)
                                                                                            .FirstOrDefault()
                                                                                            .LookupDesc.ToUpper();

                                                    latestApplication.CourseCode = null;
                                                    latestApplication.CourseCodeString = null;
                                                    latestApplication.DepartmentCode = null;
                                                    latestApplication.DepartmentCodeString = null;

                                                    latestApplication.SubjectCode = userObj.StudentProfile.SubjectCode.Trim();
                                                    latestApplication.SubjectCodeString = subjectList.Where(x => x.IsActive && x.LookupCode.TrimEnd() == userObj.StudentProfile.SubjectCode.Trim()).FirstOrDefault().LookupDesc;

                                                    latestApplication.CollegeCode = userObj.StudentProfile.CollegeCode.Trim();
                                                    latestApplication.CollegeCodeString = collegeList.Where(x => x.IsActive && x.LookupCode.TrimEnd() == userObj.StudentProfile.CollegeCode.Trim()).FirstOrDefault().LookupDesc;

                                                    latestApplication.AcademicStart = userObj.StudentProfile.YearOfAdmission;

                                                    latestApplication.PresentAddress = userObj.UserAddress[0].Address;

                                                    latestApplication.PresentCountryCode = userObj.UserAddress[0].Country;
                                                    latestApplication.PresentCountry = lookUpList.Where(x => x.LookupTypeId == (long)enLookUpType.Country
                                                                                        && x.LookupCode.Trim().ToLower() == userObj.UserAddress[0].Country.Trim().ToLower() && x.IsActive == true)
                                                                                        .FirstOrDefault()
                                                                                        .LookupDesc.ToUpper();

                                                    latestApplication.PresentStateCode = userObj.UserAddress[0].State;
                                                    latestApplication.PresentState = lookUpList.Where(x => x.LookupTypeId == (long)enLookUpType.State
                                                                                    && x.ParentLookupCode == userObj.UserAddress[0].Country
                                                                                    && x.LookupCode.Trim().ToLower() == userObj.UserAddress[0].State.Trim().ToLower() && x.IsActive == true)
                                                                                    .FirstOrDefault()
                                                                                    .LookupDesc.ToUpper();

                                                    latestApplication.PresentCityCode = userObj.UserAddress[0].City;
                                                    latestApplication.PresentCity = lookUpList.Where(x => x.LookupTypeId == (long)enLookUpType.City
                                                                                    && x.ParentLookupCode == userObj.UserAddress[0].State
                                                                                    && x.LookupCode.Trim().ToLower() == userObj.UserAddress[0].City.Trim().ToLower() && x.IsActive == true)
                                                                                    .FirstOrDefault()
                                                                                    .LookupDesc.ToUpper();

                                                    latestApplication.PresentZip = userObj.UserAddress[0].Zip;

                                                    if (latestApplication.IsSameAsPresent)
                                                    {
                                                        latestApplication.PermanentAddress = latestApplication.PresentAddress;
                                                        latestApplication.PermanentCountryCode = latestApplication.PresentCountryCode;
                                                        latestApplication.PermanentCountry = latestApplication.PresentCountry;
                                                        latestApplication.PermanentStateCode = latestApplication.PresentStateCode;
                                                        latestApplication.PermanentState = latestApplication.PresentState;
                                                        latestApplication.PermanentCityCode = latestApplication.PresentCityCode;
                                                        latestApplication.PermanentCity = latestApplication.PresentCity;
                                                        latestApplication.PermanentZip = latestApplication.PresentZip;
                                                    }

                                                    //fetch data from documents 
                                                    var oldDocumentList = _MSUoW.Documents.GetAll().Where(x => x.IsActive && x.ParentSourceId == latestApplication.Id && x.CreatorUserId == usrData.Id).ToList();

                                                    oldDocumentList.ForEach(od =>
                                                    {
                                                        var newDoc = userObj.StudentDocuments.Where(x => x.DocumentType == od.DocumentType).FirstOrDefault();

                                                        if (newDoc != null && string.IsNullOrEmpty(newDoc.FilePath))
                                                        {
                                                            if (ImageValidation(newDoc))
                                                            {
                                                                od.FilePath = NursingImageWrite(newDoc, latestApplication.Id);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            od.FilePath = od.FilePath.Replace(_IConfiguration.GetSection("ApplicationURL").Value, "");
                                                        }
                                                    });

                                                    _MSUoW.Documents.UpdateRange(oldDocumentList);

                                                    //var newDocumentList = new List<Documents>();
                                                    ////update image 
                                                    //userObj.StudentDocuments.ForEach(newDoc =>
                                                    //{
                                                    //    if (ImageValidation(newDoc))
                                                    //    {
                                                    //        newDoc.Id = Guid.NewGuid();
                                                    //        newDocumentList.Add(ConvertToNewDocument(NursingImageWrite(newDoc, latestApplication.Id),usrData.Id));
                                                    //    }
                                                    //});
                                                    //_MSUoW.Documents.RemoveRange(oldDocumentList);
                                                    //_MSUoW.Documents.AddRange(newDocumentList);

                                                    _MSUoW.NursingFormfillupDetails.Update(latestApplication);
                                                }
                                                else
                                                {
                                                    _logger.Warn("Student update rejected as application forwarded to higher authority. Updated by Principal Id: " + GetCurrentUserId() + " For the student Id: " + userObj.Id + " and Sudent Email: " + userObj.EmailAddress);
                                                    throw new MicroServiceException() { ErrorMessage="Student update rejected as application forwarded to higher authority." };
                                                }
                                            }
                                            else
                                            {                                                
                                                _logger.Warn("No record found for student update. Updated by UserId: " + GetCurrentUserId() + " For the student Id: " + userObj.Id + " and Sudent Email: " + userObj.EmailAddress);                                                
                                            }

                                            break;
                                    }
                                });
                            }
                            _MSUoW.Users.Update(usrData);
                            _MSUoW.Commit();

                            _logger.Info("Student updated Successfully. Updated by UserId: " + GetCurrentUserId() + " For the student Id: " + userObj.Id + " and Sudent Email: " + userObj.EmailAddress);

                            //***** here send mail methode call ********//
                            if (usrData.EmailAddress != null && usrData.EmailAddress.Length > 5)
                            {
                                string clientApplicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                                string applicationUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                                string collegeLogo = _IConfiguration.GetSection("UULogo").Value;
                                //EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.UserRegistration && s.IsActive == true).FirstOrDefault();
                                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.AccountCreatedByPrincipalOrHOD && s.IsActive == true).FirstOrDefault();
                                if (templateData != null)
                                {
                                    StringBuilder body = new StringBuilder(templateData.Body);
                                    body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                                    body.Replace("--userName--", usrData.Name);
                                    body.Replace("--loginId--", usrData.EmailAddress);
                                    body.Replace("--password--", tempPassword);
                                    body.Replace("--url--", clientApplicationUrl);
                                    _MSCommon.SendMails(usrData.EmailAddress, templateData.Subject, body.ToString());
                                }

                                #region SMS SEND TO STUDENT
                                //EmailTemplate acknowledgementSMS = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.AccountCreatedSMS && s.IsActive == true).FirstOrDefault();
                                EmailTemplate acknowledgementSMS = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.AccountCreatedByPrincipalOrHODSMS && s.IsActive == true).FirstOrDefault();
                                if (acknowledgementSMS != null)
                                {
                                    string contentId = string.Empty;
                                    contentId = templateData.SMSContentId;
                                    string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                                    OTPMessage otpObj = new OTPMessage();
                                    StringBuilder body = new StringBuilder(acknowledgementSMS.Body);
                                    body.Replace("--userName--", usrData.Name);
                                    body.Replace("--loginId--", usrData.EmailAddress);
                                    body.Replace("--password--", tempPassword);
                                    body.Replace("--url--", clientApplicationUrl);
                                    otpObj.Message = body.ToString();
                                    otpObj.MobileNumber = usrData.PhoneNumber;
                                    _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                                }
                                //else throw new MicroServiceException() { ErrorCode = "OUU011" };
                                #endregion
                            }

                            //***** here send sms to phone no. method will be called ********//

                            status = true;
                            return status;
                        }
                    }
                    else
                    {
                        throw new MicroServiceException() { ErrorCode = "OUU002" };
                    }
                }
                else
                {
                    throw new MicroServiceException() { ErrorCode = "OUU093" };
                }
            }   
            return status;
        }

        private string NursingImageWrite(DocumentsDto newDoc, Guid id)
        {
            string[] name = id.ToString().Split("-");
            string finalName = name[1] + name[2] + name[4] + name[0] + name[3] + newDoc.DocumentType;
            //List<Documents> documentList = new List<Documents>();
            //input.ImageDocument.ForEach((file) =>
            //{
            string path = string.Empty;
            newDoc.ParentSourceId = id;
            if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.win.ToString())
            {
                path = _environment.ContentRootPath;
            }
            else if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.linux.ToString())
            {
                path = _IConfiguration.GetSection("LinuxDocumentPath").Value;
            }

            string fileName = finalName + "." + newDoc.FileExtension;
            string DirectoryPath = _IConfiguration.GetSection("NursingFormFillup:ExamFormFillUpCertificatePath").Value;
            DirectoryPath = DirectoryPath.Replace('\\', '/');
            var Path = path + DirectoryPath + fileName;
            if (!Directory.Exists(path + DirectoryPath))
            {
                Directory.CreateDirectory(path + DirectoryPath);
            }

            if (File.Exists(Path))
            {
                File.Delete(Path);
            }
            byte[] bytes = (byte[])newDoc.FileContent;
            File.WriteAllBytes(Path, bytes);
            newDoc.FilePath = DirectoryPath + fileName;
            newDoc.FilePath = newDoc.FilePath.Replace('\\', '/');
            //    documentList.Add(file);
            //});
            //return documentList;
            //return newDoc;
            return newDoc.FilePath;
        }

        //private Documents ConvertToNewDocument(DocumentsDto documentsDto,long userId)
        //{
        //    return new Documents()
        //    {
        //        Id = documentsDto.Id,
        //        FileExtension = documentsDto.FileExtension,
        //        FileContent = documentsDto.FileContent,
        //        DocumentType = documentsDto.DocumentType,
        //        Description = documentsDto.Description,
        //        FileName = documentsDto.FileName,
        //        FilePath = documentsDto.FilePath,
        //        ParentSourceId = documentsDto.ParentSourceId,
        //        FileType = documentsDto.FileType,
        //        SourceType = documentsDto.SourceType,
        //        CreatorUserId = userId
        //    };
        //}

        public bool validate(CreateUserDto input)
        {
            string zip = input.UserAddress.Select(d => d.Zip).FirstOrDefault().ToString();
            string regularExpressionForName = @"^[a-zA-Z\s\.]+$";
            //Regex emailExpression = new Regex(@"^[a-zA-Z0-9]+(?:[_.-][a-zA-Z0-9]+)*@[a-zA-Z0-9]*\.[a-zA-Z]{2,3}$");
            Regex emailExpression = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            if (input.Name == null || input.Name.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU037" };
            }
            else if (!Regex.IsMatch(input.Name.Trim(), regularExpressionForName))
            {
                throw new MicroServiceException() { ErrorCode = "OUU038" };
            }
            else if (!emailExpression.IsMatch(input.EmailAddress.Trim().ToLower()))
            {
                throw new MicroServiceException() { ErrorCode = "OUU033" };
            }
            else if (string.IsNullOrEmpty(input.PhoneNumber) || string.IsNullOrWhiteSpace(input.PhoneNumber))
            {
                throw new MicroServiceException() { ErrorCode = "OUU096" };
            }
            else if (input.PhoneNumber.Length != 10)
            {
                throw new MicroServiceException() { ErrorCode = "OUU097" };
            }
            else if (input.UserAddress == null || input.UserAddress.Count == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU023" };
            }
            else if (string.IsNullOrEmpty(input.UserAddress.Select(d => d.Address).FirstOrDefault().ToString()) || string.IsNullOrWhiteSpace(input.UserAddress.Select(d => d.Address).FirstOrDefault().ToString()))
            {
                throw new MicroServiceException() { ErrorCode = "OUU101" };
            }
            else if (input.UserAddress.Select(d => d.Address).FirstOrDefault().Length > 500)
            {
                throw new MicroServiceException() { ErrorCode = "OUU121" };
            }
            else if (string.IsNullOrEmpty(input.UserAddress.Select(d => d.Country).FirstOrDefault().ToString()) || string.IsNullOrWhiteSpace(input.UserAddress.Select(d => d.Country).FirstOrDefault().ToString()))
            {
                throw new MicroServiceException() { ErrorCode = "OUU102" };
            }
            else if (string.IsNullOrEmpty(input.UserAddress.Select(d => d.State).FirstOrDefault().ToString()) || string.IsNullOrWhiteSpace(input.UserAddress.Select(d => d.State).FirstOrDefault().ToString()))
            {
                throw new MicroServiceException() { ErrorCode = "OUU103" };
            }
            else if (string.IsNullOrEmpty(input.UserAddress.Select(d => d.City).FirstOrDefault().ToString()) || string.IsNullOrWhiteSpace(input.UserAddress.Select(d => d.City).FirstOrDefault().ToString()))
            {
                throw new MicroServiceException() { ErrorCode = "OUU104" };
            }
            else if (string.IsNullOrEmpty(input.UserAddress.Select(d => d.Zip).FirstOrDefault().ToString()) || string.IsNullOrWhiteSpace(input.UserAddress.Select(d => d.Zip).FirstOrDefault().ToString()))
            {
                throw new MicroServiceException() { ErrorCode = "OUU105" };
            }
            else if (input.UserAddress.Select(d => d.Zip).FirstOrDefault().ToString().Length != 6)
            {
                throw new MicroServiceException() { ErrorCode = "OUU106" };
            }
            else if (input.StudentProfile.DateOfBirth == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU041" };
            }
            else if (input.StudentProfile.CollegeCode == null || input.StudentProfile.CollegeCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU042" };
            }
            else if (input.StudentProfile.CourseType == null || input.StudentProfile.CourseType.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU043" };
            }
            else if (input.StudentProfile.StreamCode == null || input.StudentProfile.StreamCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU044" };
            }
            else if (input.StudentProfile.SubjectCode == null || input.StudentProfile.SubjectCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU045" };
            }
            else if (input.StudentProfile.YearOfAdmission == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU046" };
            }
            else if (input.StudentProfile.CourseType == "01")
            {
                input.StudentProfile.StudentName = input.StudentProfile.StudentName.Trim();
                input.StudentProfile.CollegeCode = input.StudentProfile.CollegeCode.Trim();
                input.StudentProfile.CourseType = input.StudentProfile.CourseType.Trim();
                input.StudentProfile.StreamCode = input.StudentProfile.StreamCode.Trim();
                input.StudentProfile.SubjectCode = input.StudentProfile.SubjectCode.Trim();
            }
            else if (input.StudentProfile.CourseType == "02")
            {
                if (input.StudentProfile.CourseCode == null || input.StudentProfile.CourseCode.Trim() == String.Empty)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU059" };
                }
                else if (input.StudentProfile.DepartmentCode == null || input.StudentProfile.DepartmentCode == String.Empty)
                {
                    input.StudentProfile.DepartmentCode = "";
                    throw new MicroServiceException() { ErrorCode = "OUU056" };
                }
                else
                {
                    input.StudentProfile.StudentName = input.StudentProfile.StudentName.Trim();
                    input.StudentProfile.CollegeCode = input.StudentProfile.CollegeCode.Trim();
                    input.StudentProfile.CourseType = input.StudentProfile.CourseType.Trim();
                    input.StudentProfile.StreamCode = input.StudentProfile.StreamCode.Trim();
                    input.StudentProfile.SubjectCode = input.StudentProfile.SubjectCode.Trim();
                    input.StudentProfile.CourseCode = input.StudentProfile.CourseCode.Trim();
                    input.StudentProfile.DepartmentCode = input.StudentProfile.DepartmentCode.Trim();
                }
            }
            return true;
        }
        #endregion

        private string SemesterImageWrite(DocumentsDto doc,Guid Id)
        {
            string[] name = Id.ToString().Split("-");
            string finalName = name[1] + name[2] + name[4] + name[0] + name[3];
            //List<Documents> documentList = new List<Documents>();
            //input.ImageDocument.ForEach((file) =>
            //{
            string path = string.Empty;
            doc.ParentSourceId = Id;
            if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.win.ToString())
            {
                path = _environment.ContentRootPath;
            }
            else if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.linux.ToString())
            {
                path = _IConfiguration.GetSection("LinuxDocumentPath").Value;
            }

            string fileName = finalName + "." + doc.FileExtension;
            string DirectoryPath = _IConfiguration.GetSection("ExamFormFillup:ExamFormFillUpCertificatePath").Value;
            DirectoryPath = DirectoryPath.Replace('\\', '/');
            var Path = path + DirectoryPath + fileName;
            if (!Directory.Exists(path + DirectoryPath))
            {
                Directory.CreateDirectory(path + DirectoryPath);
            }

            if (File.Exists(Path))
            {
                File.Delete(Path);
            }
            byte[] bytes = (byte[])doc.FileContent;
            File.WriteAllBytes(Path, bytes);
            doc.FilePath = DirectoryPath + fileName;
            doc.FilePath = doc.FilePath.Replace('\\', '/');
            //    documentList.Add(file);
            //});
            //return documentList;
            return doc.FilePath;
        }

        private bool ImageValidation(DocumentsDto input)
        {
            if (input.FileExtension == "" || input.FileExtension == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU047" };
            }
            if (input.FileExtension != "" || input.FileExtension != null)
            {
                var regEx = new Regex(@"(jpg|jpeg)$");
                if (!regEx.IsMatch(input.FileExtension.ToLower()))
                {
                    throw new MicroServiceException() { ErrorCode = "OUU048" };
                }
            }
            if (input.FileContent.Length >= Convert.ToInt32(_IConfiguration.GetSection("RegistrationNumber:UploadImageSize").Value))
            {
                throw new MicroServiceException() { ErrorCode = "OUU049" };
            }
            if (string.IsNullOrEmpty(input.DocumentType) || string.IsNullOrWhiteSpace(input.DocumentType))
            {
                throw new MicroServiceException() { ErrorCode = "OUU131" };
            }
            if (input.SourceType == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU131" };
            }
            return true;
        }

    }
}
