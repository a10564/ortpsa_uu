﻿using Microservice.Common;
using Microservice.Common.Abstract;
using MicroService.Business.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.DuplicateRegistrationDetail;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Exception;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace MicroService.Business
{
    public class DuplicateRegistrationService : BaseService, IDuplicateRegistrationService
    {
        private IMicroServiceUOW _MSUoW;
        private IMicroserviceCommon _MSCommon;
        public DuplicateRegistrationService(IMicroServiceUOW MSUoW, IRedisDataHandlerService redisData,IMicroserviceCommon MSCommon) : base(redisData)
        {
            _MSUoW = MSUoW;
            _MSCommon = MSCommon;
        }

        /// <summary>
        /// Author  :       Anurag Digal
        /// Date    :       07-11-19
        /// Description :   This methode is used to get student data against the existing registraton No
        /// </summary>
        /// <param name="regNo"></param>
        /// <returns></returns>
        public DuplicateRegistrationDetailDto GetRegistrationDetail(string regNo)
        {
            DuplicateRegistrationDetailDto data = new DuplicateRegistrationDetailDto();
            //check wheather regno is null or blank
            if (!string.IsNullOrEmpty(regNo) || !string.IsNullOrWhiteSpace(regNo))
            {
                //get the data from universityregistrationdetails
                UniversityRegistrationDetail result = _MSUoW.UniversityRegistrationDetail
                                                      .FindBy(d => d.RegistrationNo!=null && d.RegistrationNo.Trim().ToLower() == regNo.Trim().ToLower()
                                                      && d.CreatorUserId == GetCurrentUserId()
                                                      && d.IsActive).FirstOrDefault();

                if (result != null)
                {
                    data = AutoMapper.Mapper.Map<UniversityRegistrationDetail, DuplicateRegistrationDetailDto>(result);
                }
                else
                    throw new MicroServiceException() { ErrorCode = "OUU062" };
            }
            else
                throw new MicroServiceException() { ErrorCode = "OUU061" };

            return data;
        }

        /// <summary>
        /// Author  :       Anurag Digal
        /// Date    :       06-11-19
        /// Description :   This methode is used to save data for duplicateregistrationdetail application
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public DuplicateRegistrationDetailDto DuplicateRegistrationDetailApply(DuplicateRegistrationDetailDto param)
        {
            long serviceId = (long)enServiceType.DuplicateRegistrationCertificate;
            Users user = _MSUoW.Users.FindBy(u => u.Id == GetCurrentUserId() && u.IsActive).FirstOrDefault();
            if (user.UserType == (long)enRoles.Student)
            {
                if (param != null)
                {
                    if (ValidateInput(param))
                    {
                        Guid newGuid = Guid.NewGuid();
                        DuplicateRegistrationDetail duplicateRegistration = AutoMapper.Mapper.Map<DuplicateRegistrationDetailDto, DuplicateRegistrationDetail>(param);                        
                        duplicateRegistration.Id = newGuid;
                        duplicateRegistration.TenantWorkflowId = 0;
                        duplicateRegistration.State = "Applied";
                        duplicateRegistration.AcknowledgementNo = _MSCommon.GenerateAcknowledgementNo(_MSCommon.GetEnumDescription(enAcknowledgementFormat.ACK), serviceId, newGuid.ToString());
                        _MSUoW.DuplicateRegistrationDetail.Add(duplicateRegistration);

                        // Service applied
                        UserServiceApplied usrSrvcApplied = new UserServiceApplied()
                        {
                            IsAvailableToDL = false,
                            LastStatus = duplicateRegistration.State,
                            ServiceId = serviceId,
                            UserId = GetCurrentUserId(),
                            InstanceId = newGuid
                        };
                        _MSUoW.UserServiceApplied.Add(usrSrvcApplied);
                        _MSUoW.Commit();

                        param = AutoMapper.Mapper.Map<DuplicateRegistrationDetail, DuplicateRegistrationDetailDto>(duplicateRegistration);
                    }
                }
                else throw new MicroServiceException() { ErrorCode = "OUU013" };
            }
            else throw new MicroServiceException() { ErrorCode = "OUU052" };
            return param;
        }

        #region INPUT VALIDATION
        /// <summary>
        /// Author: Anurag Digal
        /// Date: 07-11-19
        /// Description : Thid method is used to validate input param for duplicate registration details application 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private bool ValidateInput(DuplicateRegistrationDetailDto input)
        {
            string regularExpressionForName = @"^[a-zA-Z\s\.]+$";//This regex is used to allow only alphabets, dot and spaces in string
            if (input.StudentName == null || input.StudentName.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU037" };
            }
            else if (!Regex.IsMatch(input.StudentName.Trim(), regularExpressionForName))
            {
                throw new MicroServiceException() { ErrorCode = "OUU038" };
            }
            else if (input.FatherName == null || input.FatherName.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU039" };
            }
            else if (!Regex.IsMatch(input.FatherName, regularExpressionForName))
            {
                throw new MicroServiceException() { ErrorCode = "OUU040" };
            }
            else if (input.DateOfBirth == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU041" };
            }
            else if (input.CollegeCode == null || input.CollegeCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU042" };
            }
            else if (input.CourseType == null || input.CourseType.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU043" };
            }
            else if (input.StreamCode == null || input.StreamCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU044" };
            }
            else if (input.SubjectCode == null || input.SubjectCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU045" };
            }
            else if (input.YearOfAdmission == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU046" };
            }
            else if (input.DepartmentCode == null || input.DepartmentCode == String.Empty)
            {
                input.DepartmentCode = "";
            }
            else if ((input.CourseType == "02") && (input.DepartmentCode == null || input.DepartmentCode == String.Empty))
            {
                throw new MicroServiceException() { ErrorCode = "OUU056" };
            }
            else if (input.CourseCode == null || input.CourseCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU059" };
            }
            input.StudentName = input.StudentName.Trim();
            input.FatherName = input.FatherName.Trim();
            input.CollegeCode = input.CollegeCode.Trim();
            input.CourseType = input.CourseType.Trim();
            input.StreamCode = input.StreamCode.Trim();
            input.SubjectCode = input.SubjectCode.Trim();
            return true;
        }
        #endregion

        /// <summary>
        /// Author :    Anurag Digal
        /// Date    :   08-11-19
        /// Description :   This methode is used to get all the duplicate registration data to view on BiGrid
        /// </summary>
        /// <returns></returns>
        public IQueryable<DuplicateRegistrationView> GetRoleBasedActiveDuplicateRegistrationInboxData()
        {
            long currentUserType = GetUserType();
            string CollegeCode = GetCollegeCode();
            string DepartmentCode = GetDepartmentCode();

            var inboxDataAsPerRole = _MSUoW.DuplicateRegistrationView.FindBy(t => t.IsActive);

            switch (currentUserType)
            {
                case (long)enUserType.Principal:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower());
                    break;
                case (long)enUserType.HOD:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower() && data.DepartmentCode.Trim().ToLower() == DepartmentCode.Trim().ToLower());
                    break;
                case (long)enUserType.Student:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CreatorUserId == GetCurrentUserId() && data.IsActive);
                    break;
            }

            return inboxDataAsPerRole;
        }


        public DuplicateRegistrationDetailDto GetDuplicateRegistrationDetail(Guid id)
        {
            DuplicateRegistrationDetailDto duplicateData = new DuplicateRegistrationDetailDto();
            if (id != null && !(id==Guid.Empty))
            {
                DuplicateRegistrationDetail data = _MSUoW.DuplicateRegistrationDetail.FindBy(d=>d.IsActive && d.Id==id).FirstOrDefault();
                if (data != null)
                {
                    duplicateData = AutoMapper.Mapper.Map<DuplicateRegistrationDetail, DuplicateRegistrationDetailDto>(data);
                }
                else throw new MicroServiceException() { ErrorCode = "OUU002" };
            }
            else throw new MicroServiceException() { ErrorCode = "OUU073" };
            return duplicateData;
        }
    }
}
