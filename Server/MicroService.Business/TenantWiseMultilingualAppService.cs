﻿using Microservice.Common;
using MicroService.Business.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.TenantWiseMultilingual;
using MicroService.Domain.Models.Entity;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Exception;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

/// <summary>

/// Added by Anurag1 
/// file is modified by Bibhu Datta Suchi Anurag
/// </summary>
namespace MicroService.Business
{
    public class TenantWiseMultilingualAppService : BaseService, ITenantWiseMultilingualAppService
    {
        private IMicroServiceUOW _MSUoW;
        public TenantWiseMultilingualAppService(IMicroServiceUOW MSUOW,IRedisDataHandlerService redisData)
            : base(redisData)
        {
            _MSUoW = MSUOW;
        }

        /// <summary>
        /// Author-Anurag Digal
        /// Created- 16-09-2019
        /// Desc- Add Module to the database
        /// </summary>
        /// <param name="moduleDetailDto"></param>
        /// <returns></returns>
        public ModuleDto AddModule(ModuleDto moduleDetailDto)
        {
            if (moduleDetailDto != null)
            {
                moduleDetailDto.ModuleName = moduleDetailDto.ModuleName.Trim();
                //moduleDetailDto.IsActive = true;
                var module = AutoMapper.Mapper.Map<Module>(moduleDetailDto);
                _MSUoW.Module.Add(module);
                _MSUoW.Commit();
                //_module.InsertAsync(module);
                moduleDetailDto = AutoMapper.Mapper.Map<Module, ModuleDto>(module);
            }
            else
            {
                throw new MicroServiceException { ErrorCode = "OUU020" };
            }

            return moduleDetailDto;
        }

        
        /// <summary>
        /// this is going to add language to the system
        /// </summary>
        /// <param name="languageDto"></param>
        /// <returns></returns>
        public LanguageDto AddLanguage(LanguageDto languageDto)
        {
            if (languageDto != null)
            {
                languageDto.LanguageCode = languageDto.LanguageCode.Trim();
                languageDto.LanguageName = languageDto.LanguageName.Trim();
                languageDto.IsActive = true;
                var language = AutoMapper.Mapper.Map<LanguageDto, Language>(languageDto);
                _MSUoW.Language.Add(language);
                _MSUoW.Commit();
                //_language.InsertAsync(language);
                languageDto = AutoMapper.Mapper.Map<Language, LanguageDto>(language);
            }
            else
            {
                throw new MicroServiceException { ErrorCode = "OUU021" };
            }
            return languageDto;
            //return Task.FromResult(languageDto);
        }
        /// <summary>
        /// add  anew cooment
        /// </summary>
        /// <param name="moduleId"></param>
        /// <param name="languageType"></param>
        /// <returns></returns>
        public Object GetMultilingualData(long moduleId, string languageType)
        {
            string language = languageType.Split(".json")[0].ToLower();
            long languageId = 0;
            var selectedLanguage = _MSUoW.Language.GetAll().Where(lang => lang.LanguageName.ToLower() == language || lang.LanguageCode.ToLower() == language).FirstOrDefault();
            if (selectedLanguage != null)
            {
                languageId = selectedLanguage.Id;
            }
            long tenantId = GetCurrentTenantId();
            var multiLingualText =_MSUoW.Multilingual
                //_multilingual
                .GetAll()
                .Where(mLang => mLang.TenantId == tenantId
                    && mLang.IsActive == true
                    && mLang.LanguageId == languageId
                    && (mLang.ModuleId == moduleId || mLang.ModuleId == (long)enModule.SharedModule))
                .ToDictionary(x => x.Key, x => x.Value);
            return JObject.Parse(JsonConvert.SerializeObject(multiLingualText, Formatting.Indented));
        }

        public List<MultilingualDto> GetMultilingualDetail()
        {
            long tenantId = GetCurrentTenantId();
            var multiLingualText =_MSUoW.Multilingual
                //_multilingual
                .GetAll()
                .Where(mLang => mLang.TenantId == tenantId
                    && mLang.IsActive == true).ToList();

            if (multiLingualText.Count == 0)
            {
                // Fetch data for default tenant
                multiLingualText = _MSUoW.Multilingual
                //_multilingual
                .GetAll()
                .Where(mLang => mLang.TenantId == 0
                    && mLang.IsActive == true).ToList();
            }

            var rest = AutoMapper.Mapper.Map<List<Multilingual>, List<MultilingualDto>>(multiLingualText);
            return rest;
        }

        public List<MultilingualDto> GetMultilingualDetailRootTenant()
        {
            var multiLingualText = _MSUoW.Multilingual
                //_multilingual
                .GetAll()
                .Where(mLang => mLang.TenantId == 0
                    && mLang.IsActive == true).ToList();

            var result = AutoMapper.Mapper.Map<List<Multilingual>, List<MultilingualDto>>(multiLingualText);
            return result;
        }

        public bool AddMultiLingualDataForTenant(List<MultilingualDto> multilingualData)
        {
            bool status = false;
            if (multilingualData.Count > 0)
            {
                //long tenantId = GetCurrentTenantId();
                var multiLingual = AutoMapper.Mapper.Map<List<Multilingual>>(multilingualData);
                //var re = multiLingual.Select(multi => { multi.IsActive = true; return multi; }).ToList();
                multiLingual.ForEach(multi => {
                    multi.IsActive = true;
                    _MSUoW.Multilingual.Add(multi);
                    //multi.TenantId = tenantId; // If tenant id does not get saved automatically
                    //_multilingual.Insert(multi);
                });
                _MSUoW.Commit();
                multilingualData = AutoMapper.Mapper.Map<List<MultilingualDto>>(multiLingual);
                status = true;
            }
            else
            {
                throw new MicroServiceException { ErrorCode = "OUU022" };
            }
            return status;
        }

        public string AddMultiLingualDataForRootTenant(List<MultilingualDto> multilingualData)
        {
            string resultString = "Data added successfully.";
            if (multilingualData.Count > 0)
            {
                var keyList = _MSUoW.Multilingual.GetAll().Where(multi => multi.TenantId == 0).Select(multi => multi.Key).ToList();
                List<string> duplicateKeyList = new List<string>();
                var multiLingual = AutoMapper.Mapper.Map<List<Multilingual>>(multilingualData);
                //var re = multiLingual.Select(multi => { multi.IsActive = true; return multi; }).ToList();
                multiLingual.ForEach(multi => {
                    if (keyList.IndexOf(multi.Key) == -1)
                    {// Key is unique and can be inserted
                        multi.IsActive = true;
                        multi.TenantId = 0;
                        //if(multi.ModuleId == (long)EnModule.SharedModule)
                        //{
                        //    multi.Key += "-sh";
                        //}
                        _MSUoW.Multilingual.Add(multi);
                        //_multilingual.Insert(multi);
                        keyList.Add(multi.Key);
                    }
                    {
                        duplicateKeyList.Add(multi.Key);
                    }
                });
                _MSUoW.Commit();
                multilingualData = AutoMapper.Mapper.Map<List<MultilingualDto>>(multiLingual);
                if (duplicateKeyList.Count > 0)
                {
                    resultString = "Duplicate entries found for keys : " + string.Join(",", duplicateKeyList);
                }
            }
            else
            {
                throw new MicroServiceException { ErrorCode= "Multilingual data could not be added." };
            }

            return resultString;
        }

        public IQueryable<Multilingual> GetAllMultilingualsData()
        {
            return _MSUoW.Multilingual.GetAll().AsQueryable();
        }
    }
}
