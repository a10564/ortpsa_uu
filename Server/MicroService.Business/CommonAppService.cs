﻿using Microservice.Common;
using Microservice.Common.Abstract;
using MicroService.Business.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.AdminActivity;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Dto.NumberGeneration;
using MicroService.Domain.Models.Dto.QRCode;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Entity.Lookup;
using MicroService.Domain.Models.Views;
using MicroService.SharedKernel;
using MicroService.Utility.Caching.Abstract;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Exception;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Business
{
    public class CommonAppService : BaseService, ICommonAppService
    {
        //private static object _lockObject = new object();
        static readonly object _lockObject = new object();
        private IMicroServiceUOW _MSUoW;
        private IMicroserviceCommon _MSCommon;
        private IConfiguration _configuration;
        private ICacheService _cacheService;

        public CommonAppService(IMicroServiceUOW MSUoW, IRedisDataHandlerService redisData, IMicroserviceCommon MSCommon, IConfiguration configuration, ICacheService cacheService) : base(redisData)
        {
            _MSUoW = MSUoW;
            _MSCommon = MSCommon;
            _configuration = configuration;
            _cacheService = cacheService;
        }

        /// <summary>
        /// Author          : Dattatreya Dash
        /// Date            : 15-10-2019
        /// Description     : Data fetched from lookup table
        /// <summary>
        /// <param name="parentLookupTypeList"></param>
        /// <returns>Lookup data filtered by parent lookup type list</returns>
        public IQueryable<Lookup> GetLookupData(long[] lookupTypeIds)
        {
            return GetAllLookupsDataFromCache(s => lookupTypeIds.Contains(s.LookupTypeId) && s.IsActive == true && s.TenantId == GetCurrentTenantId()).OrderBy(s => s.LookupId).AsQueryable();
            //return _MSUoW.Lookup.FindBy(lkp => lkp.IsActive && parentLookupTypeList.Contains(lkp.LookupTypeId)).OrderBy(lkp => lkp.LookupSequence).ThenBy(lkp => lkp.LookupDesc);
        }

        /// <summary>
        /// Author          : Dattatreya Dash
        /// Date            : 05-11-2019
        /// Description     : College details as per course type code( code for UG/PG ).. Course type data found in lookup table
        /// Author : Sitikanta Pradhan (modified)
        /// Date : 16-Nov-2019
        /// Description     : College details as per user role type
        /// </summary>
        /// <param name="courseTypeCode">Course type code( code for UG/PG)</param>
        /// <returns></returns>
        /// As discuss with bibhu and datta  (Sitikanta)
        //public IQueryable<CollegeMaster> GetCollegeDetails(string courseTypeCode)
        //{
        //    return _MSUoW.CollegeMaster.FindBy(colg => colg.IsActive && colg.CourseTypeCode == courseTypeCode).OrderBy(colg => colg.LookupSequence).ThenBy(colg => colg.LookupDesc);
        //}
        public IQueryable<CollegeMaster> GetCollegeDetails(string courseTypeCode)
        {
            long currentUserType = GetUserType();
            switch (currentUserType)
            {
                case (long)enUserType.Principal:
                    return _MSUoW.CollegeMaster.FindBy(colg => colg.IsActive && colg.CourseTypeCode == courseTypeCode && colg.LookupCode == GetCollegeCode()).OrderBy(colg => colg.LookupSequence).ThenBy(colg => colg.LookupDesc);
                case (long)enUserType.HOD:
                    return _MSUoW.CollegeMaster.FindBy(colg => colg.IsActive && colg.CourseTypeCode == courseTypeCode && colg.LookupCode == GetCollegeCode()).OrderBy(colg => colg.LookupSequence).ThenBy(colg => colg.LookupDesc);
                case (long)enUserType.CompCell:
                    var roleDetails=_MSUoW.RoleUserWiseSubjectMapper.GetAll().Where(w => w.IsActive && w.UserId == GetCurrentUserId() && w.RoleId == GetUserType()).FirstOrDefault();
                   // if null then return all college
                    if(roleDetails == null)
                    {
                        return _MSUoW.CollegeMaster.FindBy(colg => colg.IsActive && colg.CourseTypeCode == courseTypeCode).OrderBy(colg => colg.LookupSequence).ThenBy(colg => colg.LookupDesc);
                    }
                    var collegeAssignTosubjectList = _MSUoW.CollegeSubjectMapper.GetAll().Where(w => w.IsActive && w.SubjectMasterId == roleDetails.SubjectId).Select(a => a.CollegeCode).ToList();
                    // if null then return all college
                    if (collegeAssignTosubjectList == null)
                    {
                        return _MSUoW.CollegeMaster.FindBy(colg => colg.IsActive && colg.CourseTypeCode == courseTypeCode).OrderBy(colg => colg.LookupSequence).ThenBy(colg => colg.LookupDesc);
                    }
                    var collegeList = GetAllGetCollegeMasterListDataFromCache(x => x.IsActive && collegeAssignTosubjectList.Contains(x.LookupCode)).AsQueryable();
                    return collegeList;
                default:
                    return _MSUoW.CollegeMaster.FindBy(colg => colg.IsActive && colg.CourseTypeCode == courseTypeCode).OrderBy(colg => colg.LookupSequence).ThenBy(colg => colg.LookupDesc);
            }
        }

        /// <summary>
        /// Author          : Dattatreya Dash
        /// Date            : 05-11-2019
        /// Description     : Department details as per stream code and course code.. Stream data, course code found in lookup table
        /// </summary>
        /// <param name="streamCode">Stream code</param>
        /// <param name="courseCode">Course code</param>
        /// <returns></returns>
        public IQueryable<DepartmentMaster> GetDepartmentDetails(string streamCode, string courseCode)
        {
            return _MSUoW.DepartmentMaster.FindBy(dept => dept.IsActive && dept.StreamCode == streamCode && dept.CourseCode == courseCode).OrderBy(dept => dept.LookupSequence).ThenBy(dept => dept.LookupDesc);
        }

        /// <summary>
        /// Author          : Dattatreya Dash
        /// Date            : 05-11-2019
        /// Description     : Subject details as per stream code and department code.
        /// ---------------------------------------------
        /// Author          : Anurag Digal
        /// Date            : 10-12-2019
        /// Description     : Check college code to filter subject for Principal Role .
        /// </summary>
        /// <param name="filterCode">Course type code</param>
        /// <param name="departmentCode">Department code</param>
        /// <returns></returns>
        public IQueryable<SubjectMaster> GetSubjectDetails(string filterCode, string departmentCode)
        {
            if (GetUserType() == (long)enUserType.Principal || GetUserType() == (long)enUserType.HOD)
            {
                if (string.IsNullOrEmpty(departmentCode) || string.IsNullOrWhiteSpace(departmentCode))
                {
                    return _MSUoW.SubjectMasterRepository.GetSubjectMasterByCollegeCode(GetCollegeCode()).Where(sub => sub.IsActive && sub.StreamCode == filterCode && sub.IsFormFillUpAvailibility).OrderBy(sub => sub.LookupSequence).ThenBy(sub => sub.LookupDesc);
                }
                else
                {
                    return _MSUoW.SubjectMasterRepository.GetSubjectMasterByCollegeCode(GetCollegeCode()).Where(sub => sub.IsActive && sub.CourseCode == filterCode && sub.DepartmentCode == departmentCode && sub.IsFormFillUpAvailibility).OrderBy(sub => sub.LookupSequence).ThenBy(sub => sub.LookupDesc);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(departmentCode) || string.IsNullOrWhiteSpace(departmentCode))
                {
                    return _MSUoW.SubjectMaster.GetAll().Where(sub => sub.IsActive && sub.StreamCode == filterCode && sub.IsFormFillUpAvailibility).OrderBy(sub => sub.LookupSequence).ThenBy(sub => sub.LookupDesc);
                }
                else
                {
                    return _MSUoW.SubjectMaster.GetAll().Where(sub => sub.IsActive && sub.CourseCode == filterCode && sub.DepartmentCode == departmentCode && sub.IsFormFillUpAvailibility).OrderBy(sub => sub.LookupSequence).ThenBy(sub => sub.LookupDesc);
                }
            }
        }

        public IQueryable<ExaminationPaperMaster> GetPaperDetails(string courseType, string courseCode, string streamCode, string departmentCode, string subjectCode, string semesterCode)
        {
            IQueryable<ExaminationPaperMaster> examPaperMasterData = null;

            if (!string.IsNullOrEmpty(courseType) && !string.IsNullOrWhiteSpace(courseType))
            {
                if (courseType == _MSCommon.GetEnumDescription(enCourseType.Ug))
                {
                    //get examination Id from from exam master table
                    var ddd = _MSUoW.ExaminationMaster.FindBy(e => e.CourseType == courseType && e.StreamCode == streamCode
                                                                   && e.SubjectCode == subjectCode
                                                                   && e.SemesterCode == semesterCode && e.IsActive).ToList();


                    Guid examMasterId = _MSUoW.ExaminationMaster.FindBy(e => e.CourseType == courseType && e.StreamCode == streamCode
                                                                && e.SubjectCode == subjectCode
                                                                && e.SemesterCode == semesterCode && e.IsActive).Select(d => d.Id)
                                                                .FirstOrDefault();

                    if (examMasterId != null)
                    {
                        //get the paper details using exammasterid
                        examPaperMasterData = _MSUoW.ExaminationPaperMaster.FindBy(e => e.ExaminationMasterId == examMasterId && e.IsActive);
                    }
                }
                else if (courseType == _MSCommon.GetEnumDescription(enCourseType.Pg))
                {
                    //get examination Id from from exam master table
                    Guid examMasterId = _MSUoW.ExaminationMaster.FindBy(e => e.CourseType == courseType
                                                                && e.CourseCode == courseCode
                                                                && e.DepartmentCode == departmentCode
                                                                && e.StreamCode == streamCode
                                                                && e.SubjectCode == subjectCode
                                                                && e.SemesterCode == semesterCode && e.IsActive).Select(d => d.Id)
                                                                .FirstOrDefault();

                    if (examMasterId != null)
                    {
                        //get the paper details using exammasterid
                        examPaperMasterData = _MSUoW.ExaminationPaperMaster.FindBy(e => e.ExaminationMasterId == examMasterId && e.IsActive);
                    }
                }
            }
            return examPaperMasterData;
        }



        public string GetNextSeqNumber(long processType, string customPreFix = "")
        {
            try
            {
                StringBuilder newSeqNumber = new StringBuilder();
                lock (_lockObject)
                {
                    NumberGenerator currRecord = new NumberGenerator();
                    currRecord = _MSUoW.NumberGenerator.FindBy(A => A.ProcessType == processType).FirstOrDefault();

                    if (currRecord == null)
                        return "";

                    //Get the start seq no
                    string startSeqNumber = Convert.ToString(currRecord.StartNumberSeq);
                    long newSeq = currRecord.LastGeneratedNumber + Convert.ToInt32(currRecord.NumberIncrementedBy);
                    newSeqNumber.Append(Convert.ToString(newSeq));
                    int zerosToBeAppended = startSeqNumber.Length - Convert.ToString(newSeq).Length;
                    for (int i = 0; i < zerosToBeAppended; i++)
                    {
                        newSeqNumber.Insert(0, "0");
                    }
                    if (!string.IsNullOrEmpty(currRecord.PreFixWith))
                    {
                        string dateString = DateTime.UtcNow.Day.ToString() + DateTime.UtcNow.ToString("MM") + DateTime.UtcNow.Year;

                        //Insert the process prefix
                        if (string.IsNullOrEmpty(customPreFix))
                        {
                            newSeqNumber.Insert(0, currRecord.PreFixWith + dateString);
                        }
                        else
                        {
                            newSeqNumber.Insert(0, currRecord.PreFixWith + customPreFix.Trim() + dateString);
                        }
                    }

                    //Update the current seq number
                    currRecord.LastGeneratedNumber = newSeq;
                    _MSUoW.NumberGenerator.Update(currRecord);
                    _MSUoW.Commit();
                }
                return newSeqNumber.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        /// <summary>
        /// Author      :Anurag Digal
        /// Date        :19-11-2019
        /// This method is used to check whether is process completed or pending by providing process type
        /// </summary>
        /// <param name="processType"></param>
        /// <returns></returns>
        public bool GetProcessType(long processType, long serviceType)
        {
            bool status = false;     //if status is false then the process is completed...According to this page button should be enable
                                     //get the processtype from process master
            if (processType != 0)
            {
                string inCompleteState = _MSCommon.GetEnumDescription(enProcessStatus.Added).Trim().ToLower();
                string tracker = _MSUoW.ProcessTracker.FindBy(x => x.ProcessType == processType && x.IsActive
                                                    && x.ProcessStatus.Trim().ToLower() == inCompleteState
                                                    && x.CreatorUserId == GetCurrentUserId() && x.ServiceId == serviceType)
                                                    .Select(s => s.ProcessStatus)
                                                    .FirstOrDefault();
                if (tracker != null)
                {
                    if (tracker.Trim().ToLower() == inCompleteState)
                    {
                        status = true;     //if status is true then the process is incomplete....According to this page button should be disable with loading icon
                    }
                }
            }
            else throw new MicroServiceException() { ErrorCode = "" };
            return status;
        }
        /// <summary>
        /// Author  :   Sumbul Samreen
        /// Date    :   03-12-2019
        /// Description :This method is used to create registration number by getting the param of type NumberGenerationDto.
        /// </summary>
        /// <param name="studentInfo"></param>
        /// <returns></returns>
        //public string BuildRegistrationNumber(NumberGenerationDto studentInfo)
        //{

        //    string regdNo = string.Empty;
        //    string runningNum = string.Empty;
        //    string streamCodeLength = _configuration.GetSection("RegistrationNumberCodeLength:StreamLength").Value;
        //    string courseCodeLength = _configuration.GetSection("RegistrationNumberCodeLength:CourseCodeLength").Value;
        //    string subjectCodeLength = _configuration.GetSection("RegistrationNumberCodeLength:SubjectCodeLength").Value;
        //    string collegeCodeLength = _configuration.GetSection("RegistrationNumberCodeLength:CollegeCodeLength").Value;
        //    string runningSequenceLength = _configuration.GetSection("RegistrationNumberCodeLength:RunningSequenceLength").Value;
        //    string YearlengthLength = _configuration.GetSection("RegistrationNumberCodeLength:Yearlength").Value;
        //    string coursePg = _MSCommon.GetEnumDescription(enCourseType.Pg);
        //    string yearCode = studentInfo.YearOfAdmission.ToString().Substring(2);
        //    studentInfo.CourseCode = studentInfo.CourseCode == null ? "0" : studentInfo.CourseCode;
        //    NumberGenerator regdNos = _MSUoW.NumberGenerator.FindBy(data => data.YearOfAdmission == yearCode && data.StreamCode == studentInfo.StreamCode
        //                              && data.SubjectCode == studentInfo.SubjectCode && data.CollegeCode == studentInfo.CollegeCode && data.CourseCode == studentInfo.CourseCode
        //                             ).FirstOrDefault();
        //    if (regdNos != null)
        //    {// already exists

        //        if (regdNos.StreamCode.Length < int.Parse(streamCodeLength))
        //        {

        //            regdNos.StreamCode = AddPrefix(int.Parse(streamCodeLength), '0', regdNos.StreamCode);//regdNos.Stream.PadLeft(regdNos.Stream.Length, '0');
        //        }
        //        if (regdNos.SubjectCode.Length < int.Parse(subjectCodeLength))
        //        {
        //            regdNos.SubjectCode = AddPrefix(int.Parse(subjectCodeLength), '0', regdNos.SubjectCode);//regdNos.Stream.PadLeft(regdNos.Stream.Length, '0');
        //        }
        //        if (regdNos.CollegeCode.Length < int.Parse(collegeCodeLength))
        //        {
        //            regdNos.CollegeCode = AddPrefix(int.Parse(collegeCodeLength), '0', regdNos.CollegeCode);//regdNos.College.PadLeft(regdNos.College.Length, '0');
        //        }
        //        if (regdNos.CourseCode.Length < int.Parse(courseCodeLength))
        //        {
        //            regdNos.CourseCode = AddPrefix(int.Parse(courseCodeLength), '0', regdNos.CourseCode); //regdNos.College.PadLeft(regdNos.Subject.Length, '0');
        //        }
        //        runningNum = (regdNos.LastGeneratedNumber + regdNos.NumberIncrementedBy).ToString();
        //        regdNos.LastGeneratedNumber = Convert.ToInt32(runningNum);
        //        _MSUoW.NumberGenerator.Update(regdNos);
        //        _MSUoW.Commit();
        //        if (runningNum.Length < int.Parse(runningSequenceLength))
        //        {
        //            runningNum = AddPrefix(int.Parse(runningSequenceLength), '0', runningNum);//regdNos.runningNum.PadLeft(runningNum.Length, '0');
        //        }

        //        studentInfo.CourseCode = coursePg == studentInfo.CourseType ? coursePg : "0";

        //        regdNo = yearCode + studentInfo.StreamCode + studentInfo.CourseCode + studentInfo.SubjectCode + studentInfo.CollegeCode + runningNum;
        //    }
        //    else
        //    {// new combination added
        //        runningNum = "1";
        //        runningNum = AddPrefix(int.Parse(runningSequenceLength), '0', runningNum);
        //        if (studentInfo.StreamCode.Length < int.Parse(streamCodeLength))
        //        {

        //            studentInfo.StreamCode = AddPrefix(int.Parse(streamCodeLength), '0', studentInfo.StreamCode);//regdNos.Stream.PadLeft(regdNos.Stream.Length, '0');
        //        }
        //        if (studentInfo.SubjectCode.Length < int.Parse(subjectCodeLength))
        //        {
        //            studentInfo.SubjectCode = AddPrefix(int.Parse(subjectCodeLength), '0', studentInfo.SubjectCode);//regdNos.Stream.PadLeft(regdNos.Stream.Length, '0');
        //        }
        //        if (studentInfo.CollegeCode.Length < int.Parse(collegeCodeLength))
        //        {
        //            studentInfo.CollegeCode = AddPrefix(int.Parse(collegeCodeLength), '0', studentInfo.CollegeCode);//regdNos.College.PadLeft(regdNos.College.Length, '0');
        //        }
        //        if (studentInfo.CourseCode.Length < int.Parse(courseCodeLength))
        //        {
        //            studentInfo.CourseCode = AddPrefix(int.Parse(courseCodeLength), '0', studentInfo.CourseCode); //regdNos.College.PadLeft(regdNos.Subject.Length, '0');
        //        }
        //        regdNo = yearCode + studentInfo.StreamCode + studentInfo.CourseCode + studentInfo.SubjectCode + studentInfo.CollegeCode + runningNum;
        //        NumberGenerator NumberGeneratorObj = new NumberGenerator();
        //        NumberGeneratorObj.NumberIncrementedBy = 1;
        //        NumberGeneratorObj.LastGeneratedNumber = Convert.ToInt32(runningNum);
        //        NumberGeneratorObj.StartNumberSeq = "000";
        //        NumberGeneratorObj.YearOfAdmission = yearCode;
        //        NumberGeneratorObj.ProcessType = studentInfo.ProcessType;
        //        NumberGeneratorObj.StreamCode = studentInfo.StreamCode;
        //        NumberGeneratorObj.CollegeCode = studentInfo.CollegeCode;
        //        NumberGeneratorObj.CourseCode = studentInfo.CourseType == coursePg ? studentInfo.CourseCode : "0";
        //        studentInfo.CourseCode = studentInfo.CourseType == coursePg ? studentInfo.CourseCode : "0";
        //        NumberGeneratorObj.SubjectCode = studentInfo.SubjectCode;
        //        _MSUoW.NumberGenerator.Add(NumberGeneratorObj);
        //        _MSUoW.Commit();

        //    }

        //    return regdNo;
        //}


        string AddPrefix(int requiredlength, char prefix, string inputCode)
        {
            return inputCode.PadLeft(requiredlength, prefix); ;
        }

        public IQueryable<CollegeMaster> GetCollegeDetails(string courseTypeCode, string streamCode, string subjectCode)
        {
            List<string> collegeTypeCode = GetCollegeFilterCode(courseTypeCode, streamCode, subjectCode);
            var activeColleges = GetAllGetCollegeMasterListDataFromCache(colg => colg.IsActive && collegeTypeCode.Contains(colg.CollegeFilterCode)).OrderBy(colg => colg.LookupSequence).ThenBy(colg => colg.LookupDesc).AsQueryable();
            long currentUserType = GetUserType();
            switch (currentUserType)
            {
                case (long)enUserType.Principal:
                case (long)enUserType.HOD:
                    return activeColleges.Where(colg => colg.LookupCode == GetCollegeCode()).OrderBy(colg => colg.LookupSequence).ThenBy(colg => colg.LookupDesc);
                default:
                    return activeColleges;
            }
        }

        private List<string> GetCollegeFilterCode(string courseTypeCode, string streamCode, string subjectCode)
        {
            List<string> collegeFilterCode = new List<string>();
            string courseTypeDecription = _MSCommon.GetEnumDescription(enCourseType.Ug);
            string streamDescription = _MSCommon.GetEnumDescription(enStreamCode.SelfFinance);
            List<string> nursingList = new List<string>() { _MSCommon.GetEnumDescription(enSubject.PBNursing), _MSCommon.GetEnumDescription(enSubject.BSCNursing) };
            if (courseTypeCode == courseTypeDecription)
            {
                if (streamCode == streamDescription)
                {
                    if (nursingList.Contains(subjectCode))
                    {
                        collegeFilterCode.Add(enCollegeType.Has_UG_PBN_BSCN.ToString("D"));
                    }
                    else
                    {
                        collegeFilterCode.Add(enCollegeType.Has_BBA_BCA.ToString("D"));
                        collegeFilterCode.Add(enCollegeType.Has_UG_BBA_BCA.ToString("D"));
                    }
                }
                else
                {
                    collegeFilterCode.Add(enCollegeType.Has_UG.ToString("D"));
                }
            }
            else
            {
                collegeFilterCode.Add(enCollegeType.Has_PG.ToString("D"));
            }
            return collegeFilterCode;
        }

        /// <summary>
        /// Author  :   Jayanta Abinash
        /// Date    :   14-12-2019
        /// Description :   This method is used to create a new date that is 15 years less than current date.
        /// </summary>
        /// <param name="studentInfo"></param>
        /// <returns></returns>
        public DateTime GetMinusCurrentDate()
        {
            int maximumAge = 0 - (int.Parse(_configuration.GetSection("MaximumAge").Value));
            DateTime curDate = DateTime.UtcNow;
            return curDate.AddYears(maximumAge);
        }
        public RequestResult CallExternalAPI(string baseUrl, string apiURL, object content, string apiType)
        {
            using (var client = new HttpClient())
            {
                string jsoninstring = JsonConvert.SerializeObject(content);
                var result = client.PostAsync(baseUrl + apiURL, new StringContent(jsoninstring, Encoding.UTF8, "application/json")).GetAwaiter().GetResult();
                string resultContent = result.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                return JsonConvert.DeserializeObject<RequestResult>(resultContent);
            }
        }

        #region Odata Bussiness Section
        /// <summary>
        /// Author  :   Anurag Digal
        /// Date    :   14-01-2020
        /// Description :   This will give the iquerable record where isactive should be true for SubjectMaster
        /// </summary>
        /// <returns></returns>
        public IQueryable<SubjectMaster> GetAllSubject()
        {
            if (GetUserType() == (long)enUserType.Principal || GetUserType() == (long)enUserType.HOD)
            {
                return _MSUoW.SubjectMasterRepository.GetSubjectMasterByCollegeCode(GetCollegeCode()).Where(x => x.IsActive);
            }
            else
            {
                return _MSUoW.SubjectMaster.GetAll().Where(x => x.IsActive);
            }
        }

        /// <summary>
        /// Author  :   Anurag Digal
        /// Date    :   14-01-2020
        /// Description :   This will give the iquerable record where isactive should be true for DepartmentMaster
        /// </summary>
        /// <returns></returns>
        public IQueryable<DepartmentMaster> GetAllDepartment()
        {
            return _MSUoW.DepartmentMaster.GetAll().Where(x => x.IsActive);
        }

        /// <summary>
        /// Author  :   Anurag Digal
        /// Date    :   23-01-2020
        /// Description :   This will give the iquerable record where isactive should be true for CollegeMaster
        /// </summary>
        /// <returns></returns>
        public IQueryable<CollegeMaster> GetAllCollege()
        {
            return _MSUoW.CollegeMaster.GetAll().Where(x => x.IsActive).OrderBy(colg => colg.LookupDesc);
        }

        #endregion

        #region Get User Role Wise College
        /// <summary>
        /// Author  :   Sitikanta Pradhan
        /// Date    :   19-02-2020
        /// Description :   Get user role wise college
        /// </summary>
        /// <returns></returns>
        public string GetUserRoleWiseCollege(long roleId)
        {
            string collegecode = null;
            if (roleId != 0)
            {
                long usertype = GetUserType();
                if (roleId == usertype)
                {
                    if (usertype == (long)enRoles.Principal || usertype == (long)enRoles.HOD)
                    {
                        long currentUserId = GetCurrentUserId();
                        Users user = _MSUoW.Users.FindBy(data => data.Id == currentUserId && data.IsActive).OrderByDescending(data => data.CreationTime).FirstOrDefault();
                        if (user != null)
                        {
                            collegecode = user.CollegeCode;
                        }
                    }
                }
            }
            return collegecode;
        }
        #endregion

        #region Validate QRCode
        /// <summary>
        /// Author  :   Sumbul Samreen
        /// Date    :   25-02-2020
        /// Description :  This method validates the QRCode
        /// </summary>
        /// <returns></returns>
        public QRCodeValidateResponseDto ValidateQRCode(long id, string number)
        {
            string applicantName = string.Empty;
            QRCodeValidateResponseDto qrCodeValidObj = new QRCodeValidateResponseDto();
            switch (id)
            {
                case (long)(enServiceType.RegistrationService):

                    UniversityRegistrationDetail universityRegistrationDetails = _MSUoW.UniversityRegistrationDetail.FindBy(b => b.RegistrationNo == number
                    && b.IsActive && b.State.Trim().ToLower() == _configuration.GetSection("RegistrationNumber:RegistrationNumberApprovedStatus").Value.Trim().ToLower()
                    )
                    .FirstOrDefault();
                    if (universityRegistrationDetails != null)
                    {
                        qrCodeValidObj.ApplicantName = universityRegistrationDetails.StudentName;
                        qrCodeValidObj.CertificateNumber = universityRegistrationDetails.RegistrationNo;
                        qrCodeValidObj.IsValid = true;
                    }
                    else
                    {
                        qrCodeValidObj.IsValid = false;
                    }
                    break;

                case (long)(enServiceType.MigrationService):

                    UniversityMigrationDetail universityMigrationDetail = _MSUoW.UniversityMigrationDetail.FindBy(b => b.MigrationNumber == number
                    && b.IsActive && b.State.Trim().ToLower() == _configuration.GetSection("MigrationCertificate:MigrationApplicationApprovedStatus").Value.Trim().ToLower()
                    )
                    .FirstOrDefault();
                    if (universityMigrationDetail != null)
                    {
                        qrCodeValidObj.ApplicantName = universityMigrationDetail.StudentName;
                        qrCodeValidObj.CertificateNumber = universityMigrationDetail.MigrationNumber;
                        qrCodeValidObj.IsValid = true;
                    }
                    else
                    {
                        qrCodeValidObj.IsValid = false;
                    }
                    break;


                default:
                    break;
            }
            return qrCodeValidObj;
        }
        #endregion

        /// <summary>
        /// Author : Sitikanta Pradhan
        /// Date : 06-Mar-2020
        /// Get addmission year list on each services
        /// </summary>
        /// <param name="serviceNumber"></param>
        /// <returns></returns>
        public List<int> GetYearOfAdmissionList(long serviceNumber)
        {
            List<int> yearList = new List<int>();
            switch (serviceNumber)
            {
                case (long)(enServiceType.RegistrationService):

                    yearList = _MSUoW.UniversityRegistrationDetail.FindBy(sess => sess.IsActive).OrderBy(a => a.YearOfAdmission).Select(a => a.YearOfAdmission).Distinct().ToList();
                    if (yearList.Count() == 0)
                    {
                        yearList.Add(DateTime.Now.Year);
                    }
                    break;

                case (long)(enServiceType.MigrationService):

                    yearList = _MSUoW.UniversityMigrationDetail.FindBy(sess => sess.IsActive).OrderBy(a => a.YearOfAdmission.Year).Select(a => a.YearOfAdmission.Year).Distinct().ToList();
                    if (yearList.Count() == 0)
                    {
                        yearList.Add(DateTime.Now.Year);
                    }
                    break;

                case (long)(enServiceType.BCABBAExamApplicationForm):

                    yearList = _MSUoW.ExaminationFormFillUpDetails.FindBy(sess => sess.IsActive).OrderBy(a => a.AcademicStart).Select(a => a.AcademicStart).Distinct().ToList();
                    if (yearList.Count() == 0)
                    {
                        yearList.Add(DateTime.Now.Year);
                    }
                    break;

                case (long)(enServiceType.StudentList):

                    yearList = _MSUoW.StudentProfile.FindBy(sess => sess.IsActive).OrderBy(a => a.YearOfAdmission).Select(a => a.YearOfAdmission).Distinct().ToList();
                    if (yearList.Count() == 0)
                    {
                        yearList.Add(DateTime.Now.Year);
                    }
                    break;

                default:
                    break;
            }
            return yearList;
        }

        /// <summary>
        /// Author : Sitikanta Pradhan
        /// Date : 18-Sep-2020
        /// Get Login Student Record
        /// </summary>
        /// <returns></returns>
        public StudentProfileView GetLoginStudentData()
        {
            StudentProfileView studentProfile = new StudentProfileView();
            var LoginUserId = GetCurrentUserId();
            studentProfile = _MSUoW.StudentProfileView.FindBy(a => a.IsActive && a.UserId == LoginUserId).FirstOrDefault();
            return studentProfile;
        }

        #region convert number to verbal representation
        /// <summary>
        /// Author  :   Biswaranjan Ghadai
        /// Date    :   16-Sept-21
        /// Description :   Convert number to verbal representation
        /// </summary>
        /// <param name="param"></param>
        /// <returns>Number in word</returns>
        /// 
        public string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 100000000) > 0)
            {
                words += NumberToWords(number / 100000000) + " crore ";
                number %= 100000000;
            }

            if ((number / 100000) > 0)
            {
                words += NumberToWords(number / 100000) + " lakhs ";
                number %= 100000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }
        #endregion
        #region convert DECIMAL to verbal representation
        /// <summary>
        /// Author  :   Biswaranjan Ghadai
        /// Date    :   16-Sept-21
        /// Description :   Convert DECIMAL to verbal representation
        /// </summary>
        /// <param name="param"></param>
        /// <returns>DECIMAL in word</returns>
        /// 
        public string DecimalToWords(decimal number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + DecimalToWords(Math.Abs(number));

            string words = "";

            int intPortion = (int)number;
            decimal fraction = (number - intPortion) * 100;
            int decPortion = (int)fraction;

            words = NumberToWords(intPortion);
            if (decPortion > 0)
            {
                words += " and ";
                words += NumberToWords(decPortion);
            }
            return words;
        }
        #endregion

        /// <summary>
        /// Author :Bikash kumar sethi
        /// Date : 14-09-2021
        /// Fetch CollegeMaster data from CollegeMaster table
        /// </summary>
        /// <returns></returns>
        #region get and update all college master cache data 
        public List<CollegeMaster> GetCollegeMasterList()
        {
            return _MSUoW.CollegeMaster.GetAll().Where(conf => conf.IsActive).ToList();
        }
        public List<CollegeMaster> GetAllGetCollegeMasterListDataFromCache(Func<CollegeMaster, bool> predicate)
        {
            List<CollegeMaster> configData = new List<CollegeMaster>();
            var cachedData = _cacheService.Get(Constant.CollegeMasterCacheId, () => { return GetCollegeMasterList(); });
            configData = cachedData.Where(predicate).ToList();
            return configData;
        }
        public void UpdateCollegeMasterCache()
        {
            this._cacheService.ClearCache(Constant.CollegeMasterCacheId);
            GetAllGetCollegeMasterListDataFromCache();
        }
        public List<CollegeMaster> GetAllGetCollegeMasterListDataFromCache()
        {
            List<CollegeMaster> configData = new List<CollegeMaster>();
            configData = _cacheService.Get(Constant.CollegeMasterCacheId, () => { return GetCollegeMasterList(); });
            return configData;
        }
        #endregion

        /// <summary>
        /// Author :Anurag Digal
        /// Date : 18-09-2021
        /// Fetch Late Fine Master data from Cache
        /// </summary>
        /// <returns></returns>
        #region Late Fine Master cache data 
        public List<LateFineMaster> GetLateFineMasterList()
        {
            return _MSUoW.LateFineMaster.GetAll().Where(conf => conf.IsActive).ToList();
        }
        public void UpdateLateFineMasterCache()
        {
            this._cacheService.ClearCache(Constant.LateFineMasterCacheId);
            GetAllGetCollegeMasterListDataFromCache();
        }
        public List<LateFineMaster> GetAllLateFineMasterListDataFromCache()
        {
            List<LateFineMaster> configData = new List<LateFineMaster>();
            configData = _cacheService.Get(Constant.LateFineMasterCacheId, () => { return GetLateFineMasterList(); });
            return configData;
        }
        #endregion

        #region Lookups cache data 
        public List<Lookup> GetAllLookupsDataFromCache(Func<Lookup, bool> predicate)
        {
            List<Lookup> configData = new List<Lookup>();
            var cachedData = _cacheService.Get(Constant.LookupCacheId, () => { return GetActiveLookupsList(); });
            configData = cachedData.Where(predicate).ToList();
            return configData;
        }
        private List<Lookup> GetActiveLookupsList()
        {
            return _MSUoW.Lookup.FindBy(conf => conf.IsActive).ToList();
        }
        #endregion

        #region ExaminationPaperMaster cache data 
        public List<ExaminationPaperMaster> GetActiveExaminationPaperMasterList()
        {
            List<ExaminationPaperMaster> dataList = new List<ExaminationPaperMaster>();
            var cachedData = _cacheService.Get(Constant.ExaminationPaperMasterCacheId, () => { return GetAllExaminationPaperMaster(); });
            return cachedData;
        }
        private List<ExaminationPaperMaster> GetAllExaminationPaperMaster()
        {
            return _MSUoW.ExaminationPaperMaster.FindBy(s => s.IsActive == true).ToList();
        }
        #endregion

        #region ExaminationMaster cache data 
        public List<ExaminationMaster> GetActiveExaminationMasterList()
        {
            List<ExaminationMaster> dataList = new List<ExaminationMaster>();
            var cachedData = _cacheService.Get(Constant.ExaminationMasterCacheId, () => { return GetAllExaminationMaster(); });
            return cachedData;
        }
        private List<ExaminationMaster> GetAllExaminationMaster()
        {
            return _MSUoW.ExaminationMaster.FindBy(s => s.IsActive == true).ToList();
        }
        #endregion

        #region ExaminationSession cache data 
        public List<ExaminationSession> GetActiveExaminationSessionList()
        {
            List<ExaminationSession> dataList = new List<ExaminationSession>();
            var cachedData = _cacheService.Get(Constant.ExaminationSessionCacheId, () => { return GetAllExaminationSession(); });
            return cachedData;
        }
        private List<ExaminationSession> GetAllExaminationSession()
        {
            return _MSUoW.ExaminationSession.FindBy(s => s.IsActive == true).ToList();
        }
        #endregion

        #region Reading Email Template From Cache
        /// <summary>
        /// Author: Ch Sikon Kumar
        /// Date: 06-10-2020
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public List<EmailTemplate> GetEmailTemplateDataFromCache()
        {
            return _cacheService.Get(Constant.EmailTemplateCacheId, () => { return GetEmailTemplateList(); });
        }
        private List<EmailTemplate> GetEmailTemplateList()
        {
            return _MSUoW.EmailTemplate.FindBy(et => et.IsActive == true).ToList();
        }
        #endregion

        public List<Lookup> GetSessionListBySubject(string subjectCode)
        {
            List<Lookup> sessionList = new List<Lookup>();
            if (!string.IsNullOrEmpty(subjectCode))
            {
                var subjectDetails = _MSUoW.SubjectMaster.GetAll().Where(sub => sub.IsActive && sub.LookupCode == subjectCode).FirstOrDefault();
                if(subjectDetails != null)
                {
                    if (subjectDetails.IsSemesterOrYear == (long)enSessionType.Year)
                    {
                        sessionList = GetAllLookupsDataFromCache(lkp => lkp.LookupTypeId == (long)enLookUpType.BscNursing).Take((int)subjectDetails.SubjectDuration).ToList();
                    }
                    else if(subjectDetails.IsSemesterOrYear == (long)enSessionType.Semester)
                    {
                        sessionList = GetAllLookupsDataFromCache(lkp => lkp.LookupTypeId == (long)enLookUpType.Semester).Take((int)subjectDetails.SubjectDuration * 2).ToList();
                    }
                }
                else
                {
                    throw new MicroServiceException() { ErrorMessage = "No subjects details found" };
                }
            }
            return sessionList;
        }

        public List<CollegeMaster> GetCollegeMasterBySubject(string courseTypeCode, string streamCode, string subjectCode)
        {
            //get subjectId from subject master
            long subMstId = GetAllSubject().Where(s => s.LookupCode == subjectCode).FirstOrDefault().Id;
            var collegeSubjectMapperList = GetAllGetCollegeMasterListDataFromCache().AsQueryable()
                   .Join(_MSUoW.CollegeSubjectMapper.GetAll(), col => col.LookupCode, csm => csm.CollegeCode,
                   (col, csm) => new
                   {
                       Id=col.Id,
                       LookupCode = col.LookupCode,
                       LookupDesc = col.LookupDesc,
                       LookupSequence = col.LookupSequence,
                       Address = col.Address,
                       CourseTypeCode = col.CourseTypeCode,
                       IsAffiliationRequired = col.IsAffiliationRequired,
                       CollegeFilterCode = col.CollegeFilterCode,
                       SubjectMasterId = csm.SubjectMasterId
                   }).ToList();

            return collegeSubjectMapperList.Where(s => s.SubjectMasterId == subMstId)
                .Select(n => new CollegeMaster
                {
                    Id = n.Id,
                    LookupCode = n.LookupCode,
                    LookupDesc = n.LookupDesc,
                    LookupSequence = n.LookupSequence,
                    Address = n.Address,
                    CourseTypeCode = n.CourseTypeCode,
                    IsAffiliationRequired = n.IsAffiliationRequired,
                    CollegeFilterCode = n.CollegeFilterCode
                }).ToList();
        }

        public RoleUserWiseSubjectMapper GetRoleUserWiseSubject()
        {
            RoleUserWiseSubjectMapper roleUsrSubject = new RoleUserWiseSubjectMapper();
            if (GetRoleListOfCurrentUser().Contains(Convert.ToString((long)enRoles.CompCell)))
            {
                roleUsrSubject = _MSUoW.RoleUserWiseSubjectMapper.GetAll().Where(w => w.RoleId == (long)enRoles.CompCell && w.UserId == GetCurrentUserId() && w.IsActive).FirstOrDefault();
            }                
            return roleUsrSubject;
        }

        public CollegeServiceMapper GetCollegeWiseServiceDetails(string CollegeCode) 
        {
            return _MSUoW.CollegeServiceMapper.GetAll().Where(w => w.IsActive && w.CollegeCode == CollegeCode && w.ServiceMasterId != (long)enServiceType.StudentList).FirstOrDefault();
        }
    }
}



