﻿using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;
using MicroService.Domain.Models.Entity;
using Microservice.Common;
using Microservice.Common.Abstract;
using MicroService.Business.Abstract;
using MicroService.Utility.Common.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Dto.Payment;
using MicroService.Domain.Models.Dto.User;
using System.Collections.Generic;
using MicroService.Core.WorkflowTransactionHistrory;
using MicroService.Core;
using MicroService.Domain.Models.Entity.Lookup;
using MicroService.Domain.Models.Dto.StudentDashboard;
using MicroService.Utility.Common;
using MicroService.Utility.Exception;
using System.Globalization;
using TimeZoneConverter;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.CLC;
using MicroService.Domain.Models.Dto.Nursing;
using MicroService.Domain.Models.Entity.Nursing;

namespace MicroService.Business
{
	public class PaymentService : BaseService, IPaymentService
	{

		private IConfiguration _iconfiguration;
		private readonly ICommonAppService _commonService;
		private IMicroserviceCommon _MSCommon;
		private IMicroServiceUOW _MSUoW;		
		private IUserService _userService;
        private IWFTransactionHistroryAppService _WFTransactionHistroryAppService;
        private WorkflowDbContext _workflowDbContext;
		private IExaminationFormFillUpService _ExaminationFormFillUpService;
		private INursingFormFillUpService _nursingFormFillUpService;
		private IStudentRegistrationNoService _studentRegistrationNoService;
        private IStudentMigrationService _studentMigrationService;
        private IStudentCLCService _studentCLCService;

        public PaymentService(		
		IUserService userService,
		IRedisDataHandlerService ids,
		IMicroServiceUOW MSUoW,
		ICommonAppService commonService,
		IConfiguration iconfiguration,
		IMicroserviceCommon MSCommon,
        IWFTransactionHistroryAppService WFTransactionHistroryAppService,
		IExaminationFormFillUpService examinationFormFillUpService,
		IStudentRegistrationNoService studentRegistrationNoService,
        IStudentMigrationService studentMigrationService,
        IStudentCLCService studentCLCService, 
		INursingFormFillUpService nursingFormFillUpService

		) : base(ids)
		{			
			_MSUoW = MSUoW;
			_userService = userService;
			_iconfiguration = iconfiguration;
			_commonService = commonService;
			_MSCommon = MSCommon;
            _WFTransactionHistroryAppService = WFTransactionHistroryAppService;
            _workflowDbContext = new WorkflowDbContext();
			_ExaminationFormFillUpService = examinationFormFillUpService;
			_studentRegistrationNoService = studentRegistrationNoService;
            _studentMigrationService = studentMigrationService;
            _studentCLCService = studentCLCService;
			_nursingFormFillUpService = nursingFormFillUpService;
        }

		public void CreateRequestPayu(TransactionLog inputData)
		{
			inputData.IsActive = true;
			inputData.CallType = enCallType.Request.ToString();
			inputData.Description = "Request For Payment";
			inputData.ErrorDescription = "No Error";
			_MSUoW.TransactionLog.Add(inputData);
			_MSUoW.Commit();

		}		


		public string PreparePGForm(TransactionLog paymentDetails)
		{
			string action_url = string.Empty;
			string hash1 = string.Empty;
			string[] hashVarsSeq;
			string hash_string = string.Empty;

			
			string strHash = generateTransactionID();
			paymentDetails.UniqueRefNo = paymentDetails.RequestersUniqueRefNo = strHash;

			if (
				string.IsNullOrEmpty(_iconfiguration.GetSection("Payment").GetSection("BIPGKey").Value) ||
				string.IsNullOrEmpty(paymentDetails.RequestersUniqueRefNo) ||
				string.IsNullOrEmpty(paymentDetails.ConsumerName) ||
				string.IsNullOrEmpty(paymentDetails.EmailId) ||
				string.IsNullOrEmpty(paymentDetails.ContactNo) ||
				string.IsNullOrEmpty(_iconfiguration.GetSection("Payment").GetSection("SuccessUrl").Value) ||
				string.IsNullOrEmpty(_iconfiguration.GetSection("Payment").GetSection("FailureUrl").Value)
				)
			{
				return "";
			}
			else
			{
				hashVarsSeq = _iconfiguration.GetSection("Payment").GetSection("HashSequence").Value.Split('|'); // spliting hash sequence from config
				hash_string = "";
				foreach (string hash_var in hashVarsSeq)
				{
					if (hash_var == "bipgkey")
					{					
						hash_string = hash_string + _iconfiguration.GetSection("Payment").GetSection("BIPGKey").Value;
						hash_string = hash_string + '|';
					}
					else if (hash_var == "reqtxnid")
					{
						hash_string = hash_string + paymentDetails.RequestersUniqueRefNo;
						hash_string = hash_string + '|';
					}
					else if (hash_var == "txnid")
					{
						hash_string = hash_string + paymentDetails.UniqueRefNo;
						hash_string = hash_string + '|';
					}
					else if (hash_var == "amount")
					{
						hash_string = hash_string + Convert.ToDecimal(paymentDetails.Amount).ToString("F2");						
						hash_string = hash_string + '|';
					}
					else if (hash_var == "productinfo")
					{
						hash_string = hash_string + paymentDetails.ProductInfo;
						hash_string = hash_string + '|';
					}
					else if (hash_var == "firstname")
					{
						hash_string = hash_string + paymentDetails.ConsumerName;
						hash_string = hash_string + '|';
					}
					else if (hash_var == "email")
					{
						hash_string = hash_string + paymentDetails.EmailId;
						hash_string = hash_string + '|';
					}
					else if (hash_var == "phone")
					{
						hash_string = hash_string + paymentDetails.ContactNo;
						hash_string = hash_string + '|';
					}
					else if (hash_var == "udf1")
					{
						hash_string = hash_string + paymentDetails.RequestId;
						hash_string = hash_string + '|';
					}
					else if (hash_var == "udf2")
					{
						hash_string = hash_string + paymentDetails.DeviceType;
						hash_string = hash_string + '|';
					}
					else
					{
						hash_string = hash_string + "";// isset if else
						hash_string = hash_string + '|';
					}
				}
				
				hash_string += _iconfiguration.GetSection("Payment").GetSection("BIPGSalt").Value;// appending SALT
				//generating hash
				hash1 = GenerateMD5SignatureForPayU(hash_string).ToLower();// Generatehash512(hash_string).ToLower();    

				paymentDetails.HashSequence = hash1;

				action_url = _iconfiguration.GetSection("Payment").GetSection("BIPaymentGatewayURL").Value + _iconfiguration.GetSection("Payment").GetSection("PaymentUrl").Value ;

			}
			if (!string.IsNullOrEmpty(hash1))
			{
				// adding values in gash table for data post
				System.Collections.Hashtable data = new System.Collections.Hashtable(); 																						
				
				data.Add("reqhash", hash1);
				data.Add("reqtxnid", paymentDetails.RequestersUniqueRefNo);	
				string AmountForm = Convert.ToDecimal(paymentDetails.Amount).ToString("F2");// eliminating trailing zeros																							 
				data.Add("amount", AmountForm);
				data.Add("firstname", paymentDetails.ConsumerName);
				data.Add("email", paymentDetails.EmailId);
				data.Add("phone", paymentDetails.ContactNo);
				data.Add("productinfo", paymentDetails.ProductInfo);
				data.Add("reqsurl", _iconfiguration.GetSection("ApplicationURL").Value + _iconfiguration.GetSection("Payment").GetSection("SuccessUrl").Value);
				data.Add("reqfurl", _iconfiguration.GetSection("ApplicationURL").Value +  _iconfiguration.GetSection("Payment").GetSection("FailureUrl").Value);			
				data.Add("reqcurl", _iconfiguration.GetSection("ApplicationURL").Value + _iconfiguration.GetSection("Payment").GetSection("CancelUrl").Value);
				data.Add("udf1", paymentDetails.RequestId);
				data.Add("udf2", paymentDetails.DeviceType);
				data.Add("bipgkey", _iconfiguration.GetSection("Payment").GetSection("BIPGKey").Value);
				//data.Add("bipgsalt", _iconfiguration.GetSection("Payment").GetSection("BIPGSalt").Value);

				string strForm = PreparePOSTForm(action_url, data);
				return strForm;
			}
			else
			{
				//no hash
				return "";
			}
		}


		private string PreparePOSTForm(string url, System.Collections.Hashtable data)      // post form
		{
			//Set a name for the form
			string formID = "PostForm";
			//Build the form using the specified data to be posted.
			StringBuilder strForm = new StringBuilder();
			strForm.Append("<form id=\"" + formID + "\" name=\"" +
						   formID + "\" action=\"" + url +
						   "\" method=\"POST\">");

			foreach (System.Collections.DictionaryEntry key in data)
			{

				strForm.Append("<input type=\"hidden\" name=\"" + key.Key +
							   "\" value=\"" + key.Value + "\">");
			}


			strForm.Append("</form>");
			//Build the JavaScript which will do the Posting operation.
			StringBuilder strScript = new StringBuilder();
			strScript.Append("<script language='javascript'>");
			strScript.Append("var v" + formID + " = document." +
							 formID + ";");
			strScript.Append("v" + formID + ".submit();");
			strScript.Append("</script>");
			//Return the form and the script concatenated.
			//(The order is important, Form then JavaScript)
			return strForm.ToString() + strScript.ToString();
		}

		/// <summary>
		/// Generate HASH for encrypt all parameter passing while transaction
		/// </summary>
		/// <param name="Rawdata"></param>
		/// <returns></returns>		

		public string GenerateMD5SignatureForPayU(string Rawdata)
		{
			byte[] message = Encoding.UTF8.GetBytes(Rawdata);

			UnicodeEncoding UE = new UnicodeEncoding();
			byte[] hashValue;
			SHA512Managed hashString = new SHA512Managed();
			string hex = "";
			hashValue = hashString.ComputeHash(message);
			foreach (byte x in hashValue)
			{
				hex += String.Format("{0:x2}", x);
			}
			return hex;
		}
       
        public void SendPaymentSuccessRecieptEmail(TransactionLog paymnetDetail)
        {
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.PaymentSuccessful && s.IsActive == true).FirstOrDefault();
            {
                if (templateData != null)
                {
                    string serverUrl = _iconfiguration.GetSection("ApplicationURL").Value;
                    string applicationUrl = _iconfiguration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _iconfiguration.GetSection("UULogo").Value;
                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", serverUrl + collegeLogo);
                    body.Replace("--ProductIfo--", paymnetDetail.ProductDesc);
                    body.Replace("--Name--", paymnetDetail.ConsumerName);
                    body.Replace("--EmailId--", paymnetDetail.EmailId);
                    body.Replace("--PhoneNumber--", paymnetDetail.ContactNo);
                    body.Replace("--PaidAmount--", paymnetDetail.Amount.ToString());
                    body.Replace("--PaymentReceiptNo--", paymnetDetail.RecieptNo);
                    body.Replace("--TransactionDate--", paymnetDetail.CreationTime.ToString("dd-MM-yyyy"));
                    body.Replace("--TransactionId--", paymnetDetail.UniqueRefNo);
                    body.Replace("--BankRefNo--", paymnetDetail.BankRefNo);
                    body.Replace("--Success--", serverUrl + _iconfiguration.GetSection("Payment:PaymentChecked").Value);
                    _MSCommon.SendMails(paymnetDetail.EmailId, templateData.Subject, body.ToString());

                }
            }
        }
       
        public void SendPaymentFailedRecieptEmail(TransactionLog paymnetDetail)
        {
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.PaymentFailed && s.IsActive == true).FirstOrDefault();
            {
                if (templateData != null)
                {
                    string serverUrl = _iconfiguration.GetSection("ApplicationURL").Value;
                    string applicationUrl = _iconfiguration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _iconfiguration.GetSection("UULogo").Value;
                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", serverUrl + collegeLogo);
                    body.Replace("--ProductIfo--", paymnetDetail.ProductDesc);
                    body.Replace("--Name--", paymnetDetail.ConsumerName);//Name of current month
                    body.Replace("--EmailId--", paymnetDetail.EmailId);
                    body.Replace("--PhoneNumber--", paymnetDetail.ContactNo);
                    body.Replace("--PaidAmount--", paymnetDetail.Amount.ToString());
                    body.Replace("--TransactionDate--", DateTime.UtcNow.ToString("dd-MM-yyyy"));
                    body.Replace("--Failed--", serverUrl +  _iconfiguration.GetSection("Payment:PaymentCancelImg").Value);
                    //body.Replace("--TransactionId--", paymnetDetail.PaymentId);
                   // body.Replace("--BankRefNo--", paymnetDetail.BankRefNo);
                    _MSCommon.SendMails(paymnetDetail.EmailId, templateData.Subject, body.ToString());

                }


            }

        }
       
        public void SendPaymentCancelledRecieptEmail(TransactionLog paymnetDetail)
        {
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.PaymentCancelled && s.IsActive == true).FirstOrDefault();
            {
                if (templateData != null)
                {
                    string serverUrl = _iconfiguration.GetSection("ApplicationURL").Value;
                    string applicationUrl = _iconfiguration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _iconfiguration.GetSection("UULogo").Value;
                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", serverUrl + collegeLogo);
                    body.Replace("--ProductIfo--", paymnetDetail.ProductDesc);
                    body.Replace("--Name--", paymnetDetail.ConsumerName);//Name of current month
                    body.Replace("--EmailId--", paymnetDetail.EmailId);
                    body.Replace("--PhoneNumber--", paymnetDetail.ContactNo);
                    body.Replace("--PaidAmount--", paymnetDetail.Amount.ToString());
                    body.Replace("--TransactionDate--", DateTime.UtcNow.ToString("dd-MM-yyyy"));
                    body.Replace("--Failed--", serverUrl +  _iconfiguration.GetSection("Payment:PaymentCancelImg").Value);
                    //body.Replace("--TransactionId--", paymnetDetail.PaymentId);
                   // body.Replace("--BankRefNo--", paymnetDetail.BankRefNo);
                    _MSCommon.SendMails(paymnetDetail.EmailId, templateData.Subject, body.ToString());

                }
            }
        }      

        private string GenerateRecieptSequenceNo()
		{
			return _commonService.GetNextSeqNumber(Convert.ToInt64(EnProcessType.Reciept));

		}

		private string generateTransactionID()
		{
			return _commonService.GetNextSeqNumber(Convert.ToInt64(EnProcessType.Transaction));
		}

		public TransactionLog buildPaymentDetail(IFormCollection form, EnPaymentStatus statusType)
		{
			TransactionLog paymentDetail = new TransactionLog();
			if (!string.IsNullOrEmpty(form["txnid"]))
			{
				
				string payment_Id = Convert.ToString(form["mihpayid"]);
				string paymentStatus = form["status"].ToString();
				string payment_txn = form["reqtxnid"].ToString();
				paymentDetail.CallType = "Response";
				paymentDetail.MihPayid = payment_Id;
				paymentDetail.CSCPayid = Convert.ToString(form["txnid"]);
				paymentDetail.HashSequence = Convert.ToString(form["reqhash"]);
				paymentDetail.UniqueRefNo = Convert.ToString(form["reqtxnid"]);
				paymentDetail.ConsumerName = Convert.ToString(form["firstname"]);
				paymentDetail.EmailId = Convert.ToString(form["email"]);
				paymentDetail.ContactNo = Convert.ToString(form["phone"]);				
				paymentDetail.Amount = Convert.ToDecimal(form["amount"]);
				paymentDetail.ProductInfo = Convert.ToString(form["productinfo"]);
				paymentDetail.RequestId = Convert.ToString(form["udf1"]);
				paymentDetail.DeviceType = Convert.ToString(form["udf2"]);

				paymentDetail.CardNumber = Convert.ToString(form["cardnum"]);
				paymentDetail.CardType = Convert.ToString(form["card_type"]);
				paymentDetail.NameOnCard = Convert.ToString(form["name_on_card"]);
				paymentDetail.BankRefNo = Convert.ToString(form["bank_ref_num"]);
				paymentDetail.BankCode = Convert.ToString(form["bankcode"]);
				paymentDetail.IsActive = true;
				paymentDetail.ErrorDescription = Convert.ToString(form["error_message"]);
                paymentDetail.CSCPayid = Convert.ToString(form["txnid"]);

                if (paymentStatus.Equals("success", StringComparison.InvariantCultureIgnoreCase))
				{
					paymentDetail.Status = "Successful";
				}
				else if (paymentStatus.Equals("failure", StringComparison.InvariantCultureIgnoreCase))
				{
					if (statusType == EnPaymentStatus.Cancel)
					{
						paymentDetail.Status = "Cancelled";
					}
					else if (statusType == EnPaymentStatus.Failure)
					{
						paymentDetail.Status = "Failed";
					}

				}
				//paymentDetail.DeviceType = GetDeviceType();

			}
			return paymentDetail;
		}

		public Tuple<bool, Guid?, int> doOperation(TransactionLog paymentDetail, bool updateServiceTable)
        {
            Tuple<bool, Guid?, int> result = new Tuple<bool, Guid?, int>(false, null, 0);
            dynamic key;
            if (paymentDetail != null && paymentDetail.MihPayid != null && paymentDetail.RequestId != null)
            {
                //Save in DB & Get the primary key 
                paymentDetail.Id = Guid.NewGuid();
                paymentDetail.RecieptNo = GenerateRecieptSequenceNo();
                _MSUoW.TransactionLog.Add(paymentDetail);
                key = paymentDetail.Id;
				//_MSUoW.Commit();

                ServiceMaster serviceMaster = _MSUoW.ServiceMaster.GetById(Convert.ToInt64(paymentDetail.ProductInfo));
                //paymentDetail.ProductNo = paymentDetail.ProductInfo;
                paymentDetail.ProductDesc = serviceMaster.ServiceName;



                // if updateServiceTable is false onlylog the request do not update the service db.				
                if (paymentDetail.Status == "Successful" && updateServiceTable == true)
                {
                    long productInfo;
                    bool isStatusUpdated;
                    switch (Convert.ToInt16(paymentDetail.ProductInfo))
                    {
                        case (int)(enServiceType.BCABBAExamApplicationForm):
                         #region BCABBAExamApplicationForm

                             productInfo = Convert.ToInt64(enServiceType.BCABBAExamApplicationForm);
                            //Update the Status in respected page
                             isStatusUpdated = UpdateStatusToPaymentReceived(paymentDetail.RequestId, productInfo, paymentDetail.CSCPayid);
                            if (isStatusUpdated)
                            {
                               // _MSUoW.Commit();

                                //Send Mail For registering the application
                                SendPaymentSuccessRecieptEmail(paymentDetail);
                                //Send sms after payment done successfully
                                SendPaymentSuccessfulSMS(paymentDetail);
                                SendMailToPrincipal(paymentDetail);
                            }
                            #endregion
                            break;

                        case (int)(enServiceType.RegistrationService):
                            #region RegistrationService

                             productInfo = Convert.ToInt64(enServiceType.RegistrationService);
                            //Update the Status in respected page
                             isStatusUpdated = UpdateStatusToPaymentReceived(paymentDetail.RequestId, productInfo, paymentDetail.CSCPayid);
                            if (isStatusUpdated)
                            {
                               // _MSUoW.Commit();

                                //Send Mail For registering the application
                                SendPaymentSuccessRecieptEmail(paymentDetail);
                                //Send sms after payment done successfully
                                SendPaymentSuccessfulSMS(paymentDetail);

                                SendMailToPrincipal(paymentDetail);
                            }
                            #endregion
                            break;

                        case (int)(enServiceType.MigrationService):
                            #region RegistrationService

                            productInfo = Convert.ToInt64(enServiceType.MigrationService);
                            //Update the Status in respected page
                            isStatusUpdated = UpdateStatusToPaymentReceived(paymentDetail.RequestId, productInfo, paymentDetail.CSCPayid);
                            if (isStatusUpdated)
                            {
                               // _MSUoW.Commit();

                                //Send Mail For registering the application
                                SendPaymentSuccessRecieptEmail(paymentDetail);
                                //Send sms after payment done successfully
                                SendPaymentSuccessfulSMS(paymentDetail);

                                SendMailToPrincipal(paymentDetail);
                            }
                            #endregion
                            break;

						case (int)(enServiceType.NursingApplicationForm):
							#region NursingApplicationForm

							productInfo = Convert.ToInt64(enServiceType.NursingApplicationForm);
							//Update the Status in respected page
							isStatusUpdated = UpdateStatusToPaymentReceived(paymentDetail.RequestId, productInfo, paymentDetail.CSCPayid);
							if (isStatusUpdated)
							{
								// _MSUoW.Commit();

								//Send Mail For registering the application
								SendPaymentSuccessRecieptEmail(paymentDetail);
								//Send sms after payment done successfully
								SendPaymentSuccessfulSMS(paymentDetail);
								SendMailToPrincipal(paymentDetail);
							}
							#endregion
							break;

					}
                 }
                else if (paymentDetail.Status == "Failed")
                {
                    _MSUoW.Commit();

                    SendPaymentFailedRecieptEmail(paymentDetail);
                    SendPaymentFailedSMS(paymentDetail);


                }
                else if (paymentDetail.Status == "Cancelled")
                {
                    _MSUoW.Commit();

                    SendPaymentCancelledRecieptEmail(paymentDetail);
                }


                
                result = new Tuple<bool, Guid?, int>(true, key, Convert.ToInt32(EnPaymentStatus.Success));
				
            }
            else
            {
                paymentDetail.RequestId = paymentDetail.RequestId != null ? paymentDetail.RequestId : null;
                paymentDetail.CallType = "Response";
                paymentDetail.Amount = paymentDetail.Amount != 0 ? paymentDetail.Amount : 0;
                
                key = 0;                
                result = new Tuple<bool, Guid?, int>(true, key, Convert.ToInt32(EnPaymentStatus.Incomplete));
            }
            //_MSUoW.Commit();
            return result;
        }
        public void SendPaymentSuccessfulSMS(TransactionLog paymentDetail)
        {
            #region SMS send to student after payment done successfully.
            EmailTemplate paymentSMS = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.PaymentSuccessfulSMS && s.IsActive == true).FirstOrDefault();
            if (paymentSMS != null)
            {
				string contentId = string.Empty;
				contentId = paymentSMS.SMSContentId;

				string SMSAPIUrl = _iconfiguration.GetSection("SMSApiPath").Value;
                OTPMessage otpObj = new OTPMessage();
                StringBuilder body = new StringBuilder(paymentSMS.Body);
                body.Replace("--studentName--", paymentDetail.ConsumerName);
                body.Replace("--Amount--", paymentDetail.Amount.ToString());
                body.Replace("--transactionDate--", paymentDetail.CreationTime.ToString("dd-MM-yyyy"));
                body.Replace("--transactionId--", paymentDetail.UniqueRefNo);
                otpObj.Message = body.ToString();
                otpObj.MobileNumber = paymentDetail.ContactNo;
                _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
            }
            #endregion
        }
        public void SendPaymentFailedSMS(TransactionLog paymentDetail)
        {
            #region SMS send to student after payment failed.
            EmailTemplate paymentSMS = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.PaymentFailedSMS && s.IsActive == true).FirstOrDefault();
            if (paymentSMS != null)
            {
				string contentId = string.Empty;
				contentId = paymentSMS.SMSContentId;

				string SMSAPIUrl = _iconfiguration.GetSection("SMSApiPath").Value;
                OTPMessage otpObj = new OTPMessage();
                StringBuilder body = new StringBuilder(paymentSMS.Body);
                body.Replace("--studentName--", paymentDetail.ConsumerName);
                body.Replace("--transactionDate--", paymentDetail.CreationTime.ToString("dd-MM-yyyy"));
                body.Replace("--transactionId--", paymentDetail.UniqueRefNo);
                otpObj.Message = body.ToString();
                otpObj.MobileNumber = paymentDetail.ContactNo;
                _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
            }
            #endregion
        }

        public void SendMailToPrincipal(TransactionLog paymentDetails)
        {
            long SelectedTenantWorkflowId = 0;
            string SelectedState = String.Empty;
            string SelectedCollegeCode = String.Empty;
            string SelectedDepartmentCode = String.Empty;
            string serviceName = String.Empty;
            string courseType = String.Empty;
            bool IsData = false;

            switch (Convert.ToInt16(paymentDetails.ProductInfo))
            {
                case (int)(enServiceType.BCABBAExamApplicationForm):
                    #region BCABBAExamApplicationForm
                    var examFormFillUpDetails = _MSUoW.ExaminationFormFillUpDetails.FindBy(form => form.Id == Guid.Parse(paymentDetails.RequestId)).FirstOrDefault();
                    if (examFormFillUpDetails != null)
                    {
                        SelectedTenantWorkflowId = examFormFillUpDetails.TenantWorkflowId;
                        SelectedState = examFormFillUpDetails.State;
                        SelectedCollegeCode = examFormFillUpDetails.CollegeCode;
                        courseType = examFormFillUpDetails.CourseTypeCode;
                        IsData = true;
                        serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.BCABBAExamApplicationForm && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                    }

                    #endregion
                    break;

                case (int)(enServiceType.RegistrationService):
                    #region RegistrationService

                    var universityRegistrationDetail = _MSUoW.UniversityRegistrationDetail.FindBy(form => form.Id == Guid.Parse(paymentDetails.RequestId)).FirstOrDefault();
                    if (universityRegistrationDetail != null)
                    {
                        SelectedTenantWorkflowId = universityRegistrationDetail.TenantWorkflowId;
                        SelectedState = universityRegistrationDetail.State;
                        SelectedCollegeCode = universityRegistrationDetail.CollegeCode;
                        SelectedDepartmentCode = universityRegistrationDetail.DepartmentCode;
                        courseType = universityRegistrationDetail.CourseType;
                        IsData = true;
                        serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.RegistrationService && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                    }

                    #endregion
                    break;

                case (int)(enServiceType.MigrationService):
                    #region MigrationService

                    var universityMigrationDetail = _MSUoW.UniversityRegistrationDetail.FindBy(form => form.Id == Guid.Parse(paymentDetails.RequestId)).FirstOrDefault();
                    if (universityMigrationDetail != null)
                    {
                        SelectedTenantWorkflowId = universityMigrationDetail.TenantWorkflowId;
                        SelectedState = universityMigrationDetail.State;
                        SelectedCollegeCode = universityMigrationDetail.CollegeCode;
                        SelectedDepartmentCode = universityMigrationDetail.DepartmentCode;
                        courseType = universityMigrationDetail.CourseType;
                        IsData = true;
                        serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.MigrationService && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                    }

                    #endregion
                    break;
				case (int)(enServiceType.NursingApplicationForm):
					#region NursingApplicationForm
					var nursingDetails = _MSUoW.NursingFormfillupDetails.FindBy(form => form.Id == Guid.Parse(paymentDetails.RequestId)).FirstOrDefault();
					if (nursingDetails != null)
					{
						SelectedTenantWorkflowId = nursingDetails.TenantWorkflowId;
						SelectedState = nursingDetails.State;
						SelectedCollegeCode = nursingDetails.CollegeCode;
						courseType = nursingDetails.CourseTypeCode;
						IsData = true;
						serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.NursingApplicationForm && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
					}

					#endregion
					break;


			}
              if (IsData == true)
            {
                 var nextApprovingRoles = _workflowDbContext.TenantWorkflowTransitionConfig
                        .Where(transConfig => transConfig.TenantWorkflowId == SelectedTenantWorkflowId
                                && transConfig.IsActive && transConfig.SourceState == SelectedState).Select(transConfig => transConfig.Roles).FirstOrDefault();
                // It is assumed that a single role will be fetched, no need to separate these with 'comma'.
                if (nextApprovingRoles != null && nextApprovingRoles != string.Empty)
                {
                    // Find user details of that role
                    var approvingRoleArray = nextApprovingRoles.Split(",").Select(role => Convert.ToInt64(role)).ToArray();
                    if (Array.IndexOf(approvingRoleArray, (long)enRoles.Student) == -1)
                    {
                        // No student role involved, send email
                        var nextApprovingUser = new Users();

                        //get the user whether principal/HOD
                        bool isAffiliated = _MSUoW.CollegeMaster.FindBy(clg => clg.IsActive && clg.LookupCode.Trim().ToLower() == SelectedCollegeCode.Trim().ToLower()).Select(s => s.IsAffiliationRequired).FirstOrDefault();


                        //if (!isAffiliated && courseType.Trim().ToString() == _MSCommon.GetEnumDescription(enCourseType.Pg).Trim().ToString())
                        //{
                        //    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.HOD && usr.CollegeCode.Trim().ToLower() == SelectedCollegeCode.Trim().ToLower() && usr.DepartmentCode.Trim().ToLower() == SelectedDepartmentCode.Trim().ToLower() && usr.IsActive).FirstOrDefault();
                        //}
                        //else
                        //{
                        //    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.CollegeCode.Trim().ToLower() == SelectedCollegeCode.Trim().ToLower() && usr.IsActive).FirstOrDefault();
                        //}

                        //if isAffiliated true then mail should go to Principal
                        if (Array.IndexOf(approvingRoleArray, (long)enRoles.Principal) != -1 && isAffiliated)
                        { // Find the corresponding principal of the applying student
                            nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.CollegeCode == SelectedCollegeCode).FirstOrDefault();
                        }
                       else if (!isAffiliated && courseType.Trim().ToString() == _MSCommon.GetEnumDescription(enCourseType.Pg).Trim().ToString())
                        {
                            //if isAffiliated false and pg then mail should go to HOD
                            nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.HOD && usr.CollegeCode.Trim().ToLower() == SelectedCollegeCode.Trim().ToLower() && usr.DepartmentCode.Trim().ToLower() == SelectedDepartmentCode.Trim().ToLower() && usr.IsActive).FirstOrDefault();
                        }
                        else
                        {
                            var nextApprovingUserRole = _MSUoW.UserRoles.FindBy(usrRole => approvingRoleArray.Contains(usrRole.RoleId) && usrRole.IsActive).FirstOrDefault();
                            if (nextApprovingUserRole != null) // Checking this
                            {
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                            }
                        }


                        if (nextApprovingUser != null)
                        {
                            if (serviceName != null && serviceName != string.Empty && nextApprovingUser.EmailAddress != null && nextApprovingUser.EmailAddress != string.Empty)
                                SendEmailToApprovingAuthorityOnStateChange(nextApprovingUser.EmailAddress, nextApprovingUser.Name, serviceName);
                        }
                    }
                }
            }
        }

		private bool SendEmailToApprovingAuthorityOnStateChange(string emailAddress, string userName, string serviceName)
        {
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ServiceRequestReceived && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    // Decide whether 
                    string serverUrl = _iconfiguration.GetSection("ApplicationURL").Value;
                    string applicationUrl = _iconfiguration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _iconfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", serverUrl + collegeLogo);
                    body.Replace("--userName--", userName);
                    body.Replace("--url--", applicationUrl);
                    body.Replace("--serviceName--", serviceName);
                    _MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            return true;
        }

        public bool UpdateStatusToPaymentReceived(string id, long productinfoType, string CSCPayid)
		{
			bool isStatusUpdatedSucessfully = false;
            Guid gid;
            switch (Convert.ToInt16(productinfoType))
            {
                case (int)(enServiceType.BCABBAExamApplicationForm):
                    #region BCABBAExamApplicationForm

                     gid = new Guid(id);
                    ExaminationFormFillUpDetails examinationFormFillUpDetailsApplication = _MSUoW.ExaminationFormFillUpDetails.FindBy(a => a.IsActive == true && a.TenantId == 3 && a.Id == gid).FirstOrDefault();
                    if (examinationFormFillUpDetailsApplication != null)
                    {
                        Dictionary<string, object> triggerParameterDictionary = null;
                        string initialTrigger = _iconfiguration.GetSection("ExamFormFillup").GetSection("PaymentTrigger").Value;
                        string initialState = examinationFormFillUpDetailsApplication.State;
                        examinationFormFillUpDetailsApplication.LastStatusUpdateDate = DateTime.UtcNow;

                        examinationFormFillUpDetailsApplication.State = _iconfiguration.GetSection("ExamFormFillup").GetSection("PaymentDoneState").Value;
						examinationFormFillUpDetailsApplication.PaymentDate = DateTime.UtcNow;
						examinationFormFillUpDetailsApplication.CSCPayid = CSCPayid;

						_MSUoW.ExaminationFormFillUpDetails.Update(examinationFormFillUpDetailsApplication);

                        UserServiceApplied usa = _MSUoW.UserServiceApplied.FindBy(a => a.IsActive == true && a.TenantId == 3 && a.InstanceId == gid).FirstOrDefault();
                        usa.LastStatus = _iconfiguration.GetSection("ExamFormFillup").GetSection("PaymentDoneState").Value;
                        usa.IsActionRequired = false;
                        _MSUoW.UserServiceApplied.Update(usa);



                        _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, gid, examinationFormFillUpDetailsApplication.LastStatusUpdateDate, initialState, initialTrigger, examinationFormFillUpDetailsApplication.State, examinationFormFillUpDetailsApplication.TenantWorkflowId);
                        isStatusUpdatedSucessfully = true;
                        _MSUoW.Commit();
                    }
                    #endregion
                    break;

                case (int)(enServiceType.RegistrationService):
                    #region RegistrationService

                     gid = new Guid(id);
                    UniversityRegistrationDetail universityRegistrationDetailApplication = _MSUoW.UniversityRegistrationDetail.FindBy(a => a.IsActive == true && a.TenantId == 3 && a.Id == gid).FirstOrDefault();
                    if (universityRegistrationDetailApplication != null)
                    {
                        //Changing the State to Pending Done
                        Dictionary<string, object> triggerParameterDictionary = null;
                        string initialTrigger = _iconfiguration.GetSection("RegistrationNumber").GetSection("PaymentTrigger").Value;
                        string initialState = universityRegistrationDetailApplication.State;
                        string state = _iconfiguration.GetSection("RegistrationNumber").GetSection("PaymentDoneState").Value;
                        _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, gid, universityRegistrationDetailApplication.LastStatusUpdateDate, initialState, initialTrigger, state, universityRegistrationDetailApplication.TenantWorkflowId);

                        //Chenging the State to PendingWithPrincipalHOD
                        string initialTrigger2 = _iconfiguration.GetSection("RegistrationNumber").GetSection("RegistrationNumberApplyTrigger").Value;
                        string initialState2 = state;
                        string state2 = _iconfiguration.GetSection("RegistrationNumber").GetSection("PendingWithPrincipalHOD").Value;
                        _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, gid, universityRegistrationDetailApplication.LastStatusUpdateDate, initialState2, initialTrigger2, state2, universityRegistrationDetailApplication.TenantWorkflowId);
                        universityRegistrationDetailApplication.State = state2;
                        universityRegistrationDetailApplication.LastStatusUpdateDate = DateTime.UtcNow;
						universityRegistrationDetailApplication.PaymentDate = DateTime.UtcNow;
						universityRegistrationDetailApplication.CSCPayid = CSCPayid;
						_MSUoW.UniversityRegistrationDetail.Update(universityRegistrationDetailApplication);

                        //update user service applied Pending Action change
                        UserServiceApplied usa = _MSUoW.UserServiceApplied.FindBy(a => a.IsActive == true && a.TenantId == 3 && a.InstanceId == gid).FirstOrDefault();
                        usa.LastStatus = _iconfiguration.GetSection("RegistrationNumber").GetSection("PendingWithPrincipalHOD").Value;
                        usa.IsActionRequired = false;
                        _MSUoW.UserServiceApplied.Update(usa);
                        
                        isStatusUpdatedSucessfully = true;
                        _MSUoW.Commit();
                    }
                    
                    #endregion
                    break;

                case (int)(enServiceType.MigrationService):
                    #region MigrationService

                    gid = new Guid(id);
                    UniversityMigrationDetail universityMigrationDetailApplication = _MSUoW.UniversityMigrationDetail.FindBy(a => a.IsActive == true && a.TenantId == 3 && a.Id == gid).FirstOrDefault();
                    if (universityMigrationDetailApplication != null)
                    {
                        //Changing the State to Pending Done
                        Dictionary<string, object> triggerParameterDictionary = null;
                        string initialTrigger = _iconfiguration.GetSection("MigrationCertificate").GetSection("PaymentTrigger").Value;
                        string initialState = universityMigrationDetailApplication.State;
                        string state = _iconfiguration.GetSection("MigrationCertificate").GetSection("PaymentDoneState").Value;
                        _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, gid, universityMigrationDetailApplication.LastStatusUpdateDate, initialState, initialTrigger, state, universityMigrationDetailApplication.TenantWorkflowId);

                        //Chenging the State to PendingWithPrincipalHOD
                        string initialTrigger2 = _iconfiguration.GetSection("MigrationCertificate").GetSection("MigrationCertificateApplyTrigger").Value;
                        string initialState2 = state;
                        string state2 = _iconfiguration.GetSection("MigrationCertificate").GetSection("PendingWithPrincipalHOD").Value;
                        _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, gid, universityMigrationDetailApplication.LastStatusUpdateDate, initialState2, initialTrigger2, state2, universityMigrationDetailApplication.TenantWorkflowId);
                        universityMigrationDetailApplication.State = state2;
                        universityMigrationDetailApplication.LastStatusUpdateDate = DateTime.UtcNow;
						universityMigrationDetailApplication.PaymentDate = DateTime.UtcNow;
						universityMigrationDetailApplication.CSCPayid = CSCPayid;
						_MSUoW.UniversityMigrationDetail.Update(universityMigrationDetailApplication);

                        //update user service applied Pending Action change
                        UserServiceApplied usa = _MSUoW.UserServiceApplied.FindBy(a => a.IsActive == true && a.TenantId == 3 && a.InstanceId == gid).FirstOrDefault();
                        usa.LastStatus = _iconfiguration.GetSection("MigrationCertificate").GetSection("PendingWithPrincipalHOD").Value;
                        usa.IsActionRequired = false;
                        _MSUoW.UserServiceApplied.Update(usa);

                        isStatusUpdatedSucessfully = true;
                        _MSUoW.Commit();
                    }

                    #endregion
                    break;
				case (int)(enServiceType.NursingApplicationForm):
					#region NursingApplication

					gid = new Guid(id);
					NursingFormfillupDetails nursingDetails = _MSUoW.NursingFormfillupDetails.FindBy(a => a.IsActive == true && a.TenantId == 3 && a.Id == gid).FirstOrDefault();
					if (nursingDetails != null)
					{
						Dictionary<string, object> triggerParameterDictionary = null;
						string initialTrigger = _iconfiguration.GetSection("NursingFormFillup").GetSection("PaymentTrigger").Value;
						string initialState = nursingDetails.State;
						nursingDetails.LastStatusUpdateDate = DateTime.UtcNow;

						nursingDetails.State = _iconfiguration.GetSection("NursingFormFillup").GetSection("PaymentDoneState").Value;
						nursingDetails.PaymentDate = DateTime.UtcNow;
						nursingDetails.CSCPayid = CSCPayid;

						_MSUoW.NursingFormfillupDetails.Update(nursingDetails);

						UserServiceApplied usa = _MSUoW.UserServiceApplied.FindBy(a => a.IsActive == true && a.TenantId == 3 && a.InstanceId == gid).FirstOrDefault();
						usa.LastStatus = _iconfiguration.GetSection("NursingFormFillup").GetSection("PaymentDoneState").Value;
						usa.IsActionRequired = false;
						_MSUoW.UserServiceApplied.Update(usa);



						_WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, gid, nursingDetails.LastStatusUpdateDate, initialState, initialTrigger, nursingDetails.State, nursingDetails.TenantWorkflowId);
						isStatusUpdatedSucessfully = true;
						_MSUoW.Commit();
					}
					#endregion
					break;



			}
        
			return isStatusUpdatedSucessfully;
		}
    
    public TransactionLog GetTransactionLogById(string id)
		{
			Guid gid = new Guid(id);
			TransactionLog transactionLog = _MSUoW.TransactionLog.FindBy(a => a.Id == gid && a.IsActive == true && a.TenantId == 3).FirstOrDefault();
			if (transactionLog != null)
			{
				ServiceMaster serviceMaster = _MSUoW.ServiceMaster.GetById(Convert.ToInt64(transactionLog.ProductInfo));
				transactionLog.ProductInfo = serviceMaster.ServiceName;
                //Sitikanta
                //convert utc datetime format to ist datetime format only showing propose 
                DateTime utcdate = DateTime.ParseExact(transactionLog.CreationTime.ToString("MM-dd-yyyy HH:mm:ss"), "MM-dd-yyyy HH:mm:ss", CultureInfo.InvariantCulture);						   				
				var istdate = TimeZoneInfo.ConvertTimeFromUtc(utcdate, TZConvert.GetTimeZoneInfo(_iconfiguration.GetSection("TimeZone").Value.ToString()));
				transactionLog.CreationTime = istdate;
            }
			return transactionLog;
		}

		public bool IsPaymentRequestinValidState(TransactionLog paymentDetail)
		{
			if (paymentDetail.ProductInfo.Equals(((int)enServiceType.BCABBAExamApplicationForm).ToString(), StringComparison.InvariantCultureIgnoreCase))
			{				
				Guid gid = new Guid(paymentDetail.RequestId);
				ExaminationFormFillUpDetails application = _MSUoW.ExaminationFormFillUpDetails.FindBy(a => a.IsActive == true && a.TenantId == 3 && a.Id == gid).FirstOrDefault();
				if (application != null && application.State == _iconfiguration.GetSection("ExamFormFillup").GetSection("PaymentPendingState").Value)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			if (paymentDetail.ProductInfo.Equals(((int)enServiceType.RegistrationService).ToString(), StringComparison.InvariantCultureIgnoreCase))
			{

                Guid gid = new Guid(paymentDetail.RequestId);
                UniversityRegistrationDetail application = _MSUoW.UniversityRegistrationDetail.FindBy(a => a.IsActive == true && a.TenantId == 3 && a.Id == gid).FirstOrDefault();
                if (application != null && application.State == _iconfiguration.GetSection("RegistrationNumber").GetSection("PaymentPendingState").Value)
                {
                    return true;
                }
                else
                {
                    return false;
                }               
			}
            if (paymentDetail.ProductInfo.Equals(((int)enServiceType.MigrationService).ToString(), StringComparison.InvariantCultureIgnoreCase))
            {                
                Guid gid = new Guid(paymentDetail.RequestId);
                UniversityMigrationDetail application = _MSUoW.UniversityMigrationDetail.FindBy(a => a.IsActive == true && a.TenantId == 3 && a.Id == gid).FirstOrDefault();
                if (application != null && application.State == _iconfiguration.GetSection("MigrationCertificate").GetSection("PaymentPendingState").Value)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
			if (paymentDetail.ProductInfo.Equals(((int)enServiceType.NursingApplicationForm).ToString(), StringComparison.InvariantCultureIgnoreCase))
			{
				Guid gid = new Guid(paymentDetail.RequestId);
				NursingFormfillupDetails application = _MSUoW.NursingFormfillupDetails.GetAll().Where(a => a.IsActive == true && a.TenantId == 3 && a.Id == gid).FirstOrDefault();
				if (application != null && application.State == _iconfiguration.GetSection("NursingFormFillup").GetSection("PaymentPendingState").Value)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			return false;			
		}
		
		private ExaminationMaster GetExaminationMaster(string CourseTypeCode, string CourseCode, string SemesterCode, string StreamCode, string SubjectCode)
		{

			if (CourseTypeCode == "01")
			{
				return _MSUoW.ExaminationMaster.FindBy(a => a.CourseType == CourseTypeCode
											&& a.SemesterCode == SemesterCode
											&& a.StreamCode == StreamCode
											&& a.SubjectCode == SubjectCode
											&& a.TenantId == 3
											&& a.IsActive == true).FirstOrDefault();


			}
			return _MSUoW.ExaminationMaster.FindBy(a => a.CourseType == CourseTypeCode
											&& a.CourseCode == CourseCode
											&& a.SemesterCode == SemesterCode
											&& a.StreamCode == StreamCode
											&& a.SubjectCode == SubjectCode
											&& a.TenantId == 3
											&& a.IsActive == true).FirstOrDefault();


		}

		private SubjectMaster GetSubjectMaster(string CourseTypeCode, string CourseCode, string StreamCode, string SubjectCode)
		{

			if (CourseTypeCode == "01")
			{
				return _MSUoW.SubjectMaster.FindBy(a => a.CourseTypeCode == CourseTypeCode											
											&& a.StreamCode == StreamCode
											&& a.LookupCode == SubjectCode
											&& a.TenantId == 3
											&& a.IsActive == true).FirstOrDefault();


			}
			return _MSUoW.SubjectMaster.FindBy(a => a.CourseTypeCode == CourseTypeCode
											&& a.CourseCode == CourseCode											
											&& a.StreamCode == StreamCode
											&& a.LookupCode == SubjectCode
											&& a.TenantId == 3
											&& a.IsActive == true).FirstOrDefault();


		}



		

		
		public Tuple<PaymentPricingInfoDto, string> GetPricingInfoByService(string id, string ser)
		{
            PaymentPricingInfoDto paymentPricingInfoDto = null;
			ExaminationMaster examMaster = new ExaminationMaster();
			ExaminationPricingMaster examinationPricingMaster = new ExaminationPricingMaster();
			ExaminationSession sessionData = new ExaminationSession();
			UniversityRegistrationDetail studentApplied = new UniversityRegistrationDetail();
			DateTime currTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TZConvert.GetTimeZoneInfo("India Standard Time")); ;

			ExaminationFormFillUpDates validDate = new ExaminationFormFillUpDates();
			LateFineMaster lateFine = new LateFineMaster();
			CenterAllocation center = new CenterAllocation();
			SubjectMaster subjectMaster = new SubjectMaster();
			CollegeSubjectMapper collegeSubjectMapper = new CollegeSubjectMapper();
			ServiceMaster srvcMaster = new ServiceMaster();

			Tuple<PaymentPricingInfoDto, string> result = new Tuple<PaymentPricingInfoDto, string> (null, String.Empty);

			string ErrorMessage = String.Empty;
			Decimal LateFee = 0;
			Decimal OriginalCertificateFee = 0;
			Decimal ProvisionalCertificateFee = 0;
			Decimal AdditionalCentreCharges = 0;

			ServiceMaster serviceMaster = _MSUoW.ServiceMasterRepository.GetServiceMasterById(Convert.ToInt16(ser));
            List<ServiceMaster> payOnlyCharges = _MSUoW.ServiceMaster.GetAll().Where(record => record.IsActive).ToList();
            //List<ServiceMaster> payOnlyCharges = _MSUoW.ServiceMaster.GetAll().Where(record=>record.IsActive).ToList();
            UserDetailsDto user = _userService.GetAllUserInfo();
			Guid guid_instance_id = new Guid(id);


			paymentPricingInfoDto = new PaymentPricingInfoDto();
			paymentPricingInfoDto.ServicePayments = new List<ServicePaymentDto>();
			paymentPricingInfoDto.ConveniencePayments = new List<ServicePaymentDto>();


			switch (Convert.ToInt16(ser))
			{
				
				case (int)(enServiceType.BCABBAExamApplicationForm):
					#region BCABBAExamApplicationForm
					
					ExaminationFormFillUpDetailsDto examinationFormFillupDto = _ExaminationFormFillUpService.GetExamFormFillUpDetails(guid_instance_id);
					examMaster = GetExaminationMaster(examinationFormFillupDto.CourseTypeCode, examinationFormFillupDto.CourseCode, examinationFormFillupDto.SemesterCode, examinationFormFillupDto.StreamCode, examinationFormFillupDto.SubjectCode);

					if (examMaster == null)
					{
						result = new Tuple<PaymentPricingInfoDto, string>(null, "No Exam is Scheduled.");
						return result;
					}
					examinationPricingMaster = _MSUoW.ExaminationPricingMaster.FindBy(e => e.IsActive && e.ExaminationMasterId == examMaster.Id).FirstOrDefault();

					if (examinationPricingMaster == null)
					{
						result = new Tuple<PaymentPricingInfoDto, string>(null, "No Pricing Detail is set for the Scheduled Exam.");
						return result;
					}

					//ServiceMaster serviceMaster = _MSUoW.ServiceMaster.GetById(Convert.ToInt64(ser));					
					//Logic For Late Fine
					sessionData = _MSUoW.ExaminationSession.FindBy(sess => sess.ExaminationMasterId == examMaster.Id && sess.IsActive && sess.StartYear == examinationFormFillupDto.AcademicStart).FirstOrDefault();

					if (sessionData == null)
					{
						result = new Tuple<PaymentPricingInfoDto, string>(null, "No Exam is Scheduled for the Session " + examinationFormFillupDto.AcademicStart);
						return result;
					}
					//var currTime = DateTime.UtcNow;
					//The Principal Verification Time is taken into consideration
					//currTime = examinationFormFillupDto.LastModificationTime != null ? Convert.ToDateTime(examinationFormFillupDto.LastModificationTime) : DateTime.UtcNow;
					//					
					validDate = _MSUoW.ExaminationFormFillUpDates.FindBy(d => d.ExaminationSessionId == sessionData.Id
																										&& d.IsActive
																										&& d.StartDate.Date <= currTime.Date
																										&& currTime.Date <= d.EndDate.Date
																										).OrderByDescending(a => a.SlotOrder).FirstOrDefault();

                    //ExaminationFormFillUpDates validDate = _MSUoW.ExaminationFormFillUpDates.FindBy(d => d.ExaminationSessionId == sessionData.Id
                    //                                                                                    && d.IsActive
                    //                                                                                    && d.StartDate <= currTime
                    //                                                                                    && currTime <= d.EndDate
                    //                                                                                    ).OrderByDescending(a => a.SlotOrder).FirstOrDefault();

                    if (validDate == null)
					{
						result = new Tuple<PaymentPricingInfoDto, string>(null, "Either the Form Fillup date is over or not yet started");
						return result;
					}
					
					lateFine = _MSUoW.LateFineMaster.FindBy(l => l.IsActive && l.SlotOrder == validDate.SlotOrder).FirstOrDefault();
					if (lateFine != null){
						LateFee = lateFine.FineAmount;
					}
					

					// Logic For Additional Center Charges
					center = _MSUoW.CenterAllocation.FindBy(c => c.IsActive && c.ExaminationSessionId == sessionData.Id).FirstOrDefault();

					if (center != null && center.CenterCollegeCode != center.CollegeCode)
					{
						AdditionalCentreCharges = examinationPricingMaster.AdditionalCentreCharges;
					}	

					//Logic For Last Semester Calculation
					subjectMaster = GetSubjectMaster(examinationFormFillupDto.CourseTypeCode, examinationFormFillupDto.CourseCode, examinationFormFillupDto.StreamCode, examinationFormFillupDto.SubjectCode);

					long maxNoOfSems = Convert.ToInt16( subjectMaster.SubjectDuration * 2 );					
					//Is Last Semester
					if (Convert.ToInt16(examinationFormFillupDto.SemesterCode) == maxNoOfSems)
					{
						OriginalCertificateFee = examinationPricingMaster.OriginalCertificateFee;
						ProvisionalCertificateFee = examinationPricingMaster.ProvisionalCertificateFee;
					}	

					paymentPricingInfoDto = new PaymentPricingInfoDto
					{
						//Student Details
						StudentName = examinationFormFillupDto.StudentName,
						PhoneNumber = user.PhoneNumber,
						EmailAddress = user.EmailAddress,
						RegistrationNumber = examinationFormFillupDto.RegistrationNumber,

						//ServiceType = enServiceType.BCABBAExamApplicationForm.ToString(),
                        ServiceType = serviceMaster.ServiceName,
						CourseTypeCode = examMaster.CourseType,

						//Fixed Charges
						ExaminationFees = examinationPricingMaster.ExaminationFees,
						CentreCharges = examinationPricingMaster.CentreCharges,
						EnrollmentFees = examinationPricingMaster.EnrollmentFees,
						FeesForSupervision = examinationPricingMaster.FeesForSupervision,
						FeesForMarks = examinationPricingMaster.FeesForMarks,
						
						//Conditional Charges
						AdditionalCentreCharges = AdditionalCentreCharges,
						ProvisionalCertificateFee = ProvisionalCertificateFee,						
						OriginalCertificateFee = OriginalCertificateFee,
						LateFee = LateFee,

						//Service Charge
						ServiceCharge = examinationPricingMaster.ServiceCharge,
						PaymentStatus = examinationFormFillupDto.State,
						ServicePayments = new List<ServicePaymentDto>(),
						ConveniencePayments = new List<ServicePaymentDto>()
					};

					paymentPricingInfoDto.TotalFee =
						+ paymentPricingInfoDto.ExaminationFees
						+ paymentPricingInfoDto.CentreCharges
						+ paymentPricingInfoDto.EnrollmentFees
						+ paymentPricingInfoDto.FeesForSupervision
						+ paymentPricingInfoDto.FeesForMarks
						+ paymentPricingInfoDto.ProvisionalCertificateFee
						+ paymentPricingInfoDto.AdditionalCentreCharges
						+ paymentPricingInfoDto.OriginalCertificateFee
						+ paymentPricingInfoDto.LateFee;


					paymentPricingInfoDto.ConveniencePayments.Add(new ServicePaymentDto { FeeDescription = "Examination Convenience Fees", FeeAmount = paymentPricingInfoDto.ServiceCharge });

					#region Additional Convenience Charge Calculation

					//if subjectdmasterId exist then get the isaffiliationcompleted from collegesubjectmapper
					collegeSubjectMapper = _MSUoW.CollegeSubjectMapper.FindBy(csm => csm.IsActive && csm.CollegeCode == examinationFormFillupDto.CollegeCode
																		&& csm.SubjectMasterId == subjectMaster.Id)
																		.FirstOrDefault();

					//if (collegeSubjectMapper.IsAffiliationCompleted)
					//{
						studentApplied = _MSUoW.UniversityRegistrationDetail.FindBy(element => element.CreatorUserId == GetCurrentUserId() && element.IsActive).OrderByDescending(d => d.LastModificationTime).FirstOrDefault();

                        if (!(studentApplied != null && studentApplied.State.Trim().ToLower() == _iconfiguration.GetSection("RegistrationNumber:RegistrationNumberApprovedStatus").Value.Trim().ToLower()))
                        {                        
							srvcMaster = _MSUoW.ServiceMasterRepository.GetServiceMasterById(Convert.ToInt16((int)(enServiceType.RegistrationService)));
							srvcMaster.ServicePricingMasters.Where(p => p.IsConvenienceFee == true).ToList().ForEach(s =>
							{
								ServicePaymentDto servicePmtDto = new ServicePaymentDto();
								servicePmtDto.FeeDescription = s.FeeDescription;
								servicePmtDto.FeeAmount = s.FeeAmount;
								paymentPricingInfoDto.ServiceCharge += Convert.ToDecimal(s.FeeAmount);
								paymentPricingInfoDto.ConveniencePayments.Add(servicePmtDto);
							});
						}
					//}
					#endregion

					if (serviceMaster.IsPayOnlyServiceCharges)
					{
						paymentPricingInfoDto.IsPayAmount = true;
						paymentPricingInfoDto.TotalAmountToBePaid = paymentPricingInfoDto.ServiceCharge;
					}
					else
					{
						paymentPricingInfoDto.IsPayAmount = false;
						paymentPricingInfoDto.TotalAmountToBePaid = paymentPricingInfoDto.TotalFee + paymentPricingInfoDto.ServiceCharge;
					}

					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Examination Fee", FeeAmount = paymentPricingInfoDto.ExaminationFees });
					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Centre Charges", FeeAmount = paymentPricingInfoDto.CentreCharges });
					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Enrollment Fee", FeeAmount = paymentPricingInfoDto.EnrollmentFees });
					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Fees For Supervision", FeeAmount = paymentPricingInfoDto.FeesForSupervision });
					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Fees For Marks", FeeAmount = paymentPricingInfoDto.FeesForMarks });
					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Provisional Certificate Fee", FeeAmount = paymentPricingInfoDto.ProvisionalCertificateFee });
					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Additional Centre Charges", FeeAmount = paymentPricingInfoDto.AdditionalCentreCharges });
					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Original Certificate Fee", FeeAmount = paymentPricingInfoDto.OriginalCertificateFee });
					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Late Fee", FeeAmount = paymentPricingInfoDto.LateFee });



					#endregion
					break;

				case (int)(enServiceType.RegistrationService):
					#region RegistrationService


					paymentPricingInfoDto = new PaymentPricingInfoDto();
					paymentPricingInfoDto.ServicePayments = new List<ServicePaymentDto>();
					paymentPricingInfoDto.ConveniencePayments = new List<ServicePaymentDto>();

					UniversityRegistrationDetail universityRegistrationDetail = _studentRegistrationNoService.GetStudentUniversityRegistrationDetailsById(guid_instance_id);


					if (universityRegistrationDetail != null)
					{						
						paymentPricingInfoDto.StudentName = universityRegistrationDetail.StudentName;
						paymentPricingInfoDto.EmailAddress = user.EmailAddress;
						paymentPricingInfoDto.PhoneNumber = user.PhoneNumber;
						paymentPricingInfoDto.PaymentStatus = universityRegistrationDetail.State;
					}
					if (serviceMaster != null)
					{
						paymentPricingInfoDto.ServiceType = serviceMaster.ServiceName;
						//paymentPricingInfoDto.ServiceCharge = serviceMaster.ServiceConveniencePricingMaster != null ? Convert.ToDecimal(serviceMaster.ServiceConveniencePricingMaster.ConvenienceFee) : 0;


						paymentPricingInfoDto.TotalFee = 0;
						paymentPricingInfoDto.ServiceCharge = 0;

						serviceMaster.ServicePricingMasters.Where(p=>p.IsConvenienceFee == false).ToList().ForEach(s =>
                        {
                            ServicePaymentDto servicePmtDto = new ServicePaymentDto();
                            servicePmtDto.FeeDescription = s.FeeDescription;
                            servicePmtDto.FeeAmount = s.FeeAmount;
                            paymentPricingInfoDto.TotalFee += Convert.ToDecimal(s.FeeAmount);
                            paymentPricingInfoDto.ServicePayments.Add(servicePmtDto);
                        });

						serviceMaster.ServicePricingMasters.Where(p => p.IsConvenienceFee == true).ToList().ForEach(s =>
						{
							ServicePaymentDto servicePmtDto = new ServicePaymentDto();
							servicePmtDto.FeeDescription = s.FeeDescription;
							servicePmtDto.FeeAmount = s.FeeAmount;
							paymentPricingInfoDto.ServiceCharge += Convert.ToDecimal(s.FeeAmount);
							paymentPricingInfoDto.ConveniencePayments.Add(servicePmtDto);
						});

						

						//ServiceMaster PayOnlyRegistartionServiceCharge = payOnlyCharges.Where(record => record.Id == (long)enServiceType.RegistrationService && record.IsPayOnlyServiceCharges == true).FirstOrDefault();

                        if (serviceMaster.IsPayOnlyServiceCharges == true)
                        {
                            paymentPricingInfoDto.IsPayAmount = true;
                            paymentPricingInfoDto.TotalAmountToBePaid = paymentPricingInfoDto.ServiceCharge;
                        }
                        else
                        {
                            paymentPricingInfoDto.IsPayAmount = false;
                            paymentPricingInfoDto.TotalAmountToBePaid = paymentPricingInfoDto.TotalFee + paymentPricingInfoDto.ServiceCharge;
                        }
                    }
                    #endregion
                    break;


                case (int)(enServiceType.MigrationService):
                    #region MigrationService


                    paymentPricingInfoDto = new PaymentPricingInfoDto();
                    paymentPricingInfoDto.ServicePayments = new List<ServicePaymentDto>();
                    paymentPricingInfoDto.ConveniencePayments = new List<ServicePaymentDto>();

                    UniversityMigrationDetail universityMigrationDetail = _studentMigrationService.GetStudentMigrationDetailById(guid_instance_id);


                    if (universityMigrationDetail != null)
                    {
                        paymentPricingInfoDto.StudentName = universityMigrationDetail.StudentName;
                        paymentPricingInfoDto.EmailAddress = user.EmailAddress;
                        paymentPricingInfoDto.PhoneNumber = user.PhoneNumber;
                        paymentPricingInfoDto.PaymentStatus = universityMigrationDetail.State;
                    }
					//if (serviceMaster != null)
					//{
					//    paymentPricingInfoDto.ServiceType = serviceMaster.ServiceName;
					//    paymentPricingInfoDto.ServiceCharge = serviceMaster.ServiceConveniencePricingMaster != null ? Convert.ToDecimal(serviceMaster.ServiceConveniencePricingMaster.ConvenienceFee) : 0;
					//    paymentPricingInfoDto.TotalFee = 0;
					//    serviceMaster.ServicePricingMasters.ToList().ForEach(s =>
					//    {
					//        ServicePaymentDto servicePmtDto = new ServicePaymentDto();
					//        servicePmtDto.FeeDescription = s.FeeDescription;
					//        servicePmtDto.FeeAmount = s.FeeAmount;
					//        paymentPricingInfoDto.TotalFee += Convert.ToDecimal(s.FeeAmount);
					//        paymentPricingInfoDto.ServicePayments.Add(servicePmtDto);
					//    });
					//    paymentPricingInfoDto.TotalAmountToBePaid = paymentPricingInfoDto.TotalFee + paymentPricingInfoDto.ServiceCharge;
					//}

					if (serviceMaster != null)
					{
						paymentPricingInfoDto.ServiceType = serviceMaster.ServiceName;
						//paymentPricingInfoDto.ServiceCharge = serviceMaster.ServiceConveniencePricingMaster != null ? Convert.ToDecimal(serviceMaster.ServiceConveniencePricingMaster.ConvenienceFee) : 0;


						paymentPricingInfoDto.TotalFee = 0;
						paymentPricingInfoDto.ServiceCharge = 0;

						serviceMaster.ServicePricingMasters.Where(p => p.IsConvenienceFee == false).ToList().ForEach(s =>
						{
							ServicePaymentDto servicePmtDto = new ServicePaymentDto();
							servicePmtDto.FeeDescription = s.FeeDescription;
							servicePmtDto.FeeAmount = s.FeeAmount;
							paymentPricingInfoDto.TotalFee += Convert.ToDecimal(s.FeeAmount);
							paymentPricingInfoDto.ServicePayments.Add(servicePmtDto);
						});

						serviceMaster.ServicePricingMasters.Where(p => p.IsConvenienceFee == true).ToList().ForEach(s =>
						{
							ServicePaymentDto servicePmtDto = new ServicePaymentDto();
							servicePmtDto.FeeDescription = s.FeeDescription;
							servicePmtDto.FeeAmount = s.FeeAmount;
							paymentPricingInfoDto.ServiceCharge += Convert.ToDecimal(s.FeeAmount);
							paymentPricingInfoDto.ConveniencePayments.Add(servicePmtDto);
						});



						//ServiceMaster PayOnlyRegistartionServiceCharge = payOnlyCharges.Where(record => record.Id == (long)enServiceType.RegistrationService && record.IsPayOnlyServiceCharges == true).FirstOrDefault();

						if (serviceMaster.IsPayOnlyServiceCharges == true)
						{
							paymentPricingInfoDto.IsPayAmount = true;
							paymentPricingInfoDto.TotalAmountToBePaid = paymentPricingInfoDto.ServiceCharge;
						}
						else
						{
							paymentPricingInfoDto.IsPayAmount = false;
							paymentPricingInfoDto.TotalAmountToBePaid = paymentPricingInfoDto.TotalFee + paymentPricingInfoDto.ServiceCharge;
						}
					}

					#endregion

					break;

                case (int)(enServiceType.CollegeLeavingCertificate):
                    #region CollegeLeavingCertificate

                    paymentPricingInfoDto = new PaymentPricingInfoDto();
                    paymentPricingInfoDto.ServicePayments = new List<ServicePaymentDto>();

                    ApplyCLCDto CollegeLeavingCertificateDetailsDto = _studentCLCService.GetClcDetailById(guid_instance_id);


                    if (CollegeLeavingCertificateDetailsDto != null)
                    {
                        paymentPricingInfoDto.StudentName = CollegeLeavingCertificateDetailsDto.StudentName;
                        paymentPricingInfoDto.EmailAddress = user.EmailAddress;
                        paymentPricingInfoDto.PhoneNumber = user.PhoneNumber;
                        paymentPricingInfoDto.PaymentStatus = CollegeLeavingCertificateDetailsDto.State;
                    }
					//if (serviceMaster != null)
					//{
					//    paymentPricingInfoDto.ServiceType = serviceMaster.ServiceName;
					//    paymentPricingInfoDto.ServiceCharge = serviceMaster.ServiceConveniencePricingMaster != null ? Convert.ToDecimal(serviceMaster.ServiceConveniencePricingMaster.ConvenienceFee) : 0;
					//    paymentPricingInfoDto.TotalFee = 0;
					//    serviceMaster.ServicePricingMasters.ToList().ForEach(s =>
					//    {
					//        ServicePaymentDto servicePmtDto = new ServicePaymentDto();
					//        servicePmtDto.FeeDescription = s.FeeDescription;
					//        servicePmtDto.FeeAmount = s.FeeAmount;
					//        paymentPricingInfoDto.TotalFee += Convert.ToDecimal(s.FeeAmount);
					//        paymentPricingInfoDto.ServicePayments.Add(servicePmtDto);
					//    });

					//    ServiceMaster PayOnlyRegistartionServiceCharge = payOnlyCharges.Where(record => record.Id == (long)enServiceType.RegistrationService && record.IsPayOnlyServiceCharges == true).FirstOrDefault();

					//    if (PayOnlyRegistartionServiceCharge != null)
					//    {
					//        paymentPricingInfoDto.IsPayAmount = PayOnlyRegistartionServiceCharge.IsPayOnlyServiceCharges;
					//        paymentPricingInfoDto.TotalAmountToBePaid = paymentPricingInfoDto.ServiceCharge;
					//    }
					//    else
					//    {
					//        paymentPricingInfoDto.IsPayAmount = false;
					//        paymentPricingInfoDto.TotalAmountToBePaid = paymentPricingInfoDto.TotalFee + paymentPricingInfoDto.ServiceCharge;
					//    }
					//}

					if (serviceMaster != null)
					{
						paymentPricingInfoDto.ServiceType = serviceMaster.ServiceName;
						//paymentPricingInfoDto.ServiceCharge = serviceMaster.ServiceConveniencePricingMaster != null ? Convert.ToDecimal(serviceMaster.ServiceConveniencePricingMaster.ConvenienceFee) : 0;


						paymentPricingInfoDto.TotalFee = 0;
						paymentPricingInfoDto.ServiceCharge = 0;

						serviceMaster.ServicePricingMasters.Where(p => p.IsConvenienceFee == false).ToList().ForEach(s =>
						{
							ServicePaymentDto servicePmtDto = new ServicePaymentDto();
							servicePmtDto.FeeDescription = s.FeeDescription;
							servicePmtDto.FeeAmount = s.FeeAmount;
							paymentPricingInfoDto.TotalFee += Convert.ToDecimal(s.FeeAmount);
							paymentPricingInfoDto.ServicePayments.Add(servicePmtDto);
						});

						serviceMaster.ServicePricingMasters.Where(p => p.IsConvenienceFee == true).ToList().ForEach(s =>
						{
							ServicePaymentDto servicePmtDto = new ServicePaymentDto();
							servicePmtDto.FeeDescription = s.FeeDescription;
							servicePmtDto.FeeAmount = s.FeeAmount;
							paymentPricingInfoDto.ServiceCharge += Convert.ToDecimal(s.FeeAmount);
							paymentPricingInfoDto.ConveniencePayments.Add(servicePmtDto);
						});



						//ServiceMaster PayOnlyRegistartionServiceCharge = payOnlyCharges.Where(record => record.Id == (long)enServiceType.RegistrationService && record.IsPayOnlyServiceCharges == true).FirstOrDefault();

						if (serviceMaster.IsPayOnlyServiceCharges == true)
						{
							paymentPricingInfoDto.IsPayAmount = true;
							paymentPricingInfoDto.TotalAmountToBePaid = paymentPricingInfoDto.ServiceCharge;
						}
						else
						{
							paymentPricingInfoDto.IsPayAmount = false;
							paymentPricingInfoDto.TotalAmountToBePaid = paymentPricingInfoDto.TotalFee + paymentPricingInfoDto.ServiceCharge;
						}
					}
					#endregion

					break;

				case (int)(enServiceType.NursingApplicationForm):
					#region Nursing Application

					NursingFormFillUpDto nursingDto = _nursingFormFillUpService.GetNursingFormFillUpDetails(guid_instance_id);
					examMaster = GetExaminationMaster(nursingDto.CourseTypeCode, nursingDto.CourseCode, nursingDto.CurrentAcademicYear, nursingDto.StreamCode, nursingDto.SubjectCode);

					if (examMaster == null)
					{
						result = new Tuple<PaymentPricingInfoDto, string>(null, "No Exam is Scheduled.");
						return result;
					}
					examinationPricingMaster = _MSUoW.ExaminationPricingMaster.FindBy(e => e.IsActive && e.ExaminationMasterId == examMaster.Id).FirstOrDefault();

					if (examinationPricingMaster == null)
					{
						result = new Tuple<PaymentPricingInfoDto, string>(null, "No Pricing Detail is set for the Scheduled Exam.");
						return result;
					}

					//ServiceMaster serviceMaster = _MSUoW.ServiceMaster.GetById(Convert.ToInt64(ser));					
					//Logic For Late Fine
					sessionData = _MSUoW.ExaminationSession.FindBy(sess => sess.ExaminationMasterId == examMaster.Id && sess.IsActive && sess.StartYear == nursingDto.AcademicStart).FirstOrDefault();

					if (sessionData == null)
					{
						result = new Tuple<PaymentPricingInfoDto, string>(null, "No Exam is Scheduled for the Session " + nursingDto.AcademicStart);
						return result;
					}
					//var currTime = DateTime.UtcNow;
					//The Principal Verification Time is taken into consideration
					//currTime = nursingDto.LastModificationTime != null ? Convert.ToDateTime(nursingDto.LastModificationTime) : DateTime.UtcNow;					
					validDate = _MSUoW.ExaminationFormFillUpDates.FindBy(d => d.ExaminationSessionId == sessionData.Id
																										&& d.IsActive
																										&& d.StartDate.Date <= currTime.Date
																										&& currTime.Date <= d.EndDate.Date
																										).OrderByDescending(a => a.SlotOrder).FirstOrDefault();

					if (validDate == null)
					{
						result = new Tuple<PaymentPricingInfoDto, string>(null, "Either the Form Fillup date is over or not yet started");
						return result;
					}

					lateFine = _MSUoW.LateFineMaster.FindBy(l => l.IsActive && l.SlotOrder == validDate.SlotOrder).FirstOrDefault();
					if (lateFine != null)
					{
						LateFee = lateFine.FineAmount;
					}


					// Logic For Additional Center Charges
					center = _MSUoW.CenterAllocation.FindBy(c => c.IsActive && c.ExaminationSessionId == sessionData.Id).FirstOrDefault();

					if (center != null && center.CenterCollegeCode != center.CollegeCode)
					{
						AdditionalCentreCharges = examinationPricingMaster.AdditionalCentreCharges;
					}

					//Logic For Last Semester Calculation
					subjectMaster = GetSubjectMaster(nursingDto.CourseTypeCode, nursingDto.CourseCode, nursingDto.StreamCode, nursingDto.SubjectCode);

					long maxNoOfYears = Convert.ToInt32(subjectMaster.SubjectDuration);
					//Is Last Semester
					if (Convert.ToInt32(nursingDto.CurrentAcademicYear) == maxNoOfYears)
					{
						OriginalCertificateFee = examinationPricingMaster.OriginalCertificateFee;
						ProvisionalCertificateFee = examinationPricingMaster.ProvisionalCertificateFee;
					}

					paymentPricingInfoDto = new PaymentPricingInfoDto
					{
						//Student Details
						StudentName = nursingDto.StudentName,
						PhoneNumber = user.PhoneNumber,
						EmailAddress = user.EmailAddress,
						RegistrationNumber = nursingDto.RegistrationNumber,

						//ServiceType = enServiceType.BCABBAExamApplicationForm.ToString(),
						ServiceType = serviceMaster.ServiceName,
						CourseTypeCode = examMaster.CourseType,

						//Fixed Charges
						ExaminationFees = examinationPricingMaster.ExaminationFees,
						CentreCharges = examinationPricingMaster.CentreCharges,
						EnrollmentFees = examinationPricingMaster.EnrollmentFees,
						FeesForMarks = examinationPricingMaster.FeesForMarks,
						FeesForSupervision = examinationPricingMaster.FeesForSupervision,
						ExamRegistrationFee = examinationPricingMaster.ExamRegistrationFee,

						//Conditional Charges
						AdditionalCentreCharges = AdditionalCentreCharges,
						ProvisionalCertificateFee = ProvisionalCertificateFee,
						OriginalCertificateFee = OriginalCertificateFee,
						LateFee = LateFee,

						//Service Charge
						ServiceCharge = examinationPricingMaster.ServiceCharge,
						PaymentStatus = nursingDto.State,
						ServicePayments = new List<ServicePaymentDto>(),
						ConveniencePayments = new List<ServicePaymentDto>()
					};

					paymentPricingInfoDto.TotalFee =
						+paymentPricingInfoDto.ExaminationFees
						+ paymentPricingInfoDto.CentreCharges
						+ paymentPricingInfoDto.EnrollmentFees
						+ paymentPricingInfoDto.FeesForSupervision
						+ paymentPricingInfoDto.FeesForMarks
						+ paymentPricingInfoDto.ExamRegistrationFee
						+ paymentPricingInfoDto.ProvisionalCertificateFee
						+ paymentPricingInfoDto.AdditionalCentreCharges
						+ paymentPricingInfoDto.OriginalCertificateFee
						+ paymentPricingInfoDto.LateFee;


					paymentPricingInfoDto.ConveniencePayments.Add(new ServicePaymentDto { FeeDescription = "Nursing Exam Convenience Fees", FeeAmount = paymentPricingInfoDto.ServiceCharge });

					#region Additional Convenience Charge Calculation

					//if subjectdmasterId exist then get the isaffiliationcompleted from collegesubjectmapper
					collegeSubjectMapper = _MSUoW.CollegeSubjectMapper.FindBy(csm => csm.IsActive && csm.CollegeCode == nursingDto.CollegeCode
																		&& csm.SubjectMasterId == subjectMaster.Id)
																		.FirstOrDefault();

					//if (collegeSubjectMapper.IsAffiliationCompleted)
					//{					
					if (GetUserType() == (long)enUserType.Principal || GetUserType() == (long)enUserType.HOD)
					{
						studentApplied = _MSUoW.UniversityRegistrationDetail.FindBy(element => element.CreatorUserId == nursingDto.CreatorUserId && element.IsActive).OrderByDescending(d => d.CreationTime).FirstOrDefault();
					}
					else if (GetUserType() == (long)enUserType.Student)
					{
						studentApplied = _MSUoW.UniversityRegistrationDetail.FindBy(element => element.CreatorUserId == GetCurrentUserId() && element.IsActive).OrderByDescending(d => d.CreationTime).FirstOrDefault();
					}
					if (!(studentApplied != null && studentApplied.State.Trim().ToLower() == _iconfiguration.GetSection("RegistrationNumber:RegistrationNumberApprovedStatus").Value.Trim().ToLower()))
						{
							srvcMaster = _MSUoW.ServiceMasterRepository.GetServiceMasterById(Convert.ToInt16((int)(enServiceType.RegistrationService)));
							srvcMaster.ServicePricingMasters.Where(p => p.IsConvenienceFee == true).ToList().ForEach(s =>
							{
								ServicePaymentDto servicePmtDto = new ServicePaymentDto();
								servicePmtDto.FeeDescription = s.FeeDescription;
								servicePmtDto.FeeAmount = s.FeeAmount;
								paymentPricingInfoDto.ServiceCharge += Convert.ToDecimal(s.FeeAmount);
								paymentPricingInfoDto.ConveniencePayments.Add(servicePmtDto);
							});
						}
					//}
					#endregion

					if (serviceMaster.IsPayOnlyServiceCharges)
					{
						paymentPricingInfoDto.IsPayAmount = true;
						paymentPricingInfoDto.TotalAmountToBePaid = paymentPricingInfoDto.ServiceCharge;
					}
					else
					{
						paymentPricingInfoDto.IsPayAmount = false;
						paymentPricingInfoDto.TotalAmountToBePaid = paymentPricingInfoDto.TotalFee + paymentPricingInfoDto.ServiceCharge;
					}

					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Examination Fee", FeeAmount = paymentPricingInfoDto.ExaminationFees });
					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Centre Charges", FeeAmount = paymentPricingInfoDto.CentreCharges });
					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Enrollment Fee", FeeAmount = paymentPricingInfoDto.EnrollmentFees });
					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Fees For Marks", FeeAmount = paymentPricingInfoDto.FeesForMarks });
					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Fees For Supervision", FeeAmount = paymentPricingInfoDto.FeesForSupervision });
					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Fees For Re-registration", FeeAmount = paymentPricingInfoDto.ExamRegistrationFee });
					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Provisional Certificate Fee", FeeAmount = paymentPricingInfoDto.ProvisionalCertificateFee });
					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Additional Centre Charges", FeeAmount = paymentPricingInfoDto.AdditionalCentreCharges });
					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Original Certificate Fee", FeeAmount = paymentPricingInfoDto.OriginalCertificateFee });
					paymentPricingInfoDto.ServicePayments.Add(new ServicePaymentDto { FeeDescription = "Late Fee", FeeAmount = paymentPricingInfoDto.LateFee });



					#endregion
					break;

				default:
					return new Tuple<PaymentPricingInfoDto, string>(null, "Not yet implemented for the service");

			}

			return new Tuple<PaymentPricingInfoDto, string>(paymentPricingInfoDto, "Successfully Fetched Pricing Info.");

		}

		public TransactionLog PrepareDataForPayment(TransactionLog transactionLog)
		{
			Tuple<PaymentPricingInfoDto, string> ret = GetPricingInfoByService(transactionLog.RequestId, transactionLog.ProductInfo);

			PaymentPricingInfoDto paymentpricing = ret.Item1;
			transactionLog.Amount = paymentpricing.TotalAmountToBePaid;
			transactionLog.ConsumerName = paymentpricing.StudentName;
			transactionLog.EmailId = paymentpricing.EmailAddress;
			transactionLog.ContactNo = paymentpricing.PhoneNumber;
			transactionLog.DeviceType = GetDeviceType();

			if (transactionLog.ProductInfo.Equals(((int)enServiceType.BCABBAExamApplicationForm).ToString(), StringComparison.InvariantCultureIgnoreCase))
			{
				//Saving the values for the principal dashboard, only required in Examination FormFillup service
				Guid gid = new Guid(transactionLog.RequestId);
				ExaminationFormFillUpDetails application = _MSUoW.ExaminationFormFillUpDetails.FindBy(a => a.IsActive == true && a.Id == gid).FirstOrDefault();
				application.InternetHandlingCharges = paymentpricing.ServiceCharge;
				application.FormFillUpMoneyToBePaid = paymentpricing.TotalFee;
				application.LateFeeAmount = paymentpricing.LateFee;
				_MSUoW.ExaminationFormFillUpDetails.Update(application);
				_MSUoW.Commit();
			}

			if (transactionLog.ProductInfo.Equals(((int)enServiceType.RegistrationService).ToString(), StringComparison.InvariantCultureIgnoreCase))
			{
				//No implementation
			}

			if (transactionLog.ProductInfo.Equals(((int)enServiceType.NursingApplicationForm).ToString(), StringComparison.InvariantCultureIgnoreCase))
			{
				//Saving the values for the principal dashboard, only required in Examination FormFillup service
				Guid gid = new Guid(transactionLog.RequestId);
				NursingFormfillupDetails application = _MSUoW.NursingFormfillupDetails.FindBy(a => a.IsActive == true && a.Id == gid).FirstOrDefault();
				application.InternetHandlingCharges = paymentpricing.ServiceCharge;
				application.FormFillUpMoneyToBePaid = paymentpricing.TotalFee;
				application.LateFeeAmount = paymentpricing.LateFee;
				application.CenterCharges = paymentpricing.CentreCharges;
				_MSUoW.NursingFormfillupDetails.Update(application);
				_MSUoW.Commit();
			}
			return transactionLog;
		}

		public bool CheckDuplicateTxnId(string txnId, enTransaction statusType)
		{
			string status = statusType.ToString();
			int tl_count = _MSUoW.TransactionLog.FindBy(a => a.UniqueRefNo == txnId && a.Status == status && a.IsActive == true).Count();
			return tl_count > 0 ? true : false;

		}

		/// <summary>
		/// Author: Sitikanta Pradhan
		/// Date: 30-Dec-2019
		/// Download Payment Receipt By Id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>

		public PDFDownloadFile DownloadPaymentReceiptById(string id)
        {
            Guid gid = new Guid(id);
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.PaymentReceipt && s.IsActive == true).FirstOrDefault();
            if (templateData != null)
            {
                string description = templateData.Body;
                TransactionLog transactionLog = _MSUoW.TransactionLog.FindBy(a => a.Id == gid && a.IsActive == true && a.TenantId == 3).FirstOrDefault();
                if (transactionLog != null)
                {
                    ServiceMaster serviceMaster = _MSUoW.ServiceMaster.GetById(Convert.ToInt64(transactionLog.ProductInfo));
                    transactionLog.ProductInfo = serviceMaster.ServiceName;

                    string applicationURL = _iconfiguration.GetSection("ApplicationURL").Value;
                    string collegeLogo = _iconfiguration.GetSection("UULogo").Value;

                    description = description.Replace("--collegeLogo--", applicationURL + collegeLogo);
                    if (transactionLog.Status.Trim().ToLower() == _MSCommon.GetEnumDescription(enTransaction.Successful).Trim().ToLower())
                    {
                        description = description.Replace("--TranStatusLogo--", applicationURL + _iconfiguration.GetSection("Payment:PaymentChecked").Value);
                        description = description.Replace("--color--", "#3c763d");
                        description = description.Replace("--bgColor--", "#dff0d8");
                        description = description.Replace("--bColor--", "#d6e9c6");
                        description = description.Replace("--status--", "Success!");
                        description = description.Replace("--message--", "Thank you for your payment!");
                    }
                    if (transactionLog.Status.Trim().ToLower() == _MSCommon.GetEnumDescription(enTransaction.Failed).Trim().ToLower())
                    {
                        description = description.Replace("--TranStatusLogo--", applicationURL + _iconfiguration.GetSection("Payment:PaymentCancelImg").Value);
                        description = description.Replace("--color--", "#a94442");
                        description = description.Replace("--bgColor--", "#f2dede");
                        description = description.Replace("--bColor--", "#f2dede");
                        description = description.Replace("--status--", "Failed!");
                        description = description.Replace("--message--", "Payment Unsuccessful.");
                    }
                    if (transactionLog.Status.Trim().ToLower() == _MSCommon.GetEnumDescription(enTransaction.Cancelled).Trim().ToLower())
                    {
                        description = description.Replace("--TranStatusLogo--", applicationURL + _iconfiguration.GetSection("Payment:PaymentCancelImg").Value);
                        description = description.Replace("--color--", "#a94442");
                        description = description.Replace("--bgColor--", "#f2dede");
                        description = description.Replace("--bColor--", "#f2dede");
                        description = description.Replace("--status--", "Cancelled!");
                        description = description.Replace("--message--", "Payment has been cancelled by the user.");
                    }
                    description = description.Replace("--productInfo--", transactionLog.ProductInfo);
                    description = description.Replace("--consumerName--", transactionLog.ConsumerName);
                    description = description.Replace("--emailId--", transactionLog.EmailId);
                    description = description.Replace("--contactNo--", transactionLog.ContactNo);
                    description = description.Replace("--amount--", transactionLog.Amount.ToString());
                    description = description.Replace("--recieptNo--", transactionLog.RecieptNo);

                    DateTime utcdate = DateTime.ParseExact(transactionLog.CreationTime.ToString("MM-dd-yyyy HH:mm:ss"), "MM-dd-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
					var istdate = TimeZoneInfo.ConvertTimeFromUtc(utcdate, TZConvert.GetTimeZoneInfo(_iconfiguration.GetSection("TimeZone").Value.ToString()));
					description = description.Replace("--creationTime--", istdate.ToString("dd-MM-yyyy hh:mm tt"));
                    description = description.Replace("--uniqueRefNo--", transactionLog.UniqueRefNo);
                    if(transactionLog.BankRefNo != null)
                    {
                        description = description.Replace("--bankRefNo--", transactionLog.BankRefNo);
                    }
                    else
                    {
                        description = description.Replace("--bankRefNo--", "-NA-");
                    }

                    flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);
                    flattenedPDFDownloadDetails.FileType = "application/octet-stream";
                    flattenedPDFDownloadDetails.FileName = "PaymentReceipt.pdf";

                    return flattenedPDFDownloadDetails;
                }
                else throw new MicroServiceException() { ErrorCode = "OUU011" };
            }
            else throw new MicroServiceException() { ErrorCode = "OUU009" };
        }


		//private PaymentPricingInfoDto GetUGEntranceExaminationService(string id, string acknowledgementNo, ServiceMaster serviceMaster)
		//{
		//	PaymentPricingInfoDto pricingInfo = new PaymentPricingInfoDto();
		//	if (!string.IsNullOrEmpty(id))
		//	{
		//		Guid guid_instance_id = new Guid(id);

		//		EntranceExaminationDto entranceExaminationDto = _EntranceExaminationService.GetEntranceExaminationDetail(guid_instance_id, acknowledgementNo);
		//		if (entranceExaminationDto != null)
		//		{
		//			pricingInfo.EmailAddress = entranceExaminationDto.EmailId;
		//			pricingInfo.ApplicantName = entranceExaminationDto.ApplicantName;
		//			pricingInfo.PhoneNumber = entranceExaminationDto.MobileNumber;
		//		}
		//		if (serviceMaster != null)
		//		{
		//			pricingInfo.ServiceType = serviceMaster.ServiceName;
		//			pricingInfo.ConvenienceAmount = serviceMaster.ServiceConveniencePricingMaster != null ? Convert.ToDecimal(serviceMaster.ServiceConveniencePricingMaster.ConvenienceFee) : 0;
		//			pricingInfo.TotalFee = 0;
		//			serviceMaster.ServicePricingMasters.ToList().ForEach(s =>
		//			{
		//				ServicePaymentDto servicePmtDto = new ServicePaymentDto();
		//				servicePmtDto.FeeDescription = s.FeeDescription;
		//				servicePmtDto.FeeAmount = s.FeeAmount;
		//				pricingInfo.TotalFee += Convert.ToDecimal(s.FeeAmount);
		//				pricingInfo.ServicePayments.Add(servicePmtDto);
		//			});
		//			pricingInfo.TotalAmountToBePaid = pricingInfo.TotalFee + pricingInfo.ConvenienceAmount;
		//		}
		//	}
		//	return pricingInfo;
		//}
	}
}
