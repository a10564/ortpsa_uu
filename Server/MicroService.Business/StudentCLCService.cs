﻿using AutoMapper;
using Microservice.Common;
using Microservice.Common.Abstract;
using MicroService.Business.Abstract;
using MicroService.Core;
using MicroService.Core.WorkflowCommon;
using MicroService.Core.WorkflowTransactionHistrory;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.CLC;
using MicroService.Domain.Models.Dto.MigrationService;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Exception;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;


namespace MicroService.Business
{
    public class StudentCLCService : BaseService, IStudentCLCService
    {
        private IMicroServiceUOW _MSUoW;
        private readonly IHostingEnvironment _environment;
        IConfiguration _IConfiguration;
        //private IWFTransactionHistroryAppService _WFTransactionHistroryAppService;
        //private MetadataAssemblyInfo _workflowHelper;
       // private WorkflowDbContext _workflowDbContext;
        private IMicroserviceCommon _MSCommon;
      //  private readonly string WorkflowName = "";

        public StudentCLCService(IMicroServiceUOW MSUoW, 
            IRedisDataHandlerService redisData, 
            IHostingEnvironment environment, 
            IConfiguration IConfiguration, 
            //IWFTransactionHistroryAppService WFTransactionHistroryAppService, 
            //MetadataAssemblyInfo workflowHelper, 
            //WorkflowDbContext workflowDbContext, 
            IMicroserviceCommon MSCommon
            //, 
            //string workflowName
            ) : base(redisData)
        {
            _MSUoW = MSUoW;
            _environment = environment;
            _IConfiguration = IConfiguration;
            //_WFTransactionHistroryAppService = WFTransactionHistroryAppService;
            //_workflowHelper = workflowHelper;
            //_workflowDbContext = workflowDbContext;
            _MSCommon = MSCommon;
            //WorkflowName = workflowName;
        }

        #region POST [CLC Applied]
        /// <summary>
        /// Author: Suchi
        /// Date: 23-02-2020
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public ApplyCLCDto StudentCLCApplied(ApplyCLCDto input)
        {
            ApplyCLC studentApplied = _MSUoW.ApplyCLC.FindBy(element => element.CreatorUserId == GetCurrentUserId() && element.IsActive).OrderByDescending(d => d.LastModificationTime).FirstOrDefault();

            if (studentApplied != null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU123" };
            }
            long serviceId = (long)enServiceType.CollegeLeavingCertificate;
            Users user = _MSUoW.Users.FindBy(u => u.Id == GetCurrentUserId() && u.IsActive).FirstOrDefault();
            if (user.UserType == (long)enRoles.Student)
            {
                if (input != null)
                {
                    if (ValidateInput(input))
                    {
                        ApplyCLC clcData = Mapper.Map<ApplyCLCDto, ApplyCLC>(input);
                        var newGuid = Guid.NewGuid();
                        string guidForAck = newGuid.ToString();
                        string guidForClcNumber = newGuid.ToString();
                        clcData.Id = newGuid;
                        Documents tempContent = new Documents();
                        List<Documents> tempDoc = new List<Documents>();
                        tempDoc = clcData.ClcDocument.ToList();
                        clcData.ClcDocument = new List<Documents>();
                        tempDoc.ForEach(data =>
                        {
                            // Writing photo on server
                            tempContent = data;
                            if (ImageValidation(tempContent))
                            {
                                tempContent.Id = Guid.NewGuid();
                                tempContent.SourceType = data.SourceType;
                                tempContent.DocumentType = data.DocumentType;
                                clcData.ClcDocument.Add(ImageWrite(tempContent, newGuid));
                            }
                        });
                        clcData.AcknowledgementNo = _MSCommon.GenerateAcknowledgementNo(_MSCommon.GetEnumDescription(enAcknowledgementFormat.ACK), serviceId, guidForAck);
                        _MSUoW.ApplyCLC.Add(clcData);

                        //student Address section
                        List<ServiceStudentAddress> examStudentAddressList = new List<ServiceStudentAddress>();
                        input.clcFormFillUpStudentAddresses.ForEach(address =>
                        {
                            ServiceStudentAddress examStudentAddress = AutoMapper.Mapper.Map<ServiceStudentAddressDto, ServiceStudentAddress>(address);
                            examStudentAddress.Id = Guid.NewGuid();
                            examStudentAddress.ServiceType = serviceId;
                            if (input.IsSameAsPresent)
                            {
                                examStudentAddress.AddressType = address.AddressType;
                            }
                            examStudentAddressList.Add(examStudentAddress);
                        });
                        clcData.clcFormFillUpStudentAddresses = examStudentAddressList;
                        // end of student Address Section

                        // Service applied
                        UserServiceApplied usrSrvcApplied = new UserServiceApplied()
                        {
                            IsAvailableToDL = false,
                            //LastStatus = clcData.State,
                            LastStatus = "Payment Pending", // remove after workflow implemented
                            ServiceId = (long)enServiceType.CollegeLeavingCertificate,
                            UserId = GetCurrentUserId(),
                            InstanceId = newGuid,
                            IsActionRequired = true
                        };
                        _MSUoW.UserServiceApplied.Add(usrSrvcApplied);
                        _MSUoW.Commit();
                        
                        #region MAIL SEND TO STUDENT

                        string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == serviceId && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                        SendAcknowledgementEmail(user.EmailAddress, clcData.StudentName, serviceName, clcData.AcknowledgementNo);

                        #endregion

                        #region SMS SEND TO STUDENT

                        EmailTemplate acknowledgementSMS = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ApplicationAcknowledgementSMS && s.IsActive == true).FirstOrDefault();
                        if (acknowledgementSMS != null)
                        {
                            string contentId = string.Empty;
                            contentId = acknowledgementSMS.SMSContentId;
                            string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                            OTPMessage otpObj = new OTPMessage();
                            StringBuilder body = new StringBuilder(acknowledgementSMS.Body);
                            body.Replace("--studentName--", clcData.StudentName);
                            body.Replace("--serviceName--", serviceName);
                            body.Replace("--acknowledgementNo--", clcData.AcknowledgementNo);
                            otpObj.Message = body.ToString();
                            otpObj.MobileNumber = user.PhoneNumber;
                            _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                        }

                        #endregion

                        input = Mapper.Map<ApplyCLC, ApplyCLCDto>(clcData);
                        return input;
                    }
                }
                else throw new MicroServiceException() { ErrorCode = "OUU013" };
            }
            else throw new MicroServiceException() { ErrorCode = "OUU052" };
            return input;
        }

        #region INPUT VALIDATION
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 07-Nov-2019
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private bool ValidateInput(ApplyCLCDto input)
        {
            DateTime minDate = new DateTime(1950, 01, 01);
            int minYear = minDate.Year;

            DateTime maxDate = DateTime.UtcNow;
            int maxYear = maxDate.Year;

            DateTime maxDOBDate = DateTime.UtcNow.AddYears(-15); // 15 year minus on current date for student date of birth 
            string regularExpressionForName = @"^(?!\.)(?!.*\.$)(?!.*?\.\.)[a-zA-Z0-9 .]+$";//This regex is used to allow only alphabets, dot and spaces in string
            string registrationExpression = @"^[0-9]{16}$";
            if (input.StudentName == null || input.StudentName.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU037" };
            }
            else if (!Regex.IsMatch(input.StudentName.Trim(), regularExpressionForName))
            {
                throw new MicroServiceException() { ErrorCode = "OUU038" };
            }
            else if (input.RegistrationNumber == null || input.RegistrationNumber.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU064" };
            }
            else if (!Regex.IsMatch(input.RegistrationNumber, registrationExpression))
            {
                throw new MicroServiceException() { ErrorCode = "OUU091" };
            }
            else if (input.DateOfBirth == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU041" };
            }
            else if (input.DateOfBirth != null)
            {
                if (minDate >= input.DateOfBirth && maxDOBDate <= input.DateOfBirth)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU069" };
                }
            }
            else if (input.CollegeCode == null || input.CollegeCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU042" };
            }
            else if (input.CourseTypeCode == null || input.CourseTypeCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU043" };
            }
            else if (input.StreamCode == null || input.StreamCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU044" };
            }
            else if (input.SubjectCode == null || input.SubjectCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU045" };
            }
            else if (input.YearOfAdmission == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU046" };
            }
            else if (input.YearOfAdmission != 0)
            {
                if (minYear >= input.YearOfAdmission && maxYear <= input.YearOfAdmission)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU070" };
                }
            }
            else if (input.CourseTypeCode == _MSCommon.GetEnumDescription(enCourseType.Pg))
            {// PG course type
                if (input.DepartmentCode == null || input.DepartmentCode == String.Empty)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU056" };
                }
                else if (input.CourseCode == null || input.CourseCode == String.Empty)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU059" };
                }
            }
            else if (input.CourseTypeCode == _MSCommon.GetEnumDescription(enCourseType.Ug))
            {// For UG course type, remove department code and course code
                input.DepartmentCode = input.CourseCode = null;
            }
            else if (input.Result == null || input.Result.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU132" };
            }
            else if (input.ReasonForTransfer == null || input.ReasonForTransfer.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU133" };
            }
            else if (input.StudentType == null || input.StudentType.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU134" };
            }
            else if (input.ClcApplicationType == null || input.ClcApplicationType.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU135" };
            }
            else if (input.ReasonForApply == null || input.ReasonForApply.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU128" };
            }
            else if (input.YearOfPassing == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU136" };
            }

            input.CourseTypeCode = input.CourseTypeCode.Trim();
            input.RegistrationNumber = input.RegistrationNumber.Trim();
            input.RollNumber = input.RollNumber.Trim();
            input.StudentName = input.StudentName.Trim();
            input.CollegeCode = input.CollegeCode.Trim();

            input.StreamCode = input.StreamCode.Trim();
            input.SubjectCode = input.SubjectCode.Trim();

            input.Result = input.Result.Trim();
            input.ReasonForTransfer = input.ReasonForTransfer.Trim();
            input.StudentType = input.StudentType.Trim();
            input.ClcApplicationType = input.ClcApplicationType.Trim();

            return true;
        }
        #endregion

        #region IMAGE VALIDATION
        /// <summary>
        /// Author: Sumbul Samreen
        /// Date: 04-02-2020
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private bool ImageValidation(Documents input)
        {
            if (input.FileExtension == "" || input.FileExtension == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU047" };
            }
            if (input.FileExtension != "" || input.FileExtension != null)
            {
                var regEx = new Regex(@"(jpg|jpeg)$");
                if (!regEx.IsMatch(input.FileExtension.ToLower()))
                {
                    throw new MicroServiceException() { ErrorCode = "OUU048" };
                }
            }
            if (input.FileContent.Length >= Convert.ToInt32(_IConfiguration.GetSection("CollegeLeavingCertificate:UploadImageSize").Value))
            {
                throw new MicroServiceException() { ErrorCode = "OUU049" };
            }
            return true;
        }
        #endregion

        #region PRIVATE COMMON DOCUMENT WRITE METHOD
        private Documents ImageWrite(Documents file, Guid id)
        {
            string[] name = id.ToString().Split("-");
            string finalName = name[1] + name[2] + name[4] + name[0] + name[3];
            //List <Documents> documentList = new List<Documents>();
            //input.RegistrationDocument.ForEach((file) =>
            //{
            string path = string.Empty;
            file.ParentSourceId = id;
            if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.win.ToString())
            {
                path = _environment.ContentRootPath;
            }
            else if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.linux.ToString())
            {
                path = _IConfiguration.GetSection("LinuxDocumentPath").Value;
            }
            string fileName = finalName + "." + file.FileExtension;
            string DirectoryPath = _IConfiguration.GetSection("CollegeLeavingCertificate:CollegeLeavingCertificatePath").Value; // "\\Documents\\RegistrationCertificates\\"
            var Path = path + DirectoryPath + fileName;

            if (!Directory.Exists(path + DirectoryPath))
            {
                Directory.CreateDirectory(path + DirectoryPath);
            }

            if (File.Exists(Path))
            {
                File.Delete(Path);
            }
            byte[] bytes = (byte[])file.FileContent;
            File.WriteAllBytes(Path, bytes);
            file.FilePath = DirectoryPath + fileName;
            //file.FilePath = file.FilePath.Replace('\\', '/');
            //documentList.Add(file);
            //});

            return file;
        }
        #endregion

        #region SEND ACKNOWLEDGEMENT EMAIL TO STUDENT
        /// Email send method
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 08-NOV-2019
        /// </summary>
        private bool SendAcknowledgementEmail(string studentEmailAddress, string studentName, string serviceName, string acknowledgementNo)
        {
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ApplicationAcknowledgementEmail && s.IsActive == true).FirstOrDefault();
            if (templateData != null)
            {
                string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                StringBuilder body = new StringBuilder(templateData.Body);
                body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                body.Replace("--studentName--", studentName);
                body.Replace("--serviceName--", serviceName);
                body.Replace("--acknowledgementNo--", acknowledgementNo);
                _MSCommon.SendMails(studentEmailAddress, templateData.Subject, body.ToString());
            }
            return true;
        }
        #endregion

        #endregion

        #region UPDATE
        /// <summary>
        /// Author: Suchismita Senapati
        /// Date: 05-02-2020
        /// Description : This method is used to get the student Details by id 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public ApplyCLCDto Update(Guid id, ApplyCLCDto input)
        {
            long currentUserType = GetUserType();
            if (id != default(Guid) && input != null)
            {
                ApplyCLC studentData = _MSUoW.ApplyCLC.FindBy(student => student.Id == id && student.IsActive == true).FirstOrDefault();
                if (studentData != null)
                {
                    if (ValidateInput(input))
                    {
                        studentData.RegistrationNumber = input.RegistrationNumber;
                        studentData.RollNumber = input.RollNumber;
                        studentData.StudentName = input.StudentName.Trim();
                        studentData.FatherName = input.FatherName.Trim();
                        studentData.MotherName = input.MotherName.Trim();
                        studentData.DateOfBirth = input.DateOfBirth;
                        studentData.StreamCode = input.StreamCode;
                        studentData.CourseCode = input.CourseCode;
                        studentData.SubjectCode = input.SubjectCode;
                        studentData.DepartmentCode = input.DepartmentCode;
                        studentData.YearOfAdmission = input.YearOfAdmission;
                        studentData.YearOfPassing = input.YearOfPassing;
                        studentData.Result = input.Result;
                        studentData.ReasonForTransfer = input.ReasonForTransfer;
                        studentData.StudentType = input.StudentType;

                        _MSUoW.ApplyCLC.Update(studentData);

                        List<ServiceStudentAddress> listServiceStudentAddress = _MSUoW.ServiceStudentAddress.FindBy(sa => sa.InstanceId == id && sa.TenantId == GetCurrentTenantId()).ToList();
                        if (listServiceStudentAddress.Count > 0)
                        {
                            listServiceStudentAddress[0].Address = input.clcFormFillUpStudentAddresses[0].Address;
                            listServiceStudentAddress[0].Country = input.clcFormFillUpStudentAddresses[0].Country;
                            listServiceStudentAddress[0].State = input.clcFormFillUpStudentAddresses[0].State;
                            listServiceStudentAddress[0].City = input.clcFormFillUpStudentAddresses[0].City;
                            listServiceStudentAddress[0].Zip = input.clcFormFillUpStudentAddresses[0].Zip;

                            _MSUoW.ServiceStudentAddress.Update(listServiceStudentAddress[0]);
                        }
                        _MSUoW.Commit();
                        input = Mapper.Map<ApplyCLC, ApplyCLCDto>(studentData);
                        return input;
                    }
                }
            }
            else throw new MicroServiceException() { ErrorCode = "OUU015" };

            return input;
        }
        #endregion

        #region Get CLC Data By Given Id
        /// <summary>
        ///Author   :   Sitikanta Pradhan
        ///Date     :   26-Feb-2019
        ///This method is used to get the CLC details by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ApplyCLCDto GetClcDetailById(Guid id)
        {
            ApplyCLCDto clcData = new ApplyCLCDto();

            if (id != Guid.Empty)
            {
                //get all the CLC data from repository
                ApplyCLC formRecord = _MSUoW.CLCRepository.GetAllClcData().Where(x => x.Id == id && x.IsActive).FirstOrDefault();
                if (formRecord != null && formRecord.ClcDocument != null && formRecord.ClcDocument.Count > 0)
                {
                    formRecord.ClcDocument[0].FilePath = string.Concat(_IConfiguration.GetSection("ApplicationURL").Value, formRecord.ClcDocument[0].FilePath);
                }
                if (formRecord != null)
                {
                    clcData = AutoMapper.Mapper.Map<ApplyCLC, ApplyCLCDto>(formRecord);
                }
            }
            return clcData;
        }
        #endregion

        #region ODATA METHOD
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 27-Feb-2019 
        /// Method is used for Odata Controller which will give clc record as per usertype 
        /// </summary>
        /// <returns></returns>
        public IQueryable<CLCView> GetAllCLCViewData()
        {
            List<string> roleNames = GetRoleListOfCurrentUser();
            long currentUserType = GetUserType();
            string CollegeCode = GetCollegeCode();
            string DepartmentCode = GetDepartmentCode();
            var inboxDataAsPerRole = _MSUoW.CLCView.GetAll();

            switch (currentUserType)
            {
                case (long)enUserType.Principal:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower() && data.IsActive);
                    break;
                case (long)enUserType.HOD:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower() && data.DepartmentCode.Trim().ToLower() == DepartmentCode.Trim().ToLower() && data.IsActive);
                    break;
                case (long)enUserType.Student:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CreatorUserId == GetCurrentUserId() && data.IsActive);
                    break;
            }

            return inboxDataAsPerRole;
        }
        #endregion
    }
}
