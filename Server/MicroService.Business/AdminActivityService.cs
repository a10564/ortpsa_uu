﻿using AutoMapper.Configuration;
using Microservice.Common;
using Microservice.Common.Abstract;
using MicroService.Business.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.AdminActivity;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Entity;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Exception;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.Business
{
    public class AdminActivityService : BaseService, IAdminActivityService
    {
        private IMicroServiceUOW _MSUoW;
        private IMicroserviceCommon _MSCommon;
        private ICommonAppService _common;
        public AdminActivityService(IMicroServiceUOW MSUoW, IRedisDataHandlerService redisData, IMicroserviceCommon MSCommon,
        ICommonAppService common) : base(redisData)
        {
            _MSUoW = MSUoW;
            _MSCommon = MSCommon;
            _common = common;
        }

        #region GET EXAM DATES
        /// <summary>
        /// Author  :   Sumbul Samreen
        /// Date    :   12-02-2020
        /// Description :   This method is used to get exam slots and dates using the filteroptions from examinationdate table
        /// </summary>
        /// <param name="filterOptions"></param>
        /// <returns></returns>
        public List<ExamFormFillupDatesDto> GetExamSlotsAndDates(ExamDateFilterPropertiesDto filterOptions)
        {
            var years = filterOptions.startAndEndYearFromSession.Split("-");
            YearFromSession year = new YearFromSession();
            year.StartYear = int.Parse(years[0]);
            year.EndYear = int.Parse(years[1]);
            List<ExamFormFillupDatesDto> slotAndDatelist = new List<ExamFormFillupDatesDto>();
            if (filterOptions != null)
            {
                Guid examMasterId;
                examMasterId = (filterOptions.CourseTypeCode == _MSCommon.GetEnumDescription(enCourseType.Pg)) ?
                               _MSUoW.ExaminationMaster.GetAll().Where(m => m.IsActive && m.CourseType == filterOptions.CourseTypeCode && m.CourseCode == filterOptions.CourseCode
                                && m.DepartmentCode == filterOptions.DepartmentCode && m.StreamCode == filterOptions.StreamCode && m.SubjectCode == filterOptions.SubjectCode &&
                                m.SemesterCode == filterOptions.SemesterCode).Select(s => s.Id).FirstOrDefault()
                                : _MSUoW.ExaminationMaster.GetAll().Where(m => m.IsActive && m.CourseType == filterOptions.CourseTypeCode
                                  && m.StreamCode == filterOptions.StreamCode && m.SubjectCode == filterOptions.SubjectCode &&
                                  m.SemesterCode == filterOptions.SemesterCode).Select(s => s.Id).FirstOrDefault();
                if (examMasterId == Guid.Empty)
                {
                    ExaminationMaster masterDataObj = new ExaminationMaster();
                    Guid newGuid = Guid.NewGuid();
                    masterDataObj.Id = newGuid;
                    examMasterId = masterDataObj.Id;
                    masterDataObj.CourseType = filterOptions.CourseTypeCode;
                    masterDataObj.StreamCode = filterOptions.StreamCode;
                    masterDataObj.SubjectCode = filterOptions.SubjectCode;
                    masterDataObj.CourseCode = filterOptions.CourseCode;
                    masterDataObj.DepartmentCode = filterOptions.DepartmentCode;
                    masterDataObj.SemesterCode = filterOptions.SemesterCode;
                    _MSUoW.ExaminationMaster.Add(masterDataObj);
                    
                }

                if (year != null)
                {
                    Guid examSessionId = _MSUoW.ExaminationSession.FindBy(e => e.IsActive && e.ExaminationMasterId == examMasterId && e.StartYear == year.StartYear && e.EndYear == year.EndYear).Select(s => s.Id).FirstOrDefault();
                    if (examSessionId == Guid.Empty)
                    {
                        ExaminationSession sessionDataObj = new ExaminationSession();
                        Guid newGuid = Guid.NewGuid();
                        sessionDataObj.Id = newGuid;
                        sessionDataObj.ExaminationMasterId = examMasterId;
                        sessionDataObj.StartYear = year.StartYear;
                        sessionDataObj.EndYear = year.EndYear;
                        sessionDataObj.ExamYear = 0;
                        _MSUoW.ExaminationSession.Add(sessionDataObj);
                    }
                    slotAndDatelist = _MSUoW.ExaminationFormFillUpDates.FindBy(d => d.IsActive && d.ExaminationMasterId == examMasterId && d.ExaminationSessionId == examSessionId)
                                        .OrderBy(o => o.SlotOrder)
                                        .Select(s => new ExamFormFillupDatesDto
                                        {
                                            SlotOrder = s.SlotOrder,
                                            StartDate = s.StartDate,
                                            EndDate = s.EndDate
                                        }).ToList();
                }
                
                else
                {
                    throw new MicroServiceException() { ErrorCode = "OUU120" };
                }
            }
            
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU110" };
            }
            _MSUoW.Commit();
            return slotAndDatelist;
        }
        #endregion

        #region UPDATE EXAM DATES 
        /// <summary>
        /// Author  :   Sumbul Samreen
        /// Date    :   12-02-2020
        /// Description :  This method is used to upadate exam slots and dates to examinationdate table
        /// </summary>
        /// <param name="filterOptions"></param>
        /// <returns></returns>
        public List<ExamFormFillupDatesDto> UpdateExamSlotsAndDates(PropertiesForDateScheduleDto input)
        {
             var years = input.FilterProperties.startAndEndYearFromSession.Split("-");
            YearFromSession year = new YearFromSession();
            year.StartYear = int.Parse(years[0]);
            year.EndYear = int.Parse(years[1]);
            DateTime dt = Convert.ToDateTime(input.Dates[0].StartDate);
            int ExamYear = dt.Year;
            Users user = _MSUoW.Users.FindBy(u => u.Id == GetCurrentUserId() && u.IsActive).FirstOrDefault();
            List<ExamFormFillupDatesDto> slotandDates = new List<ExamFormFillupDatesDto>();
            if (input != null)
            {
                ExamDateFilterPropertiesDto filterOptions = input.FilterProperties;
                List<ExamFormFillupDatesDto> slotAndDatelist = input.Dates;

                if (slotAndDatelist != null)
                {
                    Guid examMasterId;
                    examMasterId = (filterOptions.CourseTypeCode == _MSCommon.GetEnumDescription(enCourseType.Pg)) ?
                                   _MSUoW.ExaminationMaster.GetAll().Where(m => m.IsActive && m.CourseType == filterOptions.CourseTypeCode && m.CourseCode == filterOptions.CourseCode
                                    && m.DepartmentCode == filterOptions.DepartmentCode && m.StreamCode == filterOptions.StreamCode && m.SubjectCode == filterOptions.SubjectCode &&
                                    m.SemesterCode == filterOptions.SemesterCode).Select(s => s.Id).FirstOrDefault()
                                    : _MSUoW.ExaminationMaster.GetAll().Where(m => m.IsActive && m.CourseType == filterOptions.CourseTypeCode
                                      && m.StreamCode == filterOptions.StreamCode && m.SubjectCode == filterOptions.SubjectCode &&
                                      m.SemesterCode == filterOptions.SemesterCode).Select(s => s.Id).FirstOrDefault();
                    if (examMasterId != Guid.Empty)
                    {
                        Guid examSessionId = _MSUoW.ExaminationSession.FindBy(e => e.IsActive && e.ExaminationMasterId == examMasterId && e.StartYear == year.StartYear && e.EndYear == year.EndYear).Select(s => s.Id).FirstOrDefault();
                        if (examSessionId != Guid.Empty)
                        {
                            List<ExaminationFormFillUpDates> slotAndDatesFromTable = _MSUoW.ExaminationFormFillUpDates.FindBy(f => f.IsActive && f.ExaminationSessionId == examSessionId).OrderBy(o => o.SlotOrder).ToList();

                            List<int> slotNos = slotAndDatesFromTable.Select(s => s.SlotOrder).ToList();

                            //TimeSpan startTime = new TimeSpan(05, 30, 01);
                            //TimeSpan endTime = new TimeSpan(18, 29, 59);

                            slotAndDatesFromTable.ForEach((slot) =>
                            {
                                slotAndDatelist.ForEach((datesChanged) =>
                                {
                                    if (slot.SlotOrder == datesChanged.SlotOrder)
                                    {
                                        slot.StartDate = datesChanged.StartDate; //.Add(startTime);
                                        slot.EndDate = datesChanged.EndDate; //.Add(endTime);
                                    }
                                });
                            });
                            _MSUoW.ExaminationFormFillUpDates.UpdateRange(slotAndDatesFromTable);
                            slotandDates = AutoMapper.Mapper.Map<List<ExaminationFormFillUpDates>, List<ExamFormFillupDatesDto>>(slotAndDatesFromTable);
                        }
                        else
                        {
                            ExaminationSession sessionDataObj = new ExaminationSession();
                            Guid newGuid = Guid.NewGuid();
                            sessionDataObj.Id = newGuid;
                            sessionDataObj.ExaminationMasterId = examMasterId;
                            sessionDataObj.StartYear = year.StartYear;
                            sessionDataObj.EndYear = year.EndYear;
                            sessionDataObj.ExamYear = ExamYear;
                            _MSUoW.ExaminationSession.Add(sessionDataObj);
                           
                        }
                    }
                    else
                    {
                        ExaminationMaster masterDataObj = new ExaminationMaster();
                        Guid newGuid = Guid.NewGuid();
                        masterDataObj.Id = newGuid;
                        masterDataObj.CourseType = filterOptions.CourseTypeCode;
                        masterDataObj.StreamCode = filterOptions.StreamCode;
                        masterDataObj.SubjectCode = filterOptions.SubjectCode;
                        masterDataObj.CourseCode = filterOptions.CourseCode;
                        masterDataObj.DepartmentCode = filterOptions.DepartmentCode;
                        masterDataObj.SemesterCode = filterOptions.SemesterCode;
                        _MSUoW.ExaminationMaster.Add(masterDataObj);
                       
                    }
                }
                else
                {
                    throw new MicroServiceException() { ErrorCode = "OUU111" };
                }
            }
            
            _MSUoW.Commit();
            return slotandDates;
        }
        #endregion

        #region ADD EXAM DATES 
        /// <summary>
        /// Author  :  Sumbul Samreen
        /// Date    :   12-02-2020
        /// Description :   This method is used to upadate exam slots and dates to examinationdate table
        /// </summary>
        /// <param name="filterOptions"></param>
        /// <returns></returns>
        public List<ExamFormFillupDatesDto> AddExamSlotsAndDates(PropertiesForDateScheduleDto input)
        {
            int maxSlotNo = 3;
            var years = input.FilterProperties.startAndEndYearFromSession.Split("-");
            YearFromSession year = new YearFromSession();
            year.StartYear = int.Parse(years[0]);
            year.EndYear = int.Parse(years[1]);
            DateTime dt = Convert.ToDateTime(input.Dates[0].StartDate);
            int ExamYear = dt.Year;
            Users user = _MSUoW.Users.FindBy(u => u.Id == GetCurrentUserId() && u.IsActive).FirstOrDefault();
            ExamDateFilterPropertiesDto filterOptions = input.FilterProperties;
            List<ExamFormFillupDatesDto> slotAndDatelist = input.Dates;
            if (filterOptions != null && slotAndDatelist != null && slotAndDatelist.Count != 0)
            {
                List<ExaminationFormFillUpDates> slotandDatesForTable = new List<ExaminationFormFillUpDates>(maxSlotNo);
                if (filterOptions != null)
                {
                    Guid examMasterId;
                    examMasterId = (filterOptions.CourseTypeCode == _MSCommon.GetEnumDescription(enCourseType.Pg)) ?

                                   _MSUoW.ExaminationMaster.GetAll().Where(m => m.IsActive && m.CourseType == filterOptions.CourseTypeCode && m.CourseCode == filterOptions.CourseCode
                                    && m.DepartmentCode == filterOptions.DepartmentCode && m.StreamCode == filterOptions.StreamCode && m.SubjectCode == filterOptions.SubjectCode &&
                                    m.SemesterCode == filterOptions.SemesterCode).Select(s => s.Id).FirstOrDefault()

                                    : _MSUoW.ExaminationMaster.GetAll().Where(m => m.IsActive && m.CourseType == filterOptions.CourseTypeCode
                                      && m.StreamCode == filterOptions.StreamCode && m.SubjectCode == filterOptions.SubjectCode &&
                                      m.SemesterCode == filterOptions.SemesterCode).Select(s => s.Id).FirstOrDefault();
                    if (examMasterId != Guid.Empty)
                    {
                        ExaminationSession examSessionData = _MSUoW.ExaminationSession.FindBy(e => e.IsActive && e.ExaminationMasterId == examMasterId && e.StartYear == year.StartYear && e.EndYear == year.EndYear).FirstOrDefault();

                        if (examSessionData!= null)
                        {
                            examSessionData.ExamYear = year.StartYear;
                            slotandDatesForTable = slotAndDatelist.Where(s => s.StartDate != null || s.EndDate != null).Select(p => new ExaminationFormFillUpDates
                            {
                                SlotOrder = p.SlotOrder,
                                StartDate = p.StartDate,
                                EndDate = p.EndDate,
                                ExaminationMasterId = examMasterId,
                                ExaminationSessionId = examSessionData.Id,
                                Id = Guid.NewGuid()
                            }).ToList();
                            
                            _MSUoW.ExaminationFormFillUpDates.AddRange(slotandDatesForTable);
                            _MSUoW.ExaminationSession.Update(examSessionData);
                           
                        }
                        else
                        {
                            ExaminationSession sessionDataObj = new ExaminationSession();
                            Guid newGuid = Guid.NewGuid();
                            sessionDataObj.Id = newGuid;
                            sessionDataObj.ExaminationMasterId = examMasterId;
                            sessionDataObj.StartYear = year.StartYear;
                            sessionDataObj.EndYear = year.EndYear;
                            sessionDataObj.ExamYear = ExamYear;
                            _MSUoW.ExaminationSession.Add(sessionDataObj);
                           
                        }
                    }
                    else
                    {
                        ExaminationMaster masterDataObj = new ExaminationMaster();
                        Guid newGuid = Guid.NewGuid();
                        masterDataObj.Id = newGuid;
                        masterDataObj.CourseType = filterOptions.CourseTypeCode;
                        masterDataObj.StreamCode = filterOptions.StreamCode;
                        masterDataObj.SubjectCode = filterOptions.SubjectCode;
                        masterDataObj.CourseCode = filterOptions.CourseCode;
                        masterDataObj.DepartmentCode = filterOptions.DepartmentCode;
                        masterDataObj.SemesterCode = filterOptions.SemesterCode;
                        // code need to be change dynamically based on subject code 
                        if(Convert.ToInt32(filterOptions.SubjectCode) == (int)enSubject.BBA || Convert.ToInt32(filterOptions.SubjectCode) == (int)enSubject.BCA)
                        {
                            masterDataObj.ServiceMasterId = (long)enServiceType.BCABBAExamApplicationForm;
                        }
                        if (Convert.ToInt32(filterOptions.SubjectCode) == (int)enSubject.PBNursing || Convert.ToInt32(filterOptions.SubjectCode) == (int)enSubject.BSCNursing)
                        {
                            masterDataObj.ServiceMasterId = (long)enServiceType.NursingApplicationForm;
                        }
                        _MSUoW.ExaminationMaster.Add(masterDataObj);
                    }
                    _MSUoW.Commit();
                    return slotAndDatelist;
                }
                else
                {
                    throw new MicroServiceException() { ErrorCode = "OUU110" };
                }

            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU111" };
            }
            
        }
        #endregion

        #region GET EXAM CENTERS
        /// <summary>
        /// Author  :   Sumbul Samreen
        /// Date    :   13-01-2020
        /// Description :   This method is used to get exam centers allocated to colleges
        /// </summary>
        /// <param name="filterparams"></param>
        /// <returns></returns>
        public List<CollegeAndAllocatedCenterNames> GetExamCentersAllocatedToColleges(ExamDateFilterPropertiesDto filterparams)
        {
            var years = filterparams.startAndEndYearFromSession.Split("-");
            YearFromSession year = new YearFromSession();
            year.StartYear = int.Parse(years[0]);
            year.EndYear = int.Parse(years[1]);
            long subjectMasterId = 0;
            List<CollegeAndAllocatedCenterNames> allocatedCenters = new List<CollegeAndAllocatedCenterNames>();
            if (filterparams != null)
            {
                Guid examMasterId = _MSUoW.ExaminationMaster.FindBy(m => m.IsActive && m.CourseType == filterparams.CourseTypeCode && m.StreamCode == filterparams.StreamCode && m.SubjectCode == filterparams.SubjectCode
                                  && m.SemesterCode == filterparams.SemesterCode).Select(s => s.Id).FirstOrDefault();
                if (examMasterId != Guid.Empty)
                {
                    Guid sessionId = _MSUoW.ExaminationSession.FindBy(e => e.IsActive && e.ExaminationMasterId == examMasterId && e.StartYear == year.StartYear && e.EndYear == year.EndYear).Select(s => s.Id).FirstOrDefault();
                    if (sessionId != Guid.Empty)
                    {
                        subjectMasterId = _common.GetAllSubject().Where(s => s.LookupCode == filterparams.SubjectCode).FirstOrDefault().Id;
                        allocatedCenters = _MSUoW.ExamFormFillupCenterAndCollegeNameRepository.GetAllCollegeBySubject(sessionId, subjectMasterId).ToList();                        
                    }
                    else
                    {
                        ExaminationSession sessionDataObj = new ExaminationSession();
                        Guid newGuid = Guid.NewGuid();
                        sessionDataObj.Id = newGuid;
                        sessionDataObj.ExaminationMasterId = examMasterId;
                        sessionDataObj.StartYear = year.StartYear;
                        sessionDataObj.EndYear = year.StartYear;
                        sessionDataObj.ExamYear = 0;
                        _MSUoW.ExaminationSession.Add(sessionDataObj);
                    }
                }
                else
                {
                    ExaminationMaster masterDataObj = new ExaminationMaster();
                    Guid newGuid = Guid.NewGuid();
                    masterDataObj.Id = newGuid;
                    masterDataObj.CourseType = filterparams.CourseTypeCode;
                    masterDataObj.StreamCode = filterparams.StreamCode;
                    masterDataObj.SubjectCode = filterparams.SubjectCode;
                    masterDataObj.CourseCode = filterparams.CourseCode;
                    masterDataObj.DepartmentCode = filterparams.DepartmentCode;
                    masterDataObj.SemesterCode = filterparams.SemesterCode;
                    _MSUoW.ExaminationMaster.Add(masterDataObj);
                }
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU110" };
            }
            _MSUoW.Commit();
            return allocatedCenters;
        }
        #endregion

        #region Allocate Exam Centers
        /// <summary>
        /// Author  :   Sumbul Samreen
        /// Date    :   13-01-2020
        /// Description :   This method is used to get exam centers allocated to colleges
        /// </summary>
        /// <param name="filterparams"></param>
        /// <returns></returns>
        public List<CollegeAndAllocatedCenterNames> AllocateCenters(PropertiesToAllocateCentersDto filterparams)
        {
            var years = filterparams.FilterProperties.startAndEndYearFromSession.Split("-");
            Guid examMasterId = Guid.Empty;
            YearFromSession year = new YearFromSession();
            year.StartYear = int.Parse(years[0]);
            year.EndYear = int.Parse(years[1]);
            
            Guid sessionId = Guid.Empty;
            List<CenterAllocation> centerToAllocate = new List<CenterAllocation>();
            List<CollegeAndAllocatedCenterNames> allocatedCenters = new List<CollegeAndAllocatedCenterNames>();
           
            if (filterparams != null)
            {
                examMasterId = _MSUoW.ExaminationMaster.FindBy(m => m.IsActive && m.CourseType == filterparams.FilterProperties.CourseTypeCode && m.StreamCode == filterparams.FilterProperties.StreamCode && m.SubjectCode == filterparams.FilterProperties.SubjectCode
                                  && m.SemesterCode == filterparams.FilterProperties.SemesterCode).Select(s => s.Id).FirstOrDefault();
                if (examMasterId != Guid.Empty)
                {
                    ExaminationSession examSessionData = _MSUoW.ExaminationSession.FindBy(e => e.IsActive && e.ExaminationMasterId == examMasterId && e.StartYear == year.StartYear && e.EndYear == year.EndYear).FirstOrDefault();
                    if (examSessionData != null)
                    {
                        List<CenterAllocation> updateData = new List<CenterAllocation>();
                        var oldData = _MSUoW.CenterAllocation.FindBy(centerAllocation => centerAllocation.ExaminationSessionId == examSessionData.Id
                             && centerAllocation.IsActive).ToList();
                        if (oldData != null && oldData.Count > 0)
                        {

                            filterparams.CollegeAndCenters.ForEach(newData =>
                            {
                                
                                var rec = oldData.Where(p => p.CollegeCode == newData.CollegeCode && p.Id == newData.Id && p.CenterCollegeCode != newData.CenterCode).FirstOrDefault();
                                if (rec != null)
                                {
                                    rec.CenterCollegeCode = newData.CenterCode;
                                    updateData.Add(rec);
                                }

                            });
                        }

                        if (updateData != null && updateData.Count > 0)
                        {
                             _MSUoW.CenterAllocation.UpdateRange(updateData);

                        }
                        if(oldData.Count == 0)
                        {
                            centerToAllocate = filterparams.CollegeAndCenters.Select(s => new CenterAllocation
                            {
                                ExaminationSessionId = examSessionData.Id,
                                CollegeCode = s.CollegeCode,
                                CenterCollegeCode = s.CenterCode
                            }).ToList();
                            _MSUoW.CenterAllocation.AddRange(centerToAllocate);
                        }
                    }
                    else
                    {
                        ExaminationSession sessionDataObj = new ExaminationSession();
                        Guid newGuid = Guid.NewGuid();
                        sessionDataObj.Id = newGuid;
                        sessionDataObj.ExaminationMasterId = examMasterId;
                        sessionDataObj.StartYear = year.StartYear;
                        sessionDataObj.EndYear = year.EndYear;
                        sessionDataObj.ExamYear = year.StartYear;
                        _MSUoW.ExaminationSession.Add(sessionDataObj);
                    }

                }
                else
                {
                    ExaminationMaster masterDataObj = new ExaminationMaster();
                    Guid newGuid = Guid.NewGuid();
                    masterDataObj.Id = newGuid;
                    masterDataObj.CourseType = filterparams.FilterProperties.CourseTypeCode;
                    masterDataObj.StreamCode = filterparams.FilterProperties.StreamCode;
                    masterDataObj.SubjectCode = filterparams.FilterProperties.SubjectCode;
                    masterDataObj.CourseCode = filterparams.FilterProperties.CourseCode;
                    masterDataObj.DepartmentCode = filterparams.FilterProperties.DepartmentCode;
                    masterDataObj.SemesterCode = filterparams.FilterProperties.SemesterCode;
                    _MSUoW.ExaminationMaster.Add(masterDataObj);
                }
            }
            _MSUoW.Commit();
            return allocatedCenters;
        }

        #endregion
    }
}
