﻿using AutoMapper;
using Microservice.Common;
using Microservice.Common.Abstract;
using MicroService.Business.Abstract;
using MicroService.Core;
using MicroService.Core.WorkflowCommon;
using MicroService.Core.WorkflowEntity;
using MicroService.Core.WorkflowTransactionHistrory;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Dto.StudentDashboard;
using MicroService.Domain.Models.Dto.UniversityService;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Exception;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MicroService.Business
{
    public class StudentRegistrationNoService : BaseService, IStudentRegistrationNoService
    {
        static readonly object _object = new object();
        private IMicroServiceUOW _MSUoW;
        private readonly IHostingEnvironment _environment;
        IConfiguration _IConfiguration;
        private IWFTransactionHistroryAppService _WFTransactionHistroryAppService;
        private MetadataAssemblyInfo workflowHelper;
        private WorkflowDbContext _workflowDbContext;
        private IMicroserviceCommon _MSCommon;
        private ICommonAppService _common;
        private IMicroServiceLogger _logger;
        private readonly string WorkflowName = "";

        public StudentRegistrationNoService(IMicroServiceUOW MSUoW, IRedisDataHandlerService redisData, IHostingEnvironment environment, IWFTransactionHistroryAppService WFTransactionHistroryAppService, IConfiguration IConfiguration, IMicroserviceCommon MSCommon, ICommonAppService common,
            IMicroServiceLogger logger) : base(redisData)
        {
            _MSUoW = MSUoW;
            _environment = environment;
            _IConfiguration = IConfiguration;
            _WFTransactionHistroryAppService = WFTransactionHistroryAppService;
            _workflowDbContext = new WorkflowDbContext();
            workflowHelper = new MetadataAssemblyInfo();
            WorkflowName = _IConfiguration.GetSection("RegistrationNumber:WorkflowName").Value;
            _MSCommon = MSCommon;
            _common = common;
            _logger = logger;
        }
        /// <summary>
        /// Anurag Digal
        /// 16-10-19
        /// Method is used for Odata Controller which will give student registration record as per usertype
        /// Modified by         : Dattatreya Dash on 19-10-2019
        /// </summary>
        /// <returns></returns>
        public IQueryable<AcknowledgementView> GetRoleBasedActiveRegistrationInboxData()
        {
            // Modified method not yet tested, view yet to be modified. Waiting for VPN connection
            List<string> roleNames = GetRoleListOfCurrentUser();
            long currentUserType = GetUserType();
            string CollegeCode = GetCollegeCode();
            string DepartmentCode = GetDepartmentCode();
            IQueryable<AcknowledgementView> inboxDataAsPerRole = null;

            var tenantWFIdList = _workflowDbContext.TenantWorkflow
                        .Where(tenantWF => tenantWF.TenantId == GetCurrentTenantId() && tenantWF.Workflow == WorkflowName).Select(wf => wf.Id).ToList();
            var tenantWFTrans = _workflowDbContext.TenantWorkflowTransitionConfig
                    .Where(tWFTransConfig => tenantWFIdList.Contains(tWFTransConfig.TenantWorkflowId)).ToList();

            List<TenantWorkflowTransitionConfig> workflowTransition =
                    tenantWFTrans
                    .Where(tWFTransConfig => roleNames.Any(role => tWFTransConfig.Roles.Split(',', StringSplitOptions.None).ToList()
                    .Contains(role))).ToList();

            if (roleNames.Contains(Convert.ToString((long)enRoles.COE)))
            {
                List<string> stateList = GetRoleSpecificWorkflowStateList();
                inboxDataAsPerRole = _MSUoW.AcknowledgementView.GetAll().Where(w => w.IsActive && stateList.Contains(w.State));
            }
            else
            {
                inboxDataAsPerRole = _MSUoW.AcknowledgementView.FindBy(t => t.IsActive == true && workflowTransition
                .Any(transition => transition.SourceState == t.State && transition.TenantWorkflowId == t.TenantWorkflowId));
            }

            if (roleNames.Contains(((int)enRoles.Principal).ToString()) || roleNames.Contains(((int)enRoles.HOD).ToString()))
            {
                TenantWorkflowTransitionConfig workFlowObj = tenantWFTrans.Where(tt => tt.IsActive && tenantWFIdList.Contains(tt.TenantWorkflowId)
                                                             && tt.TransitState.Trim().ToLower() == _IConfiguration.GetSection("RegistrationNumber:RegistrationNumberApprovedStatus").Value.Trim().ToLower())
                                                            .OrderByDescending(d => d.TenantWorkflowId).FirstOrDefault();
                IQueryable<AcknowledgementView> acknowledgmentList = _MSUoW.AcknowledgementView.FindBy(t => t.IsActive == true && t.State== workFlowObj.TransitState && workFlowObj.TenantWorkflowId == t.TenantWorkflowId);
                inboxDataAsPerRole = inboxDataAsPerRole.Concat(acknowledgmentList);
            }


            switch (currentUserType)
            {
                case (long)enUserType.Principal:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower());
                    break;
                case (long)enUserType.HOD:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower() && data.DepartmentCode.Trim().ToLower() == DepartmentCode.Trim().ToLower());
                    break;
                case (long)enUserType.Student:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CreatorUserId == GetCurrentUserId() && data.IsActive);
                    break;
            }

            return inboxDataAsPerRole;
        }

        /// <summary>
        /// Sumbul Samreen
        /// 25-02-2020
        /// Method is used for Odata Controller which will give only items to be work upon's count as per usertype.
        /// <returns></returns>
        public IQueryable<AcknowledgementView> GetRoleBasedActiveRegistrationInboxCountData()
        {
            // Modified method not yet tested, view yet to be modified. Waiting for VPN connection
            List<string> roleNames = GetRoleListOfCurrentUser();
            long currentUserType = GetUserType();
            string CollegeCode = GetCollegeCode();
            string DepartmentCode = GetDepartmentCode();
            IQueryable<AcknowledgementView> inboxDataAsPerRole = null;

            var tenantWFIdList = _workflowDbContext.TenantWorkflow
                        .Where(tenantWF => tenantWF.TenantId == GetCurrentTenantId() && tenantWF.Workflow == WorkflowName).Select(wf => wf.Id).ToList();
            var tenantWFTrans = _workflowDbContext.TenantWorkflowTransitionConfig
                    .Where(tWFTransConfig => tenantWFIdList.Contains(tWFTransConfig.TenantWorkflowId)).ToList();

            List<TenantWorkflowTransitionConfig> workflowTransition =
                    tenantWFTrans
                    .Where(tWFTransConfig => roleNames.Any(role => tWFTransConfig.Roles.Split(',', StringSplitOptions.None).ToList()
                    .Contains(role))).ToList();

            if (roleNames.Contains(Convert.ToString((long)enRoles.COE)))
            {
                List<string> stateList = GetRoleSpecificWorkflowStateList();
                inboxDataAsPerRole = _MSUoW.AcknowledgementView.GetAll().Where(w => w.IsActive && stateList.Contains(w.State));
            }
            else
            {
                inboxDataAsPerRole = _MSUoW.AcknowledgementView.FindBy(t => t.IsActive == true && workflowTransition
                .Any(transition => transition.SourceState == t.State && transition.TenantWorkflowId == t.TenantWorkflowId));
            }

            switch (currentUserType)
            {
                case (long)enUserType.Principal:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower());
                    break;
                case (long)enUserType.HOD:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower() && data.DepartmentCode.Trim().ToLower() == DepartmentCode.Trim().ToLower());
                    break;
                case (long)enUserType.Student:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CreatorUserId == GetCurrentUserId() && data.IsActive);
                    break;
            }

            return inboxDataAsPerRole;
        }
        //public IQueryable<UniversityRegistrationDetail> GetRoleSpecificUniversityServiceAppliedData()
        //{
        //    long currentUserType = GetUserType();
        //    string CollegeCode = GetCollegeCode();
        //    List<TenantWorkflowTransitionConfig> workflowTransition = RoleSpecificWorkflowTransitionConfigData(WorkflowName);

        //    var inboxDataAsPerRole = _MSUoW.UniversityRegistrationDetail.GetAll().Where(t => t.IsActive == true && workflowTransition
        //        .Any(transition => transition.SourceState == t.State && transition.TenantWorkflowId == t.TenantWorkflowId));

        //    if (currentUserType == (long)enUserType.Principal)
        //    {
        //        inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower());
        //    }
        //    return inboxDataAsPerRole;
        //}

        private List<TenantWorkflowTransitionConfig> RoleSpecificWorkflowTransitionConfigData(string workflowName)
        {
            List<string> roleNames = GetRoleListOfCurrentUser();
            var tenantWFIdList = _workflowDbContext.TenantWorkflow
                        .Where(tenantWF => tenantWF.TenantId == GetCurrentTenantId() && tenantWF.Workflow == workflowName).Select(wf => wf.Id).ToList();
            var tenantWFTrans = _workflowDbContext.TenantWorkflowTransitionConfig
                    .Where(tWFTransConfig => tenantWFIdList.Contains(tWFTransConfig.TenantWorkflowId)).ToList();

            List<TenantWorkflowTransitionConfig> workflowTransition =
                    tenantWFTrans
                    .Where(tWFTransConfig => roleNames.Any(role => tWFTransConfig.Roles.Split(',', StringSplitOptions.None).ToList()
                    .Contains(role))).ToList();

            return workflowTransition;
        }

        /// <summary>
        /// Modified By :   Sumbul Samreen
        /// Date        :   16-01-2020
        /// Description :   Send SMS to students about Registration Number Generation/ Rejection / send mail to Principal after Rejection of application
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public UniversityRegistrationDetailDto UpdateRegistrationNumberApplicationStatus(Guid id, UniversityRegistrationDetailDto input)
        {
            #region Step b starts
            input.LoggedInUserRoles = GetRoleListOfCurrentUser();
            var universityRegistrationDetail = _MSUoW.UniversityRegistrationDetail.FindBy(univReg => univReg.Id == id).FirstOrDefault();

            string trigger = input._trigger;
            #endregion
            if (universityRegistrationDetail != null && input.Id == id)
            {
                string sourceState = universityRegistrationDetail.State;
                if (universityRegistrationDetail.TenantWorkflowId != 0)
                {
                    #region Step c
                    universityRegistrationDetail.LastStatusUpdateDate = DateTime.UtcNow;
                    #endregion

                    #region Step d
                    Dictionary<string, object> triggerParameterDictionary = new Dictionary<string, object>();
                    _WFTransactionHistroryAppService.SetEntityValue<UniversityRegistrationDetail>(universityRegistrationDetail.State, universityRegistrationDetail.TenantWorkflowId, trigger, input, ref universityRegistrationDetail, ref triggerParameterDictionary);
                    var executionStatus = input.ExecuteProcess(universityRegistrationDetail, input);
                    #endregion

                    #region Step e
                    // Update the service applied table with latest data
                    input.PossibleWorkflowTransitions = _workflowDbContext.TenantWorkflowTransitionConfig.Where(transConfig => transConfig.TenantWorkflowId == universityRegistrationDetail.TenantWorkflowId && transConfig.IsActive && transConfig.SourceState == universityRegistrationDetail.State).ToList();
                    bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, universityRegistrationDetail.State, input.ParentWorkflowId);
                    UpdateUserServiceAppliedDetail(universityRegistrationDetail, IsLastTransition);
                    #endregion


                    #region Step f

                    _MSUoW.UniversityRegistrationDetail.Update(universityRegistrationDetail);
                    #endregion

                    #region Step g
                    List<TenantWorkflowTransitionConfig> possibleTransition = input.PossibleWorkflowTransitions;
                    string workflowMessage = input.WorkflowMessage;
                    input = AutoMapper.Mapper.Map<UniversityRegistrationDetail, UniversityRegistrationDetailDto>(universityRegistrationDetail);
                    input.PossibleWorkflowTransitions = possibleTransition;
                    input.WorkflowMessage = workflowMessage;
                    _MSUoW.Commit();

                    // Custom developement, sending email after every state change
                    var nextApprovingRoles = _workflowDbContext.TenantWorkflowTransitionConfig
                        .Where(transConfig => transConfig.TenantWorkflowId == universityRegistrationDetail.TenantWorkflowId
                                && transConfig.IsActive && transConfig.SourceState == universityRegistrationDetail.State).Select(transConfig => transConfig.Roles).FirstOrDefault();
                    // It is assumed that a single role will be fetched, no need to separate these with 'comma'.
                    if (nextApprovingRoles != null && nextApprovingRoles != string.Empty)
                    {// Find user details of that role
                        var approvingRoleArray = nextApprovingRoles.Split(",").Select(role => Convert.ToInt64(role)).ToArray();
                        if (Array.IndexOf(approvingRoleArray, (long)enRoles.Student) == -1)
                        { // No student role involved, send email
                            var nextApprovingUser = new Users();

                            // get the user whether principal/ HOD
                            bool isAffiliated = _MSUoW.CollegeMaster.FindBy(clg => clg.IsActive && clg.LookupCode.Trim().ToLower() == universityRegistrationDetail.CollegeCode.Trim().ToLower()).Select(s => s.IsAffiliationRequired).FirstOrDefault();

                            if (Array.IndexOf(approvingRoleArray, (long)enRoles.Principal) != -1 && isAffiliated)
                            { // Find the corresponding principal of the applying student
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.CollegeCode == universityRegistrationDetail.CollegeCode).FirstOrDefault();
                            }
                            else if ((Array.IndexOf(approvingRoleArray, (long)enRoles.HOD) != -1) && !isAffiliated && universityRegistrationDetail.CourseType.Trim().ToString() == _MSCommon.GetEnumDescription(enCourseType.Pg).Trim().ToString())
                            {
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.HOD && usr.CollegeCode.Trim().ToLower() == universityRegistrationDetail.CollegeCode.Trim().ToLower()
                                && usr.DepartmentCode.Trim().ToLower() == universityRegistrationDetail.DepartmentCode.Trim().ToLower() && usr.IsActive).FirstOrDefault();
                            }
                            else
                            {
                                var nextApprovingUserRole = _MSUoW.UserRoles.FindBy(usrRole => approvingRoleArray.Contains(usrRole.RoleId) && usrRole.IsActive).FirstOrDefault();
                                if (nextApprovingUserRole != null) // Checking this
                                {
                                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                                }
                            }

                            //var nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                            if (nextApprovingUser != null)
                            {
                                string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.RegistrationService && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                                if (serviceName != null && serviceName != string.Empty && nextApprovingUser.EmailAddress != null && nextApprovingUser.EmailAddress != string.Empty)
                                    SendEmailToApprovingAuthorityOnStateChange(nextApprovingUser.EmailAddress, nextApprovingUser.Name, serviceName);
                            }
                        }
                    }
                    // Sending email ends here
                    #endregion

                    #region Step h
                    _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, id, universityRegistrationDetail.LastStatusUpdateDate, sourceState, trigger, universityRegistrationDetail.State, universityRegistrationDetail.TenantWorkflowId);
                    #endregion

                    #region Step k
                    //bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, input.State, input.ParentWorkflowId); // Moved to step e
                    if (IsLastTransition == true)
                    {
                        var applyingStudent = _MSUoW.Users.FindBy(usr => usr.Id == universityRegistrationDetail.CreatorUserId).FirstOrDefault();
                        if (applyingStudent != null)
                        {
                            //Send email about workflow completion
                            SendEmailOnWorkflowCompletion(applyingStudent.EmailAddress, applyingStudent.UserName, universityRegistrationDetail.RegistrationNo);

                            // Send SMS to students about Registration Number Generation
                            if (applyingStudent.PhoneNumber != null && applyingStudent.PhoneNumber != string.Empty && applyingStudent.PhoneNumber.Length == 10)
                            {
                                string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.RegistrationNumberGeneratedSMS && s.ServiceType == (long)enServiceType.RegistrationService && s.IsActive == true).FirstOrDefault();
                                OTPMessage otpObj = new OTPMessage();
                                if (templateData != null)
                                {
                                    string contentId = string.Empty;
                                    contentId = templateData.SMSContentId;
                                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                                    StringBuilder body = new StringBuilder(templateData.Body);
                                    body.Replace("--studentName--", applyingStudent.Name);
                                    body.Replace("--url--", applicationUrl);
                                    otpObj.Message = body.ToString();
                                    otpObj.MobileNumber = applyingStudent.PhoneNumber;
                                    _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "Invalid data received." };
            }
            return input;
            //return true;
        }
        #region WORKFLOW EXECUTION
        /// <summary>
        /// Author :   Sumbul Samreen
        /// Date        :   23-01-2020
        /// Description : This method is used to change status by comp cell 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public UniversityRegistrationDetailDto UpdateRegistrationNumberApplicationStatusByCompCell(Guid id, UniversityRegistrationDetailDto input)
        {
            #region Step b starts
            input.LoggedInUserRoles = GetRoleListOfCurrentUser();
            var universityRegistrationDetail = _MSUoW.UniversityRegistrationDetail.FindBy(univReg => univReg.Id == id).FirstOrDefault();

            string trigger = input._trigger;
            #endregion
            if (universityRegistrationDetail != null && input.Id == id)
            {
                string sourceState = universityRegistrationDetail.State;
                if (universityRegistrationDetail.TenantWorkflowId != 0)
                {
                    #region Step c
                    universityRegistrationDetail.LastStatusUpdateDate = DateTime.UtcNow;
                    #endregion

                    #region Step d
                    Dictionary<string, object> triggerParameterDictionary = new Dictionary<string, object>();
                    _WFTransactionHistroryAppService.SetEntityValue<UniversityRegistrationDetail>(universityRegistrationDetail.State, universityRegistrationDetail.TenantWorkflowId, trigger, input, ref universityRegistrationDetail, ref triggerParameterDictionary);
                    var executionStatus = input.ExecuteProcess(universityRegistrationDetail, input);
                    #endregion

                    #region Step e
                    // Update the service applied table with latest data
                    input.PossibleWorkflowTransitions = _workflowDbContext.TenantWorkflowTransitionConfig.Where(transConfig => transConfig.TenantWorkflowId == universityRegistrationDetail.TenantWorkflowId && transConfig.IsActive && transConfig.SourceState == universityRegistrationDetail.State).ToList();
                    bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, universityRegistrationDetail.State, input.ParentWorkflowId);
                    UpdateUserServiceAppliedDetail(universityRegistrationDetail, IsLastTransition);
                    #endregion


                    #region Step f
                    lock (_object)
                    {
                        UniversityRegistrationDetailDto universityObject = DeepCopy(input);
                        universityRegistrationDetail.RegistrationNo = UpdateRegistrationNumber(universityObject);
                        _MSUoW.UniversityRegistrationDetail.Update(universityRegistrationDetail);
                    }
                    #endregion

                    #region Step g
                    List<TenantWorkflowTransitionConfig> possibleTransition = input.PossibleWorkflowTransitions;
                    string workflowMessage = input.WorkflowMessage;
                    input = AutoMapper.Mapper.Map<UniversityRegistrationDetail, UniversityRegistrationDetailDto>(universityRegistrationDetail);
                    input.PossibleWorkflowTransitions = possibleTransition;
                    input.WorkflowMessage = workflowMessage;
                    _MSUoW.Commit();

                    // Custom developement, sending email after every state change
                    var nextApprovingRoles = _workflowDbContext.TenantWorkflowTransitionConfig
                        .Where(transConfig => transConfig.TenantWorkflowId == universityRegistrationDetail.TenantWorkflowId
                                && transConfig.IsActive && transConfig.SourceState == universityRegistrationDetail.State).Select(transConfig => transConfig.Roles).FirstOrDefault();
                    // It is assumed that a single role will be fetched, no need to separate these with 'comma'.
                    if (nextApprovingRoles != null && nextApprovingRoles != string.Empty)
                    {// Find user details of that role
                        var approvingRoleArray = nextApprovingRoles.Split(",").Select(role => Convert.ToInt64(role)).ToArray();
                        if (Array.IndexOf(approvingRoleArray, (long)enRoles.Student) == -1)
                        { // No student role involved, send email
                            var nextApprovingUser = new Users();
                            if (Array.IndexOf(approvingRoleArray, (long)enRoles.Principal) != -1)
                            { // Find the corresponding principal of the applying student
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.CollegeCode == universityRegistrationDetail.CollegeCode).FirstOrDefault();
                            }
                            else
                            {
                                var nextApprovingUserRole = _MSUoW.UserRoles.FindBy(usrRole => approvingRoleArray.Contains(usrRole.RoleId) && usrRole.IsActive).FirstOrDefault();
                                if (nextApprovingUserRole != null) // Checking this
                                {
                                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                                }
                            }

                            //var nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                            if (nextApprovingUser != null)
                            {
                                string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.RegistrationService && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                                if (serviceName != null && serviceName != string.Empty && nextApprovingUser.EmailAddress != null && nextApprovingUser.EmailAddress != string.Empty)
                                    SendEmailToApprovingAuthorityOnStateChange(nextApprovingUser.EmailAddress, nextApprovingUser.Name, serviceName);
                            }
                        }
                    }
                    // Sending email ends here
                    #endregion

                    #region Step h
                    _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, id, universityRegistrationDetail.LastStatusUpdateDate, sourceState, trigger, universityRegistrationDetail.State, universityRegistrationDetail.TenantWorkflowId);
                    #endregion

                    #region Step k
                    //bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, input.State, input.ParentWorkflowId); // Moved to step e
                    if (IsLastTransition == true)
                    {
                        var applyingStudent = _MSUoW.Users.FindBy(usr => usr.Id == universityRegistrationDetail.CreatorUserId).FirstOrDefault();
                        if (applyingStudent != null && input.State != _MSCommon.GetEnumDescription((enState)enState.RegNoApplicationRejected))
                        {
                            //string emailIdOfApplyingUser = ;
                            //Send email about workflow completion
                            SendEmailOnWorkflowCompletion(applyingStudent.EmailAddress, applyingStudent.UserName, universityRegistrationDetail.RegistrationNo);

                            // Send SMS to students about Registration Number Generation
                            if (applyingStudent.PhoneNumber != null && applyingStudent.PhoneNumber != string.Empty && applyingStudent.PhoneNumber.Length == 10)
                            {
                                string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.RegistrationNumberGeneratedSMS && s.ServiceType == (long)enServiceType.RegistrationService && s.IsActive == true).FirstOrDefault();
                                OTPMessage otpObj = new OTPMessage();
                                if (templateData != null)
                                {
                                    string contentId = string.Empty;
                                    contentId = templateData.SMSContentId;
                                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                                    StringBuilder body = new StringBuilder(templateData.Body);
                                    body.Replace("--studentName--", applyingStudent.Name);
                                    body.Replace("--url--", applicationUrl);
                                    otpObj.Message = body.ToString();
                                    otpObj.MobileNumber = applyingStudent.PhoneNumber;
                                    _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                                }
                            }
                        }
                        else
                        {
                            //Send email to students about Registration Number Rejection
                            string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.RegistrationService && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                            if (serviceName != null && serviceName != string.Empty && applyingStudent.EmailAddress != null && applyingStudent.EmailAddress != string.Empty)
                            {
                                SendEmailToStudentOnRegistrationNumberRejection(applyingStudent.EmailAddress, applyingStudent.UserName, serviceName);
                            }

                            //Send SMS to students about Registration Number Rejection
                            if (applyingStudent.PhoneNumber != null && applyingStudent.PhoneNumber != string.Empty && applyingStudent.PhoneNumber.Length == 10)
                            {
                                string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ApplicationRejectedSMS && s.IsActive == true).FirstOrDefault();
                                OTPMessage otpObj = new OTPMessage();
                                if (templateData != null)
                                {
                                    string contentId = string.Empty;
                                    contentId = templateData.SMSContentId;
                                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                                    StringBuilder body = new StringBuilder(templateData.Body);
                                    body.Replace("--studentName--", applyingStudent.Name);
                                    body.Replace("--url--", applicationUrl);
                                    otpObj.Message = body.ToString();
                                    otpObj.MobileNumber = applyingStudent.PhoneNumber;
                                    _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                                }
                            }
                            //Send email to principal about Registration Number Rejection
                            // var principalData = _MSUoW.Users.FindBy(a => a.CollegeCode == input.CollegeCode && a.UserType == (long)enUserType.Principal && a.IsActive).FirstOrDefault();
                            // SendEmailToPrincipalOnRegistrationNumberRejection(principalData.EmailAddress, applyingStudent.UserName);

                        }
                    }
                    #endregion
                }
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "Invalid data received." };
            }
            return input;
            //return true;
        }
        /// <summary>
        /// Author              : Dattatreya Dash
        /// Date                : 05-11-2019
        /// Description         : This method is called when Principal forwards the reg.no.application form to registrar
        /// </summary>
        /// <param name="id">Instance id of the reg.num. application form entity</param>
        /// <param name="input">Dto carrying all the modified information of the reg.num.application form</param>
        /// <returns></returns>
        public UniversityRegistrationDetailDto RegistrationNumberApplicationStatusUpdateByPrincipal(Guid id, UniversityRegistrationDetailDto input)
        {
            //bool IsAffiliationCompleted=false;
            #region Step b starts
            input.LoggedInUserRoles = GetRoleListOfCurrentUser();
            var universityRegistrationDetail = _MSUoW.UniversityRegistrationDetail.FindBy(univReg => univReg.Id == id).FirstOrDefault();
            long subjectMasterObjId = 0;
            //get subjectmasterId from subjectMaster
            if (universityRegistrationDetail !=null && universityRegistrationDetail.CourseType==_MSCommon.GetEnumDescription(enCourseType.Pg))
            {
                subjectMasterObjId=_MSUoW.SubjectMaster.FindBy(sub => sub.IsActive && sub.CourseTypeCode == universityRegistrationDetail.CourseType
                                                           && (sub.CourseCode == universityRegistrationDetail.CourseCode || sub.CourseCode == "" || sub.CourseCode == null)
                                                           && (sub.DepartmentCode == universityRegistrationDetail.DepartmentCode || sub.DepartmentCode == "" || sub.DepartmentCode == null)                                                           
                                                           && sub.LookupCode == universityRegistrationDetail.SubjectCode)
                                                           .FirstOrDefault().Id;
            }
            else
            {
                subjectMasterObjId = _MSUoW.SubjectMaster.FindBy(sub => sub.IsActive && sub.CourseTypeCode == universityRegistrationDetail.CourseType                                                             
                                                             && sub.StreamCode == universityRegistrationDetail.StreamCode
                                                             && sub.LookupCode == universityRegistrationDetail.SubjectCode)
                                                           .FirstOrDefault().Id;
            }            

            //if subjectdmasterId exist then get the isaffiliationcompleted from collegesubjectmapper
            CollegeSubjectMapper collegeSubjectMapper = _MSUoW.CollegeSubjectMapper.FindBy(csm => csm.IsActive && csm.CollegeCode == universityRegistrationDetail.CollegeCode
                                                                && csm.SubjectMasterId == subjectMasterObjId)
                                                                .FirstOrDefault();
            
            if (collegeSubjectMapper == null)
            {
                throw new MicroServiceException { ErrorCode = "OUU124" };
            }
            else 
            {
                if (!collegeSubjectMapper.IsAffiliationCompleted)
                {
                    throw new MicroServiceException { ErrorCode = "OUU124" };
                }
            }

            string trigger = input._trigger;
            #endregion
            if (universityRegistrationDetail != null && input.Id == id)
            {
                var principalLastUpdateTime = universityRegistrationDetail.PrincipalLastUpdateTime;
                int minimumLockingHours = 0;
                try
                {
                    minimumLockingHours = Convert.ToInt32(_IConfiguration.GetSection("RegistrationNumber:MinimumLockingHoursForStateChangeByPrincipal").Value);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    minimumLockingHours = 0;
                }
                if (principalLastUpdateTime == null || (principalLastUpdateTime != null && ((DateTime)principalLastUpdateTime).AddMinutes(minimumLockingHours) < DateTime.UtcNow))
                {// Either principal has not updated or principal has updated the record more than 48 hours ago
                    string sourceState = universityRegistrationDetail.State;
                    if (universityRegistrationDetail.TenantWorkflowId != 0)
                    {
                        #region Step c
                        universityRegistrationDetail.LastStatusUpdateDate = DateTime.UtcNow;
                        #endregion

                        #region Step d
                        Dictionary<string, object> triggerParameterDictionary = new Dictionary<string, object>();
                        _WFTransactionHistroryAppService.SetEntityValue<UniversityRegistrationDetail>(universityRegistrationDetail.State, universityRegistrationDetail.TenantWorkflowId, trigger, input, ref universityRegistrationDetail, ref triggerParameterDictionary);
                        var executionStatus = input.ExecuteProcess(universityRegistrationDetail, input);
                        #endregion

                        #region Step e
                        // Update the service applied table with latest data
                        input.PossibleWorkflowTransitions = _workflowDbContext.TenantWorkflowTransitionConfig.Where(transConfig => transConfig.TenantWorkflowId == universityRegistrationDetail.TenantWorkflowId && transConfig.IsActive && transConfig.SourceState == universityRegistrationDetail.State).ToList();
                        bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, universityRegistrationDetail.State, input.ParentWorkflowId);
                        UpdateUserServiceAppliedDetail(universityRegistrationDetail, IsLastTransition);
                        #endregion

                        #region Step f
                        _MSUoW.UniversityRegistrationDetail.Update(universityRegistrationDetail);
                        #endregion

                        #region Step g
                        List<TenantWorkflowTransitionConfig> possibleTransition = input.PossibleWorkflowTransitions;
                        string workflowMessage = input.WorkflowMessage;
                        input = AutoMapper.Mapper.Map<UniversityRegistrationDetail, UniversityRegistrationDetailDto>(universityRegistrationDetail);
                        input.PossibleWorkflowTransitions = possibleTransition;
                        input.WorkflowMessage = workflowMessage;
                        _MSUoW.Commit();

                        // Custom developement, sending email after every state change
                        var nextApprovingRoles = _workflowDbContext.TenantWorkflowTransitionConfig
                            .Where(transConfig => transConfig.TenantWorkflowId == universityRegistrationDetail.TenantWorkflowId
                                    && transConfig.IsActive && transConfig.SourceState == universityRegistrationDetail.State).Select(transConfig => transConfig.Roles).FirstOrDefault();

                        // It is assumed that a single role will be fetched, no need to separate these with 'comma'.
                        if (nextApprovingRoles != null && nextApprovingRoles != string.Empty)
                        {// Find user details of that role
                            var approvingRoleArray = nextApprovingRoles.Split(",").Select(role => Convert.ToInt64(role)).ToArray();
                            if (Array.IndexOf(approvingRoleArray, (long)enRoles.Student) == -1)
                            { // No student role involved, send email
                                var nextApprovingUser = new Users();
                                if (Array.IndexOf(approvingRoleArray, (long)enRoles.Principal) != -1)
                                { // Find the corresponding principal of the applying student
                                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.CollegeCode == universityRegistrationDetail.CollegeCode).FirstOrDefault();
                                }
                                else
                                {
                                    var nextApprovingUserRole = _MSUoW.UserRoles.FindBy(usrRole => approvingRoleArray.Contains(usrRole.RoleId) && usrRole.IsActive).FirstOrDefault();
                                    if (nextApprovingUserRole != null) // Checking this
                                    {
                                        nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                                    }
                                }

                                //var nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                                if (nextApprovingUser != null)
                                {
                                    string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.RegistrationService && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                                    if (serviceName != null && serviceName != string.Empty && nextApprovingUser.EmailAddress != null && nextApprovingUser.EmailAddress != string.Empty)
                                        SendEmailToApprovingAuthorityOnStateChange(nextApprovingUser.EmailAddress, nextApprovingUser.Name, serviceName);
                                }
                            }
                        }
                        // Sending email ends here
                        #endregion

                        #region Step h
                        _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, id, universityRegistrationDetail.LastStatusUpdateDate, sourceState, trigger, universityRegistrationDetail.State, universityRegistrationDetail.TenantWorkflowId);
                        #endregion

                        #region Step k
                        //bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, input.State, input.ParentWorkflowId); // Moved to step e
                        if (IsLastTransition == true)
                        {
                            var applyingStudent = _MSUoW.Users.FindBy(usr => usr.Id == universityRegistrationDetail.CreatorUserId).FirstOrDefault();
                            if (applyingStudent != null)
                            {
                                //string emailIdOfApplyingUser = ;
                                //Send email about workflow completion
                                SendEmailOnWorkflowCompletion(applyingStudent.EmailAddress, applyingStudent.UserName, universityRegistrationDetail.RegistrationNo);
                            }

                        }
                        #endregion
                    }
                }
                else
                {
                    var remainingTime = Math.Round(((DateTime)principalLastUpdateTime).AddHours(minimumLockingHours).Subtract(DateTime.UtcNow).TotalHours);
                    throw new MicroServiceException { ErrorCode = "OUU060", SubstitutionParams = new object[1] { remainingTime } };
                }
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "Invalid data received." };
            }
            return input;
        }

        private void UpdateUserServiceAppliedDetail(UniversityRegistrationDetail universityRegistrationDetail, bool isAvailableToDownload)
        {
            UserServiceApplied usrSrvcApplied = _MSUoW.UserServiceApplied.FindBy(usrSrvc => usrSrvc.InstanceId == universityRegistrationDetail.Id).OrderByDescending(usrSrvc => usrSrvc.CreationTime).FirstOrDefault();
            if (usrSrvcApplied != null)
            {
                usrSrvcApplied.LastStatus = universityRegistrationDetail.State;
                if (isAvailableToDownload == true && usrSrvcApplied.IsAvailableToDL == false)
                {
                    usrSrvcApplied.IsAvailableToDL = isAvailableToDownload;
                }
                try
                {
                    var actionRequiringStateListForStudent = (_IConfiguration.GetSection("RegistrationNumber:ActionRequiringStates").Value).Split(",");
                    if (actionRequiringStateListForStudent.Count() > 0 && Array.IndexOf(actionRequiringStateListForStudent, usrSrvcApplied.LastStatus) != -1)
                    {// Latest state requires student action
                        usrSrvcApplied.IsActionRequired = true;
                    }
                    else
                    {// Latest state does not require student action
                        usrSrvcApplied.IsActionRequired = false;
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                    usrSrvcApplied.IsActionRequired = false;
                }
            }
            _MSUoW.UserServiceApplied.Update(usrSrvcApplied);
        }

        #endregion

        #region Student University Registration Number Apply
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 17-oct-2019
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public UniversityRegistrationDetailDto StudentUniversityRegistrationNumberApply(UniversityRegistrationDetailDto input)
        {
            UniversityRegistrationDetail studentApplied = _MSUoW.UniversityRegistrationDetail.FindBy(element => element.CreatorUserId == GetCurrentUserId() && element.IsActive).OrderBy(d => d.LastModificationTime).FirstOrDefault();

            if (studentApplied != null && studentApplied.State.Trim().ToLower() != _IConfiguration.GetSection("RegistrationNumber:RegistrationNumberRejectedStatus").Value.Trim().ToLower())
            {
                throw new MicroServiceException { ErrorCode = "OUU123", SubstitutionParams = new object[1] { studentApplied.State } };
            }

            long serviceId = (long)enServiceType.RegistrationService;
            Users user = _MSUoW.Users.FindBy(u => u.Id == GetCurrentUserId() && u.IsActive).FirstOrDefault();
            if (user.UserType == (long)enRoles.Student)
            {
                if (input != null)
                {
                    if (ValidateInput(input))
                    {
                        UniversityRegistrationDetail registrationData = Mapper.Map<UniversityRegistrationDetailDto, UniversityRegistrationDetail>(input);
                        var newGuid = Guid.NewGuid();
                        string guidForAck = newGuid.ToString();
                        registrationData.Id = newGuid;
                        registrationData.serviceId = serviceId;

                        Documents tempContent = new Documents();
                        registrationData.RegistrationDocument = new List<Documents>();
                        // Writing photo on server
                        tempContent = input.RegistrationDocument[0];
                        if (ImageValidation(tempContent))
                        {
                            tempContent.Id = Guid.NewGuid();
                            tempContent.SourceType = (long)enRegistartionDocType.RegistartionPhoto;
                            registrationData.RegistrationDocument.Add(ImageWrite(tempContent, newGuid));
                        }

                        #region WORKFLOW
                        var tenantLatestWF = _workflowDbContext.TenantWorkflow
                            .Where(tenantWF => tenantWF.TenantId == GetCurrentTenantId() && tenantWF.Workflow == WorkflowName)
                            .OrderByDescending(tenantWF => tenantWF.CreationTime).FirstOrDefault();

                        if (tenantLatestWF != null)
                        {
                            registrationData.TenantWorkflowId = tenantLatestWF.Id;
                        }
                        else
                        {
                            var defaultWF = _workflowDbContext.TenantWorkflow
                           .Where(tenantWF => tenantWF.TenantId == 0 && tenantWF.Workflow == WorkflowName)  //Default tenant id
                           .OrderByDescending(tenantWF => tenantWF.CreationTime).FirstOrDefault();

                            registrationData.TenantWorkflowId = defaultWF.Id;
                        }
                        input.LoggedInUserRoles = GetRoleListOfCurrentUser();
                        string studentRegistrationNumberInitialApplyTrigger = string.Empty;
                        try
                        {
                            studentRegistrationNumberInitialApplyTrigger = _IConfiguration.GetSection("RegistrationNumber:RegistrationNumberApplyTrigger").Value;
                        }
                        catch (Exception)
                        {
                            studentRegistrationNumberInitialApplyTrigger = "Apply";
                        }
                        string studentRegistrationNumberInitialApplyState = string.Empty;
                        try
                        {
                            studentRegistrationNumberInitialApplyState = _IConfiguration.GetSection("RegistrationNumber:RegistrationNumberInitialHistoryState").Value;
                        }
                        catch (Exception)
                        {
                            studentRegistrationNumberInitialApplyState = "Applied";
                        }
                        registrationData.State = studentRegistrationNumberInitialApplyState;
                        input._trigger = studentRegistrationNumberInitialApplyTrigger;
                        var executionStatus = input.ExecuteProcess(registrationData, input);

                        #endregion
                        registrationData.AcknowledgementNo = _MSCommon.GenerateAcknowledgementNo(_MSCommon.GetEnumDescription(enAcknowledgementFormat.ACK), serviceId, guidForAck);
                        _MSUoW.UniversityRegistrationDetail.Add(registrationData);

                        // Service applied
                        UserServiceApplied usrSrvcApplied = new UserServiceApplied()
                        {
                            IsAvailableToDL = false,
                            LastStatus = registrationData.State,
                            ServiceId = (long)enServiceType.RegistrationService,
                            UserId = GetCurrentUserId(),
                            InstanceId = newGuid,
                            IsActionRequired = true
                        };

                        _MSUoW.UserServiceApplied.Add(usrSrvcApplied);
                        Dictionary<string, object> triggerParameterDictionary = null;
                        _MSUoW.Commit();
                        #region MAIL SEND TO STUDENT
                        string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == serviceId && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();

                        // Send email to student
                        SendAcknowledgementEmail(user.EmailAddress, registrationData.StudentName, serviceName, registrationData.AcknowledgementNo);
                        #endregion

                        #region SMS SEND TO STUDENT
                        EmailTemplate acknowledgementSMS = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ApplicationAcknowledgementSMS && s.IsActive == true).FirstOrDefault();
                        if (acknowledgementSMS != null)
                        {
                            string contentId = string.Empty;
                            contentId = acknowledgementSMS.SMSContentId;
                            string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                            OTPMessage otpObj = new OTPMessage();
                            StringBuilder body = new StringBuilder(acknowledgementSMS.Body);
                            body.Replace("--studentName--", registrationData.StudentName);
                            body.Replace("--serviceName--", serviceName);
                            body.Replace("--acknowledgementNo--", registrationData.AcknowledgementNo);
                            otpObj.Message = body.ToString();
                            otpObj.MobileNumber = user.PhoneNumber;
                            _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                        }
                        //else throw new MicroServiceException() { ErrorCode = "OUU011" };
                        #endregion

                        // Send email to principal/hod in case of application submit, because the workflow is built like wise
                        // Fetch principal/hod email id of the applying student registration form
                        Users correspondingPrincipalOrHOD = null;


                        if (_MSCommon.GetEnumDescription((enCourseType)enCourseType.Ug) == registrationData.CourseType)
                        {// Ug course type | FInd principal
                            correspondingPrincipalOrHOD = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.IsActive && usr.CollegeCode == registrationData.CollegeCode).FirstOrDefault();
                        }
                        else
                        {// Pg course type | Find HOD
                            correspondingPrincipalOrHOD = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.HOD && usr.IsActive && usr.CollegeCode == registrationData.CollegeCode && registrationData.DepartmentCode == usr.DepartmentCode).FirstOrDefault();
                        }

                        /****** commented as this mail should be done after payment done successfully ***************/
                        /****** Date    : 21-01-2020 ********/
                        //if (correspondingPrincipalOrHOD != null)
                        //{
                        //    SendEmailToApprovingAuthorityOnStateChange(correspondingPrincipalOrHOD.EmailAddress, correspondingPrincipalOrHOD.Name, serviceName);
                        //}


                        _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, newGuid, DateTime.UtcNow, studentRegistrationNumberInitialApplyState, studentRegistrationNumberInitialApplyTrigger, registrationData.State, registrationData.TenantWorkflowId);
                        input = Mapper.Map<UniversityRegistrationDetail, UniversityRegistrationDetailDto>(registrationData);
                        return input;
                    }
                }
                else throw new MicroServiceException() { ErrorCode = "OUU013" };
            }
            else throw new MicroServiceException() { ErrorCode = "OUU052" };
            return input;
        }
        #endregion

        #region INPUT VALIDATION
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 17-oct-2019
        /// </summary>
        /// <param name="input"></param>
        ///  /// Author: Lopamudra Senapati(updated for minimum run time complexity)
        /// Date: 31-oct-2019
        /// <returns></returns>
        private bool ValidateInput(UniversityRegistrationDetailDto input)
        {
            string regularExpressionForName = @"^[a-zA-Z\s\.]+$";//This regex is used to allow only alphabets, dot and spaces in string
            string regExDOB = @"^(0[1-9]|1\d|2\d|3[01])\/(0[1-9]|1[0-2])\/(19|20)\d{2}+$";
            if (input.StudentName == null || input.StudentName.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU037" };
            }
            else if (!Regex.IsMatch(input.StudentName.Trim(), regularExpressionForName))
            {
                throw new MicroServiceException() { ErrorCode = "OUU038" };
            }
            else if (input.FatherName == null || input.FatherName.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU039" };
            }
            else if (!Regex.IsMatch(input.FatherName, regularExpressionForName))
            {
                throw new MicroServiceException() { ErrorCode = "OUU040" };
            }
            else if (input.DateOfBirth == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU041" };
            }
            //else if (!Regex.IsMatch(input.DateOfBirth.ToString().Trim(), regExDOB))
            //{
            //    throw new MicroServiceException() { ErrorCode = "OUU041" };
            //}
            else if (input.CollegeCode == null || input.CollegeCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU042" };
            }
            else if (input.CourseType == null || input.CourseType.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU043" };
            }
            else if (input.StreamCode == null || input.StreamCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU044" };
            }
            else if (input.SubjectCode == null || input.SubjectCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU045" };
            }
            else if (input.YearOfAdmission == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU046" };
            }
            else if (input.DepartmentCode == null || input.DepartmentCode == String.Empty)
            {
                input.DepartmentCode = "";
            }
            else if ((input.CourseType == "02") && (input.DepartmentCode == null || input.DepartmentCode == String.Empty))
            {
                throw new MicroServiceException() { ErrorCode = "OUU056" };
            }
            else if (input.CourseCode == null || input.CourseCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU059" };
            }
            input.StudentName = input.StudentName.Trim();
            input.FatherName = input.FatherName.Trim();
            input.CollegeCode = input.CollegeCode.Trim();
            input.CourseType = input.CourseType.Trim();
            input.StreamCode = input.StreamCode.Trim();
            input.SubjectCode = input.SubjectCode.Trim();
            return true;
        }
        #endregion

        #region INPUT VALIDATION
        /// <summary>
        /// Author: Dattatreya Dash
        /// Date: 28-10-2019
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private bool ValidateInputForGenerationOfRegistrationNumber(UniversityRegistrationDetailDto input)
        {
            string regularExpressionForName = @"^[a-zA-Z\s\.]+$";
            string pgDescription = _MSCommon.GetEnumDescription(enCourseType.Pg);
            if (input.StudentName == null || input.StudentName.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU037" };
            }
            else if (!Regex.IsMatch(input.StudentName.Trim(), regularExpressionForName))
            {
                throw new MicroServiceException() { ErrorCode = "OUU038" };
            }
            else if (input.CollegeCode == null || input.CollegeCode == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU042" };
            }
            else if (input.CourseType == null || input.CourseType == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU043" };
            }
            else if (input.StreamCode == null || input.StreamCode == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU044" };
            }
            else if (input.SubjectCode == null || input.SubjectCode == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU045" };
            }
            else if (input.YearOfAdmission == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU046" };
            }
            if (input.CourseType == pgDescription)
            {// PG course type
                if (input.DepartmentCode == null || input.DepartmentCode == String.Empty)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU056" };
                }
                else if (input.CourseCode == null || input.CourseCode == String.Empty)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU059" };
                }
            }
            else
            {// For UG course type, remove department code and course code
                input.DepartmentCode = input.CourseCode = null;
            }
            input.StudentName = input.StudentName.Trim();
            return true;
        }
        #endregion

        #region GET DOCUMENT DETAILS (UniversityRegistrationDetail entity)
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 17-oct-2019
        /// </summary>
        /// <param name="parentSourceId"></param>
        /// <returns></returns>
        public List<Documents> GetDocumentDetailsByParentSourceId(Guid parentSourceId)
        {
            List<Documents> document = new List<Documents>();
            if (parentSourceId != null)
            {
                document = _MSUoW.Documents.FindBy(data => data.ParentSourceId == parentSourceId && data.IsActive == true).ToList();

            }
            else throw new MicroServiceException() { ErrorCode = "OUU017" };
            return document;
        }
        #endregion

        #region GET STUDENT DETAILS (UniversityRegistrationDetail entity)
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 17-oct-2019 
        /// this method gets Student Details 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UniversityRegistrationDetail GetStudentUniversityRegistrationDetailsById(Guid id)
        {
            UniversityRegistrationDetail studentData = new UniversityRegistrationDetail();
            if (id != default(Guid))
            {
                studentData = _MSUoW.UniversityRegistrationDetailsRepository.GetAllDataById().Where(uni => uni.Id == id).FirstOrDefault();
            }
            else throw new MicroServiceException() { ErrorCode = "OUU017" };
            return studentData;
        }
        #endregion

        #region UPDATE STUDENT REGISTRATION DETAILS
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 17-oct-2019 
        /// this method update Student Details by id 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public UniversityRegistrationDetailDto GetStudentUniversityRegistrationDetailsById(Guid id, UniversityRegistrationDetailDto input)
        {
            //long userType = _MSUoW.Users.FindBy(u => u.Id == GetCurrentUserId() && u.IsActive).Select(t => t.UserType).FirstOrDefault();
            long userType = GetUserType();
            if ((userType == (long)enUserType.Principal) || (userType == (long)enUserType.HOD))
            {
                if (id != default(Guid) && input != null)
                {
                    UniversityRegistrationDetail studentData = _MSUoW.UniversityRegistrationDetailsRepository.GetAllData().Where(student => student.Id == id && student.IsActive == true).FirstOrDefault();
                    if (studentData != null)
                    {
                        if (ValidateInput(input))
                        {
                            studentData.StudentName = input.StudentName.Trim();
                            studentData.FatherName = input.FatherName.Trim();
                            studentData.DateOfBirth = input.DateOfBirth;
                            studentData.CollegeCode = input.CollegeCode;
                            studentData.StreamCode = input.StreamCode;
                            studentData.SubjectCode = input.SubjectCode;
                            studentData.DepartmentCode = input.DepartmentCode;
                            studentData.YearOfAdmission = input.YearOfAdmission;
                            //if (input.RegistrationDocument.Count() != 0)
                            //{
                            //    input.RegistrationDocument = input.RegistrationDocument.Where(a => a.FileContent != null).ToList();
                            //    if (input.RegistrationDocument.Count() > 1)
                            //    {
                            //        var temp = input.RegistrationDocument.Count() - 1;
                            //        Documents imageObj = new Documents();
                            //        imageObj = input.RegistrationDocument[temp];
                            //        input.RegistrationDocument = new List<Documents>();
                            //        input.RegistrationDocument.Add(imageObj);
                            //    }
                            //    input.RegistrationDocument.ForEach(content =>
                            //    {
                            //        if (ImageValidation(content))
                            //        {
                            //            studentData.RegistrationDocument = ImageWrite(input, studentData.Id);
                            //        }
                            //    });
                            //}
                            // Applicant has to upload a passport size photo and signature of .jpeg type or .jpg type
                            //Documents tempContent = new Documents();
                            //studentData.RegistrationDocument = new List<Documents>();
                            //// Writing photo on server
                            //tempContent = input.RegistrationDocument[0];
                            //if (ImageValidation(tempContent))
                            //{
                            //    tempContent.Id = Guid.NewGuid();
                            //    tempContent.SourceType = (long)enRegistartionDocType.RegistartionPhoto;
                            //    studentData.RegistrationDocument.Add(ImageWrite(tempContent, id));
                            //}
                            //else throw new MicroServiceException() { ErrorCode = "OUU016" };

                            // Set the update time for principal. This will be used during state change activity by Principal. If Principal updates any details of university 
                            // registration application, he/she can only change the state if the last update by
                            // him/her was done 48 before the state change activity.
                            studentData.PrincipalLastUpdateTime = DateTime.UtcNow;
                            _MSUoW.UniversityRegistrationDetail.Update(studentData);
                            _MSUoW.Commit();
                            var applyingStudent = _MSUoW.Users.FindBy(usr => usr.Id == studentData.CreatorUserId).FirstOrDefault();
                            if (applyingStudent != null && applyingStudent.EmailAddress != null && applyingStudent.EmailAddress != string.Empty)
                            {
                                string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.RegistrationService && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                                SendUniversityRegistrationUpdateConfirmationEmail(applyingStudent.EmailAddress, applyingStudent.Name, serviceName);
                                if (applyingStudent.PhoneNumber != null && applyingStudent.PhoneNumber != string.Empty && applyingStudent.PhoneNumber.Length == 10)
                                {
                                    string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                                    EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ApplicationUpdatedSMS && s.IsActive == true).FirstOrDefault();
                                    OTPMessage otpObj = new OTPMessage();
                                    if (templateData != null)
                                    {
                                        string contentId = string.Empty;
                                        contentId = templateData.SMSContentId;
                                        string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                                        StringBuilder body = new StringBuilder(templateData.Body);
                                        body.Replace("--studentName--", applyingStudent.Name);
                                        body.Replace("--serviceName--", serviceName);
                                        body.Replace("--url--", applicationUrl);
                                        otpObj.Message = body.ToString();
                                        otpObj.MobileNumber = applyingStudent.PhoneNumber;
                                        _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                                    }
                                }

                            }
                            input = Mapper.Map<UniversityRegistrationDetail, UniversityRegistrationDetailDto>(studentData);
                            return input;
                        }
                    }
                    else throw new MicroServiceException() { ErrorCode = "OUU014" };
                }
                else throw new MicroServiceException() { ErrorCode = "OUU015" };
            }
            else throw new MicroServiceException() { ErrorCode = "OUU055" };
            return input;
        }
        #endregion

        #region PRIVATE COMMON DOCUMENT WRITE METHOD
        private Documents ImageWrite(Documents file, Guid id)
        {
            string[] name = id.ToString().Split("-");
            string finalName = name[1] + name[2] + name[4] + name[0] + name[3];
            //List <Documents> documentList = new List<Documents>();
            //input.RegistrationDocument.ForEach((file) =>
            //{
            string path = string.Empty;
            file.ParentSourceId = id;
            if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.win.ToString())
            {
                path = _environment.ContentRootPath;
            }
            else if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.linux.ToString())
            {
                path = _IConfiguration.GetSection("LinuxDocumentPath").Value;
            }
            string fileName = finalName + "." + file.FileExtension;
            string DirectoryPath = _IConfiguration.GetSection("RegistrationNumber:RegistrationCertificatePath").Value; // "\\Documents\\RegistrationCertificates\\"
            var Path = path + DirectoryPath + fileName;

            if (!Directory.Exists(path + DirectoryPath))
            {
                Directory.CreateDirectory(path + DirectoryPath);
            }

            if (File.Exists(Path))
            {
                File.Delete(Path);
            }
            byte[] bytes = (byte[])file.FileContent;
            File.WriteAllBytes(Path, bytes);
            file.FilePath = DirectoryPath + fileName;
            //file.FilePath = file.FilePath.Replace('\\', '/');
            //documentList.Add(file);
            //});

            return file;
        }
        #endregion

        #region IMAGE VALIDATION
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 22-oct-2019
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private bool ImageValidation(Documents input)
        {
            if (input.FileExtension == "" || input.FileExtension == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU047" };
            }
            if (input.FileExtension != "" || input.FileExtension != null)
            {
                var regEx = new Regex(@"(jpg|jpeg)$");
                if (!regEx.IsMatch(input.FileExtension.ToLower()))
                {
                    throw new MicroServiceException() { ErrorCode = "OUU048" };
                }
            }
            if (input.FileContent.Length >= Convert.ToInt32(_IConfiguration.GetSection("RegistrationNumber:UploadImageSize").Value))
            {
                throw new MicroServiceException() { ErrorCode = "OUU049" };
            }
            if (string.IsNullOrEmpty(input.DocumentType) || string.IsNullOrWhiteSpace(input.DocumentType))
            {
                throw new MicroServiceException() { ErrorCode = "OUU131" };
            }
            if (input.SourceType == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU131" };
            }
            return true;
        }
        #endregion

        #region STUDENT DOCUMENT DOWNLOAD
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 21-oct-2019 
        /// student image download 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PDFDownloadFile DownloadStudentDocument(Guid id)
        {// To be verified again
            // removed current user id validation
            // except student used
            PDFDownloadFile flattenedImageDownloadDetails = new PDFDownloadFile();
            if (id != default(Guid))
            {
                Documents document = _MSUoW.Documents.FindBy(image => image.Id == id && image.IsActive).FirstOrDefault();
                if (document != null && document.FilePath != null && document.FilePath != "")
                {
                    FileStream stream = null;
                    if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.win.ToString())
                    {
                        stream = File.OpenRead(_environment.ContentRootPath + document.FilePath);
                    }
                    else if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.linux.ToString())
                    {
                        stream = File.OpenRead(_IConfiguration.GetSection("LinuxDocumentPath").Value + document.FilePath);
                    }

                    byte[] fileBytes = new byte[stream.Length];
                    stream.Read(fileBytes, 0, fileBytes.Length);
                    flattenedImageDownloadDetails.PdfData = fileBytes;
                    flattenedImageDownloadDetails.FileType = document.FileType;
                    flattenedImageDownloadDetails.FileName = document.FileName;
                    stream.Close();
                }
                else throw new MicroServiceException() { ErrorCode = "OUU014" };
            }
            else throw new MicroServiceException() { ErrorCode = "OUU018" };
            return flattenedImageDownloadDetails;
        }
        #endregion

        #region REGISTRATION NUMBER PREVIEW
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 24-oct-2019 
        /// Check all the student details before generate the registartion number  
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public RegistrationNumberPreviewDto RegistrationNumberPreview(Guid id)
        {            
            RegistrationNumberPreviewDto studentData = new RegistrationNumberPreviewDto();
            if (id != default(Guid))
            {
                UniversityRegistrationDetail user = _MSUoW.UniversityRegistrationDetail.FindBy(usr => usr.Id == id && usr.IsActive).FirstOrDefault();
                
                if (user != null)
                {
                    string state = _IConfiguration.GetSection("ExamFormFillup:COEStates").Value.Split(",").LastOrDefault().Trim().ToLower();
                    //first check registration no generated from semesterexamformfillup service or not
                    ExaminationFormFillUpDetails examObj = _MSUoW.ExaminationFormFillUpDetails.FindBy(exm => exm.IsActive && exm.CreatorUserId == user.CreatorUserId
                                                            && exm.State.Trim().ToLower() == state)
                                                            .FirstOrDefault();

                    //if record available then computer cell cannot generate the registration number
                    if(examObj!=null)
                    {
                        throw new MicroServiceException() { ErrorCode = "OUU129" };
                    }

                    UniversityRegistrationDetailDto param = Mapper.Map<UniversityRegistrationDetail, UniversityRegistrationDetailDto>(user);
                    UniversityRegistrationDetailDto universityObject = DeepCopy(param);
                    string registrationNumber = GetRegistrationNumber(universityObject);
                    if (registrationNumber != null && registrationNumber != "")
                    {
                        studentData.RegistrationNo = registrationNumber;
                        studentData.StudentName = user.StudentName;
                        studentData.CollegeCode = user.CollegeCode;
                        studentData.CourseType = user.CourseType;
                        studentData.StreamCode = user.StreamCode;
                        studentData.YearOfAdmission = user.YearOfAdmission;
                        studentData.DepartmentCode = user.DepartmentCode;
                        studentData.CourseCode = user.CourseCode;
                        studentData.SubjectCode = user.SubjectCode;
                        return studentData;
                    }
                    else throw new MicroServiceException() { ErrorCode = "OUU052" };
                }
                else throw new MicroServiceException() { ErrorCode = "OUU014" };
            }
            else throw new MicroServiceException() { ErrorCode = "OUU018" };
        }

        public UniversityRegistrationDetailDto DeepCopy(UniversityRegistrationDetailDto cpyData)
        {
            UniversityRegistrationDetailDto temp = new UniversityRegistrationDetailDto();
            temp.StudentName = cpyData.StudentName;
            temp.CollegeCode = cpyData.CollegeCode;
            temp.CourseType = cpyData.CourseType;
            temp.StreamCode = cpyData.StreamCode;
            temp.YearOfAdmission = cpyData.YearOfAdmission;
            temp.DepartmentCode = cpyData.DepartmentCode;
            temp.CourseCode = cpyData.CourseCode;
            temp.SubjectCode = cpyData.SubjectCode;
            return temp;
        }
        #endregion

        /// <summary>
        /// Author  :   Sumbul Samreen
        /// Date    :   23-01-2020
        /// Description :This method is used to update registration number.
        /// </summary>
        /// <param name="studentInfo"></param>
        /// <returns></returns>
        public string UpdateRegistrationNumber(UniversityRegistrationDetailDto studentInfo)
        {
            string runningNum = string.Empty;
            string coursePg = _MSCommon.GetEnumDescription(enCourseType.Pg);
            string registrationNo = GetRegistrationNumber(studentInfo);
            string yearCode = studentInfo.YearOfAdmission.ToString().Substring(2);
            string lastGeneratedNo = registrationNo.ToString().Substring(15);

            NumberGenerator numGenObj = _MSUoW.NumberGenerator.FindBy(data => data.YearOfAdmission == yearCode
                                                             && data.SubjectCode == studentInfo.SubjectCode
                                                             && data.CollegeCode == studentInfo.CollegeCode
                                                             && data.CourseCode == studentInfo.CourseCode
                                                             && data.StreamCode.Equals(studentInfo.StreamCode, StringComparison.InvariantCultureIgnoreCase)
                                                             ).FirstOrDefault();

            if (numGenObj != null)
            {
                // NumberGeneratorObj.LastGeneratedNumber = NumberGeneratorObj.LastGeneratedNumber + NumberGeneratorObj.NumberIncrementedBy;
                numGenObj.LastGeneratedNumber = Convert.ToInt32(lastGeneratedNo);
                _MSUoW.NumberGenerator.Update(numGenObj);
                //_MSUoW.Commit();         // Explicitly removed commit statement, will be done by parent method.
            }
            return registrationNo;
        }

        /// <summary>
        /// Author  :   Sumbul Samreen
        /// Date    :   23-01-2020
        /// Description :This method is used to get registration number by getting the param of type NumberGenerationDto.
        /// </summary>
        /// <param name="studentInfo"></param>
        /// <returns></returns>
        public string GetRegistrationNumber(UniversityRegistrationDetailDto studentInfo)
        {
            int numberIncrementBy = 1;
            string registrationNumber = string.Empty;
            string runningNum = string.Empty;
            string streamCodeLength = _IConfiguration.GetSection("RegistrationNumberCodeLength:StreamLength").Value;
            string courseCodeLength = _IConfiguration.GetSection("RegistrationNumberCodeLength:CourseCodeLength").Value;
            string subjectCodeLength = _IConfiguration.GetSection("RegistrationNumberCodeLength:SubjectCodeLength").Value;
            string collegeCodeLength = _IConfiguration.GetSection("RegistrationNumberCodeLength:CollegeCodeLength").Value;
            string runningSequenceLength = _IConfiguration.GetSection("RegistrationNumberCodeLength:RunningSequenceLength").Value;
            string YearlengthLength = _IConfiguration.GetSection("RegistrationNumberCodeLength:Yearlength").Value;
            string coursePg = _MSCommon.GetEnumDescription(enCourseType.Pg);
            string yearCode = studentInfo.YearOfAdmission.ToString().Substring(2);
            studentInfo.CourseCode = studentInfo.CourseCode == null ? "0" : studentInfo.CourseCode;
            NumberGenerator regdNos = _MSUoW.NumberGenerator.FindBy(data => data.YearOfAdmission == yearCode && data.StreamCode.Equals(studentInfo.StreamCode, StringComparison.InvariantCultureIgnoreCase)
                                      && data.SubjectCode == studentInfo.SubjectCode && data.CollegeCode == studentInfo.CollegeCode && data.CourseCode == studentInfo.CourseCode
                                     ).FirstOrDefault();
            if (regdNos != null)
            {// already exists
                regdNos.CourseType = studentInfo.CourseType;
                registrationNumber = GenerateRegistrationNumber(regdNos);

            }
            else
            {// new combination added
               
                UniversityRegistrationDetailDto studentObj = DeepCopy(studentInfo);
                runningNum = "0";
                
                if (studentObj.StreamCode.Length < int.Parse(streamCodeLength))
                {
                    studentObj.StreamCode = AddPrefix(int.Parse(streamCodeLength), '0', studentObj.StreamCode);
                }
                if (studentObj.SubjectCode.Length < int.Parse(subjectCodeLength))
                {
                    studentObj.SubjectCode = AddPrefix(int.Parse(subjectCodeLength), '0', studentObj.SubjectCode);
                }
                if (studentObj.CollegeCode.Length < int.Parse(collegeCodeLength))
                {
                    studentObj.CollegeCode = AddPrefix(int.Parse(collegeCodeLength), '0', studentObj.CollegeCode);
                }
                if (studentObj.CourseCode.Length < int.Parse(courseCodeLength))
                {
                    studentObj.CourseCode = AddPrefix(int.Parse(courseCodeLength), '0', studentObj.CourseCode); 
                }
                registrationNumber = yearCode + studentObj.StreamCode + studentObj.CourseCode + studentObj.SubjectCode + studentObj.CollegeCode + AddPrefix(int.Parse(runningSequenceLength), '0', (runningNum + numberIncrementBy));
                NumberGenerator NumberGeneratorObj = new NumberGenerator();
                NumberGeneratorObj.NumberIncrementedBy = numberIncrementBy;
                NumberGeneratorObj.LastGeneratedNumber = Convert.ToInt32(runningNum);
                NumberGeneratorObj.StartNumberSeq = "000";
                NumberGeneratorObj.YearOfAdmission = yearCode;
                NumberGeneratorObj.ProcessType = 3;
                NumberGeneratorObj.StreamCode = studentInfo.StreamCode;
                NumberGeneratorObj.CollegeCode = studentInfo.CollegeCode;
                NumberGeneratorObj.CourseCode = studentInfo.CourseType == coursePg ? studentInfo.CourseCode : "0";
                studentInfo.CourseCode = studentInfo.CourseType == coursePg ? studentInfo.CourseCode : "0";
                NumberGeneratorObj.SubjectCode = studentInfo.SubjectCode;
                _MSUoW.NumberGenerator.Add(NumberGeneratorObj);
                _MSUoW.Commit();
            }

            return registrationNumber;
        }

        string AddPrefix(int requiredlength, char prefix, string inputCode)
        {
            return inputCode.PadLeft(requiredlength, prefix); ;
        }

        /// <summary>
        /// Author  :   Sumbul Samreen
        /// Date    :   23-01-2020
        /// Description :This method is used to generate registration number.
        /// </summary>
        /// <param name="regdNos"></param>
        /// <returns></returns>

        private string GenerateRegistrationNumber(NumberGenerator regdNos)
        {
            NumberGenerator registrationObj = new NumberGenerator();
            string regdNo = string.Empty;
            string runningNum = string.Empty;
            string streamCodeLength = _IConfiguration.GetSection("RegistrationNumberCodeLength:StreamLength").Value;
            string courseCodeLength = _IConfiguration.GetSection("RegistrationNumberCodeLength:CourseCodeLength").Value;
            string subjectCodeLength = _IConfiguration.GetSection("RegistrationNumberCodeLength:SubjectCodeLength").Value;
            string collegeCodeLength = _IConfiguration.GetSection("RegistrationNumberCodeLength:CollegeCodeLength").Value;
            string runningSequenceLength = _IConfiguration.GetSection("RegistrationNumberCodeLength:RunningSequenceLength").Value;
            string YearlengthLength = _IConfiguration.GetSection("RegistrationNumberCodeLength:Yearlength").Value;
            string coursePg = _MSCommon.GetEnumDescription(enCourseType.Pg);
            string yearCode = registrationObj.YearOfAdmission = regdNos.YearOfAdmission;
            registrationObj.CourseCode = regdNos.CourseCode == null ? "0" : regdNos.CourseCode;
            registrationObj.StreamCode = regdNos.StreamCode.Length < int.Parse(streamCodeLength) ? AddPrefix(int.Parse(streamCodeLength), '0', regdNos.StreamCode) : regdNos.StreamCode;
            registrationObj.SubjectCode = regdNos.SubjectCode.Length < int.Parse(subjectCodeLength) ? AddPrefix(int.Parse(subjectCodeLength), '0', regdNos.SubjectCode) : regdNos.SubjectCode;
            registrationObj.CollegeCode = regdNos.CollegeCode.Length < int.Parse(collegeCodeLength) ? AddPrefix(int.Parse(collegeCodeLength), '0', regdNos.CollegeCode) : regdNos.CollegeCode;
            registrationObj.CourseCode = regdNos.CourseCode.Length < int.Parse(courseCodeLength) ? AddPrefix(int.Parse(courseCodeLength), '0', regdNos.CourseCode) : regdNos.CourseCode;
            runningNum = (regdNos.LastGeneratedNumber + regdNos.NumberIncrementedBy).ToString();
            registrationObj.LastGeneratedNumber = Convert.ToInt32(runningNum);
            if (runningNum.Length < int.Parse(runningSequenceLength))
            {
                runningNum = AddPrefix(int.Parse(runningSequenceLength), '0', runningNum);
            }
            registrationObj.CourseCode = coursePg == regdNos.CourseType ? regdNos.CourseCode : "0";

            regdNo = yearCode + registrationObj.StreamCode + registrationObj.CourseCode + registrationObj.SubjectCode + registrationObj.CollegeCode + runningNum;

            return regdNo;
        }

        #region REGISTRATION NUMBER INSERT IN UNIVERSITY REGISTRATION DETAIL TABLE
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 24-oct-2019 
        /// ALL data update in university registration detail table with registration number  
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public UniversityRegistrationDetailDto UpdateGenerateRegistrationNumber(Guid id, UniversityRegistrationDetailDto param)
        {
            UniversityRegistrationDetailDto universityRegistrationDetailDto = new UniversityRegistrationDetailDto();
            if (id != default(Guid) && param != null)
            {
                UniversityRegistrationDetail studentData = _MSUoW.UniversityRegistrationDetail.FindBy(usr => usr.Id == id && usr.IsActive).FirstOrDefault();
                if (studentData != null)
                {
                    if (ValidateInputForGenerationOfRegistrationNumber(param))
                    {
                        universityRegistrationDetailDto = Mapper.Map<UniversityRegistrationDetail, UniversityRegistrationDetailDto>(studentData);
                        //studentData.StudentName = param.StudentName;
                        //studentData.CollegeCode = param.CollegeCode;
                        //studentData.CourseType = param.CourseType;
                        //studentData.StreamCode = param.StreamCode;
                        //studentData.YearOfAdmission = param.YearOfAdmission;
                        //studentData.SubjectCode = param.SubjectCode;
                        //studentData.DepartmentCode = param.DepartmentCode;
                        //studentData.CourseCode = param.CourseCode;

                        universityRegistrationDetailDto.StudentName = param.StudentName;
                        universityRegistrationDetailDto.CollegeCode = param.CollegeCode;
                        universityRegistrationDetailDto.CourseType = param.CourseType;
                        universityRegistrationDetailDto.StreamCode = param.StreamCode;
                        universityRegistrationDetailDto.YearOfAdmission = param.YearOfAdmission;
                        universityRegistrationDetailDto.SubjectCode = param.SubjectCode;
                        universityRegistrationDetailDto.DepartmentCode = param.DepartmentCode;
                        universityRegistrationDetailDto.CourseCode = param.CourseCode;

                        UniversityRegistrationDetailDto universityObject = DeepCopy(universityRegistrationDetailDto);

                        string registrationNumber = GetRegistrationNumber(universityObject);
                        if (registrationNumber != null && registrationNumber != "")
                        {
                            // _MSUoW.UniversityRegistrationDetail.Update(studentData);
                            // _MSUoW.Commit();
                            //param = Mapper.Map<UniversityRegistrationDetail, UniversityRegistrationDetailDto>(studentData);
                            universityRegistrationDetailDto.RegistrationNo = registrationNumber;
                        }
                        else throw new MicroServiceException() { ErrorCode = "OUU053" };
                    }
                }
                else throw new MicroServiceException() { ErrorCode = "OUU014" };
            }
            else throw new MicroServiceException() { ErrorCode = "OUU054" };
            return universityRegistrationDetailDto;
        }
        #endregion

        #region Confirm
        /// <summary>
        /// Author:Sumbul Samreen
        /// Date: 24-oct-2019 
        /// ALL data update in university registration detail table with registration number  
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public RegistrationNumberPreviewDto ConfirmRegistrationNumberPreview(Guid id, UniversityRegistrationDetailDto param)
        {
            RegistrationNumberPreviewDto previewForm = new RegistrationNumberPreviewDto();
            if (id != default(Guid) && param != null)
            {
                UniversityRegistrationDetail studentData = _MSUoW.UniversityRegistrationDetail.FindBy(usr => usr.Id == id && usr.IsActive).FirstOrDefault();
                if (studentData != null)
                {
                    if (ValidateInputForGenerationOfRegistrationNumber(param))
                    {
                        studentData.StudentName = param.StudentName;
                        studentData.CollegeCode = param.CollegeCode;
                        studentData.CourseType = param.CourseType;
                        studentData.StreamCode = param.StreamCode;
                        studentData.YearOfAdmission = param.YearOfAdmission;
                        studentData.SubjectCode = param.SubjectCode;
                        studentData.DepartmentCode = param.DepartmentCode;
                        studentData.CourseCode = param.CourseCode;
                        studentData.RegistrationNo = param.RegistrationNo;
                        studentData.IsRegistrationNumberGenerated = true;
                        //string registrationNumber = GetRegistrationNumber(studentData);
                        _MSUoW.UniversityRegistrationDetail.Update(studentData);
                        _MSUoW.Commit();
                        previewForm = Mapper.Map<UniversityRegistrationDetail, RegistrationNumberPreviewDto>(studentData);
                        
                    }
                }
                else throw new MicroServiceException() { ErrorCode = "OUU014" };
            }
            else throw new MicroServiceException() { ErrorCode = "OUU054" };
            return previewForm;
        }
        #endregion

        private bool SendEmailOnWorkflowCompletion(string emailAddress, string userName, string regNumber)
        {
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.RegistrationNumberGenerated && s.ServiceType == (long)enServiceType.RegistrationService && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    string ClientApplicationURL = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                    body.Replace("--userName--", userName);
                    body.Replace("--url--", ClientApplicationURL);
                    body.Replace("--regNo--", regNumber);
                    _MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            return true;
        }

        /// <summary>
        /// Author  :   Sumbul Samreen
        /// Date    :   15-01-2020
        /// Description :   Send email to students about Registration Number Rejection
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="userName"></param>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        /// 
        private bool SendEmailToStudentOnRegistrationNumberRejection(string emailAddress, string userName, string serviceName)
        {
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ApplicationRejectedMail && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    string clientApplicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                    body.Replace("--userName--", userName);
                    body.Replace("--serviceName--", serviceName);
                    body.Replace("--url--", clientApplicationUrl);
                    _MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            return true;
        }

        /// <summary>
        /// Sumbul Samreen
        /// 15-01-2020
        //Send email to principal about Registration Number Rejection
        private bool SendEmailToPrincipalOrHodOnRegistrationNumberRejection(Users userData, string studentName, string serviceName)
        {
            if (userData != null)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.MailToPrincipalOnRegistrationNumberRejection && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    string ClientApplicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                    body.Replace("--studentName--", studentName);
                    body.Replace("--userName--", userData.Name);
                    body.Replace("--serviceName--", serviceName);
                    body.Replace("--url--", ClientApplicationUrl);
                    _MSCommon.SendMails(userData.EmailAddress, templateData.Subject, body.ToString());
                }
            }
            return true;
        }
        private bool IsLastTransitionOfWorkflow(List<TenantWorkflowTransitionConfig> possibleTransitions, string currentState, long? parentWFId)
        {
            bool isLastStateTransition = false;
            var transitionsAvailable = possibleTransitions.Where(transition => transition.SourceState == currentState).FirstOrDefault();
            if (transitionsAvailable == null)
            {
                isLastStateTransition = true;
            }
            return isLastStateTransition;
        }

        private bool SendEmailToApprovingAuthorityOnStateChange(string emailAddress, string userName, string serviceName)
        {
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ServiceRequestReceived && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    // Decide whether 
                    string ClientApplicationURL = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                    body.Replace("--userName--", userName);
                    body.Replace("--url--", ClientApplicationURL);
                    body.Replace("--serviceName--", serviceName);
                    _MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            return true;
        }

        /// Email send method
        /// <summary>
        /// Author: Lopamudra Senapati
        /// Date: 25-oct-2019
        /// </summary>
        public bool SendAcknowledgementEmail(string studentEmailAddress, string studentName, string serviceName, string acknowledgementNo)
        {
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ApplicationAcknowledgementEmail && s.IsActive == true).FirstOrDefault();
            if (templateData != null)
            {
                string applicationUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                string ClientApplicationURL = _IConfiguration.GetSection("ClientApplicationURL").Value;
                string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                StringBuilder body = new StringBuilder(templateData.Body);
                body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                body.Replace("--studentName--", studentName);
                body.Replace("--serviceName--", serviceName);
                body.Replace("--acknowledgementNo--", acknowledgementNo);
                body.Replace("--url--", ClientApplicationURL);
                _MSCommon.SendMails(studentEmailAddress, templateData.Subject, body.ToString());
            }
            return true;
        }

        #region STUDENT DOCUMENT DOWNLOAD (THIS METHOD USING DOCUMENTS ENTITY)
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 31-oct-2019
        /// student image download
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PDFDownloadFile DownloadStudentDocumentNew(Guid id)
        {
            PDFDownloadFile flattenedImageDownloadDetails = new PDFDownloadFile();
            if (id != default(Guid))
            {
                Documents document = _MSUoW.Documents.FindBy(image => image.CreatorUserId == GetCurrentUserId() && image.Id == id && image.IsActive).FirstOrDefault();
                if (document != null && document.FilePath != null && document.FilePath != "")
                {
                    // Default image is required to mobile team for doing that linux
                    try
                    {
                        FileStream stream = null;
                        if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.win.ToString())
                        {
                            stream = File.OpenRead(_environment.ContentRootPath + document.FilePath);
                        }
                        else if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.linux.ToString())
                        {
                            stream = File.OpenRead(_IConfiguration.GetSection("LinuxDocumentPath").Value + document.FilePath);
                        }

                        byte[] fileBytes = new byte[stream.Length];
                        stream.Read(fileBytes, 0, fileBytes.Length);
                        flattenedImageDownloadDetails.PdfData = fileBytes;
                        flattenedImageDownloadDetails.FileType = document.FileType;
                        flattenedImageDownloadDetails.FileName = document.FileName;
                        stream.Close();
                    }
                    catch (Exception e)
                    {
                        FileStream defaultStream = null;
                        if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.win.ToString())
                        {
                            defaultStream = File.OpenRead(_environment.ContentRootPath + _IConfiguration.GetSection("DefaultImgForLinux").Value);
                        }
                        else if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.linux.ToString())
                        {
                            defaultStream = File.OpenRead(_IConfiguration.GetSection("LinuxDocumentPath").Value + _IConfiguration.GetSection("DefaultImgForLinux").Value);
                        }

                        byte[] fileBytes = new byte[defaultStream.Length];
                        defaultStream.Read(fileBytes, 0, fileBytes.Length);
                        flattenedImageDownloadDetails.PdfData = fileBytes;
                        flattenedImageDownloadDetails.FileType = document.FileType;
                        flattenedImageDownloadDetails.FileName = document.FileName;
                        defaultStream.Close();
                    }
                }
                else throw new MicroServiceException() { ErrorCode = "OUU014" };
            }
            else throw new MicroServiceException() { ErrorCode = "OUU018" };
            return flattenedImageDownloadDetails;
        }
        #endregion

        //#region PENDING WORKFLOW COUNT
        //public long PendingWorkflowCount()
        //{
        //    // Modified method not yet tested, view yet to be modified. Waiting for VPN connection
        //    List<string> roleNames = GetRoleListOfCurrentUser();
        //    long currentUserType = GetUserType();
        //    string CollegeCode = GetCollegeCode();
        //    string DepartmentCode = GetDepartmentCode();
        //    long totalPendingWorkflows = 0;

        //    var tenantWFIdList = _workflowDbContext.TenantWorkflow
        //                .Where(tenantWF => tenantWF.TenantId == GetCurrentTenantId() && tenantWF.Workflow == WorkflowName).Select(wf => wf.Id).ToList();
        //    var tenantWFTrans = _workflowDbContext.TenantWorkflowTransitionConfig
        //            .Where(tWFTransConfig => tenantWFIdList.Contains(tWFTransConfig.TenantWorkflowId)).ToList();

        //    List<TenantWorkflowTransitionConfig> workflowTransition =
        //            tenantWFTrans
        //            .Where(tWFTransConfig => roleNames.Any(role => tWFTransConfig.Roles.Split(',', StringSplitOptions.None).ToList()
        //            .Contains(role))).ToList();

        //    var inboxDataAsPerRole = _MSUoW.AcknowledgementView.GetAll().Where(t => t.IsActive == true && workflowTransition
        //        .Any(transition => transition.SourceState == t.State && transition.TenantWorkflowId == t.TenantWorkflowId));

        //    if (currentUserType == (long)enUserType.Principal)
        //    {
        //        totalPendingWorkflows = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower()).Count();
        //    }
        //    else if (currentUserType == (long)enUserType.HOD)
        //    {
        //        totalPendingWorkflows = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower() && data.DepartmentCode.Trim().ToLower() == DepartmentCode.Trim().ToLower()).Count();
        //    }
        //    else
        //    {
        //        totalPendingWorkflows = inboxDataAsPerRole.Count();
        //    }

        //    return totalPendingWorkflows;
        //}
        //#endregion

        /// <summary>
        /// Author              : Dattatreya Dash
        /// Date                : 05-11-2019
        /// Description         : Sends email to student when principal updates the university registraion number application form submitted by student
        /// </summary>
        /// <param name="studentEmailAddress">Student email address</param>
        /// <param name="studentName">Student name</param>
        /// <returns>True if email is sent</returns>
        /// Author : Sitikanta Pradhan (modified)
        /// Date : 06-Nov-2019
        /// Description : Add college logo in body
        private bool SendUniversityRegistrationUpdateConfirmationEmail(string studentEmailAddress, string studentName, string serviceName)
        {
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ApplicationModified && s.IsActive == true).FirstOrDefault();
            if (templateData != null)
            {
                if (studentName == null || studentName.Trim() == string.Empty)
                {
                    studentName = "User";
                }
                string ClientApplicationURL = _IConfiguration.GetSection("ClientApplicationURL").Value;
                string applicationUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                string collegeLogo = _IConfiguration.GetSection("UULogo").Value;
                StringBuilder body = new StringBuilder(templateData.Body);
                body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                body.Replace("--userName--", studentName);
                body.Replace("--serviceName--", serviceName);
                body.Replace("--url--", ClientApplicationURL);
                _MSCommon.SendMails(studentEmailAddress, templateData.Subject, body.ToString());
            }
            return true;
        }

        /// <summary>
        /// Author              : Sonymon Mishra
        /// Date                : 22-01-2020
        /// Description         : Get the List of all the states for the role of the loggedin user
        /// </summary>	
        public List<string> GetRoleSpecificWorkflowStateList()
        {
            List<string> roleNames = GetRoleListOfCurrentUser();
            var tenantWFIdList = _workflowDbContext.TenantWorkflow
                        .Where(tenantWF => tenantWF.TenantId == GetCurrentTenantId() && tenantWF.Workflow == WorkflowName).Select(wf => wf.Id).ToList();
            var tenantWFTrans = _workflowDbContext.TenantWorkflowTransitionConfig
                    .Where(tWFTransConfig => tenantWFIdList.Contains(tWFTransConfig.TenantWorkflowId)).ToList();

            long maxTenantWorkflowId = tenantWFTrans.Max(p => p.TenantWorkflowId);

            List<TenantWorkflowTransitionConfig> workflowTransition = new List<TenantWorkflowTransitionConfig>();
            if (roleNames.Contains(((int)enRoles.COE).ToString()))
            {
                List<string> RoleIds = new List<string>();
                RoleIds.Add(Convert.ToString((long)enRoles.Student));
                RoleIds.Add(Convert.ToString((long)enRoles.Principal));
                RoleIds.Add(Convert.ToString((long)enRoles.HOD));

                //workflowTransition =
                //    tenantWFTrans
                //    .Where(tWFTransConfig => RoleIds.Any(role => !tWFTransConfig.Roles.Split(',', StringSplitOptions.None).ToList().Contains(role))
                //    && tWFTransConfig.TenantWorkflowId == maxTenantWorkflowId
                //    ).ToList();

                workflowTransition =
                    tenantWFTrans
                    .Where(tWFTransConfig => tWFTransConfig.Roles.Split(',', StringSplitOptions.None).Any(x => !RoleIds.Contains(x))
                    && tWFTransConfig.TenantWorkflowId == maxTenantWorkflowId
                    ).ToList();
            }
            else
            {

                workflowTransition =
                    tenantWFTrans
                    .Where(tWFTransConfig => roleNames.Any(role => tWFTransConfig.Roles.Split(',', StringSplitOptions.None).ToList().Contains(role))
                    && tWFTransConfig.TenantWorkflowId == maxTenantWorkflowId
                    ).ToList();

            }

            string wfInitState = _IConfiguration.GetSection("WorkflowInitialState").Value;
            List<string> listOfStates = workflowTransition.Where(s => s.SourceState != wfInitState).Select(p => p.SourceState).Distinct().ToList();
            //if (roleNames.Contains(((int)enRoles.Principal).ToString()) || roleNames.Contains(((int)enRoles.HOD).ToString()))
            //{
            //    listOfStates.Add(_IConfiguration.GetSection("RegistrationNumber:RegistrationNumberApprovedStatus").Value);
            //}
            return listOfStates;
        }

        public UniversityRegistrationDetailDto UpdateRegistrationNumberApplicationStatusToRejectState(Guid id, UniversityRegistrationDetailDto input)
        {
            #region Step b starts
            input.LoggedInUserRoles = GetRoleListOfCurrentUser();
            var universityRegistrationDetail = _MSUoW.UniversityRegistrationDetail.FindBy(univReg => univReg.Id == id).FirstOrDefault();

            string trigger = input._trigger;
            #endregion
            if (universityRegistrationDetail != null && input.Id == id)
            {
                string sourceState = universityRegistrationDetail.State;
                if (universityRegistrationDetail.TenantWorkflowId != 0)
                {
                    #region Step c
                    universityRegistrationDetail.LastStatusUpdateDate = DateTime.UtcNow;
                    #endregion

                    #region Step d
                    Dictionary<string, object> triggerParameterDictionary = new Dictionary<string, object>();
                    _WFTransactionHistroryAppService.SetEntityValue<UniversityRegistrationDetail>(universityRegistrationDetail.State, universityRegistrationDetail.TenantWorkflowId, trigger, input, ref universityRegistrationDetail, ref triggerParameterDictionary);
                    var executionStatus = input.ExecuteProcess(universityRegistrationDetail, input);
                    #endregion

                    #region Step e
                    // Update the service applied table with latest data
                    input.PossibleWorkflowTransitions = _workflowDbContext.TenantWorkflowTransitionConfig.Where(transConfig => transConfig.TenantWorkflowId == universityRegistrationDetail.TenantWorkflowId && transConfig.IsActive && transConfig.SourceState == universityRegistrationDetail.State).ToList();
                    bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, universityRegistrationDetail.State, input.ParentWorkflowId);
                    UpdateUserServiceAppliedDetail(universityRegistrationDetail, false);
                    #endregion


                    #region Step f

                    _MSUoW.UniversityRegistrationDetail.Update(universityRegistrationDetail);
                    #endregion

                    #region Step g
                    List<TenantWorkflowTransitionConfig> possibleTransition = input.PossibleWorkflowTransitions;
                    string workflowMessage = input.WorkflowMessage;
                    input = AutoMapper.Mapper.Map<UniversityRegistrationDetail, UniversityRegistrationDetailDto>(universityRegistrationDetail);
                    input.PossibleWorkflowTransitions = possibleTransition;
                    input.WorkflowMessage = workflowMessage;
                    _MSUoW.Commit();

                    // Custom developement, sending email after every state change
                    var nextApprovingRoles = _workflowDbContext.TenantWorkflowTransitionConfig
                        .Where(transConfig => transConfig.TenantWorkflowId == universityRegistrationDetail.TenantWorkflowId
                                && transConfig.IsActive && transConfig.SourceState == universityRegistrationDetail.State).Select(transConfig => transConfig.Roles).FirstOrDefault();

                    // It is assumed that a single role will be fetched, no need to separate these with 'comma'.
                    if (nextApprovingRoles != null && nextApprovingRoles != string.Empty)
                    {// Find user details of that role
                        var approvingRoleArray = nextApprovingRoles.Split(",").Select(role => Convert.ToInt64(role)).ToArray();
                        var nextApprovingUser = new Users();
                        if (Array.IndexOf(approvingRoleArray, (long)enRoles.Student) == -1)
                        { // No student role involved, send email

                            // get the user whether principal/ HOD
                            bool isAffiliated = _MSUoW.CollegeMaster.FindBy(clg => clg.IsActive && clg.LookupCode.Trim().ToLower() == universityRegistrationDetail.CollegeCode.Trim().ToLower()).Select(s => s.IsAffiliationRequired).FirstOrDefault();

                            if (Array.IndexOf(approvingRoleArray, (long)enRoles.Principal) != -1 && isAffiliated)
                            { // Find the corresponding principal of the applying student
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.CollegeCode == universityRegistrationDetail.CollegeCode).FirstOrDefault();
                            }
                            else if ((Array.IndexOf(approvingRoleArray, (long)enRoles.HOD) != -1) && !isAffiliated && universityRegistrationDetail.CourseType.Trim().ToString() == _MSCommon.GetEnumDescription(enCourseType.Pg).Trim().ToString())
                            {
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.HOD && usr.CollegeCode.Trim().ToLower() == universityRegistrationDetail.CollegeCode.Trim().ToLower()
                                && usr.DepartmentCode.Trim().ToLower() == universityRegistrationDetail.DepartmentCode.Trim().ToLower() && usr.IsActive).FirstOrDefault();
                            }
                            else
                            {
                                var nextApprovingUserRole = _MSUoW.UserRoles.FindBy(usrRole => approvingRoleArray.Contains(usrRole.RoleId) && usrRole.IsActive).FirstOrDefault();
                                if (nextApprovingUserRole != null) // Checking this
                                {
                                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                                }
                            }

                            //var nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                            if (nextApprovingUser != null)
                            {
                                string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.RegistrationService && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                                if (serviceName != null && serviceName != string.Empty && nextApprovingUser.EmailAddress != null && nextApprovingUser.EmailAddress != string.Empty)
                                    SendEmailToApprovingAuthorityOnStateChange(nextApprovingUser.EmailAddress, nextApprovingUser.Name, serviceName);
                            }
                        }
                    }
                    // Sending email ends here
                    #endregion

                    #region Step h
                    _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, id, universityRegistrationDetail.LastStatusUpdateDate, sourceState, trigger, universityRegistrationDetail.State, universityRegistrationDetail.TenantWorkflowId);
                    #endregion

                    #region Step k
                    //bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, input.State, input.ParentWorkflowId); // Moved to step e
                    if (IsLastTransition == true)
                    {
                        var applyingStudent = _MSUoW.Users.FindBy(usr => usr.Id == universityRegistrationDetail.CreatorUserId).FirstOrDefault();
                        if (applyingStudent != null)
                        {
                            //Send email to students about Registration Number Rejection
                            string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.RegistrationService && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                            if (serviceName != null && serviceName != string.Empty && applyingStudent.EmailAddress != null && applyingStudent.EmailAddress != string.Empty)
                            {
                                SendEmailToStudentOnRegistrationNumberRejection(applyingStudent.EmailAddress, applyingStudent.UserName, serviceName);
                            }

                            //Send SMS to students about Registration Number Rejection
                            if (applyingStudent.PhoneNumber != null && applyingStudent.PhoneNumber != string.Empty && applyingStudent.PhoneNumber.Length == 10)
                            {
                                string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ApplicationRejectedSMS && s.IsActive == true).FirstOrDefault();
                                OTPMessage otpObj = new OTPMessage();
                                if (templateData != null)
                                {
                                    string contentId = string.Empty;
                                    contentId = templateData.SMSContentId;
                                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                                    StringBuilder body = new StringBuilder(templateData.Body);
                                    body.Replace("--studentName--", applyingStudent.Name);
                                    body.Replace("--serviceName--", serviceName);
                                    body.Replace("--url--", applicationUrl);
                                    otpObj.Message = body.ToString();
                                    otpObj.MobileNumber = applyingStudent.PhoneNumber;
                                    _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                                }
                            }

                            ////Send email to principal/hod about Registration Number Rejection

                            bool isAffiliated = _MSUoW.CollegeMaster.FindBy(clg => clg.IsActive && clg.LookupCode.Trim().ToLower() == input.CollegeCode.Trim().ToLower()).Select(s => s.IsAffiliationRequired).FirstOrDefault();

                            Users nextApprovingUser = new Users();

                            if (isAffiliated)
                            { // Find the corresponding principal of the applying student
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.CollegeCode == input.CollegeCode).FirstOrDefault();

                            }
                            else if (!isAffiliated && input.CourseType.Trim().ToString() == _MSCommon.GetEnumDescription(enCourseType.Pg).Trim().ToString())
                            {
                                //if isAffiliated false and pg then mail should go to HOD
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.HOD && usr.CollegeCode.Trim().ToLower() == input.CollegeCode.Trim().ToLower() && usr.DepartmentCode.Trim().ToLower() == input.DepartmentCode.Trim().ToLower() && usr.IsActive).FirstOrDefault();
                            }
                            //var principalData = _MSUoW.Users.FindBy(a => a.CollegeCode == input.CollegeCode && a.UserType == (long)enUserType.Principal && a.IsActive).FirstOrDefault();
                            SendEmailToPrincipalOrHodOnRegistrationNumberRejection(nextApprovingUser, applyingStudent.UserName, serviceName);

                        }
                    }
                    #endregion
                }
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "Invalid data received." };
            }
            return input;
        }

        public PDFDownloadFile DownloadRegistrationNo(certificateDownloadDto param)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            Guid id = default(Guid);
            string userName = string.Empty;
            AcknowledgementView registrationObj = new AcknowledgementView();
            List<string> userRole = GetRoleListOfCurrentUser();
            long subjectMasterObjId = 0;
            if (userRole.Contains(Convert.ToString((long)enRoles.Principal)) || userRole.Contains(Convert.ToString((long)enRoles.HOD)) 
                || userRole.Contains(Convert.ToString((long)enRoles.AsstCoE))|| userRole.Contains(Convert.ToString((long)enRoles.COE)))
            {
                if(param==null)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU072" };
                }

                if(param.IsAutoGeneratedFromExam)
                {
                    //first check whether the service is able to download or not
                    registrationObj = _MSUoW.AcknowledgementView.FindBy(u => u.AcknowledgementNo != null && u.AcknowledgementNo.Trim().ToLower() == param.AcknowNum.Trim().ToLower()
                                && u.RegistrationNo.Trim() == param.RegdOrRollNo.Trim() && u.IsActive && u.IsRegistrationNumberGenerated && u.AutogeneratedFromExam).OrderByDescending(d => d.CreationTime)
                            .FirstOrDefault();
                }
                else
                {
                    //first check whether the service is able to download or not
                    registrationObj = _MSUoW.AcknowledgementView.FindBy(u => u.AcknowledgementNo != null && u.AcknowledgementNo.Trim().ToLower() == param.AcknowNum.Trim().ToLower()
                                && u.RegistrationNo.Trim() == param.RegdOrRollNo.Trim() && u.IsActive && u.IsRegistrationNumberGenerated).OrderByDescending(d => d.CreationTime)
                            .FirstOrDefault();
                }


                //get subjectmasterId from subjectMaster
                if (registrationObj != null)
                {
                    if (registrationObj.CourseType == enCourseType.Pg.ToString())
                    {
                        subjectMasterObjId = _MSUoW.SubjectMaster.FindBy(sub => sub.IsActive && sub.CourseTypeCode == registrationObj.CourseType
                                                                     && (sub.CourseCode == registrationObj.CourseCode || sub.CourseCode == "" || sub.CourseCode == null)
                                                                     && (sub.DepartmentCode == registrationObj.DepartmentCode || sub.DepartmentCode == "" || sub.DepartmentCode == null)
                                                                     && sub.LookupCode == registrationObj.SubjectCode)
                                                                   .FirstOrDefault().Id;
                    }
                    else
                    {
                        subjectMasterObjId = _MSUoW.SubjectMaster.FindBy(sub => sub.IsActive && sub.CourseTypeCode == registrationObj.CourseType
                                                                     && sub.StreamCode == registrationObj.StreamCode
                                                                     && sub.LookupCode == registrationObj.SubjectCode)
                                                                   .FirstOrDefault().Id;
                    }
                }

                //if subjectdmasterId exist then get the isaffiliationcompleted from collegesubjectmapper
                CollegeSubjectMapper collegeSubjectMapper = new CollegeSubjectMapper();
                if (registrationObj != null)
                {
                    collegeSubjectMapper = _MSUoW.CollegeSubjectMapper.FindBy(csm => csm.IsActive && csm.CollegeCode == registrationObj.CollegeCode
                                                                    && csm.SubjectMasterId == subjectMasterObjId)
                                                                    .FirstOrDefault();
                }


                //var isAffiliatedCompleted = _MSUoW.CollegeMaster.FindBy(clg => clg.IsActive && clg.LookupCode == GetCollegeCode()).FirstOrDefault().IsAffiliationCompleted;
                if (registrationObj == null || collegeSubjectMapper == null || !collegeSubjectMapper.IsAffiliationCompleted)
                {
                    throw new MicroServiceException { ErrorCode = "OUU127" };
                }

                id = registrationObj.Id;
                userName = registrationObj.StudentName; // To Do : Bring the actual applying student name from claim // Ask datta

                if (id != Guid.Empty)
                {
                    UserServiceApplied appliedData = _MSUoW.UserServiceApplied.FindBy(serv => serv.ServiceId == param.ServiceType && serv.InstanceId == id && serv.IsActive)
                                .OrderByDescending(d => d.CreationTime).FirstOrDefault();

                    if (appliedData != null && !appliedData.IsAvailableToDL)
                        throw new MicroServiceException() { ErrorCode = "OUU050" };
                }
                else
                    throw new MicroServiceException() { ErrorCode = "OUU051" };


                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.RegistrationCertificate && s.ServiceType == param.ServiceType && s.IsActive == true).FirstOrDefault();

                if (templateData != null)
                {
                    //EmailTemplateDto emailTemplate = AutoMapper.Mapper.Map<EmailTemplate, EmailTemplateDto>(templateData);

                    string description = templateData.Body;

                    string url = _IConfiguration.GetSection("ApplicationURL").Value;
                    string clientUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;
                    string examControllerSign = _IConfiguration.GetSection("ControllerOfExaminationSignature").Value;
                    
                    //creating the qrcodeurl for validating the qrcode
                    string qrCodeUrl = clientUrl + _IConfiguration.GetSection("QRCodeValidate:ClientReturnUrl").Value + (long)enServiceType.RegistrationService + "/" + registrationObj.RegistrationNo;

                    description = description.Replace("--qrCode--", MicroserviceCommon.GenerateQrCode(qrCodeUrl));
                    description = description.Replace("--studentName--", userName);
                    description = description.Replace("--url--", clientUrl);
                    description = description.Replace("--registrationNo--"," " + registrationObj.RegistrationNo);
                    description = description.Replace("--collegeNameString--", " " + registrationObj.CollegeString);
                    description = description.Replace("--streamString--", registrationObj.StreamString);
                    description = description.Replace("--courseTypeString--", registrationObj.CourseTypeString);
                    description = description.Replace("--subjectString--", registrationObj.SubjectString);
                    description = description.Replace("--yearOfAdmission--", registrationObj.YearOfAdmission.ToString());
                    description = description.Replace("--collegeLogo--", url + collegeLogo);
                    description = description.Replace("--examControllerSig--", url + examControllerSign);

                    flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);
                    flattenedPDFDownloadDetails.FileType = "application/octet-stream";
                    flattenedPDFDownloadDetails.FileName = "RegistrationCertificate.pdf";
                }
                else
                    throw new MicroServiceException { ErrorCode = "OUU009" };
            }
            else
            {
                throw new MicroServiceException { ErrorCode = "OUU126" };
            }
            return flattenedPDFDownloadDetails;
        }
    }
}