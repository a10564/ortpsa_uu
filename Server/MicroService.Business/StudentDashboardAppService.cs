﻿using Microservice.Common;
using Microservice.Common.Abstract;
using MicroService.Business.Abstract;
using MicroService.Core;
using MicroService.Core.WorkflowEntity;
using MicroService.Core.WorkflowTransactionHistrory;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Dto.StudentDashboard;
using MicroService.Domain.Models.Dto.UniversityService;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Entity.Nursing;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Exception;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using TimeZoneConverter;

namespace MicroService.Business
{
    public class StudentDashboardAppService : BaseService, IStudentDashboardAppService
    {
        private IConfiguration _configuration;
        private IMicroServiceUOW _MSUoW;
        private IWFTransactionHistroryAppService _WFTransactionHistroryAppService;
        private WorkflowDbContext _workflowDbContext;
        private ICommonAppService _commonAppService;
        private IStudentRegistrationNoService _studentRegistrationNoService;
        private IDuplicateRegistrationService _duplicateRegistrationService;
        private IMicroserviceCommon _MSCommon;
        private IExaminationFormFillUpService _examinationFormFillUpService;
        private INursingFormFillUpService _nursingFormFillUpService;
        private IStudentMigrationService _studentMigrationService;
        private IStudentCLCService _studentCLCService;

        public StudentDashboardAppService(IMicroServiceUOW MSUoW, IRedisDataHandlerService redisData, IConfiguration configuration, IWFTransactionHistroryAppService WFTransactionHistroryAppService
         , IStudentRegistrationNoService studentRegistrationNoService, IDuplicateRegistrationService duplicateRegistrationService, IMicroserviceCommon MSCommon, IExaminationFormFillUpService examinationFormFillUpService,
            IStudentMigrationService studentMigrationService, ICommonAppService commonAppService, IStudentCLCService studentCLCService,
            INursingFormFillUpService nursingFormFillUpService) : base(redisData)
        {
            _MSUoW = MSUoW;
            _configuration = configuration;
            _WFTransactionHistroryAppService = WFTransactionHistroryAppService;
            _workflowDbContext = new WorkflowDbContext();

            _studentRegistrationNoService = studentRegistrationNoService;
            _duplicateRegistrationService = duplicateRegistrationService;
            _examinationFormFillUpService = examinationFormFillUpService;
            _studentMigrationService = studentMigrationService;
            _MSCommon = MSCommon;
            _commonAppService = commonAppService;
            _studentCLCService = studentCLCService;
            _nursingFormFillUpService = nursingFormFillUpService;
        }

        /// <summary>
        /// Modified By     :   Anurag Digal
        /// Date            :   31-01-20
        /// Description     :   IsAvailable to download will be true in case of ismultipleapply is true and appliedcurrentservicecount >0 then it will get the first record's isavailabetodl column
        /// </summary>
        /// <returns></returns>
        public List<AppliedServiceDto> GetAllAppliedService()
        {
            List<AppliedServiceDto> appliedService = new List<AppliedServiceDto>();
            List<ServiceMaster> activeServiceMaster = new List<ServiceMaster>();
            List<ServiceMaster> activeServiceMasterForCollege = new List<ServiceMaster>();
            List<ServiceMaster> activeServiceMasterForRole = new List<ServiceMaster>();

            //Get services from roleservicemapper as per logged in user roles.
            activeServiceMasterForRole = _MSUoW.ServiceMasterRepository.GetServiceMaster(GetUserType());

            if(GetUserType()==(long)enRoles.Principal || GetUserType()== (long)enRoles.Student)
            {
                // create table of college service mapper
                activeServiceMasterForCollege =  _MSUoW.ServiceMasterRepository.GetServiceMaster(GetCollegeCode());

                activeServiceMasterForCollege.ForEach(col =>
                {
                    activeServiceMasterForRole.ForEach(role =>
                    {
                        if(role.Id==col.Id)
                        {
                            activeServiceMaster.Add(col);
                        }
                    });
                });
            }
            else
            {
                activeServiceMaster.AddRange(activeServiceMasterForRole);
            }

            var appliedServiceForCurrentUser = _MSUoW.UserServiceApplied.FindBy(a => a.UserId == GetCurrentUserId() && a.IsActive).OrderByDescending(userSrvc => userSrvc.CreationTime).ToList();

            activeServiceMaster.ForEach(activeSrvcMstr =>
            {
                var appliedCurrentService = appliedServiceForCurrentUser.Where(appldSrvc => appldSrvc.ServiceId == activeSrvcMstr.Id).ToList();
                bool IsAppliedStatus = appliedCurrentService.Count() > 0 ? true : false;
                //Previsiously isMultipleApply being checked in the query .We  have removed it as we are checking that while applying in POST Method.

                bool IsAvailableToDlStatus = appliedCurrentService.Count() > 0 ? appliedCurrentService.FirstOrDefault().IsAvailableToDL :
                                        appliedCurrentService.Count() == 0 ? false :
                                             appliedCurrentService.Count() > 0 ? appliedCurrentService.FirstOrDefault().IsAvailableToDL :
                                                false;
                Guid instanceId = appliedCurrentService.Count() > 0 ? appliedCurrentService.FirstOrDefault().InstanceId : default(Guid);

                appliedService.Add(new AppliedServiceDto()
                {
                    Name = activeSrvcMstr.ServiceName,
                    IsApplied = IsAppliedStatus,
                    IsAvailableToDl = IsAvailableToDlStatus,
                    DocType = activeSrvcMstr.Id,
                    MobileActionUrl = activeSrvcMstr.MobileActionUrl,
                    WebActionURL = activeSrvcMstr.WebActionURL,
                    InboxActionURL = activeSrvcMstr.InboxActionURL,
                    IsEnabled = activeSrvcMstr.IsMultipleApply == false
                                && appliedCurrentService.Count() >= 1
                                && appliedCurrentService.FirstOrDefault().LastStatus.Trim().ToLower() != _configuration.GetSection("FormRejectedState").Value.Trim().ToLower()
                                ? false : true,
                    LastStatus = GetLatestStatus(activeSrvcMstr.Id, IsAppliedStatus, IsAvailableToDlStatus, appliedCurrentService),
                    AppliedFormCount = PendingWorkCount(activeSrvcMstr.Id),
                    InstanceId = instanceId
                });
            });

            return appliedService;            
        }

        private string GetLatestStatus(long serviceId, bool IsAppliedStatus, bool IsAvailableToDlStatus,List<UserServiceApplied> appliedCurrentService)
        {
            string status = string.Empty; 
            switch(serviceId)
            {
                case (long)enServiceType.BCABBAExamApplicationForm:
                    status = (IsAppliedStatus == true && IsAvailableToDlStatus == true) ? "Ready to Download Admit Card" :
                                    (IsAppliedStatus == true && IsAvailableToDlStatus == false) ? appliedCurrentService.FirstOrDefault().LastStatus :
                                    (IsAppliedStatus == false && IsAvailableToDlStatus == false) ? "Yet to apply" : "Yet to apply";
                    break;
                case (long)enServiceType.NursingApplicationForm:
                    status = (IsAppliedStatus == true && IsAvailableToDlStatus == true) ? "Ready to Download Admit Card" :
                                    (IsAppliedStatus == true && IsAvailableToDlStatus == false) ? appliedCurrentService.FirstOrDefault().LastStatus :
                                    (IsAppliedStatus == false && IsAvailableToDlStatus == false) ? "Yet to apply" : "Yet to apply";
                    break;
                default:
                    status = (IsAppliedStatus == true && IsAvailableToDlStatus == true) ? "Document is ready. Please Collect it from your college Principal or HOD." :
                                    (IsAppliedStatus == true && IsAvailableToDlStatus == false) ? appliedCurrentService.FirstOrDefault().LastStatus :
                                    (IsAppliedStatus == false && IsAvailableToDlStatus == false) ? "Yet to apply" : "Yet to apply";
                    break;
            }

            return status;
        }

        private long PendingWorkCount(long serviceId)
        {
            long pendingCount = 0;
            switch (serviceId)
            {
                case (long)enServiceType.MigrationService:
                    //pendingCount = _studentMigrationService.GetAllMigrationStudentData().Count(); 
                    pendingCount = _studentMigrationService.GetRoleBasedActiveRegistrationInboxCountData().Count();
                    break;
                case (long)enServiceType.RegistrationService:
                    pendingCount = _studentRegistrationNoService.GetRoleBasedActiveRegistrationInboxCountData().Count();
                    break;
                case (long)enServiceType.DuplicateRegistrationCertificate:
                    pendingCount = _duplicateRegistrationService.GetRoleBasedActiveDuplicateRegistrationInboxData().Count();
                    break;
                case (long)enServiceType.BCABBAExamApplicationForm:
                    pendingCount = _examinationFormFillUpService.GetRoleBasedActiveExaminationInboxData(true).Count();
                    break;
                case (long)enServiceType.CollegeLeavingCertificate:
                    pendingCount = _studentCLCService.GetAllCLCViewData().Count();
                    break;
                case (long)enServiceType.NursingApplicationForm:
                    pendingCount = _nursingFormFillUpService.GetRoleBasedActiveNursingInboxData(true).Count();
                    break;
                default:
                    pendingCount = 0;
                    break;
            }
            return pendingCount;
        }

        /// <summary>
        /// Anurag Digal
        /// 03-10-19
        /// This methode is used to download certificate in pdf format
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public PDFDownloadFile DownloadService(DownloadParamDto param)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            if (param != null)
            {
                if (param.DocType != 0)
                {
                    switch (param.DocType)
                    {
                        case (long)enServiceType.MigrationService:
                            flattenedPDFDownloadDetails = DownloadMigrationService(param);
                            break;
                        case (long)enServiceType.RegistrationService:
                            flattenedPDFDownloadDetails = DownloadRegistrationCertificate(param);
                            break;
                        case (long)enServiceType.ProvisionalCertificate:
                            break;
                        case (long)enServiceType.CollegeLeavingCertificate:
                            break;
                        case (long)enServiceType.DuplicateRegistrationCertificate:
                            break;
                        case (long)enServiceType.VerificationofAdditionofMarks:
                            break;
                        case (long)enServiceType.IssueofPaperwiseMarks:
                            break;
                        case (long)enServiceType.BCABBAExamApplicationForm:
                            flattenedPDFDownloadDetails = DownloadAdmitCard(param);
                            break;
                        case (long)enServiceType.NursingApplicationForm:
                            flattenedPDFDownloadDetails = DownloadNursingAdmitCard(param);
                            break;
                    }
                }
            }

            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU010" };
            }
            return flattenedPDFDownloadDetails;
        }
        public PDFDownloadFile DownloadRegistrationCertificate(DownloadParamDto param)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            Guid id = default(Guid);
                string userName = string.Empty;
                //first check whether the service is able to download or not
                var registrationObj = _MSUoW.AcknowledgementView.FindBy(u => u.AcknowledgementNo != null && u.AcknowledgementNo.Trim().ToLower() == param.TransanctionId.Trim().ToLower()
                                                                && u.IsActive && u.CreatorUserId == GetCurrentUserId())
                                                                .FirstOrDefault();
            if (registrationObj != null)
            {
                id = registrationObj.Id;
                userName = registrationObj.StudentName; // To Do : Bring the actual applying student name from claim // Ask datta

                if (id != Guid.Empty)
                {
                    UserServiceApplied appliedData = _MSUoW.UserServiceApplied.FindBy(serv => serv.ServiceId == param.DocType && serv.InstanceId == id && serv.IsActive)
                                .OrderByDescending(d => d.CreationTime).FirstOrDefault();

                    if (appliedData != null && !appliedData.IsAvailableToDL)
                        throw new MicroServiceException() { ErrorCode = "OUU050" };
                }
                else
                    throw new MicroServiceException() { ErrorCode = "OUU051" };


                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.RegistrationCertificate && s.ServiceType == param.DocType && s.IsActive == true).FirstOrDefault();


                if (templateData != null)
                {
                    //EmailTemplateDto emailTemplate = AutoMapper.Mapper.Map<EmailTemplate, EmailTemplateDto>(templateData);

                    string description = templateData.Body;

                    string url = _configuration.GetSection("ApplicationURL").Value;
                    string clientUrl = _configuration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _configuration.GetSection("UULogo").Value;
                    string examControllerSign = _configuration.GetSection("ControllerOfExaminationSignature").Value;

                    //creating the qrcodeurl for validating the qrcode
                    string qrCodeUrl = clientUrl + _configuration.GetSection("QRCodeValidate:ClientReturnUrl").Value + (long)enServiceType.RegistrationService + "/" + registrationObj.RegistrationNo;

                    description = description.Replace("--barCode--", MicroserviceCommon.GenerateQrCode(qrCodeUrl));
                    description = description.Replace("--studentName--", userName);
                    description = description.Replace("--url--", clientUrl);
                    description = description.Replace("--registrationNo--", registrationObj.RegistrationNo);
                    description = description.Replace("--collegeNameString--", registrationObj.CollegeString);
                    description = description.Replace("--streamString--", registrationObj.StreamString);
                    description = description.Replace("--courseTypeString--", registrationObj.CourseTypeString);
                    description = description.Replace("--subjectString--", registrationObj.SubjectString);
                    description = description.Replace("--yearOfAdmission--", registrationObj.YearOfAdmission.ToString());
                    description = description.Replace("--collegeLogo--", url + collegeLogo);
                    description = description.Replace("--examControllerSig--", url + examControllerSign);

                    flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);
                    flattenedPDFDownloadDetails.FileType = "application/octet-stream";
                    flattenedPDFDownloadDetails.FileName = "RegistrationCertificate.pdf";
                }
                else
                    throw new MicroServiceException { ErrorCode = "OUU009" };
            }
            else
            {
                throw new MicroServiceException { ErrorCode = "OUU074", SubstitutionParams = new object[1] { param.TransanctionId } };
            }
            
            return flattenedPDFDownloadDetails;
        }

        /// <summary>
        /// Anurag Digal
        /// 17-10-19
        /// This methode is used to fetch service status dynamically to display the state of the application
        /// </summary>
        /// <param name="docType"></param>
        /// <returns></returns>
        public List<ServiceStatusDto> GetServiceStatus(long docType)
        {
            List<ServiceStatusDto> serviceStatusList = new List<ServiceStatusDto>();


            // First get the latest active user applied record from userserviceapplied table by doctype
            UserServiceApplied userServiceAppliedObj = _MSUoW.UserServiceApplied.FindBy(doc => doc.ServiceId == docType && doc.IsActive
                                                        && doc.UserId == GetCurrentUserId()).OrderByDescending(dd => dd.CreationTime)
                                                        .FirstOrDefault();

            

            // Get the appsetting value for creating object according to the stateStatusType with ascending order 
            //var appSettingWorkflowStateNames = _configuration.GetSection("RegistrationNumber:SourceStateType").Value.Split(',');



            if (userServiceAppliedObj != null)
            {
                //check which service type is requird
                switch (docType)
                {
                    case (long)enServiceType.MigrationService:
                        serviceStatusList = GetMigrationServiceStatus(userServiceAppliedObj);
                        break;
                    case (long)enServiceType.RegistrationService:
                        serviceStatusList = GetRegistrationServiceStatus(userServiceAppliedObj);
                        break;
                    case (long)enServiceType.ProvisionalCertificate:
                        break;

                    case (long)enServiceType.CollegeLeavingCertificate:
                        break;
                    case (long)enServiceType.DuplicateRegistrationCertificate:

                        break;
                    case (long)enServiceType.VerificationofAdditionofMarks:
                        break;
                    case (long)enServiceType.IssueofPaperwiseMarks:
                        break;
                    case (long)enServiceType.BCABBAExamApplicationForm:
                        serviceStatusList = GetExaminationServiceStatus(userServiceAppliedObj);
                        break;
                    case (long)enServiceType.NursingApplicationForm:
                        serviceStatusList = GetNursingServiceStatus(userServiceAppliedObj);
                        break;
                }
            }
            else
                throw new MicroServiceException() { ErrorCode = "OUU002" };

            return serviceStatusList;
        }

        #region Download Acknowledgement Service
        /// <summary>
        /// Author=Sitikanta Pradhan
        /// Date = 11/Oct/2019
        /// Description : check the object property and send to the respective method 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public PDFDownloadFile DownloadAcknowledgementService(DownloadParamDto param)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            if (param.DocType != 0)
            {
                switch (param.DocType)
                {
                    case (long)enServiceType.MigrationService:
                        flattenedPDFDownloadDetails = MigrationAcknowledgementPDF(param);
                        break;
                    case (long)enServiceType.RegistrationService:
                        flattenedPDFDownloadDetails = DownloadAcknowledgementPDF(param);
                        break;
                    case (long)enServiceType.ProvisionalCertificate:
                        break;
                    case (long)enServiceType.CollegeLeavingCertificate:
                        flattenedPDFDownloadDetails = DownloadAcknowledgementForCLC(param);
                        break;
                    case (long)enServiceType.DuplicateRegistrationCertificate:
                        flattenedPDFDownloadDetails = DownloadAcknowledgementForDuplicateCertificatePDF(param);
                        break;
                    case (long)enServiceType.VerificationofAdditionofMarks:
                        break;
                    case (long)enServiceType.IssueofPaperwiseMarks:
                        break;
                    case (long)enServiceType.BCABBAExamApplicationForm:
                        flattenedPDFDownloadDetails = DownloadAcknowledgementForExamFormFillUp(param);
                        break;
                    case (long)enServiceType.NursingApplicationForm:
                        flattenedPDFDownloadDetails = DownloadAcknowledgementForNursingFormFillUp(param);
                        break;
                }
            }
            return flattenedPDFDownloadDetails;
        }
        #endregion

        #region Registration Acknowledgement pdf
        /// <summary>
        /// Author=Sitikanta Pradhan
        /// Date = 11/Oct/2019
        /// Registration Acknowledgement pdf
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public PDFDownloadFile DownloadAcknowledgementPDF(DownloadParamDto param)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.RegistrationNoAcknowledgement && s.ServiceType == param.DocType && s.IsActive == true).FirstOrDefault();
            if (templateData != null)
            {
                string description = templateData.Body;

                //UniversityRegistrationDetail studentDetails = _MSUoW.UniversityRegistrationDetail.FindBy(usr => usr.CreatorUserId == GetCurrentUserId() && usr.IsActive == true).OrderByDescending(a => a.CreationTime).FirstOrDefault();
                AcknowledgementView acknowledgementViewDetails = _MSUoW.AcknowledgementView.FindBy(usr => usr.CreatorUserId == GetCurrentUserId() && usr.IsActive).OrderByDescending(a => a.CreationTime).FirstOrDefault();
                List<TransactionLog> transactionDetailsList = _MSUoW.TransactionLog.FindBy(trs => trs.RequestId == Convert.ToString(acknowledgementViewDetails.Id) && trs.IsActive).ToList();
                if (acknowledgementViewDetails != null)
                {
                    TransactionLog successData = transactionDetailsList.Where(data => data.IsActive && data.Status != _MSCommon.GetEnumDescription(enTransaction.Cancelled)).OrderByDescending(data => data.CreationTime).FirstOrDefault();
                    ServiceMaster serviceName = _MSUoW.ServiceMaster.FindBy(name => name.IsActive && name.Id == param.DocType).FirstOrDefault();
                    string applicationURL = _configuration.GetSection("ApplicationURL").Value;
                    string collegeLogo = _configuration.GetSection("UULogo").Value;
                    if (serviceName != null)
                    {
                        description = description.Replace("--serviceName--", serviceName.ServiceName);
                    }
                    else
                    {
                        description = description.Replace("--serviceName--", "-NA-");
                    }
                    description = description.Replace("--collegeLogo--", applicationURL + collegeLogo);
                    description = description.Replace("--studentName--", acknowledgementViewDetails.StudentName);
                    description = description.Replace("--fatherName--", acknowledgementViewDetails.FatherName);
                    description = description.Replace("--dob--", acknowledgementViewDetails.DateOfBirth.ToString("dd/MM/yyyy"));
                    description = description.Replace("--admissionYear--", acknowledgementViewDetails.YearOfAdmission.ToString());
                    description = description.Replace("--lastExamPassed--", acknowledgementViewDetails.CourseTypeString);
                    description = description.Replace("--subject--", acknowledgementViewDetails.SubjectString);
                    description = description.Replace("--stream--", acknowledgementViewDetails.StreamString);
                    description = description.Replace("--department--", acknowledgementViewDetails.DepartmentString);
                    description = description.Replace("--firstAdmitted--", acknowledgementViewDetails.CollegeString);
                    description = description.Replace("--address--", acknowledgementViewDetails.Address);
                    description = description.Replace("--city--", acknowledgementViewDetails.CityString);
                    description = description.Replace("--state--", acknowledgementViewDetails.StateString);
                    description = description.Replace("--country--", acknowledgementViewDetails.CountryString);
                    description = description.Replace("--pincode--", acknowledgementViewDetails.ZipCode);
                    description = description.Replace("--docName--", acknowledgementViewDetails.FileName);
                    description = description.Replace("--status--", "Uploaded");
                    description = description.Replace("--courseCode--", acknowledgementViewDetails.CourseString);
                    description = description.Replace("--acknowlegementNo--", acknowledgementViewDetails.AcknowledgementNo);
                    if(acknowledgementViewDetails.CreationTime != null)
                    {
                        DateTime utcdate = DateTime.ParseExact(acknowledgementViewDetails.CreationTime.ToString("MM-dd-yyyy HH:mm:ss"), "MM-dd-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        var istdate = TimeZoneInfo.ConvertTimeFromUtc(utcdate, TZConvert.GetTimeZoneInfo(_configuration.GetSection("TimeZone").Value.ToString()));
                        acknowledgementViewDetails.CreationTime = istdate;
                        description = description.Replace("--creationTime--", acknowledgementViewDetails.CreationTime.ToString());
                    }
                    else
                    {
                        description = description.Replace("--creationTime--", "-NA-");
                    }
                    if (successData != null)
                    {
                        description = description.Replace("--payMode--", successData.CardType);
                        description = description.Replace("--amount--", successData.Amount.ToString());
                        description = description.Replace("--bankStatus--", successData.Status);
                        description = description.Replace("--transactionRefNo--", successData.UniqueRefNo);
                    }
                    else
                    {
                        description = description.Replace("--payMode--", "-NA-");
                        description = description.Replace("--amount--", "-NA-");
                        description = description.Replace("--bankStatus--", "-NA-");
                        description = description.Replace("--transactionRefNo--", "-NA-");
                    }
                     //flattenedPDFDownloadDetails.PdfData = HtmlToPdfConverter.ExportToPDF(description, 600, 1000);
                    flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);
					flattenedPDFDownloadDetails.FileType = "application/octet-stream";
                    flattenedPDFDownloadDetails.FileName = "StudentRegistrationAcknowledgement.pdf";

                    return flattenedPDFDownloadDetails;
                }
                else throw new MicroServiceException() { ErrorCode = "OUU002" };
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU011" };
            }
        }
        #endregion

        public PDFDownloadFile DownloadAcknowledgementForDuplicateCertificatePDF(DownloadParamDto param)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.DuplicateRegistrationAcknowledgement && s.ServiceType == param.DocType && s.IsActive == true).FirstOrDefault();
            if (templateData != null)
            {
                string description = templateData.Body;

                //DuplicateRegistrationDetail studentDetails = _MSUoW.DuplicateRegistrationDetail.FindBy(usr => usr.CreatorUserId == GetCurrentUserId() && usr.IsActive == true).OrderByDescending(a => a.CreationTime).FirstOrDefault();
                DuplicateRegistrationView acknowledgementViewDetails = _MSUoW.DuplicateRegistrationView.FindBy(usr => usr.CreatorUserId == GetCurrentUserId() && usr.IsActive).OrderByDescending(a => a.CreationTime).FirstOrDefault();
                if (acknowledgementViewDetails != null)
                {
                    string bankName = "-NA-";
                    string amount = "-NA-";
                    string bankStatus = "-NA-";
                    string transactionRefNo = "-NA-";
                    string applicationURL = _configuration.GetSection("ApplicationURL").Value;
                    string collegeLogo = _configuration.GetSection("UULogo").Value;

                    description = description.Replace("--collegeLogo--", applicationURL + collegeLogo);
                    description = description.Replace("--studentName--", acknowledgementViewDetails.StudentName);
                    description = description.Replace("--fatherName--", acknowledgementViewDetails.FatherName);
                    description = description.Replace("--dob--", acknowledgementViewDetails.DateOfBirth.ToString("dd/MM/yyyy"));
                    description = description.Replace("--admissionYear--", acknowledgementViewDetails.YearOfAdmission.ToString());

                    description = description.Replace("--lastExamPassed--", acknowledgementViewDetails.CourseTypeString);
                    description = description.Replace("--subject--", acknowledgementViewDetails.SubjectString);
                    description = description.Replace("--stream--", acknowledgementViewDetails.StreamString);
                    description = description.Replace("--department--", acknowledgementViewDetails.DepartmentString);
                    description = description.Replace("--firstAdmitted--", acknowledgementViewDetails.CollegeString);

                    description = description.Replace("--address--", acknowledgementViewDetails.Address);
                    description = description.Replace("--city--", acknowledgementViewDetails.CityString);
                    description = description.Replace("--state--", acknowledgementViewDetails.StateString);
                    description = description.Replace("--country--", acknowledgementViewDetails.CountryString);
                    description = description.Replace("--pincode--", acknowledgementViewDetails.ZipCode);
                    description = description.Replace("--bankName--", bankName);
                    description = description.Replace("--amount--", amount);
                    description = description.Replace("--bankStatus--", bankStatus);
                    description = description.Replace("--transactionRefNo--", transactionRefNo);
                    description = description.Replace("--courseCode--", acknowledgementViewDetails.CourseString);


                    //flattenedPDFDownloadDetails.PdfData = HtmlToPdfConverter.ExportToPDF(description, 600, 1000);
					flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);
					flattenedPDFDownloadDetails.FileName = "DuplicateRegistrationAcknowledgement.pdf";

                    return flattenedPDFDownloadDetails;
                }
                else throw new MicroServiceException() { ErrorCode = "OUU002" };
            }
            else
                {
                    throw new MicroServiceException() { ErrorCode = "OUU011" };
                }
        }

        public PDFDownloadFile DownloadAcknowledgementForExamFormFillUp(DownloadParamDto param)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ExaminationFormFillUp && s.ServiceType == param.DocType && s.IsActive == true).FirstOrDefault();
            if (templateData != null)
            {
                string description = templateData.Body;
                long currentUserId = GetCurrentUserId();
                ExaminationView acknowledgementViewDetails = _MSUoW.ExaminationView.FindBy(usr => usr.CreatorUserId == currentUserId && usr.IsActive).OrderByDescending(a => a.CreationTime).FirstOrDefault();
                IQueryable<ExaminationPaperDetail> examPaperIds = _MSUoW.ExaminationPaperDetail.FindBy(e => e.IsActive && e.ExaminationFormFillUpDetailsId == acknowledgementViewDetails.Id);
                List<ExaminationPaperMaster> examPapers = _MSUoW.ExaminationPaperMaster.FindBy(a => examPaperIds.Select(b => b.ExaminationPaperId).Contains(a.Id)).ToList();
                List<TransactionLog> transactionDetailsList = _MSUoW.TransactionLog.FindBy(trs => trs.RequestId == Convert.ToString(acknowledgementViewDetails.Id) && trs.IsActive).ToList();

                if (acknowledgementViewDetails != null) // && acknowledgementViewDetails.AcknowledgementNo != null && !string.IsNullOrWhiteSpace(acknowledgementViewDetails.AcknowledgementNo)
                {
                    TransactionLog successData = transactionDetailsList.Where(data => data.IsActive && data.Status != _MSCommon.GetEnumDescription(enTransaction.Cancelled)).OrderByDescending(data => data.CreationTime).FirstOrDefault();
                    ServiceMaster serviceName = _MSUoW.ServiceMaster.FindBy(name => name.Id == param.DocType && name.IsActive).FirstOrDefault();
                    if (successData != null)
                    {
                        string status = "Uploaded";
                        string applicationURL = _configuration.GetSection("ApplicationURL").Value;
                        string collegeLogo = _configuration.GetSection("UULogo").Value;
                        if (serviceName != null)
                        {
                            description = description.Replace("--serviceName--", serviceName.ServiceName);
                        }
                        else
                        {
                            description = description.Replace("--serviceName--", "-NA-");
                        }
                        description = description.Replace("--collegeLogo--", applicationURL + collegeLogo);
                        description = description.Replace("--acknowledgementNo--", acknowledgementViewDetails.AcknowledgementNo);
                        if (acknowledgementViewDetails.CreationTime != null)
                        {
                            DateTime utcdate = DateTime.ParseExact(acknowledgementViewDetails.CreationTime.ToString("MM-dd-yyyy HH:mm:ss"), "MM-dd-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                            var istdate = TimeZoneInfo.ConvertTimeFromUtc(utcdate, TZConvert.GetTimeZoneInfo(_configuration.GetSection("TimeZone").Value.ToString()));
                            acknowledgementViewDetails.CreationTime = istdate;
                            description = description.Replace("--creationTime--", acknowledgementViewDetails.CreationTime.ToString());
                        }
                        else
                        {
                            description = description.Replace("--creationTime--", "-NA-");
                        }
                        description = description.Replace("--courseType--", acknowledgementViewDetails.CourseTypeString);
                        if (acknowledgementViewDetails.RegistrationNumber != null)
                        {
                            description = description.Replace("--registratioNumber--", acknowledgementViewDetails.RegistrationNumber);
                        }
                        else
                        {
                            description = description.Replace("--registratioNumber--", "-NA-");
                        }

                        description = description.Replace("--studentName--", acknowledgementViewDetails.StudentName);
                        description = description.Replace("--fatherName--", acknowledgementViewDetails.FatherName);
                        description = description.Replace("--dob--", acknowledgementViewDetails.DateOfBirth.ToString("dd/MM/yyyy"));
                        description = description.Replace("--gender--", acknowledgementViewDetails.GenderString);
                        description = description.Replace("--nationality--", acknowledgementViewDetails.NationString);
                        description = description.Replace("--caste--", acknowledgementViewDetails.CasteString);
                        description = description.Replace("--guardianName--", acknowledgementViewDetails.GuardianName);
                        description = description.Replace("--disability--", acknowledgementViewDetails.IsSpeciallyAbled == true ? "Yes" : "No");

                        //document section
                        description = description.Replace("--docName--", acknowledgementViewDetails.FileName);
                        description = description.Replace("--status--", status);

                        //address section 
                        description = description.Replace("--address--", acknowledgementViewDetails.Address);
                        description = description.Replace("--city--", acknowledgementViewDetails.CityString);
                        description = description.Replace("--state--", acknowledgementViewDetails.StateString);
                        description = description.Replace("--country--", acknowledgementViewDetails.CountryString);
                        description = description.Replace("--pincode--", acknowledgementViewDetails.ZipCode);

                        if (acknowledgementViewDetails.IsSameAsPresent)
                        {
                            description = description.Replace("--paddress--", acknowledgementViewDetails.Address);
                            description = description.Replace("--pcity--", acknowledgementViewDetails.CityString);
                            description = description.Replace("--pstate--", acknowledgementViewDetails.StateString);
                            description = description.Replace("--pcountry--", acknowledgementViewDetails.CountryString);
                            description = description.Replace("--ppincode--", acknowledgementViewDetails.ZipCode);
                        }
                        else
                        {
                            ServiceStudentAddress permanentAddress = _MSUoW.ExaminationFormFillUpRepository.GetStudentAddress(_MSCommon.GetEnumDescription(enAddressType.Permanent), acknowledgementViewDetails.Id).FirstOrDefault();
                            if (permanentAddress!=null)
                            {
                                description = description.Replace("--paddress--", permanentAddress.Address);
                                description = description.Replace("--pcity--", permanentAddress.City);
                                description = description.Replace("--pstate--", permanentAddress.State);
                                description = description.Replace("--pcountry--", permanentAddress.Country);
                                description = description.Replace("--ppincode--", permanentAddress.Zip);
                            }
                            else
                            {
                                description = description.Replace("--paddress--", "-NA-");
                                description = description.Replace("--pcity--", "-NA-");
                                description = description.Replace("--pstate--", "-NA-");
                                description = description.Replace("--pcountry--", "-NA-");
                                description = description.Replace("--ppincode--", "-NA-");
                            }
                        }

                        //educational qualificateion section
                        description = description.Replace("--matriculationExam--", acknowledgementViewDetails.YearOfMatriculation.ToString());
                        description = description.Replace("--previousExamPassed--", acknowledgementViewDetails.YearOfPreviousExamPassed.ToString());
                        description = description.Replace("--previousAcademicSubject--", acknowledgementViewDetails.PreviousAcademicSubject);

                        //exam information
                        description = description.Replace("--collegeName--", acknowledgementViewDetails.CollegeString);
                        description = description.Replace("--subject--", acknowledgementViewDetails.SubjectString);
                        description = description.Replace("--department--", acknowledgementViewDetails.DepartmentString);
                        description = description.Replace("--academicSession--", acknowledgementViewDetails.AcademicSession);
                        description = description.Replace("--stream--", acknowledgementViewDetails.StreamString);
                        description = description.Replace("--courseCode--", acknowledgementViewDetails.CourseString);
                        description = description.Replace("--semester--", acknowledgementViewDetails.SemesterString);

                        //transaction information
                        description = description.Replace("--payMode--", successData.CardType);
                        description = description.Replace("--amount--", successData.Amount.ToString());
                        description = description.Replace("--bankStatus--", successData.Status);
                        description = description.Replace("--transactionRefNo--", successData.UniqueRefNo);

                        //paper details
                        if (examPapers != null && examPapers.Count() > 0)
                        {
                            var stringtoappend = "";

                            examPapers.ForEach(sub =>
                            {

                                if (sub.IsParctical == true)
                                {
                                    stringtoappend = stringtoappend + " " + sub.PaperName + " - ( T & P )" + " ,";
                                }
                                else
                                {
                                    stringtoappend = stringtoappend + " " + sub.PaperName + " - ( T )" + " ,";
                                }

                            });
                            stringtoappend = stringtoappend.Remove(stringtoappend.Length - 1);

                            description = description.Replace("--papers--", stringtoappend);
                        }
                        else
                        {
                            description = description.Replace("--papers--", "-NA-");
                        }


						//flattenedPDFDownloadDetails.PdfData = HtmlToPdfConverter.ExportToPDF(description, 600, 1000);
						flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);



						flattenedPDFDownloadDetails.FileType = "application/octet-stream";
                        flattenedPDFDownloadDetails.FileName = "ExamFormFillUpAcknowledgement.pdf";

                        return flattenedPDFDownloadDetails;
                    }
                    else
                    {
                        string status = "Uploaded";
                        string bankName = "-NA-";
                        string amount = "-NA-";
                        string bankStatus = "Payment Pending";
                        string transactionRefNo = "-NA-";
                        string applicationURL = _configuration.GetSection("ApplicationURL").Value;
                        string collegeLogo = _configuration.GetSection("UULogo").Value;

                        if (serviceName != null)
                        {
                            description = description.Replace("--serviceName--", serviceName.ServiceName);
                        }
                        else
                        {
                            description = description.Replace("--serviceName--", "-NA-");
                        }
                        description = description.Replace("--collegeLogo--", applicationURL + collegeLogo);
                        description = description.Replace("--acknowledgementNo--", acknowledgementViewDetails.AcknowledgementNo);
                        if (acknowledgementViewDetails.CreationTime != null)
                        {
                            DateTime utcdate = DateTime.ParseExact(acknowledgementViewDetails.CreationTime.ToString("MM-dd-yyyy HH:mm:ss"), "MM-dd-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                            var istdate = TimeZoneInfo.ConvertTimeFromUtc(utcdate, TZConvert.GetTimeZoneInfo(_configuration.GetSection("TimeZone").Value.ToString()));
                            acknowledgementViewDetails.CreationTime = istdate;
                            description = description.Replace("--creationTime--", acknowledgementViewDetails.CreationTime.ToString());
                        }
                        else
                        {
                            description = description.Replace("--creationTime--", "-NA-");
                        }
                        description = description.Replace("--courseType--", acknowledgementViewDetails.CourseTypeString);
                        if (acknowledgementViewDetails.RegistrationNumber != null)
                        {
                            description = description.Replace("--registratioNumber--", acknowledgementViewDetails.RegistrationNumber);
                        }
                        else
                        {
                            description = description.Replace("--registratioNumber--", "-NA-");
                        }

                        description = description.Replace("--studentName--", acknowledgementViewDetails.StudentName);
                        description = description.Replace("--fatherName--", acknowledgementViewDetails.FatherName);
                        description = description.Replace("--dob--", acknowledgementViewDetails.DateOfBirth.ToString("dd/MM/yyyy"));
                        description = description.Replace("--gender--", acknowledgementViewDetails.GenderString);
                        description = description.Replace("--nationality--", acknowledgementViewDetails.NationString);
                        description = description.Replace("--caste--", acknowledgementViewDetails.CasteString);
                        description = description.Replace("--guardianName--", acknowledgementViewDetails.GuardianName);
                        description = description.Replace("--disability--", acknowledgementViewDetails.IsSpeciallyAbled == true ? "Yes" : "No");

                        //document section
                        description = description.Replace("--docName--", acknowledgementViewDetails.FileName);
                        description = description.Replace("--status--", status);

                        //address section 
                        description = description.Replace("--address--", acknowledgementViewDetails.Address);
                        description = description.Replace("--city--", acknowledgementViewDetails.CityString);
                        description = description.Replace("--state--", acknowledgementViewDetails.StateString);
                        description = description.Replace("--country--", acknowledgementViewDetails.CountryString);
                        description = description.Replace("--pincode--", acknowledgementViewDetails.ZipCode);

                        if (acknowledgementViewDetails.IsSameAsPresent)
                        {
                            description = description.Replace("--paddress--", acknowledgementViewDetails.Address);
                            description = description.Replace("--pcity--", acknowledgementViewDetails.CityString);
                            description = description.Replace("--pstate--", acknowledgementViewDetails.StateString);
                            description = description.Replace("--pcountry--", acknowledgementViewDetails.CountryString);
                            description = description.Replace("--ppincode--", acknowledgementViewDetails.ZipCode);
                        }
                        else
                        {
                            ServiceStudentAddress permanentAddress = _MSUoW.ExaminationFormFillUpRepository.GetStudentAddress(_MSCommon.GetEnumDescription(enAddressType.Permanent), acknowledgementViewDetails.Id).FirstOrDefault();
                            if (permanentAddress != null)
                            {
                                description = description.Replace("--paddress--", permanentAddress.Address);
                                description = description.Replace("--pcity--", permanentAddress.City);
                                description = description.Replace("--pstate--", permanentAddress.State);
                                description = description.Replace("--pcountry--", permanentAddress.Country);
                                description = description.Replace("--ppincode--", permanentAddress.Zip);
                            }
                            else
                            {
                                description = description.Replace("--paddress--", "-NA-");
                                description = description.Replace("--pcity--", "-NA-");
                                description = description.Replace("--pstate--", "-NA-");
                                description = description.Replace("--pcountry--", "-NA-");
                                description = description.Replace("--ppincode--", "-NA-");
                            }
                        }

                        //educational qualificateion section
                        description = description.Replace("--matriculationExam--", acknowledgementViewDetails.YearOfMatriculation.ToString());
                        description = description.Replace("--previousExamPassed--", acknowledgementViewDetails.YearOfPreviousExamPassed.ToString());
                        description = description.Replace("--previousAcademicSubject--", acknowledgementViewDetails.PreviousAcademicSubject);

                        //exam information
                        description = description.Replace("--collegeName--", acknowledgementViewDetails.CollegeString);
                        description = description.Replace("--subject--", acknowledgementViewDetails.SubjectString);
                        description = description.Replace("--department--", acknowledgementViewDetails.DepartmentString);
                        description = description.Replace("--academicSession--", acknowledgementViewDetails.AcademicSession);
                        description = description.Replace("--stream--", acknowledgementViewDetails.StreamString);
                        description = description.Replace("--courseCode--", acknowledgementViewDetails.CourseString);
                        description = description.Replace("--semester--", acknowledgementViewDetails.SemesterString);

                        //transaction information
                        description = description.Replace("--payMode--", bankName);
                        description = description.Replace("--amount--", amount);
                        description = description.Replace("--bankStatus--", bankStatus);
                        description = description.Replace("--transactionRefNo--", transactionRefNo);

                        //paper details
                        if (examPapers != null && examPapers.Count() > 0)
                        {
                            var stringtoappend = "";

                            examPapers.ForEach(sub =>
                            {

                                if (sub.IsParctical == true)
                                {
                                    stringtoappend = stringtoappend + " " + sub.PaperName + " - ( T & P )" + " ,";
                                }
                                else
                                {
                                    stringtoappend = stringtoappend + " " + sub.PaperName + " - ( T )" + " ,";
                                }

                            });
                            stringtoappend = stringtoappend.Remove(stringtoappend.Length - 1);

                            description = description.Replace("--papers--", stringtoappend);
                        }
                        else
                        {
                            description = description.Replace("--papers--", "-NA-");
                        }

                        //flattenedPDFDownloadDetails.PdfData = HtmlToPdfConverter.ExportToPDF(description, 600, 1000);
						flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);
						flattenedPDFDownloadDetails.FileType = "application/octet-stream";
                        flattenedPDFDownloadDetails.FileName = "ExamFormFillUpAcknowledgement.pdf";

                        return flattenedPDFDownloadDetails;
                    }
                }
                else throw new MicroServiceException() { ErrorCode = "OUU011" };
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU011" };
            }
        }


        /// <summary>
        /// Anurag Digal
        /// 18-10-19
        /// This methode is used to fetch the pending action of the service
        /// </summary>
        /// <returns></returns>
        public List<PendingActionListDto> PendingActionList()
        {
            List<PendingActionListDto> pendingList = new List<PendingActionListDto>();
            List<UserServiceApplied> serviceAppliedList = _MSUoW.UserServiceApplied.GetAll().Where(x => x.UserId == GetCurrentUserId() && x.IsActionRequired).ToList();
            List<ServiceMaster> serviceMastersList = _MSUoW.ServiceMaster.GetAll().Where(x => x.IsActive).OrderBy(c => c.Id).ToList();

            List<long> idList = new List<long>();
            //get only the id of the servicemaster which will check in the serviceapplied table
            if (serviceMastersList.Count > 0)
            {
                idList = serviceMastersList.Select(d => d.Id).ToList();
            }
            else
                throw new MicroServiceException() { ErrorCode = "OUU012" };

            if (serviceAppliedList.Count > 0)
            {
                //getting the result from serviceAppliedsList by ordering serviceid and creationtime 
                var record = serviceAppliedList.Where(c => idList.Contains(c.ServiceId))
                    .OrderBy(x => x.ServiceId).ThenByDescending(c => c.CreationTime)
                    .ToList();

                idList.ForEach(dd =>
                {
                    //creating data for fetch
                    UserServiceApplied result = record.Where(x => x.ServiceId == dd).FirstOrDefault();
                    if (result != null)
                    {
                        pendingList.Add(new PendingActionListDto
                        {
                            UserId = result.UserId,
                            ServiceId = result.ServiceId,
                            ServiceName = serviceMastersList.Find(d => d.Id == dd).ServiceName,
                            LastStatus = result.LastStatus,
                            InstanceId = result.InstanceId
                        });
                    }
                });
            }
            return pendingList;
        }

        #region MIGRATION ACKNOWLEDGEMENT PDF
        /// <summary>
        /// Author=Sitikanta Pradhan
        /// Date = 08/Nov/2019
        /// Migration Acknowledgement pdf
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public PDFDownloadFile MigrationAcknowledgementPDF(DownloadParamDto param)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.MigrationAcknowledgement && s.ServiceType == param.DocType && s.IsActive == true).FirstOrDefault();


            if (templateData != null)
            {
                string description = templateData.Body;
                //UniversityMigrationDetail studentDetails = _MSUoW.UniversityMigrationDetail.FindBy(usr => usr.CreatorUserId == GetCurrentUserId() && usr.IsActive == true).OrderByDescending(a => a.CreationTime).FirstOrDefault();
                MigrationView acknowledgementViewDetails = _MSUoW.MigrationView.FindBy(usr => usr.CreatorUserId == GetCurrentUserId() && usr.IsActive).OrderByDescending(a => a.CreationTime).FirstOrDefault();
                List<TransactionLog> transactionDetailsList = _MSUoW.TransactionLog.FindBy(trs => trs.RequestId == Convert.ToString(acknowledgementViewDetails.Id) && trs.IsActive).ToList();

                if (acknowledgementViewDetails != null)
                {
                    ServiceMaster serviceName = _MSUoW.ServiceMaster.FindBy(name => name.Id == param.DocType && name.IsActive).FirstOrDefault();
                    TransactionLog successData = transactionDetailsList.Where(data => data.IsActive && data.Status != _MSCommon.GetEnumDescription(enTransaction.Cancelled)).OrderByDescending(data => data.CreationTime).FirstOrDefault();
                    string applicationURL = _configuration.GetSection("ApplicationURL").Value;
                    string collegeLogo = _configuration.GetSection("UULogo").Value;
                    string currentDateTime = DateTime.UtcNow.ToString("dd-MM-yyyy hh:mm:ss tt");
                    description = description.Replace("--collegeLogo--", applicationURL + collegeLogo);
                    description = description.Replace("--studentName--", acknowledgementViewDetails.StudentName);
                    description = description.Replace("--dob--", acknowledgementViewDetails.DateOfBirth.ToString("dd/MM/yyyy"));
                    description = description.Replace("--admissionYear--", acknowledgementViewDetails.YearOfAdmission.ToString("MM/yyyy"));
                    description = description.Replace("--course--", acknowledgementViewDetails.CourseTypeString);
                    description = description.Replace("--subject--", acknowledgementViewDetails.SubjectString);
                    description = description.Replace("--stream--", acknowledgementViewDetails.StreamString);
                    description = description.Replace("--department--", acknowledgementViewDetails.DepartmentString);
                    description = description.Replace("--collegeName--", acknowledgementViewDetails.CollegeString);
                    description = description.Replace("--acknowlegementNo--", acknowledgementViewDetails.AcknowledgementNo);
                    //description = description.Replace("--courseCode--", acknowledgementViewDetails.CourseString);
                    if (serviceName != null)
                    {
                        description = description.Replace("--serviceName--", serviceName.ServiceName);
                    }
                    else
                    {
                        description = description.Replace("--serviceName--", serviceName.ServiceName);
                    }
                    description = description.Replace("--reasonforMigration--", acknowledgementViewDetails.ReasonForMigrationString);
                    description = description.Replace("--currentDateTime--", currentDateTime);
                    description = description.Replace("--registrationNo--", acknowledgementViewDetails.RegistrationNo);
                    //description = description.Replace("--rollNo--", acknowledgementViewDetails.RollNo);
                    description = description.Replace("--lastYear--", acknowledgementViewDetails.YearOfLeaving.ToString("MM/yyyy"));

                    if (acknowledgementViewDetails.ReasonForMigrationCode == "01")
                    {
                        description = description.Replace("--reasonForStudy--", acknowledgementViewDetails.StudyDetail);
                    }
                    else
                    {
                        description = description.Replace("--reasonForStudy--", "-NA-");
                    }
                    //transaction information
                    if (successData != null)
                    {
                        description = description.Replace("--cardType--", successData.CardType);
                        description = description.Replace("--amount--", successData.Amount.ToString());
                        description = description.Replace("--bankStatus--", successData.Status);
                        description = description.Replace("--transactionRefNo--", successData.UniqueRefNo);
                    }
                    else
                    {
                        description = description.Replace("--cardType--", "-NA-");
                        description = description.Replace("--amount--", "-NA-");
                        description = description.Replace("--bankStatus--", "-NA-");
                        description = description.Replace("--transactionRefNo--", "-NA-");
                    }
                    //flattenedPDFDownloadDetails.PdfData = HtmlToPdfConverter.ExportToPDF(description, 600, 1000);
                    flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);
					flattenedPDFDownloadDetails.FileType = "application/octet-stream";
                    flattenedPDFDownloadDetails.FileName = "StudentMigrationAcknowledgement.pdf";

                    return flattenedPDFDownloadDetails;
                }
                else throw new MicroServiceException() { ErrorCode = "OUU002" };
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU011" };
            }
        }
        #endregion

        #region MIGRATION CERTIFICATE DOWNLOAD
        /// <summary>
        /// Sitikanta Pradhan
        /// 08-Nov-19
        /// This methode is used to download certificate in pdf format
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public PDFDownloadFile DownloadMigrationService(DownloadParamDto param)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            if (param != null)
            {
                Guid id = default(Guid);
                string userName = string.Empty;

                //first check whether the service is able to download or not
                var migrationObj = _MSUoW.MigrationView.FindBy(u => u.AcknowledgementNo != null && u.AcknowledgementNo.Trim().ToLower() == param.TransanctionId.Trim().ToLower()
                            && u.IsActive && u.CreatorUserId == GetCurrentUserId() && u.MigrationNumber != null).FirstOrDefault();

                if (migrationObj != null)
                {
                    id = migrationObj.Id;
                    userName = migrationObj.StudentName; // To Do : Bring the actual applying student name from claim // Ask datta

                    if (id != Guid.Empty)
                    {
                        UserServiceApplied appliedData = _MSUoW.UserServiceApplied.FindBy(serv => serv.ServiceId == param.DocType && serv.InstanceId == id && serv.IsActive)
                                    .OrderByDescending(d => d.CreationTime).FirstOrDefault();

                        if (appliedData != null && !appliedData.IsAvailableToDL)
                            throw new MicroServiceException() { ErrorCode = "OUU050" };
                    }
                    else
                        throw new MicroServiceException() { ErrorCode = "OUU051" };


                    EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.MigrationCertificate && s.ServiceType == param.DocType && s.IsActive == true).FirstOrDefault();

                    if (templateData != null)
                    {
                        //EmailTemplateDto emailTemplate = AutoMapper.Mapper.Map<EmailTemplate, EmailTemplateDto>(templateData);

                        string description = templateData.Body;
                        string qrCode = MicroserviceCommon.GenerateQrCode(migrationObj.MigrationNumber);
                        string currentDateTime = DateTime.UtcNow.ToString("dd-MM-yyyy hh:mm:ss tt");
                        string url = _configuration.GetSection("ApplicationURL").Value;
                        string clientUrl = _configuration.GetSection("ClientApplicationURL").Value;
                        string collegeLogo = _configuration.GetSection("UULogo").Value;
                        string examControllerSign = _configuration.GetSection("ControllerOfExaminationSignature").Value;
                        //creating the qrcodeurl for validating the qrcode
                        string qrCodeUrl = clientUrl + _configuration.GetSection("QRCodeValidate:ClientReturnUrl").Value + (long)enServiceType.MigrationService + "/" + migrationObj.MigrationNumber;

                        description = description.Replace("--qrCode--", MicroserviceCommon.GenerateQrCode(qrCodeUrl));
                        description = description.Replace("--sl.no.--", migrationObj.MigrationNumber);
                        //description = description.Replace("--qrCode--", qrCode);
                        description = description.Replace("--studentName--", userName);
                        description = description.Replace("--url--", url);
                        description = description.Replace("--registrationNo--", migrationObj.RegistrationNo);
                        description = description.Replace("--collegeNameString--", migrationObj.CollegeString);
                        description = description.Replace("--streamString--", migrationObj.StreamString);
                        description = description.Replace("--yearOfAdmission--", migrationObj.YearOfAdmission.ToString("yyyy"));
                        description = description.Replace("--collegeLogo--", url + collegeLogo);
                        description = description.Replace("--examControllerSig--", url + examControllerSign);
                        description = description.Replace("--certificateConfrimDate--", Convert.ToDateTime(migrationObj.LastModificationTime).ToString("dd-MM-yyyy"));
                        //description = description.Replace("--certificateConfrimDate--", Convert.ToString(registrationObj.LastModificationTime).Substring(0,9));
                        description = description.Replace("--currentDateTime--", currentDateTime);

                        //flattenedPDFDownloadDetails.PdfData = HtmlToPdfConverter.ExportToPDF(description, 600, 1000);
						flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);
						flattenedPDFDownloadDetails.FileType = "application/octet-stream";
                        //flattenedPDFDownloadDetails.FileName = param.RollNo + "_" + param.TransanctionId + ".pdf";
                        flattenedPDFDownloadDetails.FileName = "MigrationCertificate.pdf";
                    }
                    else
                        throw new MicroServiceException { ErrorCode = "OUU009" };
                }
                else
                {
                    throw new MicroServiceException() { ErrorCode = "OUU137" };
                }
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU010" };
            }
            return flattenedPDFDownloadDetails;
        }
        #endregion

        public Guid GetServicePrimarykey(long docType)
        {
            Guid id = Guid.Empty;
            if (docType != 0)
            {
                switch (docType)
                {
                    case (long)enServiceType.MigrationService:
                        UniversityMigrationDetail migrationDetails = _MSUoW.UniversityMigrationDetail.GetAll().OrderByDescending(d => d.CreationTime)
                                                                    .Where(d => d.CreatorUserId == GetCurrentUserId()
                                                                    && d.IsActive).
                                                                    FirstOrDefault();
                        if (migrationDetails != null)
                        {
                            id = _MSUoW.UserServiceApplied.GetAll().OrderByDescending(c => c.CreationTime)
                                .Where(c => c.UserId == GetCurrentUserId() && c.ServiceId == docType && c.IsActive)
                                .Select(x => x.InstanceId).FirstOrDefault();
                            if (id == null || (id == Guid.Empty))
                            {
                                throw new MicroServiceException() { ErrorCode = "OUU002" };
                            }
                        }
                        else throw new MicroServiceException() { ErrorCode = "OUU073" };
                        break;
                    case (long)enServiceType.RegistrationService:
                        UniversityRegistrationDetail registrationData = _MSUoW.UniversityRegistrationDetail.GetAll().OrderByDescending(d => d.CreationTime)
                                                                    .Where(d => d.CreatorUserId == GetCurrentUserId()
                                                                    && d.IsActive).
                                                                    FirstOrDefault();

                        if (registrationData != null)
                        {
                            id = _MSUoW.UserServiceApplied.GetAll().OrderByDescending(c => c.CreationTime)
                                .Where(c => c.UserId == GetCurrentUserId() && c.ServiceId == docType && c.IsActive)
                                .Select(x => x.InstanceId).FirstOrDefault();
                            if (id == null || (id == Guid.Empty))
                            {
                                throw new MicroServiceException() { ErrorCode = "OUU002" };
                            }
                        }
                        else throw new MicroServiceException() { ErrorCode = "OUU073" };
                        break;
                    case (long)enServiceType.ProvisionalCertificate:
                        break;
                    case (long)enServiceType.CollegeLeavingCertificate:
                        ApplyCLC clcData = _MSUoW.ApplyCLC.GetAll().OrderByDescending(d => d.CreationTime)
                                                                    .Where(d => d.CreatorUserId == GetCurrentUserId()
                                                                    && d.IsActive).
                                                                    FirstOrDefault();

                        if (clcData != null)
                        {
                            id = _MSUoW.UserServiceApplied.GetAll().OrderByDescending(c => c.CreationTime)
                                .Where(c => c.UserId == GetCurrentUserId() && c.ServiceId == docType && c.IsActive)
                                .Select(x => x.InstanceId).FirstOrDefault();
                            if (id == null || (id == Guid.Empty))
                            {
                                throw new MicroServiceException() { ErrorCode = "OUU002" };
                            }
                        }
                        else throw new MicroServiceException() { ErrorCode = "OUU073" };
                        break;
                    case (long)enServiceType.DuplicateRegistrationCertificate:

                        DuplicateRegistrationDetail duplicateData = _MSUoW.DuplicateRegistrationDetail.GetAll().OrderByDescending(d => d.CreationTime)
                                                                    .Where(d => d.CreatorUserId == GetCurrentUserId()
                                                                    && d.IsActive).
                                                                    FirstOrDefault();

                        if (duplicateData != null)
                        {
                            id = _MSUoW.UserServiceApplied.GetAll().OrderByDescending(c => c.CreationTime)
                                .Where(c => c.UserId == GetCurrentUserId() && c.ServiceId == docType && c.IsActive)
                                .Select(x => x.InstanceId).FirstOrDefault();
                            if (id == null || (id == Guid.Empty))
                            {
                                throw new MicroServiceException() { ErrorCode = "OUU002" };
                            }
                        }
                        else throw new MicroServiceException() { ErrorCode = "OUU073" };

                        break;
                    case (long)enServiceType.VerificationofAdditionofMarks:
                        break;
                    case (long)enServiceType.IssueofPaperwiseMarks:
                        break;
                    case (long)enServiceType.BCABBAExamApplicationForm:

                        ExaminationFormFillUpDetails examinationFormDeatils = _MSUoW.ExaminationFormFillUpDetails.GetAll().OrderByDescending(d => d.CreationTime)
                                                                    .Where(d => d.CreatorUserId == GetCurrentUserId()
                                                                    && d.IsActive).
                                                                    FirstOrDefault();
                        if (examinationFormDeatils != null)
                        {
                            id = _MSUoW.UserServiceApplied.GetAll().OrderByDescending(c => c.CreationTime)
                                .Where(c => c.UserId == GetCurrentUserId() && c.ServiceId == docType && c.IsActive)
                                .Select(x => x.InstanceId).FirstOrDefault();
                            if (id == null || (id == Guid.Empty))
                            {
                                throw new MicroServiceException() { ErrorCode = "OUU002" };
                            }
                        }
                        else throw new MicroServiceException() { ErrorCode = "OUU073" };


                        break;
                    case (long)enServiceType.NursingApplicationForm:

                        NursingFormfillupDetails nursingFormFillupDetails = _MSUoW.NursingFormfillupDetails.GetAll().OrderByDescending(d => d.CreationTime)
                                                                    .Where(d => d.CreatorUserId == GetCurrentUserId()
                                                                    && d.IsActive).
                                                                    FirstOrDefault();
                        if (nursingFormFillupDetails != null)
                        {
                            id = _MSUoW.UserServiceApplied.GetAll().OrderByDescending(c => c.CreationTime)
                                .Where(c => c.UserId == GetCurrentUserId() && c.ServiceId == docType && c.IsActive)
                                .Select(x => x.InstanceId).FirstOrDefault();
                            if (id == null || (id == Guid.Empty))
                            {
                                throw new MicroServiceException() { ErrorCode = "OUU002" };
                            }
                        }
                        else throw new MicroServiceException() { ErrorCode = "OUU073" };


                        break;
                }
            }
            return id;
        }

        private List<ServiceStatusDto> GetRegistrationServiceStatus(UserServiceApplied userServiceAppliedObj)
        {
            List<ServiceStatusDto> serviceStatusList = new List<ServiceStatusDto>();
            var recordSubmissionState = _configuration.GetSection("RegistrationNumber:RegistrationNumberInitialHistoryState").Value;

            var universityRegRecord = _MSUoW.UniversityRegistrationDetail.FindBy(data => data.Id == userServiceAppliedObj.InstanceId
                                && data.CreatorUserId == GetCurrentUserId() && data.IsActive)
                                .FirstOrDefault();

            // Get the appsetting value for creating object according to the stateStatusType with ascending order 
            var appSettingWorkflowStateNames = _configuration.GetSection("RegistrationNumber:SourceStateType").Value.Split(',');

            if (universityRegRecord != null)
            {// Get the workflowtransactionHistory data from table in ascending order of their creation time(the way they are created in db)
                List<WorkflowTransactionHistory> workflowTransactionHistoryObj = _workflowDbContext.WorkflowTransactionHistory
                                                                            .Where(dd => dd.InstanceId == userServiceAppliedObj.InstanceId
                                                                            && dd.TenantWorkflowId == universityRegRecord.TenantWorkflowId && dd.IsActive).OrderBy(d => d.CreationTime)
                                                                            .ToList();

                // No need to find distinct as one state can be reached multiple times
                var workflowCompletdStates = workflowTransactionHistoryObj.Select(obj => obj.TransitState).ToList();

                if (workflowTransactionHistoryObj.Count > 0)
                {// Will always be true, as we are creating a history record when ever the application form is submitted
                 // Create status for application submission
                    serviceStatusList.Add(new ServiceStatusDto()
                    {
                        Status = recordSubmissionState,
                        StatusType = enStatusType.Completed,
                        StatusDate = workflowTransactionHistoryObj.First().CreationTime.Date.ToString("dd-MM-yyyy")
                    });
                }


                if (workflowCompletdStates.Count > 0)
                {
                    workflowCompletdStates.ForEach(state =>
                    {
                        // Completed State Data
                        serviceStatusList.Add(new ServiceStatusDto()
                        {
                            Status = state,
                            StatusType = enStatusType.Completed,
                            StatusDate = workflowTransactionHistoryObj.Where(history => history.TransitState == state).OrderByDescending(obj => obj.CreationTime).First().CreationTime.Date.ToString("dd-MM-yyyy")
                        });
                    });

                    // Latest Completed Data / Present data
                    serviceStatusList[serviceStatusList.Count - 1].StatusType = enStatusType.Present;

                    if (_configuration.GetSection("RegistrationNumber:RegistrationNumberRejectedStatus").Value != serviceStatusList[serviceStatusList.Count - 1].Status)
                    {
                        // Find if last state is in the workflow states
                        var indexOfLastStateOccured = Array.IndexOf(appSettingWorkflowStateNames, serviceStatusList[serviceStatusList.Count - 1].Status);

                        if (indexOfLastStateOccured != -1)
                        {
                            appSettingWorkflowStateNames = appSettingWorkflowStateNames.Skip(indexOfLastStateOccured + 1).ToArray();
                        }
                        foreach (var state in appSettingWorkflowStateNames)
                        {
                            serviceStatusList.Add(new ServiceStatusDto()
                            {
                                Status = state,
                                StatusType = enStatusType.Future,
                                StatusDate = "Yet to reach"
                            });
                        }
                    }
                    
                }
                else
                {
                    foreach (string state in appSettingWorkflowStateNames)
                    {
                        serviceStatusList.Add(new ServiceStatusDto()
                        {
                            Status = state,
                            StatusType = enStatusType.Future,
                            StatusDate = "Yet to reach"
                        });
                    }
                }
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU057" };
            }
            return serviceStatusList;
        }

        private List<ServiceStatusDto> GetExaminationServiceStatus(UserServiceApplied userServiceAppliedObj)
        {
            List<ServiceStatusDto> serviceStatusList = new List<ServiceStatusDto>();
            //var recordSubmissionState = _configuration.GetSection("ExamFormFillup:RegistrationNumberInitialHistoryState").Value;

            var universityExamRecord = _MSUoW.ExaminationFormFillUpDetails.FindBy(data => data.Id == userServiceAppliedObj.InstanceId
                                && data.CreatorUserId == GetCurrentUserId() && data.IsActive)
                                .FirstOrDefault();

            // Get the appsetting value for creating object according to the stateStatusType with ascending order 
            var appSettingWorkflowStateNames = _configuration.GetSection("ExamFormFillup:FormStates").Value.Split(',');

            if (universityExamRecord != null)
            {// Get the workflowtransactionHistory data from table in ascending order of their creation time(the way they are created in db)
                List<WorkflowTransactionHistory> workflowTransactionHistoryObj = _workflowDbContext.WorkflowTransactionHistory
                                                                            .Where(dd => dd.InstanceId == userServiceAppliedObj.InstanceId
                                                                            && dd.TenantWorkflowId == universityExamRecord.TenantWorkflowId && dd.IsActive).OrderBy(d => d.CreationTime)
                                                                            .ToList();

                // No need to find distinct as one state can be reached multiple times
                var workflowCompletdStates = workflowTransactionHistoryObj.Select(obj => obj.TransitState).ToList();

                //if (workflowTransactionHistoryObj.Count > 0)
                //{// Will always be true, as we are creating a history record when ever the application form is submitted
                // // Create status for application submission
                //    serviceStatusList.Add(new ServiceStatusDto()
                //    {
                //        Status = recordSubmissionState,
                //        StatusType = enStatusType.Completed,
                //        StatusDate = workflowTransactionHistoryObj.First().CreationTime.Date.ToString("dd-MM-yyyy")
                //    });
                //}


                if (workflowCompletdStates.Count > 0)
                {
                    workflowCompletdStates.ForEach(state =>
                    {
                        // Completed State Data
                        serviceStatusList.Add(new ServiceStatusDto()
                        {
                            Status = state,
                            StatusType = enStatusType.Completed,
                            StatusDate = workflowTransactionHistoryObj.Where(history => history.TransitState == state).OrderByDescending(obj => obj.CreationTime).First().CreationTime.Date.ToString("dd-MM-yyyy")
                        });
                    });

                    // Latest Completed Data / Present data
                    serviceStatusList[serviceStatusList.Count - 1].StatusType = enStatusType.Present;

                    // Find if last state is in the workflow states
                    var indexOfLastStateOccured = Array.IndexOf(appSettingWorkflowStateNames, serviceStatusList[serviceStatusList.Count - 1].Status);

                    if (indexOfLastStateOccured != -1)
                    {
                        appSettingWorkflowStateNames = appSettingWorkflowStateNames.Skip(indexOfLastStateOccured + 1).ToArray();
                    }

                    if (serviceStatusList[serviceStatusList.Count()-1].Status != _configuration.GetSection("ExamFormFillup:ExamFormFillUpRejectedStatus").Value)
                    {
                        foreach (var state in appSettingWorkflowStateNames)
                        {
                            serviceStatusList.Add(new ServiceStatusDto()
                            {
                                Status = state,
                                StatusType = enStatusType.Future,
                                StatusDate = "Yet to reach"
                            });
                        }
                    }
                }
                else
                {
                    foreach (string state in appSettingWorkflowStateNames)
                    {
                        serviceStatusList.Add(new ServiceStatusDto()
                        {
                            Status = state,
                            StatusType = enStatusType.Future,
                            StatusDate = "Yet to reach"
                        });
                    }
                }
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU057" };
            }
            return serviceStatusList;
        }

        #region ADMITCARD DOWNLOAD
        public PDFDownloadFile DownloadAdmitCard(DownloadParamDto param)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            Guid id = default(Guid);

            if (param != null)
            {

                #region DATA GATHERING FROM TABLE FOR ADMITCARD
                var examFormData = _MSUoW.FormFillupViewForAdmitCard.GetAll().Where(x=> x.IsActive && x.CreatorUserId==GetCurrentUserId() 
                                    && x.AcknowledgementNo!=null && x.AcknowledgementNo.Trim().ToLower()==param.TransanctionId.Trim().ToLower() 
                                    &&( x.State.Trim().ToLower()==_configuration.GetSection("ExamFormFillup:AdmitcardGenerated").Value.Trim().ToLower()
                                    || x.State.Trim().ToLower() == _configuration.GetSection("ExamFormFillup:MarkUploaded").Value.Trim().ToLower()))
                                    .OrderByDescending(o=>o.CreationTime).FirstOrDefault();
                   if (examFormData != null)
                   {
                    IQueryable<ExaminationPaperDetail> examPaperIds = _MSUoW.ExaminationPaperDetail.FindBy(e => e.IsActive && e.ExaminationFormFillUpDetailsId == examFormData.Id);                    
                    List<ExaminationPaperMaster> examPapers = _MSUoW.ExaminationPaperMaster.FindBy(a => a.IsActive && examPaperIds.Select(b => b.ExaminationPaperId).Contains(a.Id)).ToList();
                    AdmitCardCenterAndExamNameView centerAndSession = new AdmitCardCenterAndExamNameView();
                    if (examFormData.CourseTypeCode== _MSCommon.GetEnumDescription(enCourseType.Ug))
                    {
                        centerAndSession = _MSUoW.AdmitCardCenterAndExamNameView.GetAll().Where(e => e.IsActive && e.CourseType == examFormData.CourseTypeCode && e.StreamCode == examFormData.StreamCode
                                               && e.SubjectCode == examFormData.SubjectCode && e.SemesterCode == examFormData.SemesterCode && e.CollegeCode == examFormData.CollegeCode
                                               && e.StartYear == examFormData.AcademicStart).FirstOrDefault();
                    }
                    else
                    {
                        centerAndSession = _MSUoW.AdmitCardCenterAndExamNameView.GetAll().Where(e => e.IsActive && e.CourseType == examFormData.CourseTypeCode && e.StreamCode == examFormData.StreamCode
                                               && e.StreamCode==examFormData.StreamCode && e.DepartmentCode==examFormData.DepartmentCode && e.SubjectCode == examFormData.SubjectCode && e.SemesterCode == examFormData.SemesterCode
                                               && e.CollegeCode == examFormData.CollegeCode && e.StartYear == examFormData.AcademicStart).FirstOrDefault();
                    }

                    List<ExaminationPaperMaster> allExamPaperList = _MSUoW.ExaminationPaperMaster.FindBy(e => e.IsActive && e.ExaminationMasterId == centerAndSession.Id).ToList();

                    #endregion

                    #region  Email content set part
                    if (centerAndSession != null)
                    {
                        id = examFormData.Id;
                        if (id != Guid.Empty)
                        {
                            UserServiceApplied appliedData = _MSUoW.UserServiceApplied.FindBy(serv => serv.ServiceId == param.DocType && serv.InstanceId == id && serv.IsActive)
                                        .OrderByDescending(d => d.CreationTime).FirstOrDefault();

                            if (appliedData != null && !appliedData.IsAvailableToDL)
                                throw new MicroServiceException() { ErrorCode = "OUU050" };
                        }
                        else
                            throw new MicroServiceException() { ErrorCode = "OUU051" };
                        EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.AdmitCardToStudent && s.IsActive == true).FirstOrDefault();

                        if (templateData != null)
                        {
                            string description = templateData.Body;
                            string url = _configuration.GetSection("ApplicationURL").Value;
                            string collegeLogo = _configuration.GetSection("UULogo").Value;
                            string examControllerSign = _configuration.GetSection("ControllerOfExaminationSignature").Value;

                            description = description.Replace("--collegeLogo--", url + collegeLogo);
                            description = description.Replace("--examName--", centerAndSession.SubjectString + " " + centerAndSession.SemesterString + " " + " EXAMINATION - " + centerAndSession.ExamYear);
                            if (examFormData.ImagePath != null)
                            {
                                description = description.Replace("--photo--", url + examFormData.ImagePath);
                            }
                            else
                            {
                                description = description.Replace("--photo--", "No Photo Available");
                            }
                            description = description.Replace("--regdNo--", examFormData.RegistrationNumber);
                            description = description.Replace("--studentName--", examFormData.StudentName);
                            description = description.Replace("--rollNo--", examFormData.RollNumber);
                            if(centerAndSession.CenterCollegeString.Trim().ToLower() == examFormData.CollegeName.Trim().ToLower())
                            {
                                description = description.Replace("--college--", examFormData.CollegeName);
                                description = description.Replace("--center--", "");
                            }
                            else
                            {
                                description = description.Replace("--college--", examFormData.CollegeName);
                                description = description.Replace("--center--", " / "+centerAndSession.CenterCollegeString);

                            }
                            
                            description = description.Replace("--controllerSignature--", url + examControllerSign);
                            
                            var stringtoappend = "";

                            if (allExamPaperList.Count() == examPapers.Count())
                            {
                                stringtoappend = "ALL";
                            }
                            else
                            {
                                examPapers.ForEach(sub =>
                                {

                                    if (sub.IsParctical == true)
                                    {
                                        stringtoappend = stringtoappend + " " + sub.PaperName + " - ( T & P )" + " ,";
                                    }
                                    else
                                    {
                                        stringtoappend = stringtoappend + " " + sub.PaperName + " - ( T )" + " ,";
                                    }
                                });
                                stringtoappend = stringtoappend.Remove(stringtoappend.Length - 1);
                            }                            

                            description = description.Replace("--papers--", stringtoappend);
                            //flattenedPDFDownloadDetails.PdfData = HtmlToPdfConverter.ExportToPDF(description, 600, 1000);
							flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);
							flattenedPDFDownloadDetails.FileType = "application/octet-stream";
                            flattenedPDFDownloadDetails.FileName = "AdmitCard_"+ examFormData.RegistrationNumber + ".pdf";
                        }
                        else
                            throw new MicroServiceException { ErrorCode = "OUU009" };
                    }
                    else throw new MicroServiceException { ErrorCode = "OUU074", SubstitutionParams = new object[1] { param.TransanctionId } };
                }
                else throw new MicroServiceException { ErrorCode = "OUU074", SubstitutionParams = new object[1] { param.TransanctionId } };
                #endregion
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU010" };
            }
            return flattenedPDFDownloadDetails;

        }
        #endregion

        #region Nursing ADMITCARD DOWNLOAD
        /// <summary>
        /// Author:Bikash kumar
        /// Date: 04/Dec/2019
        /// Download nursing admit card 
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public PDFDownloadFile DownloadNursingAdmitCard(DownloadParamDto param)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            Guid id = default(Guid);

            if (param != null)
            {

                #region DATA GATHERING FROM TABLE FOR ADMITCARD
                var examFormData = _MSUoW.NursingFormfillupDetails.GetAll().Where(x => x.IsActive && x.CreatorUserId == GetCurrentUserId()
                                    && x.AcknowledgementNo != null && x.AcknowledgementNo.Trim().ToLower() == param.TransanctionId.Trim().ToLower()
                                    && (x.State.Trim().ToLower() == _configuration.GetSection("ExamFormFillup:AdmitcardGenerated").Value.Trim().ToLower()
                                    || x.State.Trim().ToLower() == _configuration.GetSection("ExamFormFillup:MarkUploaded").Value.Trim().ToLower()))
                                    .OrderByDescending(o => o.CreationTime).FirstOrDefault();
                if (examFormData != null)
                {
                    IQueryable<ExaminationPaperDetail> examPaperIds = _MSUoW.ExaminationPaperDetail.FindBy(e => e.IsActive && e.ExaminationFormFillUpDetailsId == examFormData.Id);
                    List<ExaminationPaperMaster> examPapers = _MSUoW.ExaminationPaperMaster.FindBy(a => a.IsActive && examPaperIds.Select(b => b.ExaminationPaperId).Contains(a.Id)).ToList();
                    AdmitCardCenterAndExamNameView centerAndSession = new AdmitCardCenterAndExamNameView();
                    if (examFormData.CourseTypeCode == _MSCommon.GetEnumDescription(enCourseType.Ug))
                    {
                        centerAndSession = _MSUoW.AdmitCardCenterAndExamNameView.GetAll().Where(e => e.IsActive && e.CourseType == examFormData.CourseTypeCode && e.StreamCode == examFormData.StreamCode
                                               && e.SubjectCode == examFormData.SubjectCode && e.SemesterCode == examFormData.CurrentAcademicYear && e.CollegeCode == examFormData.CollegeCode
                                               && e.StartYear == examFormData.AcademicStart).FirstOrDefault();
                    }
                    else
                    {
                        centerAndSession = _MSUoW.AdmitCardCenterAndExamNameView.GetAll().Where(e => e.IsActive && e.CourseType == examFormData.CourseTypeCode && e.StreamCode == examFormData.StreamCode
                                               && e.StreamCode == examFormData.StreamCode && e.DepartmentCode == examFormData.DepartmentCode && e.SubjectCode == examFormData.SubjectCode && e.SemesterCode == examFormData.CurrentAcademicYear
                                               && e.CollegeCode == examFormData.CollegeCode && e.StartYear == examFormData.AcademicStart).FirstOrDefault();
                    }

                    List<ExaminationPaperMaster> allExamPaperList = _MSUoW.ExaminationPaperMaster.FindBy(e => e.IsActive && e.ExaminationMasterId == centerAndSession.Id).ToList();
                    Documents docmentDetails = _MSUoW.Documents.GetAll().Where(w => w.ParentSourceId == examFormData.Id && w.IsActive && w.DocumentType == _MSCommon.GetEnumDescription(enApplicantDocs.ApplicantPhoto)).FirstOrDefault();
                    #endregion

                    #region  Email content set part
                    if (centerAndSession != null)
                    {
                        id = examFormData.Id;
                        if (id != Guid.Empty)
                        {
                            UserServiceApplied appliedData = _MSUoW.UserServiceApplied.FindBy(serv => serv.ServiceId == param.DocType && serv.InstanceId == id && serv.IsActive)
                                        .OrderByDescending(d => d.CreationTime).FirstOrDefault();

                            if (appliedData != null && !appliedData.IsAvailableToDL)
                                throw new MicroServiceException() { ErrorCode = "OUU050" };
                        }
                        else
                            throw new MicroServiceException() { ErrorCode = "OUU051" };
                        EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.AdmitCardToStudent && s.IsActive == true).FirstOrDefault();

                        if (templateData != null)
                        {
                            string description = templateData.Body;
                            string url = _configuration.GetSection("ApplicationURL").Value;
                            string collegeLogo = _configuration.GetSection("UULogo").Value;
                            string examControllerSign = _configuration.GetSection("ControllerOfExaminationSignature").Value;

                            description = description.Replace("--collegeLogo--", url + collegeLogo);
                            description = description.Replace("--examName--", centerAndSession.SubjectString + " " + centerAndSession.SemesterString + " " + " EXAMINATION - " + centerAndSession.ExamYear);
                            if (docmentDetails.FilePath != null)
                            {
                                description = description.Replace("--photo--", url + docmentDetails.FilePath);
                            }
                            else
                            {
                                description = description.Replace("--photo--", "No Photo Available");
                            }
                            description = description.Replace("--regdNo--", examFormData.RegistrationNumber);
                            description = description.Replace("--studentName--", examFormData.StudentName);
                            description = description.Replace("--rollNo--", examFormData.RollNumber);
                            if (centerAndSession.CenterCollegeString.Trim().ToLower() == examFormData.CollegeCodeString.Trim().ToLower())
                            {
                                description = description.Replace("--college--", examFormData.CollegeCodeString);
                                description = description.Replace("--center--", "");
                            }
                            else
                            {
                                description = description.Replace("--college--", examFormData.CollegeCodeString);
                                description = description.Replace("--center--", " / " + centerAndSession.CenterCollegeString);

                            }

                            description = description.Replace("--controllerSignature--", url + examControllerSign);

                            var stringtoappend = "";

                            if (allExamPaperList.Count() == examPapers.Count())
                            {
                                stringtoappend = "ALL";
                            }
                            else
                            {
                                examPapers.ForEach(sub =>
                                {

                                    if (sub.IsParctical == true)
                                    {
                                        stringtoappend = stringtoappend + " " + sub.PaperName + " - ( T & P )" + " ,";
                                    }
                                    else
                                    {
                                        stringtoappend = stringtoappend + " " + sub.PaperName + " - ( T )" + " ,";
                                    }
                                });
                                stringtoappend = stringtoappend.Remove(stringtoappend.Length - 1);
                            }

                            description = description.Replace("--papers--", stringtoappend);
                            //flattenedPDFDownloadDetails.PdfData = HtmlToPdfConverter.ExportToPDF(description, 600, 1000);
                            flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);
                            flattenedPDFDownloadDetails.FileType = "application/octet-stream";
                            flattenedPDFDownloadDetails.FileName = "AdmitCard_" + examFormData.RollNumber + ".pdf";
                        }
                        else
                            throw new MicroServiceException { ErrorCode = "OUU009" };
                    }
                    else throw new MicroServiceException { ErrorCode = "OUU074", SubstitutionParams = new object[1] { param.TransanctionId } };
                }
                else throw new MicroServiceException { ErrorCode = "OUU074", SubstitutionParams = new object[1] { param.TransanctionId } };
                #endregion
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU010" };
            }
            return flattenedPDFDownloadDetails;

        }
        #endregion

        #region
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 04/Dec/2019
        /// Download section status for migration
        /// </summary>
        /// <param name="userServiceAppliedObj"></param>
        /// <returns></returns>
        private List<ServiceStatusDto> GetMigrationServiceStatus(UserServiceApplied userServiceAppliedObj)
        {
            List<ServiceStatusDto> serviceStatusList = new List<ServiceStatusDto>();
            var recordSubmissionState = _configuration.GetSection("MigrationCertificate:MigrationCertificateInitialHistoryState").Value;

            var universityRegRecord = _MSUoW.UniversityMigrationDetail.FindBy(data => data.Id == userServiceAppliedObj.InstanceId
                                && data.CreatorUserId == GetCurrentUserId() && data.IsActive).OrderBy(d => d.CreationTime)
                                .FirstOrDefault();

            // Get the appsetting value for creating object according to the stateStatusType with ascending order 
            var appSettingWorkflowStateNames = _configuration.GetSection("MigrationCertificate:SourceStateType").Value.Split(',');

            if (universityRegRecord != null)
            {// Get the workflowtransactionHistory data from table in ascending order of their creation time(the way they are created in db)
                List<WorkflowTransactionHistory> workflowTransactionHistoryObj = _workflowDbContext.WorkflowTransactionHistory
                                                                            .Where(dd => dd.InstanceId == userServiceAppliedObj.InstanceId
                                                                            && dd.TenantWorkflowId == universityRegRecord.TenantWorkflowId && dd.IsActive).OrderBy(d => d.CreationTime)
                                                                            .ToList();

                // No need to find distinct as one state can be reached multiple times
                var workflowCompletdStates = workflowTransactionHistoryObj.Select(obj => obj.TransitState).ToList();

                if (workflowTransactionHistoryObj.Count > 0)
                {// Will always be true, as we are creating a history record when ever the application form is submitted
                 // Create status for application submission
                    serviceStatusList.Add(new ServiceStatusDto()
                    {
                        Status = recordSubmissionState,
                        StatusType = enStatusType.Completed,
                        StatusDate = workflowTransactionHistoryObj.First().CreationTime.Date.ToString("dd-MM-yyyy")
                    });
                }


                if (workflowCompletdStates.Count > 0)
                {
                    workflowCompletdStates.ForEach(state =>
                    {
                            // Completed State Data
                            serviceStatusList.Add(new ServiceStatusDto()
                        {
                            Status = state,
                            StatusType = enStatusType.Completed,
                            StatusDate = workflowTransactionHistoryObj.Where(history => history.TransitState == state).OrderByDescending(obj => obj.CreationTime).First().CreationTime.Date.ToString("dd-MM-yyyy")
                        });
                    });

                    // Latest Completed Data / Present data
                    serviceStatusList[serviceStatusList.Count - 1].StatusType = enStatusType.Present;

                    // Find if last state is in the workflow states
                    var indexOfLastStateOccured = Array.IndexOf(appSettingWorkflowStateNames, serviceStatusList[serviceStatusList.Count - 1].Status);

                    if (indexOfLastStateOccured != -1)
                    {
                        appSettingWorkflowStateNames = appSettingWorkflowStateNames.Skip(indexOfLastStateOccured + 1).ToArray();
                    }
                    foreach (var state in appSettingWorkflowStateNames)
                    {
                        serviceStatusList.Add(new ServiceStatusDto()
                        {
                            Status = state,
                            StatusType = enStatusType.Future,
                            StatusDate = "Yet to reach"
                        });
                    }
                }
                else
                {
                    //foreach (string state in appSettingWorkflowStateNames)
                    //{
                    //    serviceStatusList.Add(new ServiceStatusDto()
                    //    {
                    //        Status = state,
                    //        StatusType = enStatusType.Future,
                    //        StatusDate = "Yet to reach"
                    //    });
                    //}
                    serviceStatusList.Add(new ServiceStatusDto()
                    {
                        Status = "Migration Application Submitted",
                        StatusType = enStatusType.Future,
                        //StatusDate = "Yet to reach"
                    });
                }
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU057" };
            }
            return serviceStatusList;
        }
        #endregion

        #region Download CLC AcKnowledgement
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 25-Feb-2020
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public PDFDownloadFile DownloadAcknowledgementForCLC(DownloadParamDto param)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.CLCAcknowledgementForm && s.ServiceType == param.DocType && s.IsActive == true).FirstOrDefault();
            if (templateData != null)
            {
                string description = templateData.Body;
                CLCView clcViewDetails = _MSUoW.CLCView.FindBy(usr => usr.CreatorUserId == GetCurrentUserId() && usr.IsActive).OrderByDescending(a => a.CreationTime).FirstOrDefault();
                IQueryable<ExaminationPaperDetail> examPaperIds = _MSUoW.ExaminationPaperDetail.FindBy(e => e.IsActive && e.ExaminationFormFillUpDetailsId == clcViewDetails.Id);
                List<TransactionLog> transactionDetailsList = _MSUoW.TransactionLog.FindBy(trs => trs.RequestId == Convert.ToString(clcViewDetails.Id) && trs.IsActive).ToList();

                if (clcViewDetails != null) // && acknowledgementViewDetails.AcknowledgementNo != null && !string.IsNullOrWhiteSpace(acknowledgementViewDetails.AcknowledgementNo)
                {
                    TransactionLog successData = transactionDetailsList.Where(data => data.IsActive && data.Status != _MSCommon.GetEnumDescription(enTransaction.Cancelled)).OrderByDescending(data => data.CreationTime).FirstOrDefault();
                    ServiceMaster serviceName = _MSUoW.ServiceMaster.FindBy(name => name.Id == param.DocType && name.IsActive).FirstOrDefault();
                    if (successData != null)
                    {
                        string status = "Uploaded";
                        string applicationURL = _configuration.GetSection("ApplicationURL").Value;
                        string collegeLogo = _configuration.GetSection("UULogo").Value;
                        if (serviceName != null)
                        {
                            description = description.Replace("--serviceName--", serviceName.ServiceName);
                        }
                        else
                        {
                            description = description.Replace("--serviceName--", "-NA-");
                        }
                        description = description.Replace("--collegeLogo--", applicationURL + collegeLogo);
                        description = description.Replace("--acknowledgementNo--", clcViewDetails.AcknowledgementNo);
                        if (clcViewDetails.CreationTime != null)
                        {
                            DateTime utcdate = DateTime.ParseExact(clcViewDetails.CreationTime.ToString("MM-dd-yyyy HH:mm:ss"), "MM-dd-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                            var istdate = TimeZoneInfo.ConvertTimeFromUtc(utcdate, TZConvert.GetTimeZoneInfo(_configuration.GetSection("TimeZone").Value.ToString()));
                            clcViewDetails.CreationTime = istdate;
                            description = description.Replace("--creationTime--", clcViewDetails.CreationTime.ToString());
                        }
                        else
                        {
                            description = description.Replace("--creationTime--", "-NA-");
                        }
                        description = description.Replace("--courseType--", clcViewDetails.CourseTypeString);
                        if (clcViewDetails.RegistrationNumber != null)
                        {
                            description = description.Replace("--registratioNumber--", clcViewDetails.RegistrationNumber);
                        }
                        else
                        {
                            description = description.Replace("--registratioNumber--", "-NA-");
                        }

                        description = description.Replace("--studentName--", clcViewDetails.StudentName);
                        description = description.Replace("--dob--", clcViewDetails.DateOfBirth.ToString("dd/MM/yyyy"));

                        //document section
                        description = description.Replace("--docName--", clcViewDetails.FileName);
                        description = description.Replace("--status--", status);

                        //address section 
                        description = description.Replace("--address--", clcViewDetails.Address);
                        description = description.Replace("--city--", clcViewDetails.CityString);
                        description = description.Replace("--state--", clcViewDetails.StateString);
                        description = description.Replace("--country--", clcViewDetails.CountryString);
                        description = description.Replace("--pincode--", clcViewDetails.ZipCode);

                        if (clcViewDetails.IsSameAsPresent)
                        {
                            description = description.Replace("--paddress--", clcViewDetails.Address);
                            description = description.Replace("--pcity--", clcViewDetails.CityString);
                            description = description.Replace("--pstate--", clcViewDetails.StateString);
                            description = description.Replace("--pcountry--", clcViewDetails.CountryString);
                            description = description.Replace("--ppincode--", clcViewDetails.ZipCode);
                        }
                        else
                        {
                            ServiceStudentAddress permanentAddress = _MSUoW.ExaminationFormFillUpRepository.GetStudentAddress(_MSCommon.GetEnumDescription(enAddressType.Permanent), clcViewDetails.Id).FirstOrDefault();
                            if (permanentAddress != null)
                            {
                                description = description.Replace("--paddress--", permanentAddress.Address);
                                description = description.Replace("--pcity--", permanentAddress.City);
                                description = description.Replace("--pstate--", permanentAddress.State);
                                description = description.Replace("--pcountry--", permanentAddress.Country);
                                description = description.Replace("--ppincode--", permanentAddress.Zip);
                            }
                            else
                            {
                                description = description.Replace("--paddress--", "-NA-");
                                description = description.Replace("--pcity--", "-NA-");
                                description = description.Replace("--pstate--", "-NA-");
                                description = description.Replace("--pcountry--", "-NA-");
                                description = description.Replace("--ppincode--", "-NA-");
                            }
                        }

                        //exam information
                        description = description.Replace("--collegeName--", clcViewDetails.CollegeString);
                        description = description.Replace("--subject--", clcViewDetails.SubjectString);
                        description = description.Replace("--department--", clcViewDetails.DepartmentString);
                        description = description.Replace("--stream--", clcViewDetails.StreamString);
                        description = description.Replace("--courseCode--", clcViewDetails.CourseString);

                        //transaction information
                        description = description.Replace("--payMode--", successData.CardType);
                        description = description.Replace("--amount--", successData.Amount.ToString());
                        description = description.Replace("--bankStatus--", successData.Status);
                        description = description.Replace("--transactionRefNo--", successData.UniqueRefNo);


                        //flattenedPDFDownloadDetails.PdfData = HtmlToPdfConverter.ExportToPDF(description, 600, 1000);
                        flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);

                        flattenedPDFDownloadDetails.FileType = "application/octet-stream";
                        flattenedPDFDownloadDetails.FileName = "CollegeLeavingCertificateAcknowledgement.pdf";

                        return flattenedPDFDownloadDetails;
                    }
                    else
                    {
                        string status = "Uploaded";
                        string amount = "-NA-";
                        string bankStatus = "Payment Pending";
                        string transactionRefNo = "-NA-";
                        string applicationURL = _configuration.GetSection("ApplicationURL").Value;
                        string collegeLogo = _configuration.GetSection("UULogo").Value;

                        if (serviceName != null)
                        {
                            description = description.Replace("--serviceName--", serviceName.ServiceName);
                        }
                        else
                        {
                            description = description.Replace("--serviceName--", "-NA-");
                        }
                        description = description.Replace("--collegeLogo--", applicationURL + collegeLogo);
                        description = description.Replace("--acknowledgementNo--", clcViewDetails.AcknowledgementNo);
                        if (clcViewDetails.CreationTime != null)
                        {
                            DateTime utcdate = DateTime.ParseExact(clcViewDetails.CreationTime.ToString("MM-dd-yyyy HH:mm:ss"), "MM-dd-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                            var istdate = TimeZoneInfo.ConvertTimeFromUtc(utcdate, TZConvert.GetTimeZoneInfo(_configuration.GetSection("TimeZone").Value.ToString()));
                            clcViewDetails.CreationTime = istdate;
                            description = description.Replace("--creationTime--", clcViewDetails.CreationTime.ToString());
                        }
                        else
                        {
                            description = description.Replace("--creationTime--", "-NA-");
                        }
                        description = description.Replace("--courseType--", clcViewDetails.CourseTypeString);
                        if (clcViewDetails.RegistrationNumber != null)
                        {
                            description = description.Replace("--registratioNumber--", clcViewDetails.RegistrationNumber);
                        }
                        else
                        {
                            description = description.Replace("--registratioNumber--", "-NA-");
                        }

                        description = description.Replace("--studentName--", clcViewDetails.StudentName);
                        description = description.Replace("--dob--", clcViewDetails.DateOfBirth.ToString("dd/MM/yyyy"));

                        //document section
                        description = description.Replace("--docName--", clcViewDetails.FileName);
                        description = description.Replace("--status--", status);

                        //address section 
                        description = description.Replace("--address--", clcViewDetails.Address);
                        description = description.Replace("--city--", clcViewDetails.CityString);
                        description = description.Replace("--state--", clcViewDetails.StateString);
                        description = description.Replace("--country--", clcViewDetails.CountryString);
                        description = description.Replace("--pincode--", clcViewDetails.ZipCode);

                        if (clcViewDetails.IsSameAsPresent)
                        {
                            description = description.Replace("--paddress--", clcViewDetails.Address);
                            description = description.Replace("--pcity--", clcViewDetails.CityString);
                            description = description.Replace("--pstate--", clcViewDetails.StateString);
                            description = description.Replace("--pcountry--", clcViewDetails.CountryString);
                            description = description.Replace("--ppincode--", clcViewDetails.ZipCode);
                        }
                        else
                        {
                            ServiceStudentAddress permanentAddress = _MSUoW.ExaminationFormFillUpRepository.GetStudentAddress(_MSCommon.GetEnumDescription(enAddressType.Permanent), clcViewDetails.Id).FirstOrDefault();
                            if (permanentAddress != null)
                            {
                                description = description.Replace("--paddress--", permanentAddress.Address);
                                description = description.Replace("--pcity--", permanentAddress.City);
                                description = description.Replace("--pstate--", permanentAddress.State);
                                description = description.Replace("--pcountry--", permanentAddress.Country);
                                description = description.Replace("--ppincode--", permanentAddress.Zip);
                            }
                            else
                            {
                                description = description.Replace("--paddress--", "-NA-");
                                description = description.Replace("--pcity--", "-NA-");
                                description = description.Replace("--pstate--", "-NA-");
                                description = description.Replace("--pcountry--", "-NA-");
                                description = description.Replace("--ppincode--", "-NA-");
                            }
                        }

                        //exam information
                        description = description.Replace("--collegeName--", clcViewDetails.CollegeString);
                        description = description.Replace("--subject--", clcViewDetails.SubjectString);
                        description = description.Replace("--department--", clcViewDetails.DepartmentString);
                        description = description.Replace("--stream--", clcViewDetails.StreamString);
                        description = description.Replace("--courseCode--", clcViewDetails.CourseString);

                        //transaction information
                        description = description.Replace("--payMode--", "-NA-");
                        description = description.Replace("--amount--", amount);
                        description = description.Replace("--bankStatus--", bankStatus);
                        description = description.Replace("--transactionRefNo--", transactionRefNo);

                        //flattenedPDFDownloadDetails.PdfData = HtmlToPdfConverter.ExportToPDF(description, 600, 1000);
                        flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);
                        flattenedPDFDownloadDetails.FileType = "application/octet-stream";
                        flattenedPDFDownloadDetails.FileName = "CollegeLeavingCertificateAcknowledgement.pdf";

                        return flattenedPDFDownloadDetails;
                    }
                }
                else throw new MicroServiceException() { ErrorCode = "OUU011" };
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU011" };
            }
        }
        #endregion


        public SemesterExaminationNotificationDto GetSemesterExamNotification()
        {
            SemesterExaminationNotificationDto notificationDto = new SemesterExaminationNotificationDto();

            //get all the reference tables
            if (GetRoleListOfCurrentUser().Contains(Convert.ToString((long)enRoles.Student)))
            {
                StudentProfileView studentDetails = _commonAppService.GetLoginStudentData();

                List<ExaminationMaster> examMasterList = _MSUoW.ExaminationMasterRepository.GetServiceWiseExaminationMasterList()
                                                        .Where(mst => mst.CourseType == studentDetails.CourseType
                                                        && mst.StreamCode == studentDetails.StreamCode
                                                        && mst.SubjectCode == studentDetails.SubjectCode)
                                                        .OrderBy(o => o.SemesterCode)
                                                        .ToList();

                List<ExaminationSession> examSessionList = _commonAppService.GetActiveExaminationSessionList()
                                                        .Where(sess => examMasterList.Select(s => s.Id).Contains(sess.ExaminationMasterId)
                                                        && sess.ExamYear == studentDetails.YearOfAdmission)
                                                        .ToList();

                List<ExaminationFormFillUpDates> formFillupDatesList = _MSUoW.ExaminationFormFillUpDates.GetAll()
                                                                    .Where(dt => dt.IsActive && examSessionList.Select(s => s.Id).Contains(dt.ExaminationSessionId))
                                                                    .ToList();

                //check current date is existed on which form fill up dates from which we can get to know for which semester the examination registration is going

                DateTime indianCurrentDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TZConvert.GetTimeZoneInfo("India Standard Time"));
                int counter = 0;
                formFillupDatesList.ForEach(ffd =>
                {
                    ffd.StartDate = TimeZoneInfo.ConvertTimeFromUtc(ffd.StartDate, TZConvert.GetTimeZoneInfo("India Standard Time"));
                    ffd.EndDate = TimeZoneInfo.ConvertTimeFromUtc(ffd.EndDate, TZConvert.GetTimeZoneInfo("India Standard Time"));

                    string duration = string.Empty; 
                    long serviceId = 0;
                    string examType = string.Empty;
                    bool isNotificationDataCreate = false;

                    if (indianCurrentDate.Date >= ffd.StartDate.Date && indianCurrentDate.Date <= ffd.EndDate.Date)
                    {
                        serviceId = examMasterList[0].ServiceMaster.Id;

                        duration = examMasterList.Where(exm => exm.Id == ffd.ExaminationMasterId).FirstOrDefault().SemesterCode;

                        switch (serviceId)
                        {
                            case (long)enServiceType.BCABBAExamApplicationForm:
                                examType = "Semester";
                                //check which years/semester data
                                if (duration != _MSCommon.GetEnumDescription(EnSemester.FirstSemester))
                                {
                                    isNotificationDataCreate = true;
                                }
                                break;

                            case (long)enServiceType.NursingApplicationForm:
                                examType = "Year";
                                //check which years/semester data
                                if (duration != _MSCommon.GetEnumDescription(enNursingYear.FirstYear))
                                {
                                    isNotificationDataCreate = true;
                                }
                                break;
                            default:
                                break;
                        }

                        if (counter == 0 && isNotificationDataCreate)
                        {
                            notificationDto.ExaminationMasterId = ffd.ExaminationMasterId;
                            notificationDto.ExaminationSessionId = ffd.ExaminationSessionId;
                            notificationDto.Semester = examMasterList.Where(em => em.Id == ffd.ExaminationMasterId).FirstOrDefault().SemesterCode;
                            notificationDto.NotificationText = "Click here to form fill up " + _MSCommon.AddOrdinal(Convert.ToInt32(notificationDto.Semester)) + " " + examType;
                            counter = 1;
                        }

                    }
                });
            }        
           
            return notificationDto;
        }

        private List<ServiceStatusDto> GetNursingServiceStatus(UserServiceApplied userServiceAppliedObj)
        {
            List<ServiceStatusDto> serviceStatusList = new List<ServiceStatusDto>();
            //var recordSubmissionState = _configuration.GetSection("ExamFormFillup:RegistrationNumberInitialHistoryState").Value;

            var universityExamRecord = _MSUoW.NursingFormfillupDetails.FindBy(data => data.Id == userServiceAppliedObj.InstanceId
                                && data.CreatorUserId == GetCurrentUserId() && data.IsActive)
                                .FirstOrDefault();

            // Get the appsetting value for creating object according to the stateStatusType with ascending order 
            var appSettingWorkflowStateNames = _configuration.GetSection("NursingFormFillup:FormStates").Value.Split(',');

            if (universityExamRecord != null)
            {// Get the workflowtransactionHistory data from table in ascending order of their creation time(the way they are created in db)
                List<WorkflowTransactionHistory> workflowTransactionHistoryObj = _workflowDbContext.WorkflowTransactionHistory
                                                                            .Where(dd => dd.InstanceId == userServiceAppliedObj.InstanceId
                                                                            && dd.TenantWorkflowId == universityExamRecord.TenantWorkflowId && dd.IsActive).OrderBy(d => d.CreationTime)
                                                                            .ToList();

                // No need to find distinct as one state can be reached multiple times
                var workflowCompletdStates = workflowTransactionHistoryObj.Select(obj => obj.TransitState).ToList();

                if (workflowCompletdStates.Count > 0)
                {
                    workflowCompletdStates.ForEach(state =>
                    {
                        // Completed State Data
                        serviceStatusList.Add(new ServiceStatusDto()
                        {
                            Status = state,
                            StatusType = enStatusType.Completed,
                            StatusDate = workflowTransactionHistoryObj.Where(history => history.TransitState == state).OrderByDescending(obj => obj.CreationTime).First().CreationTime.Date.ToString("dd-MM-yyyy")
                        });
                    });

                    // Latest Completed Data / Present data
                    serviceStatusList[serviceStatusList.Count - 1].StatusType = enStatusType.Present;

                    // Find if last state is in the workflow states
                    var indexOfLastStateOccured = Array.IndexOf(appSettingWorkflowStateNames, serviceStatusList[serviceStatusList.Count - 1].Status);

                    if (indexOfLastStateOccured != -1)
                    {
                        appSettingWorkflowStateNames = appSettingWorkflowStateNames.Skip(indexOfLastStateOccured + 1).ToArray();
                    }

                    if (serviceStatusList[serviceStatusList.Count() - 1].Status != _configuration.GetSection("NursingFormFillup:ExamFormFillUpRejectedStatus").Value)
                    {
                        foreach (var state in appSettingWorkflowStateNames)
                        {
                            serviceStatusList.Add(new ServiceStatusDto()
                            {
                                Status = state,
                                StatusType = enStatusType.Future,
                                StatusDate = "Yet to reach"
                            });
                        }
                    }
                }
                else
                {
                    foreach (string state in appSettingWorkflowStateNames)
                    {
                        serviceStatusList.Add(new ServiceStatusDto()
                        {
                            Status = state,
                            StatusType = enStatusType.Future,
                            StatusDate = "Yet to reach"
                        });
                    }
                }
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU057" };
            }
            return serviceStatusList;
        }

        public PDFDownloadFile DownloadAcknowledgementForNursingFormFillUp(DownloadParamDto param)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.NursingExamFormFillUp && s.ServiceType == param.DocType && s.IsActive == true).FirstOrDefault();
            if (templateData != null)
            {
                string description = templateData.Body;
                long currentUserId = GetCurrentUserId();
                NursingFormfillupDetails acknowledgementViewDetails = _MSUoW.NursingFormfillupDetails.FindBy(usr => usr.CreatorUserId == currentUserId && usr.IsActive).OrderByDescending(a => a.CreationTime).FirstOrDefault();
                IQueryable<ExaminationPaperDetail> examPaperIds = _MSUoW.ExaminationPaperDetail.FindBy(e => e.IsActive && e.ExaminationFormFillUpDetailsId == acknowledgementViewDetails.Id);
                List<ExaminationPaperMaster> examPapers = _MSUoW.ExaminationPaperMaster.FindBy(a => examPaperIds.Select(b => b.ExaminationPaperId).Contains(a.Id)).ToList();
                List<TransactionLog> transactionDetailsList = _MSUoW.TransactionLog.FindBy(trs => trs.RequestId == Convert.ToString(acknowledgementViewDetails.Id) && trs.IsActive).ToList();
                List<Documents> documents = _MSUoW.Documents.GetAll().Where(x => x.IsActive && x.ParentSourceId == acknowledgementViewDetails.Id).ToList();
                Documents photoDocs = documents.Find(x => x.DocumentType == "EDU-01");
                Documents signDocs = documents.Find(x => x.DocumentType == "EDU-02");
                if (acknowledgementViewDetails != null) // && acknowledgementViewDetails.AcknowledgementNo != null && !string.IsNullOrWhiteSpace(acknowledgementViewDetails.AcknowledgementNo)
                {
                    TransactionLog successData = transactionDetailsList.Where(data => data.IsActive && data.Status != _MSCommon.GetEnumDescription(enTransaction.Cancelled)).OrderByDescending(data => data.CreationTime).FirstOrDefault();
                    ServiceMaster serviceName = _MSUoW.ServiceMaster.FindBy(name => name.Id == param.DocType && name.IsActive).FirstOrDefault();
                    if (successData != null)
                    {
                        string status = "Uploaded";
                        string applicationURL = _configuration.GetSection("ApplicationURL").Value;
                        string collegeLogo = _configuration.GetSection("UULogo").Value;
                        if (serviceName != null)
                        {
                            description = description.Replace("--serviceName--", serviceName.ServiceName);
                        }
                        else
                        {
                            description = description.Replace("--serviceName--", "-NA-");
                        }
                        description = description.Replace("--collegeLogo--", applicationURL + collegeLogo);
                        description = description.Replace("--acknowledgementNo--", acknowledgementViewDetails.AcknowledgementNo);
                        if (acknowledgementViewDetails.CreationTime != null)
                        {
                            DateTime utcdate = DateTime.ParseExact(acknowledgementViewDetails.CreationTime.ToString("MM-dd-yyyy HH:mm:ss"), "MM-dd-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                            var istdate = TimeZoneInfo.ConvertTimeFromUtc(utcdate, TZConvert.GetTimeZoneInfo(_configuration.GetSection("TimeZone").Value.ToString()));
                            acknowledgementViewDetails.CreationTime = istdate;
                            description = description.Replace("--creationTime--", acknowledgementViewDetails.CreationTime.ToString());
                        }
                        else
                        {
                            description = description.Replace("--creationTime--", "-NA-");
                        }
                        description = description.Replace("--courseType--", acknowledgementViewDetails.CourseTypeCodeString);
                        if (acknowledgementViewDetails.RegistrationNumber != null)
                        {
                            description = description.Replace("--registratioNumber--", acknowledgementViewDetails.RegistrationNumber);
                        }
                        else
                        {
                            description = description.Replace("--registratioNumber--", "-NA-");
                        }

                        description = description.Replace("--studentName--", acknowledgementViewDetails.StudentName);
                        description = description.Replace("--dob--", acknowledgementViewDetails.DateOfBirth.ToString("dd/MM/yyyy"));
                        description = description.Replace("--gender--", acknowledgementViewDetails.GenderString);
                        description = description.Replace("--nationality--", acknowledgementViewDetails.NationalityString);
                        description = description.Replace("--caste--", acknowledgementViewDetails.CasteString);
                        description = description.Replace("--disability--", acknowledgementViewDetails.IsPwdString);

                        //document section
                        description = description.Replace("--docPhotoName--", photoDocs.FileName);
                        description = description.Replace("--status--", status);
                        description = description.Replace("--docSignName--", signDocs.FileName);
                        description = description.Replace("--status--", status);

                        //address section 
                        description = description.Replace("--address--", acknowledgementViewDetails.PresentAddress);
                        description = description.Replace("--city--", acknowledgementViewDetails.PresentCity);
                        description = description.Replace("--state--", acknowledgementViewDetails.PresentState);
                        description = description.Replace("--country--", acknowledgementViewDetails.PresentCountry);
                        description = description.Replace("--pincode--", acknowledgementViewDetails.PresentZip);

                        //if (acknowledgementViewDetails.IsSameAsPresent)
                       // {
                            description = description.Replace("--paddress--", acknowledgementViewDetails.PermanentAddress);
                            description = description.Replace("--pcity--", acknowledgementViewDetails.PermanentCity);
                            description = description.Replace("--pstate--", acknowledgementViewDetails.PermanentState);
                            description = description.Replace("--pcountry--", acknowledgementViewDetails.PermanentCountry);
                            description = description.Replace("--ppincode--", acknowledgementViewDetails.PermanentZip);
                        //}
                        //else
                        //{
                        //    ServiceStudentAddress permanentAddress = _MSUoW.ExaminationFormFillUpRepository.GetStudentAddress(_MSCommon.GetEnumDescription(enAddressType.Permanent), acknowledgementViewDetails.Id).FirstOrDefault();
                        //    if (permanentAddress != null)
                        //    {
                        //        description = description.Replace("--paddress--", permanentAddress.Address);
                        //        description = description.Replace("--pcity--", permanentAddress.City);
                        //        description = description.Replace("--pstate--", permanentAddress.State);
                        //        description = description.Replace("--pcountry--", permanentAddress.Country);
                        //        description = description.Replace("--ppincode--", permanentAddress.Zip);
                        //    }
                        //    else
                        //    {
                        //        description = description.Replace("--paddress--", "-NA-");
                        //        description = description.Replace("--pcity--", "-NA-");
                        //        description = description.Replace("--pstate--", "-NA-");
                        //        description = description.Replace("--pcountry--", "-NA-");
                        //        description = description.Replace("--ppincode--", "-NA-");
                        //    }
                        //}

                        //educational qualificateion section
                        description = description.Replace("--matriculationExam--", acknowledgementViewDetails.YearOfMatriculation.ToString());
                        description = description.Replace("--premedicalExamYear--", acknowledgementViewDetails.YearOfPassingPreMedical.ToString());

                        if (acknowledgementViewDetails.SubjectCode == "64")
                        {
                            description = description.Replace("--isShowGnm--", "");
                            description = description.Replace("--isShowPremedical--", "style=\"display: none;\"");
                            description = description.Replace("--isGnmPassed--", acknowledgementViewDetails.IsGnmQualified ? "Yes" : "No");
                        }
                        if (acknowledgementViewDetails.SubjectCode == "65")
                        {
                            description = description.Replace("--isShowPremedical--", "");
                            description = description.Replace("--isShowGnm--", "style=\"display: none;\"");
                            description = description.Replace("--isHscQualified--", acknowledgementViewDetails.IsHscQualified ? "Yes" : "No");
                            description = description.Replace("--hscPercentage--", acknowledgementViewDetails.HscPercentage.ToString());
                        }

                        //exam information
                        description = description.Replace("--collegeName--", acknowledgementViewDetails.CollegeCodeString);
                        description = description.Replace("--subject--", acknowledgementViewDetails.SubjectCodeString);
                        description = description.Replace("--department--", acknowledgementViewDetails.DepartmentCodeString != null ? acknowledgementViewDetails.DepartmentCodeString : "-NA-");
                        description = description.Replace("--academicSession--", acknowledgementViewDetails.AcademicStart.ToString());
                        description = description.Replace("--stream--", acknowledgementViewDetails.StreamCodeString);
                        description = description.Replace("--courseCode--", acknowledgementViewDetails.CourseCodeString != null ? acknowledgementViewDetails.DepartmentCodeString : "-NA-");
                        description = description.Replace("--semester--", acknowledgementViewDetails.CurrentAcademicYearString);

                        //transaction information
                        description = description.Replace("--payMode--", successData.CardType);
                        description = description.Replace("--amount--", successData.Amount.ToString());
                        description = description.Replace("--bankStatus--", successData.Status);
                        description = description.Replace("--transactionRefNo--", successData.UniqueRefNo);

                        //paper details
                        if (examPapers != null && examPapers.Count() > 0)
                        {
                            var stringtoappend = "";

                            examPapers.ForEach(sub =>
                            {

                                if (sub.IsParctical == true)
                                {
                                    stringtoappend = stringtoappend + " " + sub.PaperName + " - ( T & P )" + " ,";
                                }
                                else
                                {
                                    stringtoappend = stringtoappend + " " + sub.PaperName + " - ( T )" + " ,";
                                }

                            });
                            stringtoappend = stringtoappend.Remove(stringtoappend.Length - 1);

                            description = description.Replace("--papers--", stringtoappend);
                        }
                        else
                        {
                            description = description.Replace("--papers--", "-NA-");
                        }


                        //flattenedPDFDownloadDetails.PdfData = HtmlToPdfConverter.ExportToPDF(description, 600, 1000);
                        flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);



                        flattenedPDFDownloadDetails.FileType = "application/octet-stream";
                        flattenedPDFDownloadDetails.FileName = "NursingFormFillUpAcknowledgement.pdf";

                        return flattenedPDFDownloadDetails;
                    }
                    else
                    {
                        string status = "Uploaded";
                        string bankName = "-NA-";
                        string amount = "-NA-";
                        string bankStatus = "Payment Pending";
                        string transactionRefNo = "-NA-";
                        string applicationURL = _configuration.GetSection("ApplicationURL").Value;
                        string collegeLogo = _configuration.GetSection("UULogo").Value;

                        if (serviceName != null)
                        {
                            description = description.Replace("--serviceName--", serviceName.ServiceName);
                        }
                        else
                        {
                            description = description.Replace("--serviceName--", "-NA-");
                        }
                        description = description.Replace("--collegeLogo--", applicationURL + collegeLogo);
                        description = description.Replace("--acknowledgementNo--", acknowledgementViewDetails.AcknowledgementNo);
                        if (acknowledgementViewDetails.CreationTime != null)
                        {
                            DateTime utcdate = DateTime.ParseExact(acknowledgementViewDetails.CreationTime.ToString("MM-dd-yyyy HH:mm:ss"), "MM-dd-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                            var istdate = TimeZoneInfo.ConvertTimeFromUtc(utcdate, TZConvert.GetTimeZoneInfo(_configuration.GetSection("TimeZone").Value.ToString()));
                            acknowledgementViewDetails.CreationTime = istdate;
                            description = description.Replace("--creationTime--", acknowledgementViewDetails.CreationTime.ToString());
                        }
                        else
                        {
                            description = description.Replace("--creationTime--", "-NA-");
                        }
                        description = description.Replace("--courseType--", acknowledgementViewDetails.CourseTypeCodeString);
                        if (acknowledgementViewDetails.RegistrationNumber != null)
                        {
                            description = description.Replace("--registratioNumber--", acknowledgementViewDetails.RegistrationNumber);
                        }
                        else
                        {
                            description = description.Replace("--registratioNumber--", "-NA-");
                        }

                        description = description.Replace("--studentName--", acknowledgementViewDetails.StudentName);
                        //description = description.Replace("--fatherName--", acknowledgementViewDetails.FatherName);
                        description = description.Replace("--dob--", acknowledgementViewDetails.DateOfBirth.ToString("dd/MM/yyyy"));
                        description = description.Replace("--gender--", acknowledgementViewDetails.GenderString);
                        description = description.Replace("--nationality--", acknowledgementViewDetails.NationalityString);
                        description = description.Replace("--caste--", acknowledgementViewDetails.CasteString);
                        //description = description.Replace("--guardianName--", acknowledgementViewDetails.GuardianName);
                        description = description.Replace("--disability--", acknowledgementViewDetails.IsPwdString);

                        //document section
                        description = description.Replace("--docPhotoName--", photoDocs.FileName);
                        description = description.Replace("--status--", status);
                        description = description.Replace("--docSignName--", signDocs.FileName);
                        description = description.Replace("--status--", status);

                        //address section 
                        description = description.Replace("--address--", acknowledgementViewDetails.PresentAddress);
                        description = description.Replace("--city--", acknowledgementViewDetails.PresentCity);
                        description = description.Replace("--state--", acknowledgementViewDetails.PresentState);
                        description = description.Replace("--country--", acknowledgementViewDetails.PresentCountry);
                        description = description.Replace("--pincode--", acknowledgementViewDetails.PresentZip);

                        //if (acknowledgementViewDetails.IsSameAsPresent)
                        //{
                            description = description.Replace("--paddress--", acknowledgementViewDetails.PermanentAddress);
                            description = description.Replace("--pcity--", acknowledgementViewDetails.PermanentCity);
                            description = description.Replace("--pstate--", acknowledgementViewDetails.PermanentState);
                            description = description.Replace("--pcountry--", acknowledgementViewDetails.PermanentCountry);
                            description = description.Replace("--ppincode--", acknowledgementViewDetails.PermanentZip);
                        //}
                        //else
                        //{
                        //    ServiceStudentAddress permanentAddress = _MSUoW.ExaminationFormFillUpRepository.GetStudentAddress(_MSCommon.GetEnumDescription(enAddressType.Permanent), acknowledgementViewDetails.Id).FirstOrDefault();
                        //    if (permanentAddress != null)
                        //    {
                        //        description = description.Replace("--paddress--", permanentAddress.Address);
                        //        description = description.Replace("--pcity--", permanentAddress.City);
                        //        description = description.Replace("--pstate--", permanentAddress.State);
                        //        description = description.Replace("--pcountry--", permanentAddress.Country);
                        //        description = description.Replace("--ppincode--", permanentAddress.Zip);
                        //    }
                        //    else
                        //    {
                        //        description = description.Replace("--paddress--", "-NA-");
                        //        description = description.Replace("--pcity--", "-NA-");
                        //        description = description.Replace("--pstate--", "-NA-");
                        //        description = description.Replace("--pcountry--", "-NA-");
                        //        description = description.Replace("--ppincode--", "-NA-");
                        //    }
                        //}

                        //educational qualificateion section
                        description = description.Replace("--matriculationExam--", acknowledgementViewDetails.YearOfMatriculation.ToString());
                        description = description.Replace("--premedicalExamYear--", acknowledgementViewDetails.YearOfPassingPreMedical.ToString());

                        //eligibility Criteria
                        if (acknowledgementViewDetails.SubjectCode == "64")
                        {
                            description = description.Replace("--isShowGnm--", "");
                            description = description.Replace("--isShowPremedical--", "style=\"display: none;\"");
                            description = description.Replace("--isGnmPassed--", acknowledgementViewDetails.IsGnmQualified ? "Yes" : "No");
                        }
                        if (acknowledgementViewDetails.SubjectCode == "65")
                        {
                            description = description.Replace("--isShowPremedical--", ""); 
                            description = description.Replace("--isShowGnm--", "style=\"display: none;\"");
                            description = description.Replace("--isHscQualified--", acknowledgementViewDetails.IsHscQualified ? "Yes" : "No");
                            description = description.Replace("--hscPercentage--", acknowledgementViewDetails.HscPercentage.ToString());
                        }
                        //exam information
                        description = description.Replace("--collegeName--", acknowledgementViewDetails.CollegeCodeString);
                        description = description.Replace("--subject--", acknowledgementViewDetails.SubjectCodeString);
                        description = description.Replace("--department--", acknowledgementViewDetails.DepartmentCodeString != null ? acknowledgementViewDetails.DepartmentCodeString : "-NA-");
                        description = description.Replace("--academicSession--", acknowledgementViewDetails.AcademicStart.ToString());
                        description = description.Replace("--stream--", acknowledgementViewDetails.StreamCodeString);
                        description = description.Replace("--courseCode--", acknowledgementViewDetails.CourseCodeString != null ? acknowledgementViewDetails.CourseCodeString : "-NA-");
                        description = description.Replace("--semester--", acknowledgementViewDetails.CurrentAcademicYearString);

                        //transaction information
                        description = description.Replace("--payMode--", bankName);
                        description = description.Replace("--amount--", amount);
                        description = description.Replace("--bankStatus--", bankStatus);
                        description = description.Replace("--transactionRefNo--", transactionRefNo);

                        //paper details
                        if (examPapers != null && examPapers.Count() > 0)
                        {
                            var stringtoappend = "";

                            examPapers.ForEach(sub =>
                            {

                                if (sub.IsParctical == true)
                                {
                                    stringtoappend = stringtoappend + " " + sub.PaperName + " - ( T & P )" + " ,";
                                }
                                else
                                {
                                    stringtoappend = stringtoappend + " " + sub.PaperName + " - ( T )" + " ,";
                                }

                            });
                            stringtoappend = stringtoappend.Remove(stringtoappend.Length - 1);

                            description = description.Replace("--papers--", stringtoappend);
                        }
                        else
                        {
                            description = description.Replace("--papers--", "-NA-");
                        }

                        //flattenedPDFDownloadDetails.PdfData = HtmlToPdfConverter.ExportToPDF(description, 600, 1000);
                        flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);
                        flattenedPDFDownloadDetails.FileType = "application/octet-stream";
                        flattenedPDFDownloadDetails.FileName = "NursingFormFillUpAcknowledgement.pdf";

                        return flattenedPDFDownloadDetails;
                    }
                }
                else throw new MicroServiceException() { ErrorCode = "OUU011" };
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU011" };
            }
        }

        public List<KeyValuePair<string, string>> GetRoleWiseReportMenu()
        {
            List<KeyValuePair<string, string>> menuList =new List<KeyValuePair<string, string>>();

            //get current role
            if(GetUserType()==(long)enUserType.Principal || GetUserType()== (long)enUserType.HOD)
            {
                //find service that the principal linked with by college code
                List<ServiceMaster> serviceMastersList = _MSUoW.ServiceMasterRepository.GetServiceMaster(GetCollegeCode()).ToList();

                if(serviceMastersList.Count>0)
                {
                    serviceMastersList.ForEach(srvc =>
                    {
                        switch(srvc.Id)
                        {
                            case (long)enServiceType.BCABBAExamApplicationForm:
                                menuList.Add(new KeyValuePair<string, string>("Account Statement", "services/statement-report"));
                                menuList.Add(new KeyValuePair<string, string>("Semester Marks", "services/semester-mark-report"));
                                menuList.Add(new KeyValuePair<string, string>("Paper Wise Marks", "services/paper-wise-marks-report"));
                                break;

                            case (long)enServiceType.NursingApplicationForm:
                                menuList.Add(new KeyValuePair<string, string>("Account Statement", "services/nursing-statement-report"));
                                break;

                        }
                    });
                }
            }
            return menuList;
        }

    }
}
