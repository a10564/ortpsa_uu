﻿using Microservice.Common;
using Microservice.Common.Abstract;
using MicroService.Business.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Entity.Lookup;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Exception;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

namespace MicroService.Business
{
    public class SupportClass : BaseService, ISupportClass
    {
        private IMicroServiceUOW _MSUoW;
        private IMicroserviceCommon _MSCommon;
        private ICommonAppService _commonAppService;
        private IExaminationFormFillUpService _examinationFormFillUpService;
        private IConfiguration _IConfiguration;
        private INursingFormFillUpService _nursingFormFillUpService;
        public SupportClass(IMicroServiceUOW MSUoW, IRedisDataHandlerService redisData, 
            IMicroserviceCommon MSCommon,
             IConfiguration IConfiguration,
            ICommonAppService commonAppService,
            IExaminationFormFillUpService examinationFormFillUpService,
             INursingFormFillUpService nursingFormFillUpService) : base(redisData)
        {
            _MSUoW = MSUoW;
            _MSCommon = MSCommon;
            _commonAppService = commonAppService;
            _examinationFormFillUpService = examinationFormFillUpService;
            _IConfiguration = IConfiguration;
            _nursingFormFillUpService = nursingFormFillUpService;
        }

        #region Get All Service details 
        public List<ServiceMaster> GetAllServiceDetails()
        {
           return _MSUoW.ServiceMaster.GetAll().Where(w => w.IsActive && w.IsPayOnlyServiceCharges).ToList();
        }
        #endregion
        #region get all student details 
        
        /// <summary>
        /// Author  :       Bikash ku sethi
        /// Date    :      14-09-2021
        /// Description :   This methode is used to get all student details for current service
        /// </summary>
        /// <param name="regNo"></param>
        /// <returns></returns>
        /// 
        public IQueryable<ServiceWiseStudentDetailsDto> ServiceWiseStudentDetail()
        {
            long[] lookupList = new long[] { (long)enLookUpType.Semester };
            IQueryable<ServiceWiseStudentDetailsDto> studentdetails = _MSUoW.ExaminationFormFillUpDetails.GetAll()
                .Join(_MSUoW.CollegeMaster.GetAll(), form => form.CollegeCode, col => col.LookupCode, (form, col) => new { form, col }) 
                .Join(_commonAppService.GetLookupData(lookupList), ppc => ppc.form.SemesterCode, c => c.LookupCode, (ppc, c) => new { ppc, c })
                .Where(examForm => examForm.c.IsActive == true && examForm.ppc.form.IsActive == true)
                .Select(data=>new ServiceWiseStudentDetailsDto () 
                {
                    Id = data.ppc.form.Id,
                    StudentName = data.ppc.form.StudentName,
                    FatherName = data.ppc.form.FatherName,
                    GuardianName = data.ppc.form.GuardianName,
                    DateOfBirth = data.ppc.form.DateOfBirth.ToString("dd-MM-yyyy"),
                    RegistrationNumber = data.ppc.form.RegistrationNumber,
                    RollNumber = data.ppc.form.RollNumber,
                    AcknowledgementNo = data.ppc.form.AcknowledgementNo,
                    PaymentDate = data.ppc.form.PaymentDate != null ? ((DateTime)data.ppc.form.PaymentDate).ToString("dd-MM-yyyy") : null,                   
                    CreationTime = data.ppc.form.CreationTime,
                    CollegeCode = data.ppc.form.CollegeCode,
                    CollegeName=data.ppc.col.LookupDesc,
                    FormFillUpAmount = data.ppc.form.FormFillUpMoneyToBePaid,
                    LateFeeAmount = data.ppc.form.LateFeeAmount,  //only for this phase it will modify next session
                    Semester=data.c.LookupDesc
                });           
               return studentdetails;
        }
        #endregion

        /// <summary>
        /// Author  :       Bikash ku sethi
        /// Date    :      14-09-2021
        /// Description :   This methode is used to get all student details for current service
        /// </summary>
        /// <param name="regNo"></param>
        /// <returns></returns>
        /// 
        public List<CollegeMaster> GetAllCollegeListData() 
        {
            return _commonAppService.GetAllGetCollegeMasterListDataFromCache().OrderBy(o=>o.LookupDesc).ToList();
        }


        public List<ServiceWiseStudentDetailsDto> GetAllCollegeWiseCandidateFeeReport(SearchParamDto param)
        {
            List<ServiceWiseStudentDetailsDto> applicantList = new List<ServiceWiseStudentDetailsDto>();
            if (param==null)
            {
                throw new MicroServiceException() { ErrorMessage = "Invalid data received." };
            }

            if (param.CourseTypeCode == _MSCommon.GetEnumDescription(enCourseType.Ug))
            {
                //get the state
                //List<string> includedStateList = new List<string>();
                //List<string> formStates = _IConfiguration.GetSection("ExamFormFillup:FormStates").Value.Trim().Split(",").ToList();
                //List<string> removeStateList = new List<string>();
                //string paymentPendingSteate = _IConfiguration.GetSection("ExamFormFillup:PaymentPendingState").Value.Trim();
                //string verificationPendingState = _IConfiguration.GetSection("ExamFormFillup:VerificationPending").Value.Trim();
                //string rejectedState = _IConfiguration.GetSection("ExamFormFillup:ExamFormFillUpRejectedStatus").Value.Trim();
                //removeStateList.Add(paymentPendingSteate);
                //removeStateList.Add(verificationPendingState);
                //removeStateList.Add(rejectedState);
                //includedStateList = formStates.Except(removeStateList).ToList();
                switch ((long)param.ServiceId)
                {
                    case (long)enServiceType.BCABBAExamApplicationForm:
                        applicantList = _examinationFormFillUpService.GetAllAcademicStatementList()
                                        .Where(aca => aca.IsActive && aca.CourseTypeCode == param.CourseTypeCode
                                        && param.CollegeCode.Contains(aca.CollegeCode)
                                        && aca.StreamCode == param.StreamCode
                                        && aca.SemesterCode == param.SemesterCode
                                        //&& includedStateList.Contains(aca.State)
                                        && aca.AcademicStart == param.StartYear
                                        && aca.SubjectCode == param.SubjectCode
                                        && aca.State == param.State)
                                        .Select(data => new ServiceWiseStudentDetailsDto()
                                        {
                                            Id = data.Id,
                                            AcknowledgementNo = data.AcknowledgementNo,
                                            CollegeCode = data.CollegeCode,
                                            StudentName = data.StudentName,
                                            FormFillUpAmount = data.FormFillUpMoneyToBePaid,
                                            LateFeeAmount = data.TotalLateFeeAmount,
                                            CollegeName = data.CollegeName,
                                            State = data.State,
                                            TotalAmountToPaid=data.FormFillUpMoneyToBePaid- data.CentreCharges
                                        })
                                        .OrderBy(o => o.StudentName)
                                        .ToList();
                        break;

                    case (long)enServiceType.NursingApplicationForm:

                        applicantList = _nursingFormFillUpService.GetNursingAcademicDetails()
                                        .Where(aca => aca.IsActive && aca.CourseTypeCode == param.CourseTypeCode
                                        && param.CollegeCode.Contains(aca.CollegeCode)
                                        && aca.StreamCode == param.StreamCode
                                        && aca.CurrentAcademicYear == param.CurrentYear
                                        //&& includedStateList.Contains(aca.State)
                                        && aca.AcademicStart == param.StartYear
                                        && aca.SubjectCode == param.SubjectCode
                                        && aca.State == param.State)
                                        .Select(data => new ServiceWiseStudentDetailsDto()
                                        {
                                            Id = data.Id,
                                            AcknowledgementNo = data.AcknowledgementNo,
                                            CollegeCode = data.CollegeCode,
                                            StudentName = data.StudentName,
                                            FormFillUpAmount = data.FormFillUpMoneyToBePaid,
                                            LateFeeAmount = data.LateFeeAmount,
                                            CollegeName = data.CollegeName,
                                            State = data.State,
                                            TotalAmountToPaid = data.FormFillUpMoneyToBePaid - data.CenterCharge
                                        })
                                        .OrderBy(o => o.StudentName)
                                        .ToList();

                        break;
                    default:
                        break;

                } 
                
                    
            }
            return applicantList;
        }


        public DataTable GetReportsCollegeWise(SearchParamDto param)
        {
            DataTable dataTable = new DataTable();
            if (param == null)
            {
                throw new MicroServiceException() { ErrorMessage = "Invalid data received." };
            }

            if (param.State == Constant.MarkUploaded)
            {
                //List<MarkStatementView> markList = new List<MarkStatementView>();
                ////List<MarkStatementView> markList = _examinationFormFillUpService.GetMarkDetailsForL1Support(param);
                //var data = _examinationFormFillUpService.GetExcelSearchParamData(param);
                ////create datatable
                //dataTable = new DataTable("PaperWiseMarkList");

                //data.Columns.ForEach(element =>
                //{
                //    dataTable.Columns.Add(element.ColumnName, typeof(string));
                //});               

                //int counter = 0;
                //DataRow rowList = null;

                //data.Rows.ForEach(row => {

                //    var obj = new Object();
                //    counter = 0;
                //    rowList = dataTable.NewRow();
                //    row.GridRowItems.ForEach((gi) => {

                //        if (gi.IsHidden == false)
                //        {
                //            rowList[data.Columns[counter].ColumnName] = gi.Value;
                //        }                                                
                //        counter++;
                //    });
                //    dataTable.Rows.Add(rowList);
                //});
            }
            else
            {

                //get data using searparam
                List<ServiceWiseStudentDetailsDto> records = GetAllCollegeWiseCandidateFeeReport(param).ToList()
                                                            .OrderBy(s => s.StudentName)
                                                            .ThenBy(s => s.CollegeCode)
                                                            .ToList();


                dataTable = new DataTable("AccountStatementList");

                dataTable.Columns.Add("SlNo", typeof(int));
                dataTable.Columns.Add("StudentName", typeof(string));
                dataTable.Columns.Add("AcknowledgementNo", typeof(string));
                dataTable.Columns.Add("CollegeCode", typeof(string));
                dataTable.Columns.Add("CollegeName", typeof(string));
                dataTable.Columns.Add("FormFillUpAmount", typeof(double));
                dataTable.Columns.Add("LateFeeAmount", typeof(double));
                dataTable.Columns.Add("TotalAmountpaid", typeof(double));
                dataTable.Columns.Add("State", typeof(string));
                int counter = 1;
                if (records.Count > 0)
                {
                    records.ForEach(item =>
                    {
                        var row = dataTable.NewRow();
                        row["SlNo"] = counter++;
                        row["StudentName"] = item.StudentName;
                        row["AcknowledgementNo"] = item.AcknowledgementNo;
                        row["CollegeCode"] = item.CollegeCode;
                        row["CollegeName"] = item.CollegeName;
                        row["FormFillUpAmount"] = item.FormFillUpAmount;
                        row["LateFeeAmount"] = item.LateFeeAmount;
                        row["TotalAmountpaid"] = item.FormFillUpAmount;
                        row["State"] = item.State;
                        dataTable.Rows.Add(row);
                    });
                }
                else
                {
                    throw new MicroServiceException() { ErrorMessage = "No record found." };
                }
            }

            return dataTable;
        }


    }
}
