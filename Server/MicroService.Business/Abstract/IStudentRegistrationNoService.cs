﻿using MicroService.Domain.Models.Dto.StudentDashboard;
using MicroService.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using MicroService.Domain.Models.Views;
using System.Linq;
using MicroService.Domain.Models.Dto.UniversityService;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;

namespace MicroService.Business.Abstract
{
    public interface IStudentRegistrationNoService
    {
        IQueryable<AcknowledgementView> GetRoleBasedActiveRegistrationInboxData();
        IQueryable<AcknowledgementView> GetRoleBasedActiveRegistrationInboxCountData();
        UniversityRegistrationDetailDto UpdateRegistrationNumberApplicationStatus(Guid id, UniversityRegistrationDetailDto input);
        UniversityRegistrationDetailDto UpdateRegistrationNumberApplicationStatusByCompCell(Guid id, UniversityRegistrationDetailDto input);
        UniversityRegistrationDetailDto RegistrationNumberApplicationStatusUpdateByPrincipal(Guid id, UniversityRegistrationDetailDto input);
        List<Documents> GetDocumentDetailsByParentSourceId(Guid parentSourceId);
        UniversityRegistrationDetailDto StudentUniversityRegistrationNumberApply(UniversityRegistrationDetailDto input);
        UniversityRegistrationDetail GetStudentUniversityRegistrationDetailsById(Guid id);
        UniversityRegistrationDetailDto GetStudentUniversityRegistrationDetailsById(Guid id, UniversityRegistrationDetailDto input);
        PDFDownloadFile DownloadStudentDocument(Guid id);
        //IQueryable<UniversityRegistrationDetail> GetRoleSpecificUniversityServiceAppliedData();
        RegistrationNumberPreviewDto RegistrationNumberPreview(Guid id);
        UniversityRegistrationDetailDto UpdateGenerateRegistrationNumber(Guid id, UniversityRegistrationDetailDto param);
        RegistrationNumberPreviewDto ConfirmRegistrationNumberPreview(Guid id, UniversityRegistrationDetailDto param);
        PDFDownloadFile DownloadStudentDocumentNew(Guid id);
        //long PendingWorkflowCount();
        bool SendAcknowledgementEmail(string studentEmailAddress, string studentName, string serviceName, string acknowledgementNo);
		List<string> GetRoleSpecificWorkflowStateList();
        UniversityRegistrationDetailDto UpdateRegistrationNumberApplicationStatusToRejectState(Guid id, UniversityRegistrationDetailDto input);
        PDFDownloadFile DownloadRegistrationNo(certificateDownloadDto obj);
    }
}
