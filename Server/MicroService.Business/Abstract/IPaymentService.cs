﻿//using RuralInfrastructure.Payment;
using Microservice.Common;
using MicroService.Domain.Models.Dto.Payment;
using MicroService.Domain.Models.Dto.StudentDashboard;
using MicroService.Domain.Models.Entity;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Business.Abstract
{
	public interface IPaymentService
	{
		// Task<TransactionLog> Get(string id);
		Tuple<bool, Guid?, int> doOperation(TransactionLog paymentDetail, bool updateServiceTable);
		string GenerateMD5SignatureForPayU(string Rawdata);
		string PreparePGForm(TransactionLog paymentDetails);
		void CreateRequestPayu(TransactionLog inputData);
		TransactionLog GetTransactionLogById(string id);
		bool IsPaymentRequestinValidState(TransactionLog paymentDetail);
		Tuple<PaymentPricingInfoDto, string> GetPricingInfoByService(string id, string ser);
		TransactionLog PrepareDataForPayment(TransactionLog TransactionLog);
		TransactionLog buildPaymentDetail(IFormCollection form, EnPaymentStatus statusType);
		PDFDownloadFile DownloadPaymentReceiptById(string id);
		bool CheckDuplicateTxnId(string txnId, enTransaction statusType);
	}
}
