﻿using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Dto.MigrationService;
using MicroService.Domain.Models.Dto.StudentDashboard;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using System;
using System.Linq;

namespace MicroService.Business.Abstract
{
    public interface IStudentMigrationService
    {
        UniversityMigrationDetailDto StudentMigrationApplied(UniversityMigrationDetailDto input);
        UniversityMigrationDetail GetStudentMigrationDetailById(Guid id);
        UniversityMigrationDetailDto GetStudentUniversityMigrationDetailsById(Guid id, UniversityMigrationDetailDto input);
        IQueryable<MigrationView> GetAllMigrationStudentData();
        UniversityRegistrationDetail GetStudentUniversityRegistrationDetails();
        UniversityMigrationDetailDto UpdateMigrationApplicationStatus(Guid id, UniversityMigrationDetailDto input);
        UniversityMigrationDetailDto MigrationCertificateApplicationStatusUpdateByPrincipal(Guid id, UniversityMigrationDetailDto input);
        UniversityMigrationDetailDto UpdateMigrationApplicationStatusByCompCell(Guid id, UniversityMigrationDetailDto input);
        UniversityMigrationDetailDto UpdateMigrationApplicationStatusToRejectState(Guid id, UniversityMigrationDetailDto input);
        IQueryable<MigrationView> GetRoleBasedActiveRegistrationInboxCountData();
        PDFDownloadFile DownloadMigrationCertificate(certificateDownloadDto param);
        IQueryable<MigrationView> GetRoleBasedActiveMigrationInboxData();
    }
}
