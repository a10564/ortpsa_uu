﻿using MicroService.Domain.Models.Views;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MicroService.Domain.Models.Dto.BackgroundProcess;
using System.Threading.Tasks;
using MicroService.Domain.Models.Dto.StudentDashboard;
using MicroService.Domain.Models.Dto.AdminActivity;
using MicroService.Domain.Models.Entity;
using System.Data;

namespace MicroService.Business.Abstract
{
    public interface IExaminationFormFillUpService
    {
        IQueryable<ExaminationViewExcludeBackgroundProcessTable> GetRoleBasedActiveExaminationInboxData(bool isCountRequired = false);
        ExaminationFormFillUpDetailsDto ApplyExaminationFormFillUp(ExaminationFormFillUpDetailsDto param);
        ExaminationFormFillUpDetailsDto GetExamFormFillUpDetails(Guid id);
        List<string> GetExamFormFillUpState();
        bool AddExamFormDetailsToBackgroundTable(BackgroundProcessDto parameters);       
        List<CollegeWiseAmountForCoeDto> GetCollegeWiseFormFillUpAmount(SearchParamDto param);
        //bool StateWiseMoveToBackground(BackgroundProcessDto param);
        bool SaveToBackGroundTable(BackgroundProcessDto param);
        ExaminationFormFillUpDetailsDto UpdateState(Guid id, ExaminationFormFillUpDetailsDto examFormDto);
        ExaminationFormFillUpDetailsDto RejectedByPrincipal(Guid id, ExaminationFormFillUpDetailsDto examFormDto);
        ExaminationFormFillUpDetailsDto VerificationByPrincipal(Guid id, ExaminationFormFillUpDetailsDto input);
        ExaminationFormFillUpDetailsDto PaymentCompleted(Guid id, ExaminationFormFillUpDetailsDto input);
        List<int> GetAcademicStartYear();
        PDFDownloadFile DownloadAdmitCard(certificateDownloadDto obj);
        ExaminationFormFillUpDetails GetFirstSemeRecordForLoginUser(string studentName, string dob, int academicStart, string courseType, string collegeCode, string courseCode, string streamCode, string departmentCode, string subjectCode, string semesterCode);
        IQueryable<AcademicStatementView> GetAllAcademicStatementList();
        IQueryable<AcademicStatementView> GetAllAcademicStatementList(SearchParamDto param);

        #region MARK ENTRY APIS
        bool AddExamFormFillUpMark(List<ExaminationFormFillUpSemesterMarksDto> input);
        List<ExaminationFormFillUpSemesterMarksDto> GetExamFormFillUpMark(Guid id,string ackNo);
        #endregion
        SemesterAndStudentDetailsDto GetSemesterPaperDetails(Guid Id, string ack);
        bool AddMarkDetailsToBackgroundTable(BackgroundProcessDto parameters);
        SemesterFilterDto GetSemesterFilterObject(SearchParamDto input);
        IQueryable<MarkStatementView> GetAllMarkDdetails();
        IQueryable<ExaminationFormFillUpSemesterMarksDto> GetStudentPaperAndMarkDetails(Guid Id, string ack);
        List<MarkStatementView> GetMarkDetailsForL1Support(SearchParamDto input);
        //GridModelExcelDto GetExcelSearchParamData(SearchParamDto input);
        PDFDownloadFile DownloadSemesterExamPdfReport(SearchParamDto input);
        ExaminationFormFillUpDetailsDto GetExamFormFillUpDetailsByRollNo(string rollNo);
        IQueryable<ExaminationPaperWiseMarkDto> GetAllPaperWiseMark();
        PDFDownloadFile DownloadPaperWiseMarkReport(SearchParamDto input);
        List<TermEndMarkDto> GetAllStudentByCollegeAndSubject(SearchParamDto param);
        bool SaveTermEndMarkAllStudentByCollege(List<TermEndMarkDto> input);
    }
}
