﻿using MicroService.Domain.Models.Dto.ApplyProvisionalCertificateDetail;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Business.Abstract
{
    public interface IApplyProvisionalCertificateService
    {
        ProvisionalCertificateApplicationDetailDto ApplyForProvisionalCertificate(ProvisionalCertificateApplicationDetailDto input);
    }
}
