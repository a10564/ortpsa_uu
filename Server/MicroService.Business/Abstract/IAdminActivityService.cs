﻿using MicroService.Domain.Models.Dto.AdminActivity;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Business.Abstract
{
    public interface IAdminActivityService
    {
        List<ExamFormFillupDatesDto> GetExamSlotsAndDates(ExamDateFilterPropertiesDto filterOptions);
        List<ExamFormFillupDatesDto> UpdateExamSlotsAndDates(PropertiesForDateScheduleDto input);
        List<ExamFormFillupDatesDto> AddExamSlotsAndDates(PropertiesForDateScheduleDto input);
        List<CollegeAndAllocatedCenterNames> GetExamCentersAllocatedToColleges(ExamDateFilterPropertiesDto filterOptions);
        List<CollegeAndAllocatedCenterNames> AllocateCenters(PropertiesToAllocateCentersDto filterparams);
    }
}
