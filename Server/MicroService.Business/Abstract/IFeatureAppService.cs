﻿using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.Feature;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.Business.Abstract
{
    public interface IFeatureAppService 
    {
        AuthorizedRoleFeatureDto GetUserAuthorizedDetails();

        FeatureDto CreateFeature(FeatureCreateDto input);
        FeatureDto UpdateFeature(long id, FeatureDto input);
        Feature GetFeature(long id);
        bool DeleteFeature(long id);
        List<FeatureView> GetAllFeature();

        FeatureOrderDto CreateFeatureOrder(FeatureOrderCreateDto input);
        FeatureOrderDto UpdateFeatureOrder(long id, FeatureOrderDto input);       
        FeatureOrder GetFeatureOrder(long id);
        List<FeatureOrder> GetAllFeatureOrder();
        bool DeleteFeatureOrder(long id);
        List<FeatureView> GetAllFeatureByOrderId(long orderId);

        ApiDetail CreateApiDetail(ApiDetailsDto input);
        List<ApiDetail> GetApiDetailsList();
        List<FeatureApiMapper> GetFeatureApiMapperList();

        IQueryable<FeatureView> GetAllFeaturesData();
    }
}
