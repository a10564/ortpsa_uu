﻿using MicroService.Domain.Models.Dto.BackgroundProcess;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Dto.Nursing;
using MicroService.Domain.Models.Dto.StudentDashboard;
using MicroService.Domain.Models.Entity.Nursing;
using MicroService.Domain.Models.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.Business.Abstract
{
    public interface INursingFormFillUpService
    {
        #region STATE CHANGE APIs FOR STUDENT NURSING FORM FILLUP WORKFLOW WORKFLOW ENTITY
        NursingFormFillUpDto UpdateState(Guid id, NursingFormFillUpDto examFormDto);
        NursingFormFillUpDto RejectedByPrincipal(Guid id, NursingFormFillUpDto examFormDto);
        NursingFormFillUpDto VerificationByPrincipal(Guid id, NursingFormFillUpDto input);
        NursingFormFillUpDto PaymentCompleted(Guid id, NursingFormFillUpDto input);
        #endregion
        NursingFormFillUpDto ApplyNursingFormFillUp(NursingFormFillUpDto param);
        IQueryable<NursingViewExcludeBackgroundProcessTable> GetRoleBasedActiveNursingInboxData(bool isCountRequired = false);
        NursingFormFillUpDto GetNursingFormFillUpDetails(Guid id);
        bool AddNursingFormDetailsToBackgroundTable(BackgroundProcessDto parameters);
        List<CollegeWiseAmountForCoeDto> GetCollegeWiseFormFillUpAmount(SearchParamDto param);
        bool SaveToBackGroundTable(BackgroundProcessDto param);
        PDFDownloadFile DownloadAdmitCard(certificateDownloadDto obj);
        bool AddMarkDetailsToBackgroundTable(BackgroundProcessDto parameters);
        IQueryable<NursingFormfillupDetails> GetNursingDetails();
        IQueryable<NursingAcademicStatementDto> GetNursingAcademicDetails();
        IQueryable<NursingMarkStatementView> GetAllNursingMarkDdetails();
        List<NursingMarkStatementView> GetNursingMarkDetailsForL1Support(SearchParamDto input);
        PDFDownloadFile DownloadSemesterExamPdfReport(SearchParamDto input);
        PDFDownloadFile DownloadConductCertificate(certificateDownloadDto input);
    }
}
